/*! Mario Peroković - Diplomski rad - dms-test v0.0.1 2014-07-29 */(function(global, factory) {
    if (typeof module === "object" && typeof module.exports === "object") {
        module.exports = global.document ? factory(global, true) : function(w) {
            if (!w.document) {
                throw new Error("jQuery requires a window with a document");
            }
            return factory(w);
        };
    } else {
        factory(global);
    }
})(typeof window !== "undefined" ? window : this, function(window, noGlobal) {
    var arr = [];
    var slice = arr.slice;
    var concat = arr.concat;
    var push = arr.push;
    var indexOf = arr.indexOf;
    var class2type = {};
    var toString = class2type.toString;
    var hasOwn = class2type.hasOwnProperty;
    var support = {};
    var document = window.document, version = "2.1.1", jQuery = function(selector, context) {
        return new jQuery.fn.init(selector, context);
    }, rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, rmsPrefix = /^-ms-/, rdashAlpha = /-([\da-z])/gi, fcamelCase = function(all, letter) {
        return letter.toUpperCase();
    };
    jQuery.fn = jQuery.prototype = {
        jquery: version,
        constructor: jQuery,
        selector: "",
        length: 0,
        toArray: function() {
            return slice.call(this);
        },
        get: function(num) {
            return num != null ? num < 0 ? this[num + this.length] : this[num] : slice.call(this);
        },
        pushStack: function(elems) {
            var ret = jQuery.merge(this.constructor(), elems);
            ret.prevObject = this;
            ret.context = this.context;
            return ret;
        },
        each: function(callback, args) {
            return jQuery.each(this, callback, args);
        },
        map: function(callback) {
            return this.pushStack(jQuery.map(this, function(elem, i) {
                return callback.call(elem, i, elem);
            }));
        },
        slice: function() {
            return this.pushStack(slice.apply(this, arguments));
        },
        first: function() {
            return this.eq(0);
        },
        last: function() {
            return this.eq(-1);
        },
        eq: function(i) {
            var len = this.length, j = +i + (i < 0 ? len : 0);
            return this.pushStack(j >= 0 && j < len ? [ this[j] ] : []);
        },
        end: function() {
            return this.prevObject || this.constructor(null);
        },
        push: push,
        sort: arr.sort,
        splice: arr.splice
    };
    jQuery.extend = jQuery.fn.extend = function() {
        var options, name, src, copy, copyIsArray, clone, target = arguments[0] || {}, i = 1, length = arguments.length, deep = false;
        if (typeof target === "boolean") {
            deep = target;
            target = arguments[i] || {};
            i++;
        }
        if (typeof target !== "object" && !jQuery.isFunction(target)) {
            target = {};
        }
        if (i === length) {
            target = this;
            i--;
        }
        for (;i < length; i++) {
            if ((options = arguments[i]) != null) {
                for (name in options) {
                    src = target[name];
                    copy = options[name];
                    if (target === copy) {
                        continue;
                    }
                    if (deep && copy && (jQuery.isPlainObject(copy) || (copyIsArray = jQuery.isArray(copy)))) {
                        if (copyIsArray) {
                            copyIsArray = false;
                            clone = src && jQuery.isArray(src) ? src : [];
                        } else {
                            clone = src && jQuery.isPlainObject(src) ? src : {};
                        }
                        target[name] = jQuery.extend(deep, clone, copy);
                    } else if (copy !== undefined) {
                        target[name] = copy;
                    }
                }
            }
        }
        return target;
    };
    jQuery.extend({
        expando: "jQuery" + (version + Math.random()).replace(/\D/g, ""),
        isReady: true,
        error: function(msg) {
            throw new Error(msg);
        },
        noop: function() {},
        isFunction: function(obj) {
            return jQuery.type(obj) === "function";
        },
        isArray: Array.isArray,
        isWindow: function(obj) {
            return obj != null && obj === obj.window;
        },
        isNumeric: function(obj) {
            return !jQuery.isArray(obj) && obj - parseFloat(obj) >= 0;
        },
        isPlainObject: function(obj) {
            if (jQuery.type(obj) !== "object" || obj.nodeType || jQuery.isWindow(obj)) {
                return false;
            }
            if (obj.constructor && !hasOwn.call(obj.constructor.prototype, "isPrototypeOf")) {
                return false;
            }
            return true;
        },
        isEmptyObject: function(obj) {
            var name;
            for (name in obj) {
                return false;
            }
            return true;
        },
        type: function(obj) {
            if (obj == null) {
                return obj + "";
            }
            return typeof obj === "object" || typeof obj === "function" ? class2type[toString.call(obj)] || "object" : typeof obj;
        },
        globalEval: function(code) {
            var script, indirect = eval;
            code = jQuery.trim(code);
            if (code) {
                if (code.indexOf("use strict") === 1) {
                    script = document.createElement("script");
                    script.text = code;
                    document.head.appendChild(script).parentNode.removeChild(script);
                } else {
                    indirect(code);
                }
            }
        },
        camelCase: function(string) {
            return string.replace(rmsPrefix, "ms-").replace(rdashAlpha, fcamelCase);
        },
        nodeName: function(elem, name) {
            return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();
        },
        each: function(obj, callback, args) {
            var value, i = 0, length = obj.length, isArray = isArraylike(obj);
            if (args) {
                if (isArray) {
                    for (;i < length; i++) {
                        value = callback.apply(obj[i], args);
                        if (value === false) {
                            break;
                        }
                    }
                } else {
                    for (i in obj) {
                        value = callback.apply(obj[i], args);
                        if (value === false) {
                            break;
                        }
                    }
                }
            } else {
                if (isArray) {
                    for (;i < length; i++) {
                        value = callback.call(obj[i], i, obj[i]);
                        if (value === false) {
                            break;
                        }
                    }
                } else {
                    for (i in obj) {
                        value = callback.call(obj[i], i, obj[i]);
                        if (value === false) {
                            break;
                        }
                    }
                }
            }
            return obj;
        },
        trim: function(text) {
            return text == null ? "" : (text + "").replace(rtrim, "");
        },
        makeArray: function(arr, results) {
            var ret = results || [];
            if (arr != null) {
                if (isArraylike(Object(arr))) {
                    jQuery.merge(ret, typeof arr === "string" ? [ arr ] : arr);
                } else {
                    push.call(ret, arr);
                }
            }
            return ret;
        },
        inArray: function(elem, arr, i) {
            return arr == null ? -1 : indexOf.call(arr, elem, i);
        },
        merge: function(first, second) {
            var len = +second.length, j = 0, i = first.length;
            for (;j < len; j++) {
                first[i++] = second[j];
            }
            first.length = i;
            return first;
        },
        grep: function(elems, callback, invert) {
            var callbackInverse, matches = [], i = 0, length = elems.length, callbackExpect = !invert;
            for (;i < length; i++) {
                callbackInverse = !callback(elems[i], i);
                if (callbackInverse !== callbackExpect) {
                    matches.push(elems[i]);
                }
            }
            return matches;
        },
        map: function(elems, callback, arg) {
            var value, i = 0, length = elems.length, isArray = isArraylike(elems), ret = [];
            if (isArray) {
                for (;i < length; i++) {
                    value = callback(elems[i], i, arg);
                    if (value != null) {
                        ret.push(value);
                    }
                }
            } else {
                for (i in elems) {
                    value = callback(elems[i], i, arg);
                    if (value != null) {
                        ret.push(value);
                    }
                }
            }
            return concat.apply([], ret);
        },
        guid: 1,
        proxy: function(fn, context) {
            var tmp, args, proxy;
            if (typeof context === "string") {
                tmp = fn[context];
                context = fn;
                fn = tmp;
            }
            if (!jQuery.isFunction(fn)) {
                return undefined;
            }
            args = slice.call(arguments, 2);
            proxy = function() {
                return fn.apply(context || this, args.concat(slice.call(arguments)));
            };
            proxy.guid = fn.guid = fn.guid || jQuery.guid++;
            return proxy;
        },
        now: Date.now,
        support: support
    });
    jQuery.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(i, name) {
        class2type["[object " + name + "]"] = name.toLowerCase();
    });
    function isArraylike(obj) {
        var length = obj.length, type = jQuery.type(obj);
        if (type === "function" || jQuery.isWindow(obj)) {
            return false;
        }
        if (obj.nodeType === 1 && length) {
            return true;
        }
        return type === "array" || length === 0 || typeof length === "number" && length > 0 && length - 1 in obj;
    }
    var Sizzle = function(window) {
        var i, support, Expr, getText, isXML, tokenize, compile, select, outermostContext, sortInput, hasDuplicate, setDocument, document, docElem, documentIsHTML, rbuggyQSA, rbuggyMatches, matches, contains, expando = "sizzle" + -new Date(), preferredDoc = window.document, dirruns = 0, done = 0, classCache = createCache(), tokenCache = createCache(), compilerCache = createCache(), sortOrder = function(a, b) {
            if (a === b) {
                hasDuplicate = true;
            }
            return 0;
        }, strundefined = typeof undefined, MAX_NEGATIVE = 1 << 31, hasOwn = {}.hasOwnProperty, arr = [], pop = arr.pop, push_native = arr.push, push = arr.push, slice = arr.slice, indexOf = arr.indexOf || function(elem) {
            var i = 0, len = this.length;
            for (;i < len; i++) {
                if (this[i] === elem) {
                    return i;
                }
            }
            return -1;
        }, booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", whitespace = "[\\x20\\t\\r\\n\\f]", characterEncoding = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", identifier = characterEncoding.replace("w", "w#"), attributes = "\\[" + whitespace + "*(" + characterEncoding + ")(?:" + whitespace + "*([*^$|!~]?=)" + whitespace + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace + "*\\]", pseudos = ":(" + characterEncoding + ")(?:\\((" + "('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" + "((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" + ".*" + ")\\)|)", rtrim = new RegExp("^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g"), rcomma = new RegExp("^" + whitespace + "*," + whitespace + "*"), rcombinators = new RegExp("^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*"), rattributeQuotes = new RegExp("=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g"), rpseudo = new RegExp(pseudos), ridentifier = new RegExp("^" + identifier + "$"), matchExpr = {
            ID: new RegExp("^#(" + characterEncoding + ")"),
            CLASS: new RegExp("^\\.(" + characterEncoding + ")"),
            TAG: new RegExp("^(" + characterEncoding.replace("w", "w*") + ")"),
            ATTR: new RegExp("^" + attributes),
            PSEUDO: new RegExp("^" + pseudos),
            CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace + "*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace + "*(\\d+)|))" + whitespace + "*\\)|)", "i"),
            bool: new RegExp("^(?:" + booleans + ")$", "i"),
            needsContext: new RegExp("^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i")
        }, rinputs = /^(?:input|select|textarea|button)$/i, rheader = /^h\d$/i, rnative = /^[^{]+\{\s*\[native \w/, rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, rsibling = /[+~]/, rescape = /'|\\/g, runescape = new RegExp("\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig"), funescape = function(_, escaped, escapedWhitespace) {
            var high = "0x" + escaped - 65536;
            return high !== high || escapedWhitespace ? escaped : high < 0 ? String.fromCharCode(high + 65536) : String.fromCharCode(high >> 10 | 55296, high & 1023 | 56320);
        };
        try {
            push.apply(arr = slice.call(preferredDoc.childNodes), preferredDoc.childNodes);
            arr[preferredDoc.childNodes.length].nodeType;
        } catch (e) {
            push = {
                apply: arr.length ? function(target, els) {
                    push_native.apply(target, slice.call(els));
                } : function(target, els) {
                    var j = target.length, i = 0;
                    while (target[j++] = els[i++]) {}
                    target.length = j - 1;
                }
            };
        }
        function Sizzle(selector, context, results, seed) {
            var match, elem, m, nodeType, i, groups, old, nid, newContext, newSelector;
            if ((context ? context.ownerDocument || context : preferredDoc) !== document) {
                setDocument(context);
            }
            context = context || document;
            results = results || [];
            if (!selector || typeof selector !== "string") {
                return results;
            }
            if ((nodeType = context.nodeType) !== 1 && nodeType !== 9) {
                return [];
            }
            if (documentIsHTML && !seed) {
                if (match = rquickExpr.exec(selector)) {
                    if (m = match[1]) {
                        if (nodeType === 9) {
                            elem = context.getElementById(m);
                            if (elem && elem.parentNode) {
                                if (elem.id === m) {
                                    results.push(elem);
                                    return results;
                                }
                            } else {
                                return results;
                            }
                        } else {
                            if (context.ownerDocument && (elem = context.ownerDocument.getElementById(m)) && contains(context, elem) && elem.id === m) {
                                results.push(elem);
                                return results;
                            }
                        }
                    } else if (match[2]) {
                        push.apply(results, context.getElementsByTagName(selector));
                        return results;
                    } else if ((m = match[3]) && support.getElementsByClassName && context.getElementsByClassName) {
                        push.apply(results, context.getElementsByClassName(m));
                        return results;
                    }
                }
                if (support.qsa && (!rbuggyQSA || !rbuggyQSA.test(selector))) {
                    nid = old = expando;
                    newContext = context;
                    newSelector = nodeType === 9 && selector;
                    if (nodeType === 1 && context.nodeName.toLowerCase() !== "object") {
                        groups = tokenize(selector);
                        if (old = context.getAttribute("id")) {
                            nid = old.replace(rescape, "\\$&");
                        } else {
                            context.setAttribute("id", nid);
                        }
                        nid = "[id='" + nid + "'] ";
                        i = groups.length;
                        while (i--) {
                            groups[i] = nid + toSelector(groups[i]);
                        }
                        newContext = rsibling.test(selector) && testContext(context.parentNode) || context;
                        newSelector = groups.join(",");
                    }
                    if (newSelector) {
                        try {
                            push.apply(results, newContext.querySelectorAll(newSelector));
                            return results;
                        } catch (qsaError) {} finally {
                            if (!old) {
                                context.removeAttribute("id");
                            }
                        }
                    }
                }
            }
            return select(selector.replace(rtrim, "$1"), context, results, seed);
        }
        function createCache() {
            var keys = [];
            function cache(key, value) {
                if (keys.push(key + " ") > Expr.cacheLength) {
                    delete cache[keys.shift()];
                }
                return cache[key + " "] = value;
            }
            return cache;
        }
        function markFunction(fn) {
            fn[expando] = true;
            return fn;
        }
        function assert(fn) {
            var div = document.createElement("div");
            try {
                return !!fn(div);
            } catch (e) {
                return false;
            } finally {
                if (div.parentNode) {
                    div.parentNode.removeChild(div);
                }
                div = null;
            }
        }
        function addHandle(attrs, handler) {
            var arr = attrs.split("|"), i = attrs.length;
            while (i--) {
                Expr.attrHandle[arr[i]] = handler;
            }
        }
        function siblingCheck(a, b) {
            var cur = b && a, diff = cur && a.nodeType === 1 && b.nodeType === 1 && (~b.sourceIndex || MAX_NEGATIVE) - (~a.sourceIndex || MAX_NEGATIVE);
            if (diff) {
                return diff;
            }
            if (cur) {
                while (cur = cur.nextSibling) {
                    if (cur === b) {
                        return -1;
                    }
                }
            }
            return a ? 1 : -1;
        }
        function createInputPseudo(type) {
            return function(elem) {
                var name = elem.nodeName.toLowerCase();
                return name === "input" && elem.type === type;
            };
        }
        function createButtonPseudo(type) {
            return function(elem) {
                var name = elem.nodeName.toLowerCase();
                return (name === "input" || name === "button") && elem.type === type;
            };
        }
        function createPositionalPseudo(fn) {
            return markFunction(function(argument) {
                argument = +argument;
                return markFunction(function(seed, matches) {
                    var j, matchIndexes = fn([], seed.length, argument), i = matchIndexes.length;
                    while (i--) {
                        if (seed[j = matchIndexes[i]]) {
                            seed[j] = !(matches[j] = seed[j]);
                        }
                    }
                });
            });
        }
        function testContext(context) {
            return context && typeof context.getElementsByTagName !== strundefined && context;
        }
        support = Sizzle.support = {};
        isXML = Sizzle.isXML = function(elem) {
            var documentElement = elem && (elem.ownerDocument || elem).documentElement;
            return documentElement ? documentElement.nodeName !== "HTML" : false;
        };
        setDocument = Sizzle.setDocument = function(node) {
            var hasCompare, doc = node ? node.ownerDocument || node : preferredDoc, parent = doc.defaultView;
            if (doc === document || doc.nodeType !== 9 || !doc.documentElement) {
                return document;
            }
            document = doc;
            docElem = doc.documentElement;
            documentIsHTML = !isXML(doc);
            if (parent && parent !== parent.top) {
                if (parent.addEventListener) {
                    parent.addEventListener("unload", function() {
                        setDocument();
                    }, false);
                } else if (parent.attachEvent) {
                    parent.attachEvent("onunload", function() {
                        setDocument();
                    });
                }
            }
            support.attributes = assert(function(div) {
                div.className = "i";
                return !div.getAttribute("className");
            });
            support.getElementsByTagName = assert(function(div) {
                div.appendChild(doc.createComment(""));
                return !div.getElementsByTagName("*").length;
            });
            support.getElementsByClassName = rnative.test(doc.getElementsByClassName) && assert(function(div) {
                div.innerHTML = "<div class='a'></div><div class='a i'></div>";
                div.firstChild.className = "i";
                return div.getElementsByClassName("i").length === 2;
            });
            support.getById = assert(function(div) {
                docElem.appendChild(div).id = expando;
                return !doc.getElementsByName || !doc.getElementsByName(expando).length;
            });
            if (support.getById) {
                Expr.find["ID"] = function(id, context) {
                    if (typeof context.getElementById !== strundefined && documentIsHTML) {
                        var m = context.getElementById(id);
                        return m && m.parentNode ? [ m ] : [];
                    }
                };
                Expr.filter["ID"] = function(id) {
                    var attrId = id.replace(runescape, funescape);
                    return function(elem) {
                        return elem.getAttribute("id") === attrId;
                    };
                };
            } else {
                delete Expr.find["ID"];
                Expr.filter["ID"] = function(id) {
                    var attrId = id.replace(runescape, funescape);
                    return function(elem) {
                        var node = typeof elem.getAttributeNode !== strundefined && elem.getAttributeNode("id");
                        return node && node.value === attrId;
                    };
                };
            }
            Expr.find["TAG"] = support.getElementsByTagName ? function(tag, context) {
                if (typeof context.getElementsByTagName !== strundefined) {
                    return context.getElementsByTagName(tag);
                }
            } : function(tag, context) {
                var elem, tmp = [], i = 0, results = context.getElementsByTagName(tag);
                if (tag === "*") {
                    while (elem = results[i++]) {
                        if (elem.nodeType === 1) {
                            tmp.push(elem);
                        }
                    }
                    return tmp;
                }
                return results;
            };
            Expr.find["CLASS"] = support.getElementsByClassName && function(className, context) {
                if (typeof context.getElementsByClassName !== strundefined && documentIsHTML) {
                    return context.getElementsByClassName(className);
                }
            };
            rbuggyMatches = [];
            rbuggyQSA = [];
            if (support.qsa = rnative.test(doc.querySelectorAll)) {
                assert(function(div) {
                    div.innerHTML = "<select msallowclip=''><option selected=''></option></select>";
                    if (div.querySelectorAll("[msallowclip^='']").length) {
                        rbuggyQSA.push("[*^$]=" + whitespace + "*(?:''|\"\")");
                    }
                    if (!div.querySelectorAll("[selected]").length) {
                        rbuggyQSA.push("\\[" + whitespace + "*(?:value|" + booleans + ")");
                    }
                    if (!div.querySelectorAll(":checked").length) {
                        rbuggyQSA.push(":checked");
                    }
                });
                assert(function(div) {
                    var input = doc.createElement("input");
                    input.setAttribute("type", "hidden");
                    div.appendChild(input).setAttribute("name", "D");
                    if (div.querySelectorAll("[name=d]").length) {
                        rbuggyQSA.push("name" + whitespace + "*[*^$|!~]?=");
                    }
                    if (!div.querySelectorAll(":enabled").length) {
                        rbuggyQSA.push(":enabled", ":disabled");
                    }
                    div.querySelectorAll("*,:x");
                    rbuggyQSA.push(",.*:");
                });
            }
            if (support.matchesSelector = rnative.test(matches = docElem.matches || docElem.webkitMatchesSelector || docElem.mozMatchesSelector || docElem.oMatchesSelector || docElem.msMatchesSelector)) {
                assert(function(div) {
                    support.disconnectedMatch = matches.call(div, "div");
                    matches.call(div, "[s!='']:x");
                    rbuggyMatches.push("!=", pseudos);
                });
            }
            rbuggyQSA = rbuggyQSA.length && new RegExp(rbuggyQSA.join("|"));
            rbuggyMatches = rbuggyMatches.length && new RegExp(rbuggyMatches.join("|"));
            hasCompare = rnative.test(docElem.compareDocumentPosition);
            contains = hasCompare || rnative.test(docElem.contains) ? function(a, b) {
                var adown = a.nodeType === 9 ? a.documentElement : a, bup = b && b.parentNode;
                return a === bup || !!(bup && bup.nodeType === 1 && (adown.contains ? adown.contains(bup) : a.compareDocumentPosition && a.compareDocumentPosition(bup) & 16));
            } : function(a, b) {
                if (b) {
                    while (b = b.parentNode) {
                        if (b === a) {
                            return true;
                        }
                    }
                }
                return false;
            };
            sortOrder = hasCompare ? function(a, b) {
                if (a === b) {
                    hasDuplicate = true;
                    return 0;
                }
                var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
                if (compare) {
                    return compare;
                }
                compare = (a.ownerDocument || a) === (b.ownerDocument || b) ? a.compareDocumentPosition(b) : 1;
                if (compare & 1 || !support.sortDetached && b.compareDocumentPosition(a) === compare) {
                    if (a === doc || a.ownerDocument === preferredDoc && contains(preferredDoc, a)) {
                        return -1;
                    }
                    if (b === doc || b.ownerDocument === preferredDoc && contains(preferredDoc, b)) {
                        return 1;
                    }
                    return sortInput ? indexOf.call(sortInput, a) - indexOf.call(sortInput, b) : 0;
                }
                return compare & 4 ? -1 : 1;
            } : function(a, b) {
                if (a === b) {
                    hasDuplicate = true;
                    return 0;
                }
                var cur, i = 0, aup = a.parentNode, bup = b.parentNode, ap = [ a ], bp = [ b ];
                if (!aup || !bup) {
                    return a === doc ? -1 : b === doc ? 1 : aup ? -1 : bup ? 1 : sortInput ? indexOf.call(sortInput, a) - indexOf.call(sortInput, b) : 0;
                } else if (aup === bup) {
                    return siblingCheck(a, b);
                }
                cur = a;
                while (cur = cur.parentNode) {
                    ap.unshift(cur);
                }
                cur = b;
                while (cur = cur.parentNode) {
                    bp.unshift(cur);
                }
                while (ap[i] === bp[i]) {
                    i++;
                }
                return i ? siblingCheck(ap[i], bp[i]) : ap[i] === preferredDoc ? -1 : bp[i] === preferredDoc ? 1 : 0;
            };
            return doc;
        };
        Sizzle.matches = function(expr, elements) {
            return Sizzle(expr, null, null, elements);
        };
        Sizzle.matchesSelector = function(elem, expr) {
            if ((elem.ownerDocument || elem) !== document) {
                setDocument(elem);
            }
            expr = expr.replace(rattributeQuotes, "='$1']");
            if (support.matchesSelector && documentIsHTML && (!rbuggyMatches || !rbuggyMatches.test(expr)) && (!rbuggyQSA || !rbuggyQSA.test(expr))) {
                try {
                    var ret = matches.call(elem, expr);
                    if (ret || support.disconnectedMatch || elem.document && elem.document.nodeType !== 11) {
                        return ret;
                    }
                } catch (e) {}
            }
            return Sizzle(expr, document, null, [ elem ]).length > 0;
        };
        Sizzle.contains = function(context, elem) {
            if ((context.ownerDocument || context) !== document) {
                setDocument(context);
            }
            return contains(context, elem);
        };
        Sizzle.attr = function(elem, name) {
            if ((elem.ownerDocument || elem) !== document) {
                setDocument(elem);
            }
            var fn = Expr.attrHandle[name.toLowerCase()], val = fn && hasOwn.call(Expr.attrHandle, name.toLowerCase()) ? fn(elem, name, !documentIsHTML) : undefined;
            return val !== undefined ? val : support.attributes || !documentIsHTML ? elem.getAttribute(name) : (val = elem.getAttributeNode(name)) && val.specified ? val.value : null;
        };
        Sizzle.error = function(msg) {
            throw new Error("Syntax error, unrecognized expression: " + msg);
        };
        Sizzle.uniqueSort = function(results) {
            var elem, duplicates = [], j = 0, i = 0;
            hasDuplicate = !support.detectDuplicates;
            sortInput = !support.sortStable && results.slice(0);
            results.sort(sortOrder);
            if (hasDuplicate) {
                while (elem = results[i++]) {
                    if (elem === results[i]) {
                        j = duplicates.push(i);
                    }
                }
                while (j--) {
                    results.splice(duplicates[j], 1);
                }
            }
            sortInput = null;
            return results;
        };
        getText = Sizzle.getText = function(elem) {
            var node, ret = "", i = 0, nodeType = elem.nodeType;
            if (!nodeType) {
                while (node = elem[i++]) {
                    ret += getText(node);
                }
            } else if (nodeType === 1 || nodeType === 9 || nodeType === 11) {
                if (typeof elem.textContent === "string") {
                    return elem.textContent;
                } else {
                    for (elem = elem.firstChild; elem; elem = elem.nextSibling) {
                        ret += getText(elem);
                    }
                }
            } else if (nodeType === 3 || nodeType === 4) {
                return elem.nodeValue;
            }
            return ret;
        };
        Expr = Sizzle.selectors = {
            cacheLength: 50,
            createPseudo: markFunction,
            match: matchExpr,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: true
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: true
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(match) {
                    match[1] = match[1].replace(runescape, funescape);
                    match[3] = (match[3] || match[4] || match[5] || "").replace(runescape, funescape);
                    if (match[2] === "~=") {
                        match[3] = " " + match[3] + " ";
                    }
                    return match.slice(0, 4);
                },
                CHILD: function(match) {
                    match[1] = match[1].toLowerCase();
                    if (match[1].slice(0, 3) === "nth") {
                        if (!match[3]) {
                            Sizzle.error(match[0]);
                        }
                        match[4] = +(match[4] ? match[5] + (match[6] || 1) : 2 * (match[3] === "even" || match[3] === "odd"));
                        match[5] = +(match[7] + match[8] || match[3] === "odd");
                    } else if (match[3]) {
                        Sizzle.error(match[0]);
                    }
                    return match;
                },
                PSEUDO: function(match) {
                    var excess, unquoted = !match[6] && match[2];
                    if (matchExpr["CHILD"].test(match[0])) {
                        return null;
                    }
                    if (match[3]) {
                        match[2] = match[4] || match[5] || "";
                    } else if (unquoted && rpseudo.test(unquoted) && (excess = tokenize(unquoted, true)) && (excess = unquoted.indexOf(")", unquoted.length - excess) - unquoted.length)) {
                        match[0] = match[0].slice(0, excess);
                        match[2] = unquoted.slice(0, excess);
                    }
                    return match.slice(0, 3);
                }
            },
            filter: {
                TAG: function(nodeNameSelector) {
                    var nodeName = nodeNameSelector.replace(runescape, funescape).toLowerCase();
                    return nodeNameSelector === "*" ? function() {
                        return true;
                    } : function(elem) {
                        return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
                    };
                },
                CLASS: function(className) {
                    var pattern = classCache[className + " "];
                    return pattern || (pattern = new RegExp("(^|" + whitespace + ")" + className + "(" + whitespace + "|$)")) && classCache(className, function(elem) {
                        return pattern.test(typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== strundefined && elem.getAttribute("class") || "");
                    });
                },
                ATTR: function(name, operator, check) {
                    return function(elem) {
                        var result = Sizzle.attr(elem, name);
                        if (result == null) {
                            return operator === "!=";
                        }
                        if (!operator) {
                            return true;
                        }
                        result += "";
                        return operator === "=" ? result === check : operator === "!=" ? result !== check : operator === "^=" ? check && result.indexOf(check) === 0 : operator === "*=" ? check && result.indexOf(check) > -1 : operator === "$=" ? check && result.slice(-check.length) === check : operator === "~=" ? (" " + result + " ").indexOf(check) > -1 : operator === "|=" ? result === check || result.slice(0, check.length + 1) === check + "-" : false;
                    };
                },
                CHILD: function(type, what, argument, first, last) {
                    var simple = type.slice(0, 3) !== "nth", forward = type.slice(-4) !== "last", ofType = what === "of-type";
                    return first === 1 && last === 0 ? function(elem) {
                        return !!elem.parentNode;
                    } : function(elem, context, xml) {
                        var cache, outerCache, node, diff, nodeIndex, start, dir = simple !== forward ? "nextSibling" : "previousSibling", parent = elem.parentNode, name = ofType && elem.nodeName.toLowerCase(), useCache = !xml && !ofType;
                        if (parent) {
                            if (simple) {
                                while (dir) {
                                    node = elem;
                                    while (node = node[dir]) {
                                        if (ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1) {
                                            return false;
                                        }
                                    }
                                    start = dir = type === "only" && !start && "nextSibling";
                                }
                                return true;
                            }
                            start = [ forward ? parent.firstChild : parent.lastChild ];
                            if (forward && useCache) {
                                outerCache = parent[expando] || (parent[expando] = {});
                                cache = outerCache[type] || [];
                                nodeIndex = cache[0] === dirruns && cache[1];
                                diff = cache[0] === dirruns && cache[2];
                                node = nodeIndex && parent.childNodes[nodeIndex];
                                while (node = ++nodeIndex && node && node[dir] || (diff = nodeIndex = 0) || start.pop()) {
                                    if (node.nodeType === 1 && ++diff && node === elem) {
                                        outerCache[type] = [ dirruns, nodeIndex, diff ];
                                        break;
                                    }
                                }
                            } else if (useCache && (cache = (elem[expando] || (elem[expando] = {}))[type]) && cache[0] === dirruns) {
                                diff = cache[1];
                            } else {
                                while (node = ++nodeIndex && node && node[dir] || (diff = nodeIndex = 0) || start.pop()) {
                                    if ((ofType ? node.nodeName.toLowerCase() === name : node.nodeType === 1) && ++diff) {
                                        if (useCache) {
                                            (node[expando] || (node[expando] = {}))[type] = [ dirruns, diff ];
                                        }
                                        if (node === elem) {
                                            break;
                                        }
                                    }
                                }
                            }
                            diff -= last;
                            return diff === first || diff % first === 0 && diff / first >= 0;
                        }
                    };
                },
                PSEUDO: function(pseudo, argument) {
                    var args, fn = Expr.pseudos[pseudo] || Expr.setFilters[pseudo.toLowerCase()] || Sizzle.error("unsupported pseudo: " + pseudo);
                    if (fn[expando]) {
                        return fn(argument);
                    }
                    if (fn.length > 1) {
                        args = [ pseudo, pseudo, "", argument ];
                        return Expr.setFilters.hasOwnProperty(pseudo.toLowerCase()) ? markFunction(function(seed, matches) {
                            var idx, matched = fn(seed, argument), i = matched.length;
                            while (i--) {
                                idx = indexOf.call(seed, matched[i]);
                                seed[idx] = !(matches[idx] = matched[i]);
                            }
                        }) : function(elem) {
                            return fn(elem, 0, args);
                        };
                    }
                    return fn;
                }
            },
            pseudos: {
                not: markFunction(function(selector) {
                    var input = [], results = [], matcher = compile(selector.replace(rtrim, "$1"));
                    return matcher[expando] ? markFunction(function(seed, matches, context, xml) {
                        var elem, unmatched = matcher(seed, null, xml, []), i = seed.length;
                        while (i--) {
                            if (elem = unmatched[i]) {
                                seed[i] = !(matches[i] = elem);
                            }
                        }
                    }) : function(elem, context, xml) {
                        input[0] = elem;
                        matcher(input, null, xml, results);
                        return !results.pop();
                    };
                }),
                has: markFunction(function(selector) {
                    return function(elem) {
                        return Sizzle(selector, elem).length > 0;
                    };
                }),
                contains: markFunction(function(text) {
                    return function(elem) {
                        return (elem.textContent || elem.innerText || getText(elem)).indexOf(text) > -1;
                    };
                }),
                lang: markFunction(function(lang) {
                    if (!ridentifier.test(lang || "")) {
                        Sizzle.error("unsupported lang: " + lang);
                    }
                    lang = lang.replace(runescape, funescape).toLowerCase();
                    return function(elem) {
                        var elemLang;
                        do {
                            if (elemLang = documentIsHTML ? elem.lang : elem.getAttribute("xml:lang") || elem.getAttribute("lang")) {
                                elemLang = elemLang.toLowerCase();
                                return elemLang === lang || elemLang.indexOf(lang + "-") === 0;
                            }
                        } while ((elem = elem.parentNode) && elem.nodeType === 1);
                        return false;
                    };
                }),
                target: function(elem) {
                    var hash = window.location && window.location.hash;
                    return hash && hash.slice(1) === elem.id;
                },
                root: function(elem) {
                    return elem === docElem;
                },
                focus: function(elem) {
                    return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
                },
                enabled: function(elem) {
                    return elem.disabled === false;
                },
                disabled: function(elem) {
                    return elem.disabled === true;
                },
                checked: function(elem) {
                    var nodeName = elem.nodeName.toLowerCase();
                    return nodeName === "input" && !!elem.checked || nodeName === "option" && !!elem.selected;
                },
                selected: function(elem) {
                    if (elem.parentNode) {
                        elem.parentNode.selectedIndex;
                    }
                    return elem.selected === true;
                },
                empty: function(elem) {
                    for (elem = elem.firstChild; elem; elem = elem.nextSibling) {
                        if (elem.nodeType < 6) {
                            return false;
                        }
                    }
                    return true;
                },
                parent: function(elem) {
                    return !Expr.pseudos["empty"](elem);
                },
                header: function(elem) {
                    return rheader.test(elem.nodeName);
                },
                input: function(elem) {
                    return rinputs.test(elem.nodeName);
                },
                button: function(elem) {
                    var name = elem.nodeName.toLowerCase();
                    return name === "input" && elem.type === "button" || name === "button";
                },
                text: function(elem) {
                    var attr;
                    return elem.nodeName.toLowerCase() === "input" && elem.type === "text" && ((attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text");
                },
                first: createPositionalPseudo(function() {
                    return [ 0 ];
                }),
                last: createPositionalPseudo(function(matchIndexes, length) {
                    return [ length - 1 ];
                }),
                eq: createPositionalPseudo(function(matchIndexes, length, argument) {
                    return [ argument < 0 ? argument + length : argument ];
                }),
                even: createPositionalPseudo(function(matchIndexes, length) {
                    var i = 0;
                    for (;i < length; i += 2) {
                        matchIndexes.push(i);
                    }
                    return matchIndexes;
                }),
                odd: createPositionalPseudo(function(matchIndexes, length) {
                    var i = 1;
                    for (;i < length; i += 2) {
                        matchIndexes.push(i);
                    }
                    return matchIndexes;
                }),
                lt: createPositionalPseudo(function(matchIndexes, length, argument) {
                    var i = argument < 0 ? argument + length : argument;
                    for (;--i >= 0; ) {
                        matchIndexes.push(i);
                    }
                    return matchIndexes;
                }),
                gt: createPositionalPseudo(function(matchIndexes, length, argument) {
                    var i = argument < 0 ? argument + length : argument;
                    for (;++i < length; ) {
                        matchIndexes.push(i);
                    }
                    return matchIndexes;
                })
            }
        };
        Expr.pseudos["nth"] = Expr.pseudos["eq"];
        for (i in {
            radio: true,
            checkbox: true,
            file: true,
            password: true,
            image: true
        }) {
            Expr.pseudos[i] = createInputPseudo(i);
        }
        for (i in {
            submit: true,
            reset: true
        }) {
            Expr.pseudos[i] = createButtonPseudo(i);
        }
        function setFilters() {}
        setFilters.prototype = Expr.filters = Expr.pseudos;
        Expr.setFilters = new setFilters();
        tokenize = Sizzle.tokenize = function(selector, parseOnly) {
            var matched, match, tokens, type, soFar, groups, preFilters, cached = tokenCache[selector + " "];
            if (cached) {
                return parseOnly ? 0 : cached.slice(0);
            }
            soFar = selector;
            groups = [];
            preFilters = Expr.preFilter;
            while (soFar) {
                if (!matched || (match = rcomma.exec(soFar))) {
                    if (match) {
                        soFar = soFar.slice(match[0].length) || soFar;
                    }
                    groups.push(tokens = []);
                }
                matched = false;
                if (match = rcombinators.exec(soFar)) {
                    matched = match.shift();
                    tokens.push({
                        value: matched,
                        type: match[0].replace(rtrim, " ")
                    });
                    soFar = soFar.slice(matched.length);
                }
                for (type in Expr.filter) {
                    if ((match = matchExpr[type].exec(soFar)) && (!preFilters[type] || (match = preFilters[type](match)))) {
                        matched = match.shift();
                        tokens.push({
                            value: matched,
                            type: type,
                            matches: match
                        });
                        soFar = soFar.slice(matched.length);
                    }
                }
                if (!matched) {
                    break;
                }
            }
            return parseOnly ? soFar.length : soFar ? Sizzle.error(selector) : tokenCache(selector, groups).slice(0);
        };
        function toSelector(tokens) {
            var i = 0, len = tokens.length, selector = "";
            for (;i < len; i++) {
                selector += tokens[i].value;
            }
            return selector;
        }
        function addCombinator(matcher, combinator, base) {
            var dir = combinator.dir, checkNonElements = base && dir === "parentNode", doneName = done++;
            return combinator.first ? function(elem, context, xml) {
                while (elem = elem[dir]) {
                    if (elem.nodeType === 1 || checkNonElements) {
                        return matcher(elem, context, xml);
                    }
                }
            } : function(elem, context, xml) {
                var oldCache, outerCache, newCache = [ dirruns, doneName ];
                if (xml) {
                    while (elem = elem[dir]) {
                        if (elem.nodeType === 1 || checkNonElements) {
                            if (matcher(elem, context, xml)) {
                                return true;
                            }
                        }
                    }
                } else {
                    while (elem = elem[dir]) {
                        if (elem.nodeType === 1 || checkNonElements) {
                            outerCache = elem[expando] || (elem[expando] = {});
                            if ((oldCache = outerCache[dir]) && oldCache[0] === dirruns && oldCache[1] === doneName) {
                                return newCache[2] = oldCache[2];
                            } else {
                                outerCache[dir] = newCache;
                                if (newCache[2] = matcher(elem, context, xml)) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            };
        }
        function elementMatcher(matchers) {
            return matchers.length > 1 ? function(elem, context, xml) {
                var i = matchers.length;
                while (i--) {
                    if (!matchers[i](elem, context, xml)) {
                        return false;
                    }
                }
                return true;
            } : matchers[0];
        }
        function multipleContexts(selector, contexts, results) {
            var i = 0, len = contexts.length;
            for (;i < len; i++) {
                Sizzle(selector, contexts[i], results);
            }
            return results;
        }
        function condense(unmatched, map, filter, context, xml) {
            var elem, newUnmatched = [], i = 0, len = unmatched.length, mapped = map != null;
            for (;i < len; i++) {
                if (elem = unmatched[i]) {
                    if (!filter || filter(elem, context, xml)) {
                        newUnmatched.push(elem);
                        if (mapped) {
                            map.push(i);
                        }
                    }
                }
            }
            return newUnmatched;
        }
        function setMatcher(preFilter, selector, matcher, postFilter, postFinder, postSelector) {
            if (postFilter && !postFilter[expando]) {
                postFilter = setMatcher(postFilter);
            }
            if (postFinder && !postFinder[expando]) {
                postFinder = setMatcher(postFinder, postSelector);
            }
            return markFunction(function(seed, results, context, xml) {
                var temp, i, elem, preMap = [], postMap = [], preexisting = results.length, elems = seed || multipleContexts(selector || "*", context.nodeType ? [ context ] : context, []), matcherIn = preFilter && (seed || !selector) ? condense(elems, preMap, preFilter, context, xml) : elems, matcherOut = matcher ? postFinder || (seed ? preFilter : preexisting || postFilter) ? [] : results : matcherIn;
                if (matcher) {
                    matcher(matcherIn, matcherOut, context, xml);
                }
                if (postFilter) {
                    temp = condense(matcherOut, postMap);
                    postFilter(temp, [], context, xml);
                    i = temp.length;
                    while (i--) {
                        if (elem = temp[i]) {
                            matcherOut[postMap[i]] = !(matcherIn[postMap[i]] = elem);
                        }
                    }
                }
                if (seed) {
                    if (postFinder || preFilter) {
                        if (postFinder) {
                            temp = [];
                            i = matcherOut.length;
                            while (i--) {
                                if (elem = matcherOut[i]) {
                                    temp.push(matcherIn[i] = elem);
                                }
                            }
                            postFinder(null, matcherOut = [], temp, xml);
                        }
                        i = matcherOut.length;
                        while (i--) {
                            if ((elem = matcherOut[i]) && (temp = postFinder ? indexOf.call(seed, elem) : preMap[i]) > -1) {
                                seed[temp] = !(results[temp] = elem);
                            }
                        }
                    }
                } else {
                    matcherOut = condense(matcherOut === results ? matcherOut.splice(preexisting, matcherOut.length) : matcherOut);
                    if (postFinder) {
                        postFinder(null, results, matcherOut, xml);
                    } else {
                        push.apply(results, matcherOut);
                    }
                }
            });
        }
        function matcherFromTokens(tokens) {
            var checkContext, matcher, j, len = tokens.length, leadingRelative = Expr.relative[tokens[0].type], implicitRelative = leadingRelative || Expr.relative[" "], i = leadingRelative ? 1 : 0, matchContext = addCombinator(function(elem) {
                return elem === checkContext;
            }, implicitRelative, true), matchAnyContext = addCombinator(function(elem) {
                return indexOf.call(checkContext, elem) > -1;
            }, implicitRelative, true), matchers = [ function(elem, context, xml) {
                return !leadingRelative && (xml || context !== outermostContext) || ((checkContext = context).nodeType ? matchContext(elem, context, xml) : matchAnyContext(elem, context, xml));
            } ];
            for (;i < len; i++) {
                if (matcher = Expr.relative[tokens[i].type]) {
                    matchers = [ addCombinator(elementMatcher(matchers), matcher) ];
                } else {
                    matcher = Expr.filter[tokens[i].type].apply(null, tokens[i].matches);
                    if (matcher[expando]) {
                        j = ++i;
                        for (;j < len; j++) {
                            if (Expr.relative[tokens[j].type]) {
                                break;
                            }
                        }
                        return setMatcher(i > 1 && elementMatcher(matchers), i > 1 && toSelector(tokens.slice(0, i - 1).concat({
                            value: tokens[i - 2].type === " " ? "*" : ""
                        })).replace(rtrim, "$1"), matcher, i < j && matcherFromTokens(tokens.slice(i, j)), j < len && matcherFromTokens(tokens = tokens.slice(j)), j < len && toSelector(tokens));
                    }
                    matchers.push(matcher);
                }
            }
            return elementMatcher(matchers);
        }
        function matcherFromGroupMatchers(elementMatchers, setMatchers) {
            var bySet = setMatchers.length > 0, byElement = elementMatchers.length > 0, superMatcher = function(seed, context, xml, results, outermost) {
                var elem, j, matcher, matchedCount = 0, i = "0", unmatched = seed && [], setMatched = [], contextBackup = outermostContext, elems = seed || byElement && Expr.find["TAG"]("*", outermost), dirrunsUnique = dirruns += contextBackup == null ? 1 : Math.random() || .1, len = elems.length;
                if (outermost) {
                    outermostContext = context !== document && context;
                }
                for (;i !== len && (elem = elems[i]) != null; i++) {
                    if (byElement && elem) {
                        j = 0;
                        while (matcher = elementMatchers[j++]) {
                            if (matcher(elem, context, xml)) {
                                results.push(elem);
                                break;
                            }
                        }
                        if (outermost) {
                            dirruns = dirrunsUnique;
                        }
                    }
                    if (bySet) {
                        if (elem = !matcher && elem) {
                            matchedCount--;
                        }
                        if (seed) {
                            unmatched.push(elem);
                        }
                    }
                }
                matchedCount += i;
                if (bySet && i !== matchedCount) {
                    j = 0;
                    while (matcher = setMatchers[j++]) {
                        matcher(unmatched, setMatched, context, xml);
                    }
                    if (seed) {
                        if (matchedCount > 0) {
                            while (i--) {
                                if (!(unmatched[i] || setMatched[i])) {
                                    setMatched[i] = pop.call(results);
                                }
                            }
                        }
                        setMatched = condense(setMatched);
                    }
                    push.apply(results, setMatched);
                    if (outermost && !seed && setMatched.length > 0 && matchedCount + setMatchers.length > 1) {
                        Sizzle.uniqueSort(results);
                    }
                }
                if (outermost) {
                    dirruns = dirrunsUnique;
                    outermostContext = contextBackup;
                }
                return unmatched;
            };
            return bySet ? markFunction(superMatcher) : superMatcher;
        }
        compile = Sizzle.compile = function(selector, match) {
            var i, setMatchers = [], elementMatchers = [], cached = compilerCache[selector + " "];
            if (!cached) {
                if (!match) {
                    match = tokenize(selector);
                }
                i = match.length;
                while (i--) {
                    cached = matcherFromTokens(match[i]);
                    if (cached[expando]) {
                        setMatchers.push(cached);
                    } else {
                        elementMatchers.push(cached);
                    }
                }
                cached = compilerCache(selector, matcherFromGroupMatchers(elementMatchers, setMatchers));
                cached.selector = selector;
            }
            return cached;
        };
        select = Sizzle.select = function(selector, context, results, seed) {
            var i, tokens, token, type, find, compiled = typeof selector === "function" && selector, match = !seed && tokenize(selector = compiled.selector || selector);
            results = results || [];
            if (match.length === 1) {
                tokens = match[0] = match[0].slice(0);
                if (tokens.length > 2 && (token = tokens[0]).type === "ID" && support.getById && context.nodeType === 9 && documentIsHTML && Expr.relative[tokens[1].type]) {
                    context = (Expr.find["ID"](token.matches[0].replace(runescape, funescape), context) || [])[0];
                    if (!context) {
                        return results;
                    } else if (compiled) {
                        context = context.parentNode;
                    }
                    selector = selector.slice(tokens.shift().value.length);
                }
                i = matchExpr["needsContext"].test(selector) ? 0 : tokens.length;
                while (i--) {
                    token = tokens[i];
                    if (Expr.relative[type = token.type]) {
                        break;
                    }
                    if (find = Expr.find[type]) {
                        if (seed = find(token.matches[0].replace(runescape, funescape), rsibling.test(tokens[0].type) && testContext(context.parentNode) || context)) {
                            tokens.splice(i, 1);
                            selector = seed.length && toSelector(tokens);
                            if (!selector) {
                                push.apply(results, seed);
                                return results;
                            }
                            break;
                        }
                    }
                }
            }
            (compiled || compile(selector, match))(seed, context, !documentIsHTML, results, rsibling.test(selector) && testContext(context.parentNode) || context);
            return results;
        };
        support.sortStable = expando.split("").sort(sortOrder).join("") === expando;
        support.detectDuplicates = !!hasDuplicate;
        setDocument();
        support.sortDetached = assert(function(div1) {
            return div1.compareDocumentPosition(document.createElement("div")) & 1;
        });
        if (!assert(function(div) {
            div.innerHTML = "<a href='#'></a>";
            return div.firstChild.getAttribute("href") === "#";
        })) {
            addHandle("type|href|height|width", function(elem, name, isXML) {
                if (!isXML) {
                    return elem.getAttribute(name, name.toLowerCase() === "type" ? 1 : 2);
                }
            });
        }
        if (!support.attributes || !assert(function(div) {
            div.innerHTML = "<input/>";
            div.firstChild.setAttribute("value", "");
            return div.firstChild.getAttribute("value") === "";
        })) {
            addHandle("value", function(elem, name, isXML) {
                if (!isXML && elem.nodeName.toLowerCase() === "input") {
                    return elem.defaultValue;
                }
            });
        }
        if (!assert(function(div) {
            return div.getAttribute("disabled") == null;
        })) {
            addHandle(booleans, function(elem, name, isXML) {
                var val;
                if (!isXML) {
                    return elem[name] === true ? name.toLowerCase() : (val = elem.getAttributeNode(name)) && val.specified ? val.value : null;
                }
            });
        }
        return Sizzle;
    }(window);
    jQuery.find = Sizzle;
    jQuery.expr = Sizzle.selectors;
    jQuery.expr[":"] = jQuery.expr.pseudos;
    jQuery.unique = Sizzle.uniqueSort;
    jQuery.text = Sizzle.getText;
    jQuery.isXMLDoc = Sizzle.isXML;
    jQuery.contains = Sizzle.contains;
    var rneedsContext = jQuery.expr.match.needsContext;
    var rsingleTag = /^<(\w+)\s*\/?>(?:<\/\1>|)$/;
    var risSimple = /^.[^:#\[\.,]*$/;
    function winnow(elements, qualifier, not) {
        if (jQuery.isFunction(qualifier)) {
            return jQuery.grep(elements, function(elem, i) {
                return !!qualifier.call(elem, i, elem) !== not;
            });
        }
        if (qualifier.nodeType) {
            return jQuery.grep(elements, function(elem) {
                return elem === qualifier !== not;
            });
        }
        if (typeof qualifier === "string") {
            if (risSimple.test(qualifier)) {
                return jQuery.filter(qualifier, elements, not);
            }
            qualifier = jQuery.filter(qualifier, elements);
        }
        return jQuery.grep(elements, function(elem) {
            return indexOf.call(qualifier, elem) >= 0 !== not;
        });
    }
    jQuery.filter = function(expr, elems, not) {
        var elem = elems[0];
        if (not) {
            expr = ":not(" + expr + ")";
        }
        return elems.length === 1 && elem.nodeType === 1 ? jQuery.find.matchesSelector(elem, expr) ? [ elem ] : [] : jQuery.find.matches(expr, jQuery.grep(elems, function(elem) {
            return elem.nodeType === 1;
        }));
    };
    jQuery.fn.extend({
        find: function(selector) {
            var i, len = this.length, ret = [], self = this;
            if (typeof selector !== "string") {
                return this.pushStack(jQuery(selector).filter(function() {
                    for (i = 0; i < len; i++) {
                        if (jQuery.contains(self[i], this)) {
                            return true;
                        }
                    }
                }));
            }
            for (i = 0; i < len; i++) {
                jQuery.find(selector, self[i], ret);
            }
            ret = this.pushStack(len > 1 ? jQuery.unique(ret) : ret);
            ret.selector = this.selector ? this.selector + " " + selector : selector;
            return ret;
        },
        filter: function(selector) {
            return this.pushStack(winnow(this, selector || [], false));
        },
        not: function(selector) {
            return this.pushStack(winnow(this, selector || [], true));
        },
        is: function(selector) {
            return !!winnow(this, typeof selector === "string" && rneedsContext.test(selector) ? jQuery(selector) : selector || [], false).length;
        }
    });
    var rootjQuery, rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/, init = jQuery.fn.init = function(selector, context) {
        var match, elem;
        if (!selector) {
            return this;
        }
        if (typeof selector === "string") {
            if (selector[0] === "<" && selector[selector.length - 1] === ">" && selector.length >= 3) {
                match = [ null, selector, null ];
            } else {
                match = rquickExpr.exec(selector);
            }
            if (match && (match[1] || !context)) {
                if (match[1]) {
                    context = context instanceof jQuery ? context[0] : context;
                    jQuery.merge(this, jQuery.parseHTML(match[1], context && context.nodeType ? context.ownerDocument || context : document, true));
                    if (rsingleTag.test(match[1]) && jQuery.isPlainObject(context)) {
                        for (match in context) {
                            if (jQuery.isFunction(this[match])) {
                                this[match](context[match]);
                            } else {
                                this.attr(match, context[match]);
                            }
                        }
                    }
                    return this;
                } else {
                    elem = document.getElementById(match[2]);
                    if (elem && elem.parentNode) {
                        this.length = 1;
                        this[0] = elem;
                    }
                    this.context = document;
                    this.selector = selector;
                    return this;
                }
            } else if (!context || context.jquery) {
                return (context || rootjQuery).find(selector);
            } else {
                return this.constructor(context).find(selector);
            }
        } else if (selector.nodeType) {
            this.context = this[0] = selector;
            this.length = 1;
            return this;
        } else if (jQuery.isFunction(selector)) {
            return typeof rootjQuery.ready !== "undefined" ? rootjQuery.ready(selector) : selector(jQuery);
        }
        if (selector.selector !== undefined) {
            this.selector = selector.selector;
            this.context = selector.context;
        }
        return jQuery.makeArray(selector, this);
    };
    init.prototype = jQuery.fn;
    rootjQuery = jQuery(document);
    var rparentsprev = /^(?:parents|prev(?:Until|All))/, guaranteedUnique = {
        children: true,
        contents: true,
        next: true,
        prev: true
    };
    jQuery.extend({
        dir: function(elem, dir, until) {
            var matched = [], truncate = until !== undefined;
            while ((elem = elem[dir]) && elem.nodeType !== 9) {
                if (elem.nodeType === 1) {
                    if (truncate && jQuery(elem).is(until)) {
                        break;
                    }
                    matched.push(elem);
                }
            }
            return matched;
        },
        sibling: function(n, elem) {
            var matched = [];
            for (;n; n = n.nextSibling) {
                if (n.nodeType === 1 && n !== elem) {
                    matched.push(n);
                }
            }
            return matched;
        }
    });
    jQuery.fn.extend({
        has: function(target) {
            var targets = jQuery(target, this), l = targets.length;
            return this.filter(function() {
                var i = 0;
                for (;i < l; i++) {
                    if (jQuery.contains(this, targets[i])) {
                        return true;
                    }
                }
            });
        },
        closest: function(selectors, context) {
            var cur, i = 0, l = this.length, matched = [], pos = rneedsContext.test(selectors) || typeof selectors !== "string" ? jQuery(selectors, context || this.context) : 0;
            for (;i < l; i++) {
                for (cur = this[i]; cur && cur !== context; cur = cur.parentNode) {
                    if (cur.nodeType < 11 && (pos ? pos.index(cur) > -1 : cur.nodeType === 1 && jQuery.find.matchesSelector(cur, selectors))) {
                        matched.push(cur);
                        break;
                    }
                }
            }
            return this.pushStack(matched.length > 1 ? jQuery.unique(matched) : matched);
        },
        index: function(elem) {
            if (!elem) {
                return this[0] && this[0].parentNode ? this.first().prevAll().length : -1;
            }
            if (typeof elem === "string") {
                return indexOf.call(jQuery(elem), this[0]);
            }
            return indexOf.call(this, elem.jquery ? elem[0] : elem);
        },
        add: function(selector, context) {
            return this.pushStack(jQuery.unique(jQuery.merge(this.get(), jQuery(selector, context))));
        },
        addBack: function(selector) {
            return this.add(selector == null ? this.prevObject : this.prevObject.filter(selector));
        }
    });
    function sibling(cur, dir) {
        while ((cur = cur[dir]) && cur.nodeType !== 1) {}
        return cur;
    }
    jQuery.each({
        parent: function(elem) {
            var parent = elem.parentNode;
            return parent && parent.nodeType !== 11 ? parent : null;
        },
        parents: function(elem) {
            return jQuery.dir(elem, "parentNode");
        },
        parentsUntil: function(elem, i, until) {
            return jQuery.dir(elem, "parentNode", until);
        },
        next: function(elem) {
            return sibling(elem, "nextSibling");
        },
        prev: function(elem) {
            return sibling(elem, "previousSibling");
        },
        nextAll: function(elem) {
            return jQuery.dir(elem, "nextSibling");
        },
        prevAll: function(elem) {
            return jQuery.dir(elem, "previousSibling");
        },
        nextUntil: function(elem, i, until) {
            return jQuery.dir(elem, "nextSibling", until);
        },
        prevUntil: function(elem, i, until) {
            return jQuery.dir(elem, "previousSibling", until);
        },
        siblings: function(elem) {
            return jQuery.sibling((elem.parentNode || {}).firstChild, elem);
        },
        children: function(elem) {
            return jQuery.sibling(elem.firstChild);
        },
        contents: function(elem) {
            return elem.contentDocument || jQuery.merge([], elem.childNodes);
        }
    }, function(name, fn) {
        jQuery.fn[name] = function(until, selector) {
            var matched = jQuery.map(this, fn, until);
            if (name.slice(-5) !== "Until") {
                selector = until;
            }
            if (selector && typeof selector === "string") {
                matched = jQuery.filter(selector, matched);
            }
            if (this.length > 1) {
                if (!guaranteedUnique[name]) {
                    jQuery.unique(matched);
                }
                if (rparentsprev.test(name)) {
                    matched.reverse();
                }
            }
            return this.pushStack(matched);
        };
    });
    var rnotwhite = /\S+/g;
    var optionsCache = {};
    function createOptions(options) {
        var object = optionsCache[options] = {};
        jQuery.each(options.match(rnotwhite) || [], function(_, flag) {
            object[flag] = true;
        });
        return object;
    }
    jQuery.Callbacks = function(options) {
        options = typeof options === "string" ? optionsCache[options] || createOptions(options) : jQuery.extend({}, options);
        var memory, fired, firing, firingStart, firingLength, firingIndex, list = [], stack = !options.once && [], fire = function(data) {
            memory = options.memory && data;
            fired = true;
            firingIndex = firingStart || 0;
            firingStart = 0;
            firingLength = list.length;
            firing = true;
            for (;list && firingIndex < firingLength; firingIndex++) {
                if (list[firingIndex].apply(data[0], data[1]) === false && options.stopOnFalse) {
                    memory = false;
                    break;
                }
            }
            firing = false;
            if (list) {
                if (stack) {
                    if (stack.length) {
                        fire(stack.shift());
                    }
                } else if (memory) {
                    list = [];
                } else {
                    self.disable();
                }
            }
        }, self = {
            add: function() {
                if (list) {
                    var start = list.length;
                    (function add(args) {
                        jQuery.each(args, function(_, arg) {
                            var type = jQuery.type(arg);
                            if (type === "function") {
                                if (!options.unique || !self.has(arg)) {
                                    list.push(arg);
                                }
                            } else if (arg && arg.length && type !== "string") {
                                add(arg);
                            }
                        });
                    })(arguments);
                    if (firing) {
                        firingLength = list.length;
                    } else if (memory) {
                        firingStart = start;
                        fire(memory);
                    }
                }
                return this;
            },
            remove: function() {
                if (list) {
                    jQuery.each(arguments, function(_, arg) {
                        var index;
                        while ((index = jQuery.inArray(arg, list, index)) > -1) {
                            list.splice(index, 1);
                            if (firing) {
                                if (index <= firingLength) {
                                    firingLength--;
                                }
                                if (index <= firingIndex) {
                                    firingIndex--;
                                }
                            }
                        }
                    });
                }
                return this;
            },
            has: function(fn) {
                return fn ? jQuery.inArray(fn, list) > -1 : !!(list && list.length);
            },
            empty: function() {
                list = [];
                firingLength = 0;
                return this;
            },
            disable: function() {
                list = stack = memory = undefined;
                return this;
            },
            disabled: function() {
                return !list;
            },
            lock: function() {
                stack = undefined;
                if (!memory) {
                    self.disable();
                }
                return this;
            },
            locked: function() {
                return !stack;
            },
            fireWith: function(context, args) {
                if (list && (!fired || stack)) {
                    args = args || [];
                    args = [ context, args.slice ? args.slice() : args ];
                    if (firing) {
                        stack.push(args);
                    } else {
                        fire(args);
                    }
                }
                return this;
            },
            fire: function() {
                self.fireWith(this, arguments);
                return this;
            },
            fired: function() {
                return !!fired;
            }
        };
        return self;
    };
    jQuery.extend({
        Deferred: function(func) {
            var tuples = [ [ "resolve", "done", jQuery.Callbacks("once memory"), "resolved" ], [ "reject", "fail", jQuery.Callbacks("once memory"), "rejected" ], [ "notify", "progress", jQuery.Callbacks("memory") ] ], state = "pending", promise = {
                state: function() {
                    return state;
                },
                always: function() {
                    deferred.done(arguments).fail(arguments);
                    return this;
                },
                then: function() {
                    var fns = arguments;
                    return jQuery.Deferred(function(newDefer) {
                        jQuery.each(tuples, function(i, tuple) {
                            var fn = jQuery.isFunction(fns[i]) && fns[i];
                            deferred[tuple[1]](function() {
                                var returned = fn && fn.apply(this, arguments);
                                if (returned && jQuery.isFunction(returned.promise)) {
                                    returned.promise().done(newDefer.resolve).fail(newDefer.reject).progress(newDefer.notify);
                                } else {
                                    newDefer[tuple[0] + "With"](this === promise ? newDefer.promise() : this, fn ? [ returned ] : arguments);
                                }
                            });
                        });
                        fns = null;
                    }).promise();
                },
                promise: function(obj) {
                    return obj != null ? jQuery.extend(obj, promise) : promise;
                }
            }, deferred = {};
            promise.pipe = promise.then;
            jQuery.each(tuples, function(i, tuple) {
                var list = tuple[2], stateString = tuple[3];
                promise[tuple[1]] = list.add;
                if (stateString) {
                    list.add(function() {
                        state = stateString;
                    }, tuples[i ^ 1][2].disable, tuples[2][2].lock);
                }
                deferred[tuple[0]] = function() {
                    deferred[tuple[0] + "With"](this === deferred ? promise : this, arguments);
                    return this;
                };
                deferred[tuple[0] + "With"] = list.fireWith;
            });
            promise.promise(deferred);
            if (func) {
                func.call(deferred, deferred);
            }
            return deferred;
        },
        when: function(subordinate) {
            var i = 0, resolveValues = slice.call(arguments), length = resolveValues.length, remaining = length !== 1 || subordinate && jQuery.isFunction(subordinate.promise) ? length : 0, deferred = remaining === 1 ? subordinate : jQuery.Deferred(), updateFunc = function(i, contexts, values) {
                return function(value) {
                    contexts[i] = this;
                    values[i] = arguments.length > 1 ? slice.call(arguments) : value;
                    if (values === progressValues) {
                        deferred.notifyWith(contexts, values);
                    } else if (!--remaining) {
                        deferred.resolveWith(contexts, values);
                    }
                };
            }, progressValues, progressContexts, resolveContexts;
            if (length > 1) {
                progressValues = new Array(length);
                progressContexts = new Array(length);
                resolveContexts = new Array(length);
                for (;i < length; i++) {
                    if (resolveValues[i] && jQuery.isFunction(resolveValues[i].promise)) {
                        resolveValues[i].promise().done(updateFunc(i, resolveContexts, resolveValues)).fail(deferred.reject).progress(updateFunc(i, progressContexts, progressValues));
                    } else {
                        --remaining;
                    }
                }
            }
            if (!remaining) {
                deferred.resolveWith(resolveContexts, resolveValues);
            }
            return deferred.promise();
        }
    });
    var readyList;
    jQuery.fn.ready = function(fn) {
        jQuery.ready.promise().done(fn);
        return this;
    };
    jQuery.extend({
        isReady: false,
        readyWait: 1,
        holdReady: function(hold) {
            if (hold) {
                jQuery.readyWait++;
            } else {
                jQuery.ready(true);
            }
        },
        ready: function(wait) {
            if (wait === true ? --jQuery.readyWait : jQuery.isReady) {
                return;
            }
            jQuery.isReady = true;
            if (wait !== true && --jQuery.readyWait > 0) {
                return;
            }
            readyList.resolveWith(document, [ jQuery ]);
            if (jQuery.fn.triggerHandler) {
                jQuery(document).triggerHandler("ready");
                jQuery(document).off("ready");
            }
        }
    });
    function completed() {
        document.removeEventListener("DOMContentLoaded", completed, false);
        window.removeEventListener("load", completed, false);
        jQuery.ready();
    }
    jQuery.ready.promise = function(obj) {
        if (!readyList) {
            readyList = jQuery.Deferred();
            if (document.readyState === "complete") {
                setTimeout(jQuery.ready);
            } else {
                document.addEventListener("DOMContentLoaded", completed, false);
                window.addEventListener("load", completed, false);
            }
        }
        return readyList.promise(obj);
    };
    jQuery.ready.promise();
    var access = jQuery.access = function(elems, fn, key, value, chainable, emptyGet, raw) {
        var i = 0, len = elems.length, bulk = key == null;
        if (jQuery.type(key) === "object") {
            chainable = true;
            for (i in key) {
                jQuery.access(elems, fn, i, key[i], true, emptyGet, raw);
            }
        } else if (value !== undefined) {
            chainable = true;
            if (!jQuery.isFunction(value)) {
                raw = true;
            }
            if (bulk) {
                if (raw) {
                    fn.call(elems, value);
                    fn = null;
                } else {
                    bulk = fn;
                    fn = function(elem, key, value) {
                        return bulk.call(jQuery(elem), value);
                    };
                }
            }
            if (fn) {
                for (;i < len; i++) {
                    fn(elems[i], key, raw ? value : value.call(elems[i], i, fn(elems[i], key)));
                }
            }
        }
        return chainable ? elems : bulk ? fn.call(elems) : len ? fn(elems[0], key) : emptyGet;
    };
    jQuery.acceptData = function(owner) {
        return owner.nodeType === 1 || owner.nodeType === 9 || !+owner.nodeType;
    };
    function Data() {
        Object.defineProperty(this.cache = {}, 0, {
            get: function() {
                return {};
            }
        });
        this.expando = jQuery.expando + Math.random();
    }
    Data.uid = 1;
    Data.accepts = jQuery.acceptData;
    Data.prototype = {
        key: function(owner) {
            if (!Data.accepts(owner)) {
                return 0;
            }
            var descriptor = {}, unlock = owner[this.expando];
            if (!unlock) {
                unlock = Data.uid++;
                try {
                    descriptor[this.expando] = {
                        value: unlock
                    };
                    Object.defineProperties(owner, descriptor);
                } catch (e) {
                    descriptor[this.expando] = unlock;
                    jQuery.extend(owner, descriptor);
                }
            }
            if (!this.cache[unlock]) {
                this.cache[unlock] = {};
            }
            return unlock;
        },
        set: function(owner, data, value) {
            var prop, unlock = this.key(owner), cache = this.cache[unlock];
            if (typeof data === "string") {
                cache[data] = value;
            } else {
                if (jQuery.isEmptyObject(cache)) {
                    jQuery.extend(this.cache[unlock], data);
                } else {
                    for (prop in data) {
                        cache[prop] = data[prop];
                    }
                }
            }
            return cache;
        },
        get: function(owner, key) {
            var cache = this.cache[this.key(owner)];
            return key === undefined ? cache : cache[key];
        },
        access: function(owner, key, value) {
            var stored;
            if (key === undefined || key && typeof key === "string" && value === undefined) {
                stored = this.get(owner, key);
                return stored !== undefined ? stored : this.get(owner, jQuery.camelCase(key));
            }
            this.set(owner, key, value);
            return value !== undefined ? value : key;
        },
        remove: function(owner, key) {
            var i, name, camel, unlock = this.key(owner), cache = this.cache[unlock];
            if (key === undefined) {
                this.cache[unlock] = {};
            } else {
                if (jQuery.isArray(key)) {
                    name = key.concat(key.map(jQuery.camelCase));
                } else {
                    camel = jQuery.camelCase(key);
                    if (key in cache) {
                        name = [ key, camel ];
                    } else {
                        name = camel;
                        name = name in cache ? [ name ] : name.match(rnotwhite) || [];
                    }
                }
                i = name.length;
                while (i--) {
                    delete cache[name[i]];
                }
            }
        },
        hasData: function(owner) {
            return !jQuery.isEmptyObject(this.cache[owner[this.expando]] || {});
        },
        discard: function(owner) {
            if (owner[this.expando]) {
                delete this.cache[owner[this.expando]];
            }
        }
    };
    var data_priv = new Data();
    var data_user = new Data();
    var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, rmultiDash = /([A-Z])/g;
    function dataAttr(elem, key, data) {
        var name;
        if (data === undefined && elem.nodeType === 1) {
            name = "data-" + key.replace(rmultiDash, "-$1").toLowerCase();
            data = elem.getAttribute(name);
            if (typeof data === "string") {
                try {
                    data = data === "true" ? true : data === "false" ? false : data === "null" ? null : +data + "" === data ? +data : rbrace.test(data) ? jQuery.parseJSON(data) : data;
                } catch (e) {}
                data_user.set(elem, key, data);
            } else {
                data = undefined;
            }
        }
        return data;
    }
    jQuery.extend({
        hasData: function(elem) {
            return data_user.hasData(elem) || data_priv.hasData(elem);
        },
        data: function(elem, name, data) {
            return data_user.access(elem, name, data);
        },
        removeData: function(elem, name) {
            data_user.remove(elem, name);
        },
        _data: function(elem, name, data) {
            return data_priv.access(elem, name, data);
        },
        _removeData: function(elem, name) {
            data_priv.remove(elem, name);
        }
    });
    jQuery.fn.extend({
        data: function(key, value) {
            var i, name, data, elem = this[0], attrs = elem && elem.attributes;
            if (key === undefined) {
                if (this.length) {
                    data = data_user.get(elem);
                    if (elem.nodeType === 1 && !data_priv.get(elem, "hasDataAttrs")) {
                        i = attrs.length;
                        while (i--) {
                            if (attrs[i]) {
                                name = attrs[i].name;
                                if (name.indexOf("data-") === 0) {
                                    name = jQuery.camelCase(name.slice(5));
                                    dataAttr(elem, name, data[name]);
                                }
                            }
                        }
                        data_priv.set(elem, "hasDataAttrs", true);
                    }
                }
                return data;
            }
            if (typeof key === "object") {
                return this.each(function() {
                    data_user.set(this, key);
                });
            }
            return access(this, function(value) {
                var data, camelKey = jQuery.camelCase(key);
                if (elem && value === undefined) {
                    data = data_user.get(elem, key);
                    if (data !== undefined) {
                        return data;
                    }
                    data = data_user.get(elem, camelKey);
                    if (data !== undefined) {
                        return data;
                    }
                    data = dataAttr(elem, camelKey, undefined);
                    if (data !== undefined) {
                        return data;
                    }
                    return;
                }
                this.each(function() {
                    var data = data_user.get(this, camelKey);
                    data_user.set(this, camelKey, value);
                    if (key.indexOf("-") !== -1 && data !== undefined) {
                        data_user.set(this, key, value);
                    }
                });
            }, null, value, arguments.length > 1, null, true);
        },
        removeData: function(key) {
            return this.each(function() {
                data_user.remove(this, key);
            });
        }
    });
    jQuery.extend({
        queue: function(elem, type, data) {
            var queue;
            if (elem) {
                type = (type || "fx") + "queue";
                queue = data_priv.get(elem, type);
                if (data) {
                    if (!queue || jQuery.isArray(data)) {
                        queue = data_priv.access(elem, type, jQuery.makeArray(data));
                    } else {
                        queue.push(data);
                    }
                }
                return queue || [];
            }
        },
        dequeue: function(elem, type) {
            type = type || "fx";
            var queue = jQuery.queue(elem, type), startLength = queue.length, fn = queue.shift(), hooks = jQuery._queueHooks(elem, type), next = function() {
                jQuery.dequeue(elem, type);
            };
            if (fn === "inprogress") {
                fn = queue.shift();
                startLength--;
            }
            if (fn) {
                if (type === "fx") {
                    queue.unshift("inprogress");
                }
                delete hooks.stop;
                fn.call(elem, next, hooks);
            }
            if (!startLength && hooks) {
                hooks.empty.fire();
            }
        },
        _queueHooks: function(elem, type) {
            var key = type + "queueHooks";
            return data_priv.get(elem, key) || data_priv.access(elem, key, {
                empty: jQuery.Callbacks("once memory").add(function() {
                    data_priv.remove(elem, [ type + "queue", key ]);
                })
            });
        }
    });
    jQuery.fn.extend({
        queue: function(type, data) {
            var setter = 2;
            if (typeof type !== "string") {
                data = type;
                type = "fx";
                setter--;
            }
            if (arguments.length < setter) {
                return jQuery.queue(this[0], type);
            }
            return data === undefined ? this : this.each(function() {
                var queue = jQuery.queue(this, type, data);
                jQuery._queueHooks(this, type);
                if (type === "fx" && queue[0] !== "inprogress") {
                    jQuery.dequeue(this, type);
                }
            });
        },
        dequeue: function(type) {
            return this.each(function() {
                jQuery.dequeue(this, type);
            });
        },
        clearQueue: function(type) {
            return this.queue(type || "fx", []);
        },
        promise: function(type, obj) {
            var tmp, count = 1, defer = jQuery.Deferred(), elements = this, i = this.length, resolve = function() {
                if (!--count) {
                    defer.resolveWith(elements, [ elements ]);
                }
            };
            if (typeof type !== "string") {
                obj = type;
                type = undefined;
            }
            type = type || "fx";
            while (i--) {
                tmp = data_priv.get(elements[i], type + "queueHooks");
                if (tmp && tmp.empty) {
                    count++;
                    tmp.empty.add(resolve);
                }
            }
            resolve();
            return defer.promise(obj);
        }
    });
    var pnum = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source;
    var cssExpand = [ "Top", "Right", "Bottom", "Left" ];
    var isHidden = function(elem, el) {
        elem = el || elem;
        return jQuery.css(elem, "display") === "none" || !jQuery.contains(elem.ownerDocument, elem);
    };
    var rcheckableType = /^(?:checkbox|radio)$/i;
    (function() {
        var fragment = document.createDocumentFragment(), div = fragment.appendChild(document.createElement("div")), input = document.createElement("input");
        input.setAttribute("type", "radio");
        input.setAttribute("checked", "checked");
        input.setAttribute("name", "t");
        div.appendChild(input);
        support.checkClone = div.cloneNode(true).cloneNode(true).lastChild.checked;
        div.innerHTML = "<textarea>x</textarea>";
        support.noCloneChecked = !!div.cloneNode(true).lastChild.defaultValue;
    })();
    var strundefined = typeof undefined;
    support.focusinBubbles = "onfocusin" in window;
    var rkeyEvent = /^key/, rmouseEvent = /^(?:mouse|pointer|contextmenu)|click/, rfocusMorph = /^(?:focusinfocus|focusoutblur)$/, rtypenamespace = /^([^.]*)(?:\.(.+)|)$/;
    function returnTrue() {
        return true;
    }
    function returnFalse() {
        return false;
    }
    function safeActiveElement() {
        try {
            return document.activeElement;
        } catch (err) {}
    }
    jQuery.event = {
        global: {},
        add: function(elem, types, handler, data, selector) {
            var handleObjIn, eventHandle, tmp, events, t, handleObj, special, handlers, type, namespaces, origType, elemData = data_priv.get(elem);
            if (!elemData) {
                return;
            }
            if (handler.handler) {
                handleObjIn = handler;
                handler = handleObjIn.handler;
                selector = handleObjIn.selector;
            }
            if (!handler.guid) {
                handler.guid = jQuery.guid++;
            }
            if (!(events = elemData.events)) {
                events = elemData.events = {};
            }
            if (!(eventHandle = elemData.handle)) {
                eventHandle = elemData.handle = function(e) {
                    return typeof jQuery !== strundefined && jQuery.event.triggered !== e.type ? jQuery.event.dispatch.apply(elem, arguments) : undefined;
                };
            }
            types = (types || "").match(rnotwhite) || [ "" ];
            t = types.length;
            while (t--) {
                tmp = rtypenamespace.exec(types[t]) || [];
                type = origType = tmp[1];
                namespaces = (tmp[2] || "").split(".").sort();
                if (!type) {
                    continue;
                }
                special = jQuery.event.special[type] || {};
                type = (selector ? special.delegateType : special.bindType) || type;
                special = jQuery.event.special[type] || {};
                handleObj = jQuery.extend({
                    type: type,
                    origType: origType,
                    data: data,
                    handler: handler,
                    guid: handler.guid,
                    selector: selector,
                    needsContext: selector && jQuery.expr.match.needsContext.test(selector),
                    namespace: namespaces.join(".")
                }, handleObjIn);
                if (!(handlers = events[type])) {
                    handlers = events[type] = [];
                    handlers.delegateCount = 0;
                    if (!special.setup || special.setup.call(elem, data, namespaces, eventHandle) === false) {
                        if (elem.addEventListener) {
                            elem.addEventListener(type, eventHandle, false);
                        }
                    }
                }
                if (special.add) {
                    special.add.call(elem, handleObj);
                    if (!handleObj.handler.guid) {
                        handleObj.handler.guid = handler.guid;
                    }
                }
                if (selector) {
                    handlers.splice(handlers.delegateCount++, 0, handleObj);
                } else {
                    handlers.push(handleObj);
                }
                jQuery.event.global[type] = true;
            }
        },
        remove: function(elem, types, handler, selector, mappedTypes) {
            var j, origCount, tmp, events, t, handleObj, special, handlers, type, namespaces, origType, elemData = data_priv.hasData(elem) && data_priv.get(elem);
            if (!elemData || !(events = elemData.events)) {
                return;
            }
            types = (types || "").match(rnotwhite) || [ "" ];
            t = types.length;
            while (t--) {
                tmp = rtypenamespace.exec(types[t]) || [];
                type = origType = tmp[1];
                namespaces = (tmp[2] || "").split(".").sort();
                if (!type) {
                    for (type in events) {
                        jQuery.event.remove(elem, type + types[t], handler, selector, true);
                    }
                    continue;
                }
                special = jQuery.event.special[type] || {};
                type = (selector ? special.delegateType : special.bindType) || type;
                handlers = events[type] || [];
                tmp = tmp[2] && new RegExp("(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)");
                origCount = j = handlers.length;
                while (j--) {
                    handleObj = handlers[j];
                    if ((mappedTypes || origType === handleObj.origType) && (!handler || handler.guid === handleObj.guid) && (!tmp || tmp.test(handleObj.namespace)) && (!selector || selector === handleObj.selector || selector === "**" && handleObj.selector)) {
                        handlers.splice(j, 1);
                        if (handleObj.selector) {
                            handlers.delegateCount--;
                        }
                        if (special.remove) {
                            special.remove.call(elem, handleObj);
                        }
                    }
                }
                if (origCount && !handlers.length) {
                    if (!special.teardown || special.teardown.call(elem, namespaces, elemData.handle) === false) {
                        jQuery.removeEvent(elem, type, elemData.handle);
                    }
                    delete events[type];
                }
            }
            if (jQuery.isEmptyObject(events)) {
                delete elemData.handle;
                data_priv.remove(elem, "events");
            }
        },
        trigger: function(event, data, elem, onlyHandlers) {
            var i, cur, tmp, bubbleType, ontype, handle, special, eventPath = [ elem || document ], type = hasOwn.call(event, "type") ? event.type : event, namespaces = hasOwn.call(event, "namespace") ? event.namespace.split(".") : [];
            cur = tmp = elem = elem || document;
            if (elem.nodeType === 3 || elem.nodeType === 8) {
                return;
            }
            if (rfocusMorph.test(type + jQuery.event.triggered)) {
                return;
            }
            if (type.indexOf(".") >= 0) {
                namespaces = type.split(".");
                type = namespaces.shift();
                namespaces.sort();
            }
            ontype = type.indexOf(":") < 0 && "on" + type;
            event = event[jQuery.expando] ? event : new jQuery.Event(type, typeof event === "object" && event);
            event.isTrigger = onlyHandlers ? 2 : 3;
            event.namespace = namespaces.join(".");
            event.namespace_re = event.namespace ? new RegExp("(^|\\.)" + namespaces.join("\\.(?:.*\\.|)") + "(\\.|$)") : null;
            event.result = undefined;
            if (!event.target) {
                event.target = elem;
            }
            data = data == null ? [ event ] : jQuery.makeArray(data, [ event ]);
            special = jQuery.event.special[type] || {};
            if (!onlyHandlers && special.trigger && special.trigger.apply(elem, data) === false) {
                return;
            }
            if (!onlyHandlers && !special.noBubble && !jQuery.isWindow(elem)) {
                bubbleType = special.delegateType || type;
                if (!rfocusMorph.test(bubbleType + type)) {
                    cur = cur.parentNode;
                }
                for (;cur; cur = cur.parentNode) {
                    eventPath.push(cur);
                    tmp = cur;
                }
                if (tmp === (elem.ownerDocument || document)) {
                    eventPath.push(tmp.defaultView || tmp.parentWindow || window);
                }
            }
            i = 0;
            while ((cur = eventPath[i++]) && !event.isPropagationStopped()) {
                event.type = i > 1 ? bubbleType : special.bindType || type;
                handle = (data_priv.get(cur, "events") || {})[event.type] && data_priv.get(cur, "handle");
                if (handle) {
                    handle.apply(cur, data);
                }
                handle = ontype && cur[ontype];
                if (handle && handle.apply && jQuery.acceptData(cur)) {
                    event.result = handle.apply(cur, data);
                    if (event.result === false) {
                        event.preventDefault();
                    }
                }
            }
            event.type = type;
            if (!onlyHandlers && !event.isDefaultPrevented()) {
                if ((!special._default || special._default.apply(eventPath.pop(), data) === false) && jQuery.acceptData(elem)) {
                    if (ontype && jQuery.isFunction(elem[type]) && !jQuery.isWindow(elem)) {
                        tmp = elem[ontype];
                        if (tmp) {
                            elem[ontype] = null;
                        }
                        jQuery.event.triggered = type;
                        elem[type]();
                        jQuery.event.triggered = undefined;
                        if (tmp) {
                            elem[ontype] = tmp;
                        }
                    }
                }
            }
            return event.result;
        },
        dispatch: function(event) {
            event = jQuery.event.fix(event);
            var i, j, ret, matched, handleObj, handlerQueue = [], args = slice.call(arguments), handlers = (data_priv.get(this, "events") || {})[event.type] || [], special = jQuery.event.special[event.type] || {};
            args[0] = event;
            event.delegateTarget = this;
            if (special.preDispatch && special.preDispatch.call(this, event) === false) {
                return;
            }
            handlerQueue = jQuery.event.handlers.call(this, event, handlers);
            i = 0;
            while ((matched = handlerQueue[i++]) && !event.isPropagationStopped()) {
                event.currentTarget = matched.elem;
                j = 0;
                while ((handleObj = matched.handlers[j++]) && !event.isImmediatePropagationStopped()) {
                    if (!event.namespace_re || event.namespace_re.test(handleObj.namespace)) {
                        event.handleObj = handleObj;
                        event.data = handleObj.data;
                        ret = ((jQuery.event.special[handleObj.origType] || {}).handle || handleObj.handler).apply(matched.elem, args);
                        if (ret !== undefined) {
                            if ((event.result = ret) === false) {
                                event.preventDefault();
                                event.stopPropagation();
                            }
                        }
                    }
                }
            }
            if (special.postDispatch) {
                special.postDispatch.call(this, event);
            }
            return event.result;
        },
        handlers: function(event, handlers) {
            var i, matches, sel, handleObj, handlerQueue = [], delegateCount = handlers.delegateCount, cur = event.target;
            if (delegateCount && cur.nodeType && (!event.button || event.type !== "click")) {
                for (;cur !== this; cur = cur.parentNode || this) {
                    if (cur.disabled !== true || event.type !== "click") {
                        matches = [];
                        for (i = 0; i < delegateCount; i++) {
                            handleObj = handlers[i];
                            sel = handleObj.selector + " ";
                            if (matches[sel] === undefined) {
                                matches[sel] = handleObj.needsContext ? jQuery(sel, this).index(cur) >= 0 : jQuery.find(sel, this, null, [ cur ]).length;
                            }
                            if (matches[sel]) {
                                matches.push(handleObj);
                            }
                        }
                        if (matches.length) {
                            handlerQueue.push({
                                elem: cur,
                                handlers: matches
                            });
                        }
                    }
                }
            }
            if (delegateCount < handlers.length) {
                handlerQueue.push({
                    elem: this,
                    handlers: handlers.slice(delegateCount)
                });
            }
            return handlerQueue;
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function(event, original) {
                if (event.which == null) {
                    event.which = original.charCode != null ? original.charCode : original.keyCode;
                }
                return event;
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function(event, original) {
                var eventDoc, doc, body, button = original.button;
                if (event.pageX == null && original.clientX != null) {
                    eventDoc = event.target.ownerDocument || document;
                    doc = eventDoc.documentElement;
                    body = eventDoc.body;
                    event.pageX = original.clientX + (doc && doc.scrollLeft || body && body.scrollLeft || 0) - (doc && doc.clientLeft || body && body.clientLeft || 0);
                    event.pageY = original.clientY + (doc && doc.scrollTop || body && body.scrollTop || 0) - (doc && doc.clientTop || body && body.clientTop || 0);
                }
                if (!event.which && button !== undefined) {
                    event.which = button & 1 ? 1 : button & 2 ? 3 : button & 4 ? 2 : 0;
                }
                return event;
            }
        },
        fix: function(event) {
            if (event[jQuery.expando]) {
                return event;
            }
            var i, prop, copy, type = event.type, originalEvent = event, fixHook = this.fixHooks[type];
            if (!fixHook) {
                this.fixHooks[type] = fixHook = rmouseEvent.test(type) ? this.mouseHooks : rkeyEvent.test(type) ? this.keyHooks : {};
            }
            copy = fixHook.props ? this.props.concat(fixHook.props) : this.props;
            event = new jQuery.Event(originalEvent);
            i = copy.length;
            while (i--) {
                prop = copy[i];
                event[prop] = originalEvent[prop];
            }
            if (!event.target) {
                event.target = document;
            }
            if (event.target.nodeType === 3) {
                event.target = event.target.parentNode;
            }
            return fixHook.filter ? fixHook.filter(event, originalEvent) : event;
        },
        special: {
            load: {
                noBubble: true
            },
            focus: {
                trigger: function() {
                    if (this !== safeActiveElement() && this.focus) {
                        this.focus();
                        return false;
                    }
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    if (this === safeActiveElement() && this.blur) {
                        this.blur();
                        return false;
                    }
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    if (this.type === "checkbox" && this.click && jQuery.nodeName(this, "input")) {
                        this.click();
                        return false;
                    }
                },
                _default: function(event) {
                    return jQuery.nodeName(event.target, "a");
                }
            },
            beforeunload: {
                postDispatch: function(event) {
                    if (event.result !== undefined && event.originalEvent) {
                        event.originalEvent.returnValue = event.result;
                    }
                }
            }
        },
        simulate: function(type, elem, event, bubble) {
            var e = jQuery.extend(new jQuery.Event(), event, {
                type: type,
                isSimulated: true,
                originalEvent: {}
            });
            if (bubble) {
                jQuery.event.trigger(e, null, elem);
            } else {
                jQuery.event.dispatch.call(elem, e);
            }
            if (e.isDefaultPrevented()) {
                event.preventDefault();
            }
        }
    };
    jQuery.removeEvent = function(elem, type, handle) {
        if (elem.removeEventListener) {
            elem.removeEventListener(type, handle, false);
        }
    };
    jQuery.Event = function(src, props) {
        if (!(this instanceof jQuery.Event)) {
            return new jQuery.Event(src, props);
        }
        if (src && src.type) {
            this.originalEvent = src;
            this.type = src.type;
            this.isDefaultPrevented = src.defaultPrevented || src.defaultPrevented === undefined && src.returnValue === false ? returnTrue : returnFalse;
        } else {
            this.type = src;
        }
        if (props) {
            jQuery.extend(this, props);
        }
        this.timeStamp = src && src.timeStamp || jQuery.now();
        this[jQuery.expando] = true;
    };
    jQuery.Event.prototype = {
        isDefaultPrevented: returnFalse,
        isPropagationStopped: returnFalse,
        isImmediatePropagationStopped: returnFalse,
        preventDefault: function() {
            var e = this.originalEvent;
            this.isDefaultPrevented = returnTrue;
            if (e && e.preventDefault) {
                e.preventDefault();
            }
        },
        stopPropagation: function() {
            var e = this.originalEvent;
            this.isPropagationStopped = returnTrue;
            if (e && e.stopPropagation) {
                e.stopPropagation();
            }
        },
        stopImmediatePropagation: function() {
            var e = this.originalEvent;
            this.isImmediatePropagationStopped = returnTrue;
            if (e && e.stopImmediatePropagation) {
                e.stopImmediatePropagation();
            }
            this.stopPropagation();
        }
    };
    jQuery.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(orig, fix) {
        jQuery.event.special[orig] = {
            delegateType: fix,
            bindType: fix,
            handle: function(event) {
                var ret, target = this, related = event.relatedTarget, handleObj = event.handleObj;
                if (!related || related !== target && !jQuery.contains(target, related)) {
                    event.type = handleObj.origType;
                    ret = handleObj.handler.apply(this, arguments);
                    event.type = fix;
                }
                return ret;
            }
        };
    });
    if (!support.focusinBubbles) {
        jQuery.each({
            focus: "focusin",
            blur: "focusout"
        }, function(orig, fix) {
            var handler = function(event) {
                jQuery.event.simulate(fix, event.target, jQuery.event.fix(event), true);
            };
            jQuery.event.special[fix] = {
                setup: function() {
                    var doc = this.ownerDocument || this, attaches = data_priv.access(doc, fix);
                    if (!attaches) {
                        doc.addEventListener(orig, handler, true);
                    }
                    data_priv.access(doc, fix, (attaches || 0) + 1);
                },
                teardown: function() {
                    var doc = this.ownerDocument || this, attaches = data_priv.access(doc, fix) - 1;
                    if (!attaches) {
                        doc.removeEventListener(orig, handler, true);
                        data_priv.remove(doc, fix);
                    } else {
                        data_priv.access(doc, fix, attaches);
                    }
                }
            };
        });
    }
    jQuery.fn.extend({
        on: function(types, selector, data, fn, one) {
            var origFn, type;
            if (typeof types === "object") {
                if (typeof selector !== "string") {
                    data = data || selector;
                    selector = undefined;
                }
                for (type in types) {
                    this.on(type, selector, data, types[type], one);
                }
                return this;
            }
            if (data == null && fn == null) {
                fn = selector;
                data = selector = undefined;
            } else if (fn == null) {
                if (typeof selector === "string") {
                    fn = data;
                    data = undefined;
                } else {
                    fn = data;
                    data = selector;
                    selector = undefined;
                }
            }
            if (fn === false) {
                fn = returnFalse;
            } else if (!fn) {
                return this;
            }
            if (one === 1) {
                origFn = fn;
                fn = function(event) {
                    jQuery().off(event);
                    return origFn.apply(this, arguments);
                };
                fn.guid = origFn.guid || (origFn.guid = jQuery.guid++);
            }
            return this.each(function() {
                jQuery.event.add(this, types, fn, data, selector);
            });
        },
        one: function(types, selector, data, fn) {
            return this.on(types, selector, data, fn, 1);
        },
        off: function(types, selector, fn) {
            var handleObj, type;
            if (types && types.preventDefault && types.handleObj) {
                handleObj = types.handleObj;
                jQuery(types.delegateTarget).off(handleObj.namespace ? handleObj.origType + "." + handleObj.namespace : handleObj.origType, handleObj.selector, handleObj.handler);
                return this;
            }
            if (typeof types === "object") {
                for (type in types) {
                    this.off(type, selector, types[type]);
                }
                return this;
            }
            if (selector === false || typeof selector === "function") {
                fn = selector;
                selector = undefined;
            }
            if (fn === false) {
                fn = returnFalse;
            }
            return this.each(function() {
                jQuery.event.remove(this, types, fn, selector);
            });
        },
        trigger: function(type, data) {
            return this.each(function() {
                jQuery.event.trigger(type, data, this);
            });
        },
        triggerHandler: function(type, data) {
            var elem = this[0];
            if (elem) {
                return jQuery.event.trigger(type, data, elem, true);
            }
        }
    });
    var rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, rtagName = /<([\w:]+)/, rhtml = /<|&#?\w+;/, rnoInnerhtml = /<(?:script|style|link)/i, rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i, rscriptType = /^$|\/(?:java|ecma)script/i, rscriptTypeMasked = /^true\/(.*)/, rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g, wrapMap = {
        option: [ 1, "<select multiple='multiple'>", "</select>" ],
        thead: [ 1, "<table>", "</table>" ],
        col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
        tr: [ 2, "<table><tbody>", "</tbody></table>" ],
        td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],
        _default: [ 0, "", "" ]
    };
    wrapMap.optgroup = wrapMap.option;
    wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
    wrapMap.th = wrapMap.td;
    function manipulationTarget(elem, content) {
        return jQuery.nodeName(elem, "table") && jQuery.nodeName(content.nodeType !== 11 ? content : content.firstChild, "tr") ? elem.getElementsByTagName("tbody")[0] || elem.appendChild(elem.ownerDocument.createElement("tbody")) : elem;
    }
    function disableScript(elem) {
        elem.type = (elem.getAttribute("type") !== null) + "/" + elem.type;
        return elem;
    }
    function restoreScript(elem) {
        var match = rscriptTypeMasked.exec(elem.type);
        if (match) {
            elem.type = match[1];
        } else {
            elem.removeAttribute("type");
        }
        return elem;
    }
    function setGlobalEval(elems, refElements) {
        var i = 0, l = elems.length;
        for (;i < l; i++) {
            data_priv.set(elems[i], "globalEval", !refElements || data_priv.get(refElements[i], "globalEval"));
        }
    }
    function cloneCopyEvent(src, dest) {
        var i, l, type, pdataOld, pdataCur, udataOld, udataCur, events;
        if (dest.nodeType !== 1) {
            return;
        }
        if (data_priv.hasData(src)) {
            pdataOld = data_priv.access(src);
            pdataCur = data_priv.set(dest, pdataOld);
            events = pdataOld.events;
            if (events) {
                delete pdataCur.handle;
                pdataCur.events = {};
                for (type in events) {
                    for (i = 0, l = events[type].length; i < l; i++) {
                        jQuery.event.add(dest, type, events[type][i]);
                    }
                }
            }
        }
        if (data_user.hasData(src)) {
            udataOld = data_user.access(src);
            udataCur = jQuery.extend({}, udataOld);
            data_user.set(dest, udataCur);
        }
    }
    function getAll(context, tag) {
        var ret = context.getElementsByTagName ? context.getElementsByTagName(tag || "*") : context.querySelectorAll ? context.querySelectorAll(tag || "*") : [];
        return tag === undefined || tag && jQuery.nodeName(context, tag) ? jQuery.merge([ context ], ret) : ret;
    }
    function fixInput(src, dest) {
        var nodeName = dest.nodeName.toLowerCase();
        if (nodeName === "input" && rcheckableType.test(src.type)) {
            dest.checked = src.checked;
        } else if (nodeName === "input" || nodeName === "textarea") {
            dest.defaultValue = src.defaultValue;
        }
    }
    jQuery.extend({
        clone: function(elem, dataAndEvents, deepDataAndEvents) {
            var i, l, srcElements, destElements, clone = elem.cloneNode(true), inPage = jQuery.contains(elem.ownerDocument, elem);
            if (!support.noCloneChecked && (elem.nodeType === 1 || elem.nodeType === 11) && !jQuery.isXMLDoc(elem)) {
                destElements = getAll(clone);
                srcElements = getAll(elem);
                for (i = 0, l = srcElements.length; i < l; i++) {
                    fixInput(srcElements[i], destElements[i]);
                }
            }
            if (dataAndEvents) {
                if (deepDataAndEvents) {
                    srcElements = srcElements || getAll(elem);
                    destElements = destElements || getAll(clone);
                    for (i = 0, l = srcElements.length; i < l; i++) {
                        cloneCopyEvent(srcElements[i], destElements[i]);
                    }
                } else {
                    cloneCopyEvent(elem, clone);
                }
            }
            destElements = getAll(clone, "script");
            if (destElements.length > 0) {
                setGlobalEval(destElements, !inPage && getAll(elem, "script"));
            }
            return clone;
        },
        buildFragment: function(elems, context, scripts, selection) {
            var elem, tmp, tag, wrap, contains, j, fragment = context.createDocumentFragment(), nodes = [], i = 0, l = elems.length;
            for (;i < l; i++) {
                elem = elems[i];
                if (elem || elem === 0) {
                    if (jQuery.type(elem) === "object") {
                        jQuery.merge(nodes, elem.nodeType ? [ elem ] : elem);
                    } else if (!rhtml.test(elem)) {
                        nodes.push(context.createTextNode(elem));
                    } else {
                        tmp = tmp || fragment.appendChild(context.createElement("div"));
                        tag = (rtagName.exec(elem) || [ "", "" ])[1].toLowerCase();
                        wrap = wrapMap[tag] || wrapMap._default;
                        tmp.innerHTML = wrap[1] + elem.replace(rxhtmlTag, "<$1></$2>") + wrap[2];
                        j = wrap[0];
                        while (j--) {
                            tmp = tmp.lastChild;
                        }
                        jQuery.merge(nodes, tmp.childNodes);
                        tmp = fragment.firstChild;
                        tmp.textContent = "";
                    }
                }
            }
            fragment.textContent = "";
            i = 0;
            while (elem = nodes[i++]) {
                if (selection && jQuery.inArray(elem, selection) !== -1) {
                    continue;
                }
                contains = jQuery.contains(elem.ownerDocument, elem);
                tmp = getAll(fragment.appendChild(elem), "script");
                if (contains) {
                    setGlobalEval(tmp);
                }
                if (scripts) {
                    j = 0;
                    while (elem = tmp[j++]) {
                        if (rscriptType.test(elem.type || "")) {
                            scripts.push(elem);
                        }
                    }
                }
            }
            return fragment;
        },
        cleanData: function(elems) {
            var data, elem, type, key, special = jQuery.event.special, i = 0;
            for (;(elem = elems[i]) !== undefined; i++) {
                if (jQuery.acceptData(elem)) {
                    key = elem[data_priv.expando];
                    if (key && (data = data_priv.cache[key])) {
                        if (data.events) {
                            for (type in data.events) {
                                if (special[type]) {
                                    jQuery.event.remove(elem, type);
                                } else {
                                    jQuery.removeEvent(elem, type, data.handle);
                                }
                            }
                        }
                        if (data_priv.cache[key]) {
                            delete data_priv.cache[key];
                        }
                    }
                }
                delete data_user.cache[elem[data_user.expando]];
            }
        }
    });
    jQuery.fn.extend({
        text: function(value) {
            return access(this, function(value) {
                return value === undefined ? jQuery.text(this) : this.empty().each(function() {
                    if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
                        this.textContent = value;
                    }
                });
            }, null, value, arguments.length);
        },
        append: function() {
            return this.domManip(arguments, function(elem) {
                if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
                    var target = manipulationTarget(this, elem);
                    target.appendChild(elem);
                }
            });
        },
        prepend: function() {
            return this.domManip(arguments, function(elem) {
                if (this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9) {
                    var target = manipulationTarget(this, elem);
                    target.insertBefore(elem, target.firstChild);
                }
            });
        },
        before: function() {
            return this.domManip(arguments, function(elem) {
                if (this.parentNode) {
                    this.parentNode.insertBefore(elem, this);
                }
            });
        },
        after: function() {
            return this.domManip(arguments, function(elem) {
                if (this.parentNode) {
                    this.parentNode.insertBefore(elem, this.nextSibling);
                }
            });
        },
        remove: function(selector, keepData) {
            var elem, elems = selector ? jQuery.filter(selector, this) : this, i = 0;
            for (;(elem = elems[i]) != null; i++) {
                if (!keepData && elem.nodeType === 1) {
                    jQuery.cleanData(getAll(elem));
                }
                if (elem.parentNode) {
                    if (keepData && jQuery.contains(elem.ownerDocument, elem)) {
                        setGlobalEval(getAll(elem, "script"));
                    }
                    elem.parentNode.removeChild(elem);
                }
            }
            return this;
        },
        empty: function() {
            var elem, i = 0;
            for (;(elem = this[i]) != null; i++) {
                if (elem.nodeType === 1) {
                    jQuery.cleanData(getAll(elem, false));
                    elem.textContent = "";
                }
            }
            return this;
        },
        clone: function(dataAndEvents, deepDataAndEvents) {
            dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
            deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;
            return this.map(function() {
                return jQuery.clone(this, dataAndEvents, deepDataAndEvents);
            });
        },
        html: function(value) {
            return access(this, function(value) {
                var elem = this[0] || {}, i = 0, l = this.length;
                if (value === undefined && elem.nodeType === 1) {
                    return elem.innerHTML;
                }
                if (typeof value === "string" && !rnoInnerhtml.test(value) && !wrapMap[(rtagName.exec(value) || [ "", "" ])[1].toLowerCase()]) {
                    value = value.replace(rxhtmlTag, "<$1></$2>");
                    try {
                        for (;i < l; i++) {
                            elem = this[i] || {};
                            if (elem.nodeType === 1) {
                                jQuery.cleanData(getAll(elem, false));
                                elem.innerHTML = value;
                            }
                        }
                        elem = 0;
                    } catch (e) {}
                }
                if (elem) {
                    this.empty().append(value);
                }
            }, null, value, arguments.length);
        },
        replaceWith: function() {
            var arg = arguments[0];
            this.domManip(arguments, function(elem) {
                arg = this.parentNode;
                jQuery.cleanData(getAll(this));
                if (arg) {
                    arg.replaceChild(elem, this);
                }
            });
            return arg && (arg.length || arg.nodeType) ? this : this.remove();
        },
        detach: function(selector) {
            return this.remove(selector, true);
        },
        domManip: function(args, callback) {
            args = concat.apply([], args);
            var fragment, first, scripts, hasScripts, node, doc, i = 0, l = this.length, set = this, iNoClone = l - 1, value = args[0], isFunction = jQuery.isFunction(value);
            if (isFunction || l > 1 && typeof value === "string" && !support.checkClone && rchecked.test(value)) {
                return this.each(function(index) {
                    var self = set.eq(index);
                    if (isFunction) {
                        args[0] = value.call(this, index, self.html());
                    }
                    self.domManip(args, callback);
                });
            }
            if (l) {
                fragment = jQuery.buildFragment(args, this[0].ownerDocument, false, this);
                first = fragment.firstChild;
                if (fragment.childNodes.length === 1) {
                    fragment = first;
                }
                if (first) {
                    scripts = jQuery.map(getAll(fragment, "script"), disableScript);
                    hasScripts = scripts.length;
                    for (;i < l; i++) {
                        node = fragment;
                        if (i !== iNoClone) {
                            node = jQuery.clone(node, true, true);
                            if (hasScripts) {
                                jQuery.merge(scripts, getAll(node, "script"));
                            }
                        }
                        callback.call(this[i], node, i);
                    }
                    if (hasScripts) {
                        doc = scripts[scripts.length - 1].ownerDocument;
                        jQuery.map(scripts, restoreScript);
                        for (i = 0; i < hasScripts; i++) {
                            node = scripts[i];
                            if (rscriptType.test(node.type || "") && !data_priv.access(node, "globalEval") && jQuery.contains(doc, node)) {
                                if (node.src) {
                                    if (jQuery._evalUrl) {
                                        jQuery._evalUrl(node.src);
                                    }
                                } else {
                                    jQuery.globalEval(node.textContent.replace(rcleanScript, ""));
                                }
                            }
                        }
                    }
                }
            }
            return this;
        }
    });
    jQuery.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(name, original) {
        jQuery.fn[name] = function(selector) {
            var elems, ret = [], insert = jQuery(selector), last = insert.length - 1, i = 0;
            for (;i <= last; i++) {
                elems = i === last ? this : this.clone(true);
                jQuery(insert[i])[original](elems);
                push.apply(ret, elems.get());
            }
            return this.pushStack(ret);
        };
    });
    var iframe, elemdisplay = {};
    function actualDisplay(name, doc) {
        var style, elem = jQuery(doc.createElement(name)).appendTo(doc.body), display = window.getDefaultComputedStyle && (style = window.getDefaultComputedStyle(elem[0])) ? style.display : jQuery.css(elem[0], "display");
        elem.detach();
        return display;
    }
    function defaultDisplay(nodeName) {
        var doc = document, display = elemdisplay[nodeName];
        if (!display) {
            display = actualDisplay(nodeName, doc);
            if (display === "none" || !display) {
                iframe = (iframe || jQuery("<iframe frameborder='0' width='0' height='0'/>")).appendTo(doc.documentElement);
                doc = iframe[0].contentDocument;
                doc.write();
                doc.close();
                display = actualDisplay(nodeName, doc);
                iframe.detach();
            }
            elemdisplay[nodeName] = display;
        }
        return display;
    }
    var rmargin = /^margin/;
    var rnumnonpx = new RegExp("^(" + pnum + ")(?!px)[a-z%]+$", "i");
    var getStyles = function(elem) {
        return elem.ownerDocument.defaultView.getComputedStyle(elem, null);
    };
    function curCSS(elem, name, computed) {
        var width, minWidth, maxWidth, ret, style = elem.style;
        computed = computed || getStyles(elem);
        if (computed) {
            ret = computed.getPropertyValue(name) || computed[name];
        }
        if (computed) {
            if (ret === "" && !jQuery.contains(elem.ownerDocument, elem)) {
                ret = jQuery.style(elem, name);
            }
            if (rnumnonpx.test(ret) && rmargin.test(name)) {
                width = style.width;
                minWidth = style.minWidth;
                maxWidth = style.maxWidth;
                style.minWidth = style.maxWidth = style.width = ret;
                ret = computed.width;
                style.width = width;
                style.minWidth = minWidth;
                style.maxWidth = maxWidth;
            }
        }
        return ret !== undefined ? ret + "" : ret;
    }
    function addGetHookIf(conditionFn, hookFn) {
        return {
            get: function() {
                if (conditionFn()) {
                    delete this.get;
                    return;
                }
                return (this.get = hookFn).apply(this, arguments);
            }
        };
    }
    (function() {
        var pixelPositionVal, boxSizingReliableVal, docElem = document.documentElement, container = document.createElement("div"), div = document.createElement("div");
        if (!div.style) {
            return;
        }
        div.style.backgroundClip = "content-box";
        div.cloneNode(true).style.backgroundClip = "";
        support.clearCloneStyle = div.style.backgroundClip === "content-box";
        container.style.cssText = "border:0;width:0;height:0;top:0;left:-9999px;margin-top:1px;" + "position:absolute";
        container.appendChild(div);
        function computePixelPositionAndBoxSizingReliable() {
            div.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;" + "box-sizing:border-box;display:block;margin-top:1%;top:1%;" + "border:1px;padding:1px;width:4px;position:absolute";
            div.innerHTML = "";
            docElem.appendChild(container);
            var divStyle = window.getComputedStyle(div, null);
            pixelPositionVal = divStyle.top !== "1%";
            boxSizingReliableVal = divStyle.width === "4px";
            docElem.removeChild(container);
        }
        if (window.getComputedStyle) {
            jQuery.extend(support, {
                pixelPosition: function() {
                    computePixelPositionAndBoxSizingReliable();
                    return pixelPositionVal;
                },
                boxSizingReliable: function() {
                    if (boxSizingReliableVal == null) {
                        computePixelPositionAndBoxSizingReliable();
                    }
                    return boxSizingReliableVal;
                },
                reliableMarginRight: function() {
                    var ret, marginDiv = div.appendChild(document.createElement("div"));
                    marginDiv.style.cssText = div.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;" + "box-sizing:content-box;display:block;margin:0;border:0;padding:0";
                    marginDiv.style.marginRight = marginDiv.style.width = "0";
                    div.style.width = "1px";
                    docElem.appendChild(container);
                    ret = !parseFloat(window.getComputedStyle(marginDiv, null).marginRight);
                    docElem.removeChild(container);
                    return ret;
                }
            });
        }
    })();
    jQuery.swap = function(elem, options, callback, args) {
        var ret, name, old = {};
        for (name in options) {
            old[name] = elem.style[name];
            elem.style[name] = options[name];
        }
        ret = callback.apply(elem, args || []);
        for (name in options) {
            elem.style[name] = old[name];
        }
        return ret;
    };
    var rdisplayswap = /^(none|table(?!-c[ea]).+)/, rnumsplit = new RegExp("^(" + pnum + ")(.*)$", "i"), rrelNum = new RegExp("^([+-])=(" + pnum + ")", "i"), cssShow = {
        position: "absolute",
        visibility: "hidden",
        display: "block"
    }, cssNormalTransform = {
        letterSpacing: "0",
        fontWeight: "400"
    }, cssPrefixes = [ "Webkit", "O", "Moz", "ms" ];
    function vendorPropName(style, name) {
        if (name in style) {
            return name;
        }
        var capName = name[0].toUpperCase() + name.slice(1), origName = name, i = cssPrefixes.length;
        while (i--) {
            name = cssPrefixes[i] + capName;
            if (name in style) {
                return name;
            }
        }
        return origName;
    }
    function setPositiveNumber(elem, value, subtract) {
        var matches = rnumsplit.exec(value);
        return matches ? Math.max(0, matches[1] - (subtract || 0)) + (matches[2] || "px") : value;
    }
    function augmentWidthOrHeight(elem, name, extra, isBorderBox, styles) {
        var i = extra === (isBorderBox ? "border" : "content") ? 4 : name === "width" ? 1 : 0, val = 0;
        for (;i < 4; i += 2) {
            if (extra === "margin") {
                val += jQuery.css(elem, extra + cssExpand[i], true, styles);
            }
            if (isBorderBox) {
                if (extra === "content") {
                    val -= jQuery.css(elem, "padding" + cssExpand[i], true, styles);
                }
                if (extra !== "margin") {
                    val -= jQuery.css(elem, "border" + cssExpand[i] + "Width", true, styles);
                }
            } else {
                val += jQuery.css(elem, "padding" + cssExpand[i], true, styles);
                if (extra !== "padding") {
                    val += jQuery.css(elem, "border" + cssExpand[i] + "Width", true, styles);
                }
            }
        }
        return val;
    }
    function getWidthOrHeight(elem, name, extra) {
        var valueIsBorderBox = true, val = name === "width" ? elem.offsetWidth : elem.offsetHeight, styles = getStyles(elem), isBorderBox = jQuery.css(elem, "boxSizing", false, styles) === "border-box";
        if (val <= 0 || val == null) {
            val = curCSS(elem, name, styles);
            if (val < 0 || val == null) {
                val = elem.style[name];
            }
            if (rnumnonpx.test(val)) {
                return val;
            }
            valueIsBorderBox = isBorderBox && (support.boxSizingReliable() || val === elem.style[name]);
            val = parseFloat(val) || 0;
        }
        return val + augmentWidthOrHeight(elem, name, extra || (isBorderBox ? "border" : "content"), valueIsBorderBox, styles) + "px";
    }
    function showHide(elements, show) {
        var display, elem, hidden, values = [], index = 0, length = elements.length;
        for (;index < length; index++) {
            elem = elements[index];
            if (!elem.style) {
                continue;
            }
            values[index] = data_priv.get(elem, "olddisplay");
            display = elem.style.display;
            if (show) {
                if (!values[index] && display === "none") {
                    elem.style.display = "";
                }
                if (elem.style.display === "" && isHidden(elem)) {
                    values[index] = data_priv.access(elem, "olddisplay", defaultDisplay(elem.nodeName));
                }
            } else {
                hidden = isHidden(elem);
                if (display !== "none" || !hidden) {
                    data_priv.set(elem, "olddisplay", hidden ? display : jQuery.css(elem, "display"));
                }
            }
        }
        for (index = 0; index < length; index++) {
            elem = elements[index];
            if (!elem.style) {
                continue;
            }
            if (!show || elem.style.display === "none" || elem.style.display === "") {
                elem.style.display = show ? values[index] || "" : "none";
            }
        }
        return elements;
    }
    jQuery.extend({
        cssHooks: {
            opacity: {
                get: function(elem, computed) {
                    if (computed) {
                        var ret = curCSS(elem, "opacity");
                        return ret === "" ? "1" : ret;
                    }
                }
            }
        },
        cssNumber: {
            columnCount: true,
            fillOpacity: true,
            flexGrow: true,
            flexShrink: true,
            fontWeight: true,
            lineHeight: true,
            opacity: true,
            order: true,
            orphans: true,
            widows: true,
            zIndex: true,
            zoom: true
        },
        cssProps: {
            "float": "cssFloat"
        },
        style: function(elem, name, value, extra) {
            if (!elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style) {
                return;
            }
            var ret, type, hooks, origName = jQuery.camelCase(name), style = elem.style;
            name = jQuery.cssProps[origName] || (jQuery.cssProps[origName] = vendorPropName(style, origName));
            hooks = jQuery.cssHooks[name] || jQuery.cssHooks[origName];
            if (value !== undefined) {
                type = typeof value;
                if (type === "string" && (ret = rrelNum.exec(value))) {
                    value = (ret[1] + 1) * ret[2] + parseFloat(jQuery.css(elem, name));
                    type = "number";
                }
                if (value == null || value !== value) {
                    return;
                }
                if (type === "number" && !jQuery.cssNumber[origName]) {
                    value += "px";
                }
                if (!support.clearCloneStyle && value === "" && name.indexOf("background") === 0) {
                    style[name] = "inherit";
                }
                if (!hooks || !("set" in hooks) || (value = hooks.set(elem, value, extra)) !== undefined) {
                    style[name] = value;
                }
            } else {
                if (hooks && "get" in hooks && (ret = hooks.get(elem, false, extra)) !== undefined) {
                    return ret;
                }
                return style[name];
            }
        },
        css: function(elem, name, extra, styles) {
            var val, num, hooks, origName = jQuery.camelCase(name);
            name = jQuery.cssProps[origName] || (jQuery.cssProps[origName] = vendorPropName(elem.style, origName));
            hooks = jQuery.cssHooks[name] || jQuery.cssHooks[origName];
            if (hooks && "get" in hooks) {
                val = hooks.get(elem, true, extra);
            }
            if (val === undefined) {
                val = curCSS(elem, name, styles);
            }
            if (val === "normal" && name in cssNormalTransform) {
                val = cssNormalTransform[name];
            }
            if (extra === "" || extra) {
                num = parseFloat(val);
                return extra === true || jQuery.isNumeric(num) ? num || 0 : val;
            }
            return val;
        }
    });
    jQuery.each([ "height", "width" ], function(i, name) {
        jQuery.cssHooks[name] = {
            get: function(elem, computed, extra) {
                if (computed) {
                    return rdisplayswap.test(jQuery.css(elem, "display")) && elem.offsetWidth === 0 ? jQuery.swap(elem, cssShow, function() {
                        return getWidthOrHeight(elem, name, extra);
                    }) : getWidthOrHeight(elem, name, extra);
                }
            },
            set: function(elem, value, extra) {
                var styles = extra && getStyles(elem);
                return setPositiveNumber(elem, value, extra ? augmentWidthOrHeight(elem, name, extra, jQuery.css(elem, "boxSizing", false, styles) === "border-box", styles) : 0);
            }
        };
    });
    jQuery.cssHooks.marginRight = addGetHookIf(support.reliableMarginRight, function(elem, computed) {
        if (computed) {
            return jQuery.swap(elem, {
                display: "inline-block"
            }, curCSS, [ elem, "marginRight" ]);
        }
    });
    jQuery.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(prefix, suffix) {
        jQuery.cssHooks[prefix + suffix] = {
            expand: function(value) {
                var i = 0, expanded = {}, parts = typeof value === "string" ? value.split(" ") : [ value ];
                for (;i < 4; i++) {
                    expanded[prefix + cssExpand[i] + suffix] = parts[i] || parts[i - 2] || parts[0];
                }
                return expanded;
            }
        };
        if (!rmargin.test(prefix)) {
            jQuery.cssHooks[prefix + suffix].set = setPositiveNumber;
        }
    });
    jQuery.fn.extend({
        css: function(name, value) {
            return access(this, function(elem, name, value) {
                var styles, len, map = {}, i = 0;
                if (jQuery.isArray(name)) {
                    styles = getStyles(elem);
                    len = name.length;
                    for (;i < len; i++) {
                        map[name[i]] = jQuery.css(elem, name[i], false, styles);
                    }
                    return map;
                }
                return value !== undefined ? jQuery.style(elem, name, value) : jQuery.css(elem, name);
            }, name, value, arguments.length > 1);
        },
        show: function() {
            return showHide(this, true);
        },
        hide: function() {
            return showHide(this);
        },
        toggle: function(state) {
            if (typeof state === "boolean") {
                return state ? this.show() : this.hide();
            }
            return this.each(function() {
                if (isHidden(this)) {
                    jQuery(this).show();
                } else {
                    jQuery(this).hide();
                }
            });
        }
    });
    function Tween(elem, options, prop, end, easing) {
        return new Tween.prototype.init(elem, options, prop, end, easing);
    }
    jQuery.Tween = Tween;
    Tween.prototype = {
        constructor: Tween,
        init: function(elem, options, prop, end, easing, unit) {
            this.elem = elem;
            this.prop = prop;
            this.easing = easing || "swing";
            this.options = options;
            this.start = this.now = this.cur();
            this.end = end;
            this.unit = unit || (jQuery.cssNumber[prop] ? "" : "px");
        },
        cur: function() {
            var hooks = Tween.propHooks[this.prop];
            return hooks && hooks.get ? hooks.get(this) : Tween.propHooks._default.get(this);
        },
        run: function(percent) {
            var eased, hooks = Tween.propHooks[this.prop];
            if (this.options.duration) {
                this.pos = eased = jQuery.easing[this.easing](percent, this.options.duration * percent, 0, 1, this.options.duration);
            } else {
                this.pos = eased = percent;
            }
            this.now = (this.end - this.start) * eased + this.start;
            if (this.options.step) {
                this.options.step.call(this.elem, this.now, this);
            }
            if (hooks && hooks.set) {
                hooks.set(this);
            } else {
                Tween.propHooks._default.set(this);
            }
            return this;
        }
    };
    Tween.prototype.init.prototype = Tween.prototype;
    Tween.propHooks = {
        _default: {
            get: function(tween) {
                var result;
                if (tween.elem[tween.prop] != null && (!tween.elem.style || tween.elem.style[tween.prop] == null)) {
                    return tween.elem[tween.prop];
                }
                result = jQuery.css(tween.elem, tween.prop, "");
                return !result || result === "auto" ? 0 : result;
            },
            set: function(tween) {
                if (jQuery.fx.step[tween.prop]) {
                    jQuery.fx.step[tween.prop](tween);
                } else if (tween.elem.style && (tween.elem.style[jQuery.cssProps[tween.prop]] != null || jQuery.cssHooks[tween.prop])) {
                    jQuery.style(tween.elem, tween.prop, tween.now + tween.unit);
                } else {
                    tween.elem[tween.prop] = tween.now;
                }
            }
        }
    };
    Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
        set: function(tween) {
            if (tween.elem.nodeType && tween.elem.parentNode) {
                tween.elem[tween.prop] = tween.now;
            }
        }
    };
    jQuery.easing = {
        linear: function(p) {
            return p;
        },
        swing: function(p) {
            return .5 - Math.cos(p * Math.PI) / 2;
        }
    };
    jQuery.fx = Tween.prototype.init;
    jQuery.fx.step = {};
    var fxNow, timerId, rfxtypes = /^(?:toggle|show|hide)$/, rfxnum = new RegExp("^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i"), rrun = /queueHooks$/, animationPrefilters = [ defaultPrefilter ], tweeners = {
        "*": [ function(prop, value) {
            var tween = this.createTween(prop, value), target = tween.cur(), parts = rfxnum.exec(value), unit = parts && parts[3] || (jQuery.cssNumber[prop] ? "" : "px"), start = (jQuery.cssNumber[prop] || unit !== "px" && +target) && rfxnum.exec(jQuery.css(tween.elem, prop)), scale = 1, maxIterations = 20;
            if (start && start[3] !== unit) {
                unit = unit || start[3];
                parts = parts || [];
                start = +target || 1;
                do {
                    scale = scale || ".5";
                    start = start / scale;
                    jQuery.style(tween.elem, prop, start + unit);
                } while (scale !== (scale = tween.cur() / target) && scale !== 1 && --maxIterations);
            }
            if (parts) {
                start = tween.start = +start || +target || 0;
                tween.unit = unit;
                tween.end = parts[1] ? start + (parts[1] + 1) * parts[2] : +parts[2];
            }
            return tween;
        } ]
    };
    function createFxNow() {
        setTimeout(function() {
            fxNow = undefined;
        });
        return fxNow = jQuery.now();
    }
    function genFx(type, includeWidth) {
        var which, i = 0, attrs = {
            height: type
        };
        includeWidth = includeWidth ? 1 : 0;
        for (;i < 4; i += 2 - includeWidth) {
            which = cssExpand[i];
            attrs["margin" + which] = attrs["padding" + which] = type;
        }
        if (includeWidth) {
            attrs.opacity = attrs.width = type;
        }
        return attrs;
    }
    function createTween(value, prop, animation) {
        var tween, collection = (tweeners[prop] || []).concat(tweeners["*"]), index = 0, length = collection.length;
        for (;index < length; index++) {
            if (tween = collection[index].call(animation, prop, value)) {
                return tween;
            }
        }
    }
    function defaultPrefilter(elem, props, opts) {
        var prop, value, toggle, tween, hooks, oldfire, display, checkDisplay, anim = this, orig = {}, style = elem.style, hidden = elem.nodeType && isHidden(elem), dataShow = data_priv.get(elem, "fxshow");
        if (!opts.queue) {
            hooks = jQuery._queueHooks(elem, "fx");
            if (hooks.unqueued == null) {
                hooks.unqueued = 0;
                oldfire = hooks.empty.fire;
                hooks.empty.fire = function() {
                    if (!hooks.unqueued) {
                        oldfire();
                    }
                };
            }
            hooks.unqueued++;
            anim.always(function() {
                anim.always(function() {
                    hooks.unqueued--;
                    if (!jQuery.queue(elem, "fx").length) {
                        hooks.empty.fire();
                    }
                });
            });
        }
        if (elem.nodeType === 1 && ("height" in props || "width" in props)) {
            opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];
            display = jQuery.css(elem, "display");
            checkDisplay = display === "none" ? data_priv.get(elem, "olddisplay") || defaultDisplay(elem.nodeName) : display;
            if (checkDisplay === "inline" && jQuery.css(elem, "float") === "none") {
                style.display = "inline-block";
            }
        }
        if (opts.overflow) {
            style.overflow = "hidden";
            anim.always(function() {
                style.overflow = opts.overflow[0];
                style.overflowX = opts.overflow[1];
                style.overflowY = opts.overflow[2];
            });
        }
        for (prop in props) {
            value = props[prop];
            if (rfxtypes.exec(value)) {
                delete props[prop];
                toggle = toggle || value === "toggle";
                if (value === (hidden ? "hide" : "show")) {
                    if (value === "show" && dataShow && dataShow[prop] !== undefined) {
                        hidden = true;
                    } else {
                        continue;
                    }
                }
                orig[prop] = dataShow && dataShow[prop] || jQuery.style(elem, prop);
            } else {
                display = undefined;
            }
        }
        if (!jQuery.isEmptyObject(orig)) {
            if (dataShow) {
                if ("hidden" in dataShow) {
                    hidden = dataShow.hidden;
                }
            } else {
                dataShow = data_priv.access(elem, "fxshow", {});
            }
            if (toggle) {
                dataShow.hidden = !hidden;
            }
            if (hidden) {
                jQuery(elem).show();
            } else {
                anim.done(function() {
                    jQuery(elem).hide();
                });
            }
            anim.done(function() {
                var prop;
                data_priv.remove(elem, "fxshow");
                for (prop in orig) {
                    jQuery.style(elem, prop, orig[prop]);
                }
            });
            for (prop in orig) {
                tween = createTween(hidden ? dataShow[prop] : 0, prop, anim);
                if (!(prop in dataShow)) {
                    dataShow[prop] = tween.start;
                    if (hidden) {
                        tween.end = tween.start;
                        tween.start = prop === "width" || prop === "height" ? 1 : 0;
                    }
                }
            }
        } else if ((display === "none" ? defaultDisplay(elem.nodeName) : display) === "inline") {
            style.display = display;
        }
    }
    function propFilter(props, specialEasing) {
        var index, name, easing, value, hooks;
        for (index in props) {
            name = jQuery.camelCase(index);
            easing = specialEasing[name];
            value = props[index];
            if (jQuery.isArray(value)) {
                easing = value[1];
                value = props[index] = value[0];
            }
            if (index !== name) {
                props[name] = value;
                delete props[index];
            }
            hooks = jQuery.cssHooks[name];
            if (hooks && "expand" in hooks) {
                value = hooks.expand(value);
                delete props[name];
                for (index in value) {
                    if (!(index in props)) {
                        props[index] = value[index];
                        specialEasing[index] = easing;
                    }
                }
            } else {
                specialEasing[name] = easing;
            }
        }
    }
    function Animation(elem, properties, options) {
        var result, stopped, index = 0, length = animationPrefilters.length, deferred = jQuery.Deferred().always(function() {
            delete tick.elem;
        }), tick = function() {
            if (stopped) {
                return false;
            }
            var currentTime = fxNow || createFxNow(), remaining = Math.max(0, animation.startTime + animation.duration - currentTime), temp = remaining / animation.duration || 0, percent = 1 - temp, index = 0, length = animation.tweens.length;
            for (;index < length; index++) {
                animation.tweens[index].run(percent);
            }
            deferred.notifyWith(elem, [ animation, percent, remaining ]);
            if (percent < 1 && length) {
                return remaining;
            } else {
                deferred.resolveWith(elem, [ animation ]);
                return false;
            }
        }, animation = deferred.promise({
            elem: elem,
            props: jQuery.extend({}, properties),
            opts: jQuery.extend(true, {
                specialEasing: {}
            }, options),
            originalProperties: properties,
            originalOptions: options,
            startTime: fxNow || createFxNow(),
            duration: options.duration,
            tweens: [],
            createTween: function(prop, end) {
                var tween = jQuery.Tween(elem, animation.opts, prop, end, animation.opts.specialEasing[prop] || animation.opts.easing);
                animation.tweens.push(tween);
                return tween;
            },
            stop: function(gotoEnd) {
                var index = 0, length = gotoEnd ? animation.tweens.length : 0;
                if (stopped) {
                    return this;
                }
                stopped = true;
                for (;index < length; index++) {
                    animation.tweens[index].run(1);
                }
                if (gotoEnd) {
                    deferred.resolveWith(elem, [ animation, gotoEnd ]);
                } else {
                    deferred.rejectWith(elem, [ animation, gotoEnd ]);
                }
                return this;
            }
        }), props = animation.props;
        propFilter(props, animation.opts.specialEasing);
        for (;index < length; index++) {
            result = animationPrefilters[index].call(animation, elem, props, animation.opts);
            if (result) {
                return result;
            }
        }
        jQuery.map(props, createTween, animation);
        if (jQuery.isFunction(animation.opts.start)) {
            animation.opts.start.call(elem, animation);
        }
        jQuery.fx.timer(jQuery.extend(tick, {
            elem: elem,
            anim: animation,
            queue: animation.opts.queue
        }));
        return animation.progress(animation.opts.progress).done(animation.opts.done, animation.opts.complete).fail(animation.opts.fail).always(animation.opts.always);
    }
    jQuery.Animation = jQuery.extend(Animation, {
        tweener: function(props, callback) {
            if (jQuery.isFunction(props)) {
                callback = props;
                props = [ "*" ];
            } else {
                props = props.split(" ");
            }
            var prop, index = 0, length = props.length;
            for (;index < length; index++) {
                prop = props[index];
                tweeners[prop] = tweeners[prop] || [];
                tweeners[prop].unshift(callback);
            }
        },
        prefilter: function(callback, prepend) {
            if (prepend) {
                animationPrefilters.unshift(callback);
            } else {
                animationPrefilters.push(callback);
            }
        }
    });
    jQuery.speed = function(speed, easing, fn) {
        var opt = speed && typeof speed === "object" ? jQuery.extend({}, speed) : {
            complete: fn || !fn && easing || jQuery.isFunction(speed) && speed,
            duration: speed,
            easing: fn && easing || easing && !jQuery.isFunction(easing) && easing
        };
        opt.duration = jQuery.fx.off ? 0 : typeof opt.duration === "number" ? opt.duration : opt.duration in jQuery.fx.speeds ? jQuery.fx.speeds[opt.duration] : jQuery.fx.speeds._default;
        if (opt.queue == null || opt.queue === true) {
            opt.queue = "fx";
        }
        opt.old = opt.complete;
        opt.complete = function() {
            if (jQuery.isFunction(opt.old)) {
                opt.old.call(this);
            }
            if (opt.queue) {
                jQuery.dequeue(this, opt.queue);
            }
        };
        return opt;
    };
    jQuery.fn.extend({
        fadeTo: function(speed, to, easing, callback) {
            return this.filter(isHidden).css("opacity", 0).show().end().animate({
                opacity: to
            }, speed, easing, callback);
        },
        animate: function(prop, speed, easing, callback) {
            var empty = jQuery.isEmptyObject(prop), optall = jQuery.speed(speed, easing, callback), doAnimation = function() {
                var anim = Animation(this, jQuery.extend({}, prop), optall);
                if (empty || data_priv.get(this, "finish")) {
                    anim.stop(true);
                }
            };
            doAnimation.finish = doAnimation;
            return empty || optall.queue === false ? this.each(doAnimation) : this.queue(optall.queue, doAnimation);
        },
        stop: function(type, clearQueue, gotoEnd) {
            var stopQueue = function(hooks) {
                var stop = hooks.stop;
                delete hooks.stop;
                stop(gotoEnd);
            };
            if (typeof type !== "string") {
                gotoEnd = clearQueue;
                clearQueue = type;
                type = undefined;
            }
            if (clearQueue && type !== false) {
                this.queue(type || "fx", []);
            }
            return this.each(function() {
                var dequeue = true, index = type != null && type + "queueHooks", timers = jQuery.timers, data = data_priv.get(this);
                if (index) {
                    if (data[index] && data[index].stop) {
                        stopQueue(data[index]);
                    }
                } else {
                    for (index in data) {
                        if (data[index] && data[index].stop && rrun.test(index)) {
                            stopQueue(data[index]);
                        }
                    }
                }
                for (index = timers.length; index--; ) {
                    if (timers[index].elem === this && (type == null || timers[index].queue === type)) {
                        timers[index].anim.stop(gotoEnd);
                        dequeue = false;
                        timers.splice(index, 1);
                    }
                }
                if (dequeue || !gotoEnd) {
                    jQuery.dequeue(this, type);
                }
            });
        },
        finish: function(type) {
            if (type !== false) {
                type = type || "fx";
            }
            return this.each(function() {
                var index, data = data_priv.get(this), queue = data[type + "queue"], hooks = data[type + "queueHooks"], timers = jQuery.timers, length = queue ? queue.length : 0;
                data.finish = true;
                jQuery.queue(this, type, []);
                if (hooks && hooks.stop) {
                    hooks.stop.call(this, true);
                }
                for (index = timers.length; index--; ) {
                    if (timers[index].elem === this && timers[index].queue === type) {
                        timers[index].anim.stop(true);
                        timers.splice(index, 1);
                    }
                }
                for (index = 0; index < length; index++) {
                    if (queue[index] && queue[index].finish) {
                        queue[index].finish.call(this);
                    }
                }
                delete data.finish;
            });
        }
    });
    jQuery.each([ "toggle", "show", "hide" ], function(i, name) {
        var cssFn = jQuery.fn[name];
        jQuery.fn[name] = function(speed, easing, callback) {
            return speed == null || typeof speed === "boolean" ? cssFn.apply(this, arguments) : this.animate(genFx(name, true), speed, easing, callback);
        };
    });
    jQuery.each({
        slideDown: genFx("show"),
        slideUp: genFx("hide"),
        slideToggle: genFx("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(name, props) {
        jQuery.fn[name] = function(speed, easing, callback) {
            return this.animate(props, speed, easing, callback);
        };
    });
    jQuery.timers = [];
    jQuery.fx.tick = function() {
        var timer, i = 0, timers = jQuery.timers;
        fxNow = jQuery.now();
        for (;i < timers.length; i++) {
            timer = timers[i];
            if (!timer() && timers[i] === timer) {
                timers.splice(i--, 1);
            }
        }
        if (!timers.length) {
            jQuery.fx.stop();
        }
        fxNow = undefined;
    };
    jQuery.fx.timer = function(timer) {
        jQuery.timers.push(timer);
        if (timer()) {
            jQuery.fx.start();
        } else {
            jQuery.timers.pop();
        }
    };
    jQuery.fx.interval = 13;
    jQuery.fx.start = function() {
        if (!timerId) {
            timerId = setInterval(jQuery.fx.tick, jQuery.fx.interval);
        }
    };
    jQuery.fx.stop = function() {
        clearInterval(timerId);
        timerId = null;
    };
    jQuery.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    };
    jQuery.fn.delay = function(time, type) {
        time = jQuery.fx ? jQuery.fx.speeds[time] || time : time;
        type = type || "fx";
        return this.queue(type, function(next, hooks) {
            var timeout = setTimeout(next, time);
            hooks.stop = function() {
                clearTimeout(timeout);
            };
        });
    };
    (function() {
        var input = document.createElement("input"), select = document.createElement("select"), opt = select.appendChild(document.createElement("option"));
        input.type = "checkbox";
        support.checkOn = input.value !== "";
        support.optSelected = opt.selected;
        select.disabled = true;
        support.optDisabled = !opt.disabled;
        input = document.createElement("input");
        input.value = "t";
        input.type = "radio";
        support.radioValue = input.value === "t";
    })();
    var nodeHook, boolHook, attrHandle = jQuery.expr.attrHandle;
    jQuery.fn.extend({
        attr: function(name, value) {
            return access(this, jQuery.attr, name, value, arguments.length > 1);
        },
        removeAttr: function(name) {
            return this.each(function() {
                jQuery.removeAttr(this, name);
            });
        }
    });
    jQuery.extend({
        attr: function(elem, name, value) {
            var hooks, ret, nType = elem.nodeType;
            if (!elem || nType === 3 || nType === 8 || nType === 2) {
                return;
            }
            if (typeof elem.getAttribute === strundefined) {
                return jQuery.prop(elem, name, value);
            }
            if (nType !== 1 || !jQuery.isXMLDoc(elem)) {
                name = name.toLowerCase();
                hooks = jQuery.attrHooks[name] || (jQuery.expr.match.bool.test(name) ? boolHook : nodeHook);
            }
            if (value !== undefined) {
                if (value === null) {
                    jQuery.removeAttr(elem, name);
                } else if (hooks && "set" in hooks && (ret = hooks.set(elem, value, name)) !== undefined) {
                    return ret;
                } else {
                    elem.setAttribute(name, value + "");
                    return value;
                }
            } else if (hooks && "get" in hooks && (ret = hooks.get(elem, name)) !== null) {
                return ret;
            } else {
                ret = jQuery.find.attr(elem, name);
                return ret == null ? undefined : ret;
            }
        },
        removeAttr: function(elem, value) {
            var name, propName, i = 0, attrNames = value && value.match(rnotwhite);
            if (attrNames && elem.nodeType === 1) {
                while (name = attrNames[i++]) {
                    propName = jQuery.propFix[name] || name;
                    if (jQuery.expr.match.bool.test(name)) {
                        elem[propName] = false;
                    }
                    elem.removeAttribute(name);
                }
            }
        },
        attrHooks: {
            type: {
                set: function(elem, value) {
                    if (!support.radioValue && value === "radio" && jQuery.nodeName(elem, "input")) {
                        var val = elem.value;
                        elem.setAttribute("type", value);
                        if (val) {
                            elem.value = val;
                        }
                        return value;
                    }
                }
            }
        }
    });
    boolHook = {
        set: function(elem, value, name) {
            if (value === false) {
                jQuery.removeAttr(elem, name);
            } else {
                elem.setAttribute(name, name);
            }
            return name;
        }
    };
    jQuery.each(jQuery.expr.match.bool.source.match(/\w+/g), function(i, name) {
        var getter = attrHandle[name] || jQuery.find.attr;
        attrHandle[name] = function(elem, name, isXML) {
            var ret, handle;
            if (!isXML) {
                handle = attrHandle[name];
                attrHandle[name] = ret;
                ret = getter(elem, name, isXML) != null ? name.toLowerCase() : null;
                attrHandle[name] = handle;
            }
            return ret;
        };
    });
    var rfocusable = /^(?:input|select|textarea|button)$/i;
    jQuery.fn.extend({
        prop: function(name, value) {
            return access(this, jQuery.prop, name, value, arguments.length > 1);
        },
        removeProp: function(name) {
            return this.each(function() {
                delete this[jQuery.propFix[name] || name];
            });
        }
    });
    jQuery.extend({
        propFix: {
            "for": "htmlFor",
            "class": "className"
        },
        prop: function(elem, name, value) {
            var ret, hooks, notxml, nType = elem.nodeType;
            if (!elem || nType === 3 || nType === 8 || nType === 2) {
                return;
            }
            notxml = nType !== 1 || !jQuery.isXMLDoc(elem);
            if (notxml) {
                name = jQuery.propFix[name] || name;
                hooks = jQuery.propHooks[name];
            }
            if (value !== undefined) {
                return hooks && "set" in hooks && (ret = hooks.set(elem, value, name)) !== undefined ? ret : elem[name] = value;
            } else {
                return hooks && "get" in hooks && (ret = hooks.get(elem, name)) !== null ? ret : elem[name];
            }
        },
        propHooks: {
            tabIndex: {
                get: function(elem) {
                    return elem.hasAttribute("tabindex") || rfocusable.test(elem.nodeName) || elem.href ? elem.tabIndex : -1;
                }
            }
        }
    });
    if (!support.optSelected) {
        jQuery.propHooks.selected = {
            get: function(elem) {
                var parent = elem.parentNode;
                if (parent && parent.parentNode) {
                    parent.parentNode.selectedIndex;
                }
                return null;
            }
        };
    }
    jQuery.each([ "tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable" ], function() {
        jQuery.propFix[this.toLowerCase()] = this;
    });
    var rclass = /[\t\r\n\f]/g;
    jQuery.fn.extend({
        addClass: function(value) {
            var classes, elem, cur, clazz, j, finalValue, proceed = typeof value === "string" && value, i = 0, len = this.length;
            if (jQuery.isFunction(value)) {
                return this.each(function(j) {
                    jQuery(this).addClass(value.call(this, j, this.className));
                });
            }
            if (proceed) {
                classes = (value || "").match(rnotwhite) || [];
                for (;i < len; i++) {
                    elem = this[i];
                    cur = elem.nodeType === 1 && (elem.className ? (" " + elem.className + " ").replace(rclass, " ") : " ");
                    if (cur) {
                        j = 0;
                        while (clazz = classes[j++]) {
                            if (cur.indexOf(" " + clazz + " ") < 0) {
                                cur += clazz + " ";
                            }
                        }
                        finalValue = jQuery.trim(cur);
                        if (elem.className !== finalValue) {
                            elem.className = finalValue;
                        }
                    }
                }
            }
            return this;
        },
        removeClass: function(value) {
            var classes, elem, cur, clazz, j, finalValue, proceed = arguments.length === 0 || typeof value === "string" && value, i = 0, len = this.length;
            if (jQuery.isFunction(value)) {
                return this.each(function(j) {
                    jQuery(this).removeClass(value.call(this, j, this.className));
                });
            }
            if (proceed) {
                classes = (value || "").match(rnotwhite) || [];
                for (;i < len; i++) {
                    elem = this[i];
                    cur = elem.nodeType === 1 && (elem.className ? (" " + elem.className + " ").replace(rclass, " ") : "");
                    if (cur) {
                        j = 0;
                        while (clazz = classes[j++]) {
                            while (cur.indexOf(" " + clazz + " ") >= 0) {
                                cur = cur.replace(" " + clazz + " ", " ");
                            }
                        }
                        finalValue = value ? jQuery.trim(cur) : "";
                        if (elem.className !== finalValue) {
                            elem.className = finalValue;
                        }
                    }
                }
            }
            return this;
        },
        toggleClass: function(value, stateVal) {
            var type = typeof value;
            if (typeof stateVal === "boolean" && type === "string") {
                return stateVal ? this.addClass(value) : this.removeClass(value);
            }
            if (jQuery.isFunction(value)) {
                return this.each(function(i) {
                    jQuery(this).toggleClass(value.call(this, i, this.className, stateVal), stateVal);
                });
            }
            return this.each(function() {
                if (type === "string") {
                    var className, i = 0, self = jQuery(this), classNames = value.match(rnotwhite) || [];
                    while (className = classNames[i++]) {
                        if (self.hasClass(className)) {
                            self.removeClass(className);
                        } else {
                            self.addClass(className);
                        }
                    }
                } else if (type === strundefined || type === "boolean") {
                    if (this.className) {
                        data_priv.set(this, "__className__", this.className);
                    }
                    this.className = this.className || value === false ? "" : data_priv.get(this, "__className__") || "";
                }
            });
        },
        hasClass: function(selector) {
            var className = " " + selector + " ", i = 0, l = this.length;
            for (;i < l; i++) {
                if (this[i].nodeType === 1 && (" " + this[i].className + " ").replace(rclass, " ").indexOf(className) >= 0) {
                    return true;
                }
            }
            return false;
        }
    });
    var rreturn = /\r/g;
    jQuery.fn.extend({
        val: function(value) {
            var hooks, ret, isFunction, elem = this[0];
            if (!arguments.length) {
                if (elem) {
                    hooks = jQuery.valHooks[elem.type] || jQuery.valHooks[elem.nodeName.toLowerCase()];
                    if (hooks && "get" in hooks && (ret = hooks.get(elem, "value")) !== undefined) {
                        return ret;
                    }
                    ret = elem.value;
                    return typeof ret === "string" ? ret.replace(rreturn, "") : ret == null ? "" : ret;
                }
                return;
            }
            isFunction = jQuery.isFunction(value);
            return this.each(function(i) {
                var val;
                if (this.nodeType !== 1) {
                    return;
                }
                if (isFunction) {
                    val = value.call(this, i, jQuery(this).val());
                } else {
                    val = value;
                }
                if (val == null) {
                    val = "";
                } else if (typeof val === "number") {
                    val += "";
                } else if (jQuery.isArray(val)) {
                    val = jQuery.map(val, function(value) {
                        return value == null ? "" : value + "";
                    });
                }
                hooks = jQuery.valHooks[this.type] || jQuery.valHooks[this.nodeName.toLowerCase()];
                if (!hooks || !("set" in hooks) || hooks.set(this, val, "value") === undefined) {
                    this.value = val;
                }
            });
        }
    });
    jQuery.extend({
        valHooks: {
            option: {
                get: function(elem) {
                    var val = jQuery.find.attr(elem, "value");
                    return val != null ? val : jQuery.trim(jQuery.text(elem));
                }
            },
            select: {
                get: function(elem) {
                    var value, option, options = elem.options, index = elem.selectedIndex, one = elem.type === "select-one" || index < 0, values = one ? null : [], max = one ? index + 1 : options.length, i = index < 0 ? max : one ? index : 0;
                    for (;i < max; i++) {
                        option = options[i];
                        if ((option.selected || i === index) && (support.optDisabled ? !option.disabled : option.getAttribute("disabled") === null) && (!option.parentNode.disabled || !jQuery.nodeName(option.parentNode, "optgroup"))) {
                            value = jQuery(option).val();
                            if (one) {
                                return value;
                            }
                            values.push(value);
                        }
                    }
                    return values;
                },
                set: function(elem, value) {
                    var optionSet, option, options = elem.options, values = jQuery.makeArray(value), i = options.length;
                    while (i--) {
                        option = options[i];
                        if (option.selected = jQuery.inArray(option.value, values) >= 0) {
                            optionSet = true;
                        }
                    }
                    if (!optionSet) {
                        elem.selectedIndex = -1;
                    }
                    return values;
                }
            }
        }
    });
    jQuery.each([ "radio", "checkbox" ], function() {
        jQuery.valHooks[this] = {
            set: function(elem, value) {
                if (jQuery.isArray(value)) {
                    return elem.checked = jQuery.inArray(jQuery(elem).val(), value) >= 0;
                }
            }
        };
        if (!support.checkOn) {
            jQuery.valHooks[this].get = function(elem) {
                return elem.getAttribute("value") === null ? "on" : elem.value;
            };
        }
    });
    jQuery.each(("blur focus focusin focusout load resize scroll unload click dblclick " + "mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " + "change select submit keydown keypress keyup error contextmenu").split(" "), function(i, name) {
        jQuery.fn[name] = function(data, fn) {
            return arguments.length > 0 ? this.on(name, null, data, fn) : this.trigger(name);
        };
    });
    jQuery.fn.extend({
        hover: function(fnOver, fnOut) {
            return this.mouseenter(fnOver).mouseleave(fnOut || fnOver);
        },
        bind: function(types, data, fn) {
            return this.on(types, null, data, fn);
        },
        unbind: function(types, fn) {
            return this.off(types, null, fn);
        },
        delegate: function(selector, types, data, fn) {
            return this.on(types, selector, data, fn);
        },
        undelegate: function(selector, types, fn) {
            return arguments.length === 1 ? this.off(selector, "**") : this.off(types, selector || "**", fn);
        }
    });
    var nonce = jQuery.now();
    var rquery = /\?/;
    jQuery.parseJSON = function(data) {
        return JSON.parse(data + "");
    };
    jQuery.parseXML = function(data) {
        var xml, tmp;
        if (!data || typeof data !== "string") {
            return null;
        }
        try {
            tmp = new DOMParser();
            xml = tmp.parseFromString(data, "text/xml");
        } catch (e) {
            xml = undefined;
        }
        if (!xml || xml.getElementsByTagName("parsererror").length) {
            jQuery.error("Invalid XML: " + data);
        }
        return xml;
    };
    var ajaxLocParts, ajaxLocation, rhash = /#.*$/, rts = /([?&])_=[^&]*/, rheaders = /^(.*?):[ \t]*([^\r\n]*)$/gm, rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, rnoContent = /^(?:GET|HEAD)$/, rprotocol = /^\/\//, rurl = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, prefilters = {}, transports = {}, allTypes = "*/".concat("*");
    try {
        ajaxLocation = location.href;
    } catch (e) {
        ajaxLocation = document.createElement("a");
        ajaxLocation.href = "";
        ajaxLocation = ajaxLocation.href;
    }
    ajaxLocParts = rurl.exec(ajaxLocation.toLowerCase()) || [];
    function addToPrefiltersOrTransports(structure) {
        return function(dataTypeExpression, func) {
            if (typeof dataTypeExpression !== "string") {
                func = dataTypeExpression;
                dataTypeExpression = "*";
            }
            var dataType, i = 0, dataTypes = dataTypeExpression.toLowerCase().match(rnotwhite) || [];
            if (jQuery.isFunction(func)) {
                while (dataType = dataTypes[i++]) {
                    if (dataType[0] === "+") {
                        dataType = dataType.slice(1) || "*";
                        (structure[dataType] = structure[dataType] || []).unshift(func);
                    } else {
                        (structure[dataType] = structure[dataType] || []).push(func);
                    }
                }
            }
        };
    }
    function inspectPrefiltersOrTransports(structure, options, originalOptions, jqXHR) {
        var inspected = {}, seekingTransport = structure === transports;
        function inspect(dataType) {
            var selected;
            inspected[dataType] = true;
            jQuery.each(structure[dataType] || [], function(_, prefilterOrFactory) {
                var dataTypeOrTransport = prefilterOrFactory(options, originalOptions, jqXHR);
                if (typeof dataTypeOrTransport === "string" && !seekingTransport && !inspected[dataTypeOrTransport]) {
                    options.dataTypes.unshift(dataTypeOrTransport);
                    inspect(dataTypeOrTransport);
                    return false;
                } else if (seekingTransport) {
                    return !(selected = dataTypeOrTransport);
                }
            });
            return selected;
        }
        return inspect(options.dataTypes[0]) || !inspected["*"] && inspect("*");
    }
    function ajaxExtend(target, src) {
        var key, deep, flatOptions = jQuery.ajaxSettings.flatOptions || {};
        for (key in src) {
            if (src[key] !== undefined) {
                (flatOptions[key] ? target : deep || (deep = {}))[key] = src[key];
            }
        }
        if (deep) {
            jQuery.extend(true, target, deep);
        }
        return target;
    }
    function ajaxHandleResponses(s, jqXHR, responses) {
        var ct, type, finalDataType, firstDataType, contents = s.contents, dataTypes = s.dataTypes;
        while (dataTypes[0] === "*") {
            dataTypes.shift();
            if (ct === undefined) {
                ct = s.mimeType || jqXHR.getResponseHeader("Content-Type");
            }
        }
        if (ct) {
            for (type in contents) {
                if (contents[type] && contents[type].test(ct)) {
                    dataTypes.unshift(type);
                    break;
                }
            }
        }
        if (dataTypes[0] in responses) {
            finalDataType = dataTypes[0];
        } else {
            for (type in responses) {
                if (!dataTypes[0] || s.converters[type + " " + dataTypes[0]]) {
                    finalDataType = type;
                    break;
                }
                if (!firstDataType) {
                    firstDataType = type;
                }
            }
            finalDataType = finalDataType || firstDataType;
        }
        if (finalDataType) {
            if (finalDataType !== dataTypes[0]) {
                dataTypes.unshift(finalDataType);
            }
            return responses[finalDataType];
        }
    }
    function ajaxConvert(s, response, jqXHR, isSuccess) {
        var conv2, current, conv, tmp, prev, converters = {}, dataTypes = s.dataTypes.slice();
        if (dataTypes[1]) {
            for (conv in s.converters) {
                converters[conv.toLowerCase()] = s.converters[conv];
            }
        }
        current = dataTypes.shift();
        while (current) {
            if (s.responseFields[current]) {
                jqXHR[s.responseFields[current]] = response;
            }
            if (!prev && isSuccess && s.dataFilter) {
                response = s.dataFilter(response, s.dataType);
            }
            prev = current;
            current = dataTypes.shift();
            if (current) {
                if (current === "*") {
                    current = prev;
                } else if (prev !== "*" && prev !== current) {
                    conv = converters[prev + " " + current] || converters["* " + current];
                    if (!conv) {
                        for (conv2 in converters) {
                            tmp = conv2.split(" ");
                            if (tmp[1] === current) {
                                conv = converters[prev + " " + tmp[0]] || converters["* " + tmp[0]];
                                if (conv) {
                                    if (conv === true) {
                                        conv = converters[conv2];
                                    } else if (converters[conv2] !== true) {
                                        current = tmp[0];
                                        dataTypes.unshift(tmp[1]);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    if (conv !== true) {
                        if (conv && s["throws"]) {
                            response = conv(response);
                        } else {
                            try {
                                response = conv(response);
                            } catch (e) {
                                return {
                                    state: "parsererror",
                                    error: conv ? e : "No conversion from " + prev + " to " + current
                                };
                            }
                        }
                    }
                }
            }
        }
        return {
            state: "success",
            data: response
        };
    }
    jQuery.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: ajaxLocation,
            type: "GET",
            isLocal: rlocalProtocol.test(ajaxLocParts[1]),
            global: true,
            processData: true,
            async: true,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": allTypes,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": true,
                "text json": jQuery.parseJSON,
                "text xml": jQuery.parseXML
            },
            flatOptions: {
                url: true,
                context: true
            }
        },
        ajaxSetup: function(target, settings) {
            return settings ? ajaxExtend(ajaxExtend(target, jQuery.ajaxSettings), settings) : ajaxExtend(jQuery.ajaxSettings, target);
        },
        ajaxPrefilter: addToPrefiltersOrTransports(prefilters),
        ajaxTransport: addToPrefiltersOrTransports(transports),
        ajax: function(url, options) {
            if (typeof url === "object") {
                options = url;
                url = undefined;
            }
            options = options || {};
            var transport, cacheURL, responseHeadersString, responseHeaders, timeoutTimer, parts, fireGlobals, i, s = jQuery.ajaxSetup({}, options), callbackContext = s.context || s, globalEventContext = s.context && (callbackContext.nodeType || callbackContext.jquery) ? jQuery(callbackContext) : jQuery.event, deferred = jQuery.Deferred(), completeDeferred = jQuery.Callbacks("once memory"), statusCode = s.statusCode || {}, requestHeaders = {}, requestHeadersNames = {}, state = 0, strAbort = "canceled", jqXHR = {
                readyState: 0,
                getResponseHeader: function(key) {
                    var match;
                    if (state === 2) {
                        if (!responseHeaders) {
                            responseHeaders = {};
                            while (match = rheaders.exec(responseHeadersString)) {
                                responseHeaders[match[1].toLowerCase()] = match[2];
                            }
                        }
                        match = responseHeaders[key.toLowerCase()];
                    }
                    return match == null ? null : match;
                },
                getAllResponseHeaders: function() {
                    return state === 2 ? responseHeadersString : null;
                },
                setRequestHeader: function(name, value) {
                    var lname = name.toLowerCase();
                    if (!state) {
                        name = requestHeadersNames[lname] = requestHeadersNames[lname] || name;
                        requestHeaders[name] = value;
                    }
                    return this;
                },
                overrideMimeType: function(type) {
                    if (!state) {
                        s.mimeType = type;
                    }
                    return this;
                },
                statusCode: function(map) {
                    var code;
                    if (map) {
                        if (state < 2) {
                            for (code in map) {
                                statusCode[code] = [ statusCode[code], map[code] ];
                            }
                        } else {
                            jqXHR.always(map[jqXHR.status]);
                        }
                    }
                    return this;
                },
                abort: function(statusText) {
                    var finalText = statusText || strAbort;
                    if (transport) {
                        transport.abort(finalText);
                    }
                    done(0, finalText);
                    return this;
                }
            };
            deferred.promise(jqXHR).complete = completeDeferred.add;
            jqXHR.success = jqXHR.done;
            jqXHR.error = jqXHR.fail;
            s.url = ((url || s.url || ajaxLocation) + "").replace(rhash, "").replace(rprotocol, ajaxLocParts[1] + "//");
            s.type = options.method || options.type || s.method || s.type;
            s.dataTypes = jQuery.trim(s.dataType || "*").toLowerCase().match(rnotwhite) || [ "" ];
            if (s.crossDomain == null) {
                parts = rurl.exec(s.url.toLowerCase());
                s.crossDomain = !!(parts && (parts[1] !== ajaxLocParts[1] || parts[2] !== ajaxLocParts[2] || (parts[3] || (parts[1] === "http:" ? "80" : "443")) !== (ajaxLocParts[3] || (ajaxLocParts[1] === "http:" ? "80" : "443"))));
            }
            if (s.data && s.processData && typeof s.data !== "string") {
                s.data = jQuery.param(s.data, s.traditional);
            }
            inspectPrefiltersOrTransports(prefilters, s, options, jqXHR);
            if (state === 2) {
                return jqXHR;
            }
            fireGlobals = s.global;
            if (fireGlobals && jQuery.active++ === 0) {
                jQuery.event.trigger("ajaxStart");
            }
            s.type = s.type.toUpperCase();
            s.hasContent = !rnoContent.test(s.type);
            cacheURL = s.url;
            if (!s.hasContent) {
                if (s.data) {
                    cacheURL = s.url += (rquery.test(cacheURL) ? "&" : "?") + s.data;
                    delete s.data;
                }
                if (s.cache === false) {
                    s.url = rts.test(cacheURL) ? cacheURL.replace(rts, "$1_=" + nonce++) : cacheURL + (rquery.test(cacheURL) ? "&" : "?") + "_=" + nonce++;
                }
            }
            if (s.ifModified) {
                if (jQuery.lastModified[cacheURL]) {
                    jqXHR.setRequestHeader("If-Modified-Since", jQuery.lastModified[cacheURL]);
                }
                if (jQuery.etag[cacheURL]) {
                    jqXHR.setRequestHeader("If-None-Match", jQuery.etag[cacheURL]);
                }
            }
            if (s.data && s.hasContent && s.contentType !== false || options.contentType) {
                jqXHR.setRequestHeader("Content-Type", s.contentType);
            }
            jqXHR.setRequestHeader("Accept", s.dataTypes[0] && s.accepts[s.dataTypes[0]] ? s.accepts[s.dataTypes[0]] + (s.dataTypes[0] !== "*" ? ", " + allTypes + "; q=0.01" : "") : s.accepts["*"]);
            for (i in s.headers) {
                jqXHR.setRequestHeader(i, s.headers[i]);
            }
            if (s.beforeSend && (s.beforeSend.call(callbackContext, jqXHR, s) === false || state === 2)) {
                return jqXHR.abort();
            }
            strAbort = "abort";
            for (i in {
                success: 1,
                error: 1,
                complete: 1
            }) {
                jqXHR[i](s[i]);
            }
            transport = inspectPrefiltersOrTransports(transports, s, options, jqXHR);
            if (!transport) {
                done(-1, "No Transport");
            } else {
                jqXHR.readyState = 1;
                if (fireGlobals) {
                    globalEventContext.trigger("ajaxSend", [ jqXHR, s ]);
                }
                if (s.async && s.timeout > 0) {
                    timeoutTimer = setTimeout(function() {
                        jqXHR.abort("timeout");
                    }, s.timeout);
                }
                try {
                    state = 1;
                    transport.send(requestHeaders, done);
                } catch (e) {
                    if (state < 2) {
                        done(-1, e);
                    } else {
                        throw e;
                    }
                }
            }
            function done(status, nativeStatusText, responses, headers) {
                var isSuccess, success, error, response, modified, statusText = nativeStatusText;
                if (state === 2) {
                    return;
                }
                state = 2;
                if (timeoutTimer) {
                    clearTimeout(timeoutTimer);
                }
                transport = undefined;
                responseHeadersString = headers || "";
                jqXHR.readyState = status > 0 ? 4 : 0;
                isSuccess = status >= 200 && status < 300 || status === 304;
                if (responses) {
                    response = ajaxHandleResponses(s, jqXHR, responses);
                }
                response = ajaxConvert(s, response, jqXHR, isSuccess);
                if (isSuccess) {
                    if (s.ifModified) {
                        modified = jqXHR.getResponseHeader("Last-Modified");
                        if (modified) {
                            jQuery.lastModified[cacheURL] = modified;
                        }
                        modified = jqXHR.getResponseHeader("etag");
                        if (modified) {
                            jQuery.etag[cacheURL] = modified;
                        }
                    }
                    if (status === 204 || s.type === "HEAD") {
                        statusText = "nocontent";
                    } else if (status === 304) {
                        statusText = "notmodified";
                    } else {
                        statusText = response.state;
                        success = response.data;
                        error = response.error;
                        isSuccess = !error;
                    }
                } else {
                    error = statusText;
                    if (status || !statusText) {
                        statusText = "error";
                        if (status < 0) {
                            status = 0;
                        }
                    }
                }
                jqXHR.status = status;
                jqXHR.statusText = (nativeStatusText || statusText) + "";
                if (isSuccess) {
                    deferred.resolveWith(callbackContext, [ success, statusText, jqXHR ]);
                } else {
                    deferred.rejectWith(callbackContext, [ jqXHR, statusText, error ]);
                }
                jqXHR.statusCode(statusCode);
                statusCode = undefined;
                if (fireGlobals) {
                    globalEventContext.trigger(isSuccess ? "ajaxSuccess" : "ajaxError", [ jqXHR, s, isSuccess ? success : error ]);
                }
                completeDeferred.fireWith(callbackContext, [ jqXHR, statusText ]);
                if (fireGlobals) {
                    globalEventContext.trigger("ajaxComplete", [ jqXHR, s ]);
                    if (!--jQuery.active) {
                        jQuery.event.trigger("ajaxStop");
                    }
                }
            }
            return jqXHR;
        },
        getJSON: function(url, data, callback) {
            return jQuery.get(url, data, callback, "json");
        },
        getScript: function(url, callback) {
            return jQuery.get(url, undefined, callback, "script");
        }
    });
    jQuery.each([ "get", "post" ], function(i, method) {
        jQuery[method] = function(url, data, callback, type) {
            if (jQuery.isFunction(data)) {
                type = type || callback;
                callback = data;
                data = undefined;
            }
            return jQuery.ajax({
                url: url,
                type: method,
                dataType: type,
                data: data,
                success: callback
            });
        };
    });
    jQuery.each([ "ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend" ], function(i, type) {
        jQuery.fn[type] = function(fn) {
            return this.on(type, fn);
        };
    });
    jQuery._evalUrl = function(url) {
        return jQuery.ajax({
            url: url,
            type: "GET",
            dataType: "script",
            async: false,
            global: false,
            "throws": true
        });
    };
    jQuery.fn.extend({
        wrapAll: function(html) {
            var wrap;
            if (jQuery.isFunction(html)) {
                return this.each(function(i) {
                    jQuery(this).wrapAll(html.call(this, i));
                });
            }
            if (this[0]) {
                wrap = jQuery(html, this[0].ownerDocument).eq(0).clone(true);
                if (this[0].parentNode) {
                    wrap.insertBefore(this[0]);
                }
                wrap.map(function() {
                    var elem = this;
                    while (elem.firstElementChild) {
                        elem = elem.firstElementChild;
                    }
                    return elem;
                }).append(this);
            }
            return this;
        },
        wrapInner: function(html) {
            if (jQuery.isFunction(html)) {
                return this.each(function(i) {
                    jQuery(this).wrapInner(html.call(this, i));
                });
            }
            return this.each(function() {
                var self = jQuery(this), contents = self.contents();
                if (contents.length) {
                    contents.wrapAll(html);
                } else {
                    self.append(html);
                }
            });
        },
        wrap: function(html) {
            var isFunction = jQuery.isFunction(html);
            return this.each(function(i) {
                jQuery(this).wrapAll(isFunction ? html.call(this, i) : html);
            });
        },
        unwrap: function() {
            return this.parent().each(function() {
                if (!jQuery.nodeName(this, "body")) {
                    jQuery(this).replaceWith(this.childNodes);
                }
            }).end();
        }
    });
    jQuery.expr.filters.hidden = function(elem) {
        return elem.offsetWidth <= 0 && elem.offsetHeight <= 0;
    };
    jQuery.expr.filters.visible = function(elem) {
        return !jQuery.expr.filters.hidden(elem);
    };
    var r20 = /%20/g, rbracket = /\[\]$/, rCRLF = /\r?\n/g, rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i, rsubmittable = /^(?:input|select|textarea|keygen)/i;
    function buildParams(prefix, obj, traditional, add) {
        var name;
        if (jQuery.isArray(obj)) {
            jQuery.each(obj, function(i, v) {
                if (traditional || rbracket.test(prefix)) {
                    add(prefix, v);
                } else {
                    buildParams(prefix + "[" + (typeof v === "object" ? i : "") + "]", v, traditional, add);
                }
            });
        } else if (!traditional && jQuery.type(obj) === "object") {
            for (name in obj) {
                buildParams(prefix + "[" + name + "]", obj[name], traditional, add);
            }
        } else {
            add(prefix, obj);
        }
    }
    jQuery.param = function(a, traditional) {
        var prefix, s = [], add = function(key, value) {
            value = jQuery.isFunction(value) ? value() : value == null ? "" : value;
            s[s.length] = encodeURIComponent(key) + "=" + encodeURIComponent(value);
        };
        if (traditional === undefined) {
            traditional = jQuery.ajaxSettings && jQuery.ajaxSettings.traditional;
        }
        if (jQuery.isArray(a) || a.jquery && !jQuery.isPlainObject(a)) {
            jQuery.each(a, function() {
                add(this.name, this.value);
            });
        } else {
            for (prefix in a) {
                buildParams(prefix, a[prefix], traditional, add);
            }
        }
        return s.join("&").replace(r20, "+");
    };
    jQuery.fn.extend({
        serialize: function() {
            return jQuery.param(this.serializeArray());
        },
        serializeArray: function() {
            return this.map(function() {
                var elements = jQuery.prop(this, "elements");
                return elements ? jQuery.makeArray(elements) : this;
            }).filter(function() {
                var type = this.type;
                return this.name && !jQuery(this).is(":disabled") && rsubmittable.test(this.nodeName) && !rsubmitterTypes.test(type) && (this.checked || !rcheckableType.test(type));
            }).map(function(i, elem) {
                var val = jQuery(this).val();
                return val == null ? null : jQuery.isArray(val) ? jQuery.map(val, function(val) {
                    return {
                        name: elem.name,
                        value: val.replace(rCRLF, "\r\n")
                    };
                }) : {
                    name: elem.name,
                    value: val.replace(rCRLF, "\r\n")
                };
            }).get();
        }
    });
    jQuery.ajaxSettings.xhr = function() {
        try {
            return new XMLHttpRequest();
        } catch (e) {}
    };
    var xhrId = 0, xhrCallbacks = {}, xhrSuccessStatus = {
        0: 200,
        1223: 204
    }, xhrSupported = jQuery.ajaxSettings.xhr();
    if (window.ActiveXObject) {
        jQuery(window).on("unload", function() {
            for (var key in xhrCallbacks) {
                xhrCallbacks[key]();
            }
        });
    }
    support.cors = !!xhrSupported && "withCredentials" in xhrSupported;
    support.ajax = xhrSupported = !!xhrSupported;
    jQuery.ajaxTransport(function(options) {
        var callback;
        if (support.cors || xhrSupported && !options.crossDomain) {
            return {
                send: function(headers, complete) {
                    var i, xhr = options.xhr(), id = ++xhrId;
                    xhr.open(options.type, options.url, options.async, options.username, options.password);
                    if (options.xhrFields) {
                        for (i in options.xhrFields) {
                            xhr[i] = options.xhrFields[i];
                        }
                    }
                    if (options.mimeType && xhr.overrideMimeType) {
                        xhr.overrideMimeType(options.mimeType);
                    }
                    if (!options.crossDomain && !headers["X-Requested-With"]) {
                        headers["X-Requested-With"] = "XMLHttpRequest";
                    }
                    for (i in headers) {
                        xhr.setRequestHeader(i, headers[i]);
                    }
                    callback = function(type) {
                        return function() {
                            if (callback) {
                                delete xhrCallbacks[id];
                                callback = xhr.onload = xhr.onerror = null;
                                if (type === "abort") {
                                    xhr.abort();
                                } else if (type === "error") {
                                    complete(xhr.status, xhr.statusText);
                                } else {
                                    complete(xhrSuccessStatus[xhr.status] || xhr.status, xhr.statusText, typeof xhr.responseText === "string" ? {
                                        text: xhr.responseText
                                    } : undefined, xhr.getAllResponseHeaders());
                                }
                            }
                        };
                    };
                    xhr.onload = callback();
                    xhr.onerror = callback("error");
                    callback = xhrCallbacks[id] = callback("abort");
                    try {
                        xhr.send(options.hasContent && options.data || null);
                    } catch (e) {
                        if (callback) {
                            throw e;
                        }
                    }
                },
                abort: function() {
                    if (callback) {
                        callback();
                    }
                }
            };
        }
    });
    jQuery.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /(?:java|ecma)script/
        },
        converters: {
            "text script": function(text) {
                jQuery.globalEval(text);
                return text;
            }
        }
    });
    jQuery.ajaxPrefilter("script", function(s) {
        if (s.cache === undefined) {
            s.cache = false;
        }
        if (s.crossDomain) {
            s.type = "GET";
        }
    });
    jQuery.ajaxTransport("script", function(s) {
        if (s.crossDomain) {
            var script, callback;
            return {
                send: function(_, complete) {
                    script = jQuery("<script>").prop({
                        async: true,
                        charset: s.scriptCharset,
                        src: s.url
                    }).on("load error", callback = function(evt) {
                        script.remove();
                        callback = null;
                        if (evt) {
                            complete(evt.type === "error" ? 404 : 200, evt.type);
                        }
                    });
                    document.head.appendChild(script[0]);
                },
                abort: function() {
                    if (callback) {
                        callback();
                    }
                }
            };
        }
    });
    var oldCallbacks = [], rjsonp = /(=)\?(?=&|$)|\?\?/;
    jQuery.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var callback = oldCallbacks.pop() || jQuery.expando + "_" + nonce++;
            this[callback] = true;
            return callback;
        }
    });
    jQuery.ajaxPrefilter("json jsonp", function(s, originalSettings, jqXHR) {
        var callbackName, overwritten, responseContainer, jsonProp = s.jsonp !== false && (rjsonp.test(s.url) ? "url" : typeof s.data === "string" && !(s.contentType || "").indexOf("application/x-www-form-urlencoded") && rjsonp.test(s.data) && "data");
        if (jsonProp || s.dataTypes[0] === "jsonp") {
            callbackName = s.jsonpCallback = jQuery.isFunction(s.jsonpCallback) ? s.jsonpCallback() : s.jsonpCallback;
            if (jsonProp) {
                s[jsonProp] = s[jsonProp].replace(rjsonp, "$1" + callbackName);
            } else if (s.jsonp !== false) {
                s.url += (rquery.test(s.url) ? "&" : "?") + s.jsonp + "=" + callbackName;
            }
            s.converters["script json"] = function() {
                if (!responseContainer) {
                    jQuery.error(callbackName + " was not called");
                }
                return responseContainer[0];
            };
            s.dataTypes[0] = "json";
            overwritten = window[callbackName];
            window[callbackName] = function() {
                responseContainer = arguments;
            };
            jqXHR.always(function() {
                window[callbackName] = overwritten;
                if (s[callbackName]) {
                    s.jsonpCallback = originalSettings.jsonpCallback;
                    oldCallbacks.push(callbackName);
                }
                if (responseContainer && jQuery.isFunction(overwritten)) {
                    overwritten(responseContainer[0]);
                }
                responseContainer = overwritten = undefined;
            });
            return "script";
        }
    });
    jQuery.parseHTML = function(data, context, keepScripts) {
        if (!data || typeof data !== "string") {
            return null;
        }
        if (typeof context === "boolean") {
            keepScripts = context;
            context = false;
        }
        context = context || document;
        var parsed = rsingleTag.exec(data), scripts = !keepScripts && [];
        if (parsed) {
            return [ context.createElement(parsed[1]) ];
        }
        parsed = jQuery.buildFragment([ data ], context, scripts);
        if (scripts && scripts.length) {
            jQuery(scripts).remove();
        }
        return jQuery.merge([], parsed.childNodes);
    };
    var _load = jQuery.fn.load;
    jQuery.fn.load = function(url, params, callback) {
        if (typeof url !== "string" && _load) {
            return _load.apply(this, arguments);
        }
        var selector, type, response, self = this, off = url.indexOf(" ");
        if (off >= 0) {
            selector = jQuery.trim(url.slice(off));
            url = url.slice(0, off);
        }
        if (jQuery.isFunction(params)) {
            callback = params;
            params = undefined;
        } else if (params && typeof params === "object") {
            type = "POST";
        }
        if (self.length > 0) {
            jQuery.ajax({
                url: url,
                type: type,
                dataType: "html",
                data: params
            }).done(function(responseText) {
                response = arguments;
                self.html(selector ? jQuery("<div>").append(jQuery.parseHTML(responseText)).find(selector) : responseText);
            }).complete(callback && function(jqXHR, status) {
                self.each(callback, response || [ jqXHR.responseText, status, jqXHR ]);
            });
        }
        return this;
    };
    jQuery.expr.filters.animated = function(elem) {
        return jQuery.grep(jQuery.timers, function(fn) {
            return elem === fn.elem;
        }).length;
    };
    var docElem = window.document.documentElement;
    function getWindow(elem) {
        return jQuery.isWindow(elem) ? elem : elem.nodeType === 9 && elem.defaultView;
    }
    jQuery.offset = {
        setOffset: function(elem, options, i) {
            var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition, position = jQuery.css(elem, "position"), curElem = jQuery(elem), props = {};
            if (position === "static") {
                elem.style.position = "relative";
            }
            curOffset = curElem.offset();
            curCSSTop = jQuery.css(elem, "top");
            curCSSLeft = jQuery.css(elem, "left");
            calculatePosition = (position === "absolute" || position === "fixed") && (curCSSTop + curCSSLeft).indexOf("auto") > -1;
            if (calculatePosition) {
                curPosition = curElem.position();
                curTop = curPosition.top;
                curLeft = curPosition.left;
            } else {
                curTop = parseFloat(curCSSTop) || 0;
                curLeft = parseFloat(curCSSLeft) || 0;
            }
            if (jQuery.isFunction(options)) {
                options = options.call(elem, i, curOffset);
            }
            if (options.top != null) {
                props.top = options.top - curOffset.top + curTop;
            }
            if (options.left != null) {
                props.left = options.left - curOffset.left + curLeft;
            }
            if ("using" in options) {
                options.using.call(elem, props);
            } else {
                curElem.css(props);
            }
        }
    };
    jQuery.fn.extend({
        offset: function(options) {
            if (arguments.length) {
                return options === undefined ? this : this.each(function(i) {
                    jQuery.offset.setOffset(this, options, i);
                });
            }
            var docElem, win, elem = this[0], box = {
                top: 0,
                left: 0
            }, doc = elem && elem.ownerDocument;
            if (!doc) {
                return;
            }
            docElem = doc.documentElement;
            if (!jQuery.contains(docElem, elem)) {
                return box;
            }
            if (typeof elem.getBoundingClientRect !== strundefined) {
                box = elem.getBoundingClientRect();
            }
            win = getWindow(doc);
            return {
                top: box.top + win.pageYOffset - docElem.clientTop,
                left: box.left + win.pageXOffset - docElem.clientLeft
            };
        },
        position: function() {
            if (!this[0]) {
                return;
            }
            var offsetParent, offset, elem = this[0], parentOffset = {
                top: 0,
                left: 0
            };
            if (jQuery.css(elem, "position") === "fixed") {
                offset = elem.getBoundingClientRect();
            } else {
                offsetParent = this.offsetParent();
                offset = this.offset();
                if (!jQuery.nodeName(offsetParent[0], "html")) {
                    parentOffset = offsetParent.offset();
                }
                parentOffset.top += jQuery.css(offsetParent[0], "borderTopWidth", true);
                parentOffset.left += jQuery.css(offsetParent[0], "borderLeftWidth", true);
            }
            return {
                top: offset.top - parentOffset.top - jQuery.css(elem, "marginTop", true),
                left: offset.left - parentOffset.left - jQuery.css(elem, "marginLeft", true)
            };
        },
        offsetParent: function() {
            return this.map(function() {
                var offsetParent = this.offsetParent || docElem;
                while (offsetParent && (!jQuery.nodeName(offsetParent, "html") && jQuery.css(offsetParent, "position") === "static")) {
                    offsetParent = offsetParent.offsetParent;
                }
                return offsetParent || docElem;
            });
        }
    });
    jQuery.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(method, prop) {
        var top = "pageYOffset" === prop;
        jQuery.fn[method] = function(val) {
            return access(this, function(elem, method, val) {
                var win = getWindow(elem);
                if (val === undefined) {
                    return win ? win[prop] : elem[method];
                }
                if (win) {
                    win.scrollTo(!top ? val : window.pageXOffset, top ? val : window.pageYOffset);
                } else {
                    elem[method] = val;
                }
            }, method, val, arguments.length, null);
        };
    });
    jQuery.each([ "top", "left" ], function(i, prop) {
        jQuery.cssHooks[prop] = addGetHookIf(support.pixelPosition, function(elem, computed) {
            if (computed) {
                computed = curCSS(elem, prop);
                return rnumnonpx.test(computed) ? jQuery(elem).position()[prop] + "px" : computed;
            }
        });
    });
    jQuery.each({
        Height: "height",
        Width: "width"
    }, function(name, type) {
        jQuery.each({
            padding: "inner" + name,
            content: type,
            "": "outer" + name
        }, function(defaultExtra, funcName) {
            jQuery.fn[funcName] = function(margin, value) {
                var chainable = arguments.length && (defaultExtra || typeof margin !== "boolean"), extra = defaultExtra || (margin === true || value === true ? "margin" : "border");
                return access(this, function(elem, type, value) {
                    var doc;
                    if (jQuery.isWindow(elem)) {
                        return elem.document.documentElement["client" + name];
                    }
                    if (elem.nodeType === 9) {
                        doc = elem.documentElement;
                        return Math.max(elem.body["scroll" + name], doc["scroll" + name], elem.body["offset" + name], doc["offset" + name], doc["client" + name]);
                    }
                    return value === undefined ? jQuery.css(elem, type, extra) : jQuery.style(elem, type, value, extra);
                }, type, chainable ? margin : undefined, chainable, null);
            };
        });
    });
    jQuery.fn.size = function() {
        return this.length;
    };
    jQuery.fn.andSelf = jQuery.fn.addBack;
    if (typeof define === "function" && define.amd) {
        define("jquery", [], function() {
            return jQuery;
        });
    }
    var _jQuery = window.jQuery, _$ = window.$;
    jQuery.noConflict = function(deep) {
        if (window.$ === jQuery) {
            window.$ = _$;
        }
        if (deep && window.jQuery === jQuery) {
            window.jQuery = _jQuery;
        }
        return jQuery;
    };
    if (typeof noGlobal === strundefined) {
        window.jQuery = window.$ = jQuery;
    }
    return jQuery;
});

(function(a) {
    function k(a, b, c, d) {
        var e = 0, f = 0, g = 0, c = (c || "(").toString(), d = (d || ")").toString(), h;
        for (h = 0; h < a.length; h++) {
            var i = a[h];
            if (i.indexOf(c, e) > i.indexOf(d, e) || ~i.indexOf(c, e) && !~i.indexOf(d, e) || !~i.indexOf(c, e) && ~i.indexOf(d, e)) {
                f = i.indexOf(c, e), g = i.indexOf(d, e);
                if (~f && !~g || !~f && ~g) {
                    var j = a.slice(0, (h || 1) + 1).join(b);
                    a = [ j ].concat(a.slice((h || 1) + 1));
                }
                e = (g > f ? g : f) + 1, h = 0;
            } else e = 0;
        }
        return a;
    }
    function j(a, b) {
        var c, d = 0, e = "";
        while (c = a.substr(d).match(/[^\w\d\- %@&]*\*[^\w\d\- %@&]*/)) d = c.index + c[0].length, 
        c[0] = c[0].replace(/^\*/, "([_.()!\\ %@&a-zA-Z0-9-]+)"), e += a.substr(0, c.index) + c[0];
        a = e += a.substr(d);
        var f = a.match(/:([^\/]+)/gi), g, h;
        if (f) {
            h = f.length;
            for (var j = 0; j < h; j++) g = f[j], g.slice(0, 2) === "::" ? a = g.slice(1) : a = a.replace(g, i(g, b));
        }
        return a;
    }
    function i(a, b, c) {
        c = a;
        for (var d in b) if (b.hasOwnProperty(d)) {
            c = b[d](a);
            if (c !== a) break;
        }
        return c === a ? "([._a-zA-Z0-9-]+)" : c;
    }
    function h(a, b, c) {
        if (!a.length) return c();
        var d = 0;
        (function e() {
            b(a[d], function(b) {
                b || b === !1 ? (c(b), c = function() {}) : (d += 1, d === a.length ? c() : e());
            });
        })();
    }
    function g(a) {
        var b = [];
        for (var c = 0, d = a.length; c < d; c++) b = b.concat(a[c]);
        return b;
    }
    function f(a, b) {
        for (var c = 0; c < a.length; c += 1) if (b(a[c], c, a) === !1) return;
    }
    function c() {
        return b.hash === "" || b.hash === "#";
    }
    Array.prototype.filter || (Array.prototype.filter = function(a, b) {
        var c = [], d;
        for (var e = 0, f = this.length; e < f; e++) e in this && a.call(b, d = this[e], e, this) && c.push(d);
        return c;
    }), Array.isArray || (Array.isArray = function(a) {
        return Object.prototype.toString.call(a) === "[object Array]";
    });
    var b = document.location, d = {
        mode: "modern",
        hash: b.hash,
        history: !1,
        check: function() {
            var a = b.hash;
            a != this.hash && (this.hash = a, this.onHashChanged());
        },
        fire: function() {
            this.mode === "modern" ? this.history === !0 ? window.onpopstate() : window.onhashchange() : this.onHashChanged();
        },
        init: function(a, b) {
            function d(a) {
                for (var b = 0, c = e.listeners.length; b < c; b++) e.listeners[b](a);
            }
            var c = this;
            this.history = b, e.listeners || (e.listeners = []);
            if ("onhashchange" in window && (document.documentMode === undefined || document.documentMode > 7)) this.history === !0 ? setTimeout(function() {
                window.onpopstate = d;
            }, 500) : window.onhashchange = d, this.mode = "modern"; else {
                var f = document.createElement("iframe");
                f.id = "state-frame", f.style.display = "none", document.body.appendChild(f), this.writeFrame(""), 
                "onpropertychange" in document && "attachEvent" in document && document.attachEvent("onpropertychange", function() {
                    event.propertyName === "location" && c.check();
                }), window.setInterval(function() {
                    c.check();
                }, 50), this.onHashChanged = d, this.mode = "legacy";
            }
            e.listeners.push(a);
            return this.mode;
        },
        destroy: function(a) {
            if (!!e && !!e.listeners) {
                var b = e.listeners;
                for (var c = b.length - 1; c >= 0; c--) b[c] === a && b.splice(c, 1);
            }
        },
        setHash: function(a) {
            this.mode === "legacy" && this.writeFrame(a), this.history === !0 ? (window.history.pushState({}, document.title, a), 
            this.fire()) : b.hash = a[0] === "/" ? a : "/" + a;
            return this;
        },
        writeFrame: function(a) {
            var b = document.getElementById("state-frame"), c = b.contentDocument || b.contentWindow.document;
            c.open(), c.write("<script>_hash = '" + a + "'; onload = parent.listener.syncHash;<script>"), 
            c.close();
        },
        syncHash: function() {
            var a = this._hash;
            a != b.hash && (b.hash = a);
            return this;
        },
        onHashChanged: function() {}
    }, e = a.Router = function(a) {
        if (this instanceof e) this.params = {}, this.routes = {}, this.methods = [ "on", "once", "after", "before" ], 
        this.scope = [], this._methods = {}, this._insert = this.insert, this.insert = this.insertEx, 
        this.historySupport = (window.history != null ? window.history.pushState : null) != null, 
        this.configure(), this.mount(a || {}); else return new e(a);
    };
    e.prototype.init = function(a) {
        var e = this;
        this.handler = function(a) {
            var b = a && a.newURL || window.location.hash, c = e.history === !0 ? e.getPath() : b.replace(/.*#/, "");
            e.dispatch("on", c.charAt(0) === "/" ? c : "/" + c);
        }, d.init(this.handler, this.history);
        if (this.history === !1) c() && a ? b.hash = a : c() || e.dispatch("on", "/" + b.hash.replace(/^(#\/|#|\/)/, "")); else {
            var f = c() && a ? a : c() ? null : b.hash.replace(/^#/, "");
            f && window.history.replaceState({}, document.title, f), (f || this.run_in_init === !0) && this.handler();
        }
        return this;
    }, e.prototype.explode = function() {
        var a = this.history === !0 ? this.getPath() : b.hash;
        a.charAt(1) === "/" && (a = a.slice(1));
        return a.slice(1, a.length).split("/");
    }, e.prototype.setRoute = function(a, b, c) {
        var e = this.explode();
        typeof a == "number" && typeof b == "string" ? e[a] = b : typeof c == "string" ? e.splice(a, b, s) : e = [ a ], 
        d.setHash(e.join("/"));
        return e;
    }, e.prototype.insertEx = function(a, b, c, d) {
        a === "once" && (a = "on", c = function(a) {
            var b = !1;
            return function() {
                if (!b) {
                    b = !0;
                    return a.apply(this, arguments);
                }
            };
        }(c));
        return this._insert(a, b, c, d);
    }, e.prototype.getRoute = function(a) {
        var b = a;
        if (typeof a == "number") b = this.explode()[a]; else if (typeof a == "string") {
            var c = this.explode();
            b = c.indexOf(a);
        } else b = this.explode();
        return b;
    }, e.prototype.destroy = function() {
        d.destroy(this.handler);
        return this;
    }, e.prototype.getPath = function() {
        var a = window.location.pathname;
        a.substr(0, 1) !== "/" && (a = "/" + a);
        return a;
    }, e.prototype.configure = function(a) {
        a = a || {};
        for (var b = 0; b < this.methods.length; b++) this._methods[this.methods[b]] = !0;
        this.recurse = a.recurse || this.recurse || !1, this.async = a.async || !1, this.delimiter = a.delimiter || "/", 
        this.strict = typeof a.strict == "undefined" ? !0 : a.strict, this.notfound = a.notfound, 
        this.resource = a.resource, this.history = a.html5history && this.historySupport || !1, 
        this.run_in_init = this.history === !0 && a.run_handler_in_init !== !1, this.every = {
            after: a.after || null,
            before: a.before || null,
            on: a.on || null
        };
        return this;
    }, e.prototype.param = function(a, b) {
        a[0] !== ":" && (a = ":" + a);
        var c = new RegExp(a, "g");
        this.params[a] = function(a) {
            return a.replace(c, b.source || b);
        };
    }, e.prototype.on = e.prototype.route = function(a, b, c) {
        var d = this;
        !c && typeof b == "function" && (c = b, b = a, a = "on");
        if (Array.isArray(b)) return b.forEach(function(b) {
            d.on(a, b, c);
        });
        b.source && (b = b.source.replace(/\\\//gi, "/"));
        if (Array.isArray(a)) return a.forEach(function(a) {
            d.on(a.toLowerCase(), b, c);
        });
        b = b.split(new RegExp(this.delimiter)), b = k(b, this.delimiter), this.insert(a, this.scope.concat(b), c);
    }, e.prototype.dispatch = function(a, b, c) {
        function h() {
            d.last = e.after, d.invoke(d.runlist(e), d, c);
        }
        var d = this, e = this.traverse(a, b, this.routes, ""), f = this._invoked, g;
        this._invoked = !0;
        if (!e || e.length === 0) {
            this.last = [], typeof this.notfound == "function" && this.invoke([ this.notfound ], {
                method: a,
                path: b
            }, c);
            return !1;
        }
        this.recurse === "forward" && (e = e.reverse()), g = this.every && this.every.after ? [ this.every.after ].concat(this.last) : [ this.last ];
        if (g && g.length > 0 && f) {
            this.async ? this.invoke(g, this, h) : (this.invoke(g, this), h());
            return !0;
        }
        h();
        return !0;
    }, e.prototype.invoke = function(a, b, c) {
        var d = this, e;
        this.async ? (e = function(c, d) {
            if (Array.isArray(c)) return h(c, e, d);
            typeof c == "function" && c.apply(b, a.captures.concat(d));
        }, h(a, e, function() {
            c && c.apply(b, arguments);
        })) : (e = function(c) {
            if (Array.isArray(c)) return f(c, e);
            if (typeof c == "function") return c.apply(b, a.captures || []);
            typeof c == "string" && d.resource && d.resource[c].apply(b, a.captures || []);
        }, f(a, e));
    }, e.prototype.traverse = function(a, b, c, d, e) {
        function l(a) {
            function c(a) {
                for (var b = a.length - 1; b >= 0; b--) Array.isArray(a[b]) ? (c(a[b]), a[b].length === 0 && a.splice(b, 1)) : e(a[b]) || a.splice(b, 1);
            }
            function b(a) {
                var c = [];
                for (var d = 0; d < a.length; d++) c[d] = Array.isArray(a[d]) ? b(a[d]) : a[d];
                return c;
            }
            if (!e) return a;
            var d = b(a);
            d.matched = a.matched, d.captures = a.captures, d.after = a.after.filter(e), c(d);
            return d;
        }
        var f = [], g, h, i, j, k;
        if (b === this.delimiter && c[a]) {
            j = [ [ c.before, c[a] ].filter(Boolean) ], j.after = [ c.after ].filter(Boolean), 
            j.matched = !0, j.captures = [];
            return l(j);
        }
        for (var m in c) if (c.hasOwnProperty(m) && (!this._methods[m] || this._methods[m] && typeof c[m] == "object" && !Array.isArray(c[m]))) {
            g = h = d + this.delimiter + m, this.strict || (h += "[" + this.delimiter + "]?"), 
            i = b.match(new RegExp("^" + h));
            if (!i) continue;
            if (i[0] && i[0] == b && c[m][a]) {
                j = [ [ c[m].before, c[m][a] ].filter(Boolean) ], j.after = [ c[m].after ].filter(Boolean), 
                j.matched = !0, j.captures = i.slice(1), this.recurse && c === this.routes && (j.push([ c.before, c.on ].filter(Boolean)), 
                j.after = j.after.concat([ c.after ].filter(Boolean)));
                return l(j);
            }
            j = this.traverse(a, b, c[m], g);
            if (j.matched) {
                j.length > 0 && (f = f.concat(j)), this.recurse && (f.push([ c[m].before, c[m].on ].filter(Boolean)), 
                j.after = j.after.concat([ c[m].after ].filter(Boolean)), c === this.routes && (f.push([ c.before, c.on ].filter(Boolean)), 
                j.after = j.after.concat([ c.after ].filter(Boolean)))), f.matched = !0, f.captures = j.captures, 
                f.after = j.after;
                return l(f);
            }
        }
        return !1;
    }, e.prototype.insert = function(a, b, c, d) {
        var e, f, g, h, i;
        b = b.filter(function(a) {
            return a && a.length > 0;
        }), d = d || this.routes, i = b.shift(), /\:|\*/.test(i) && !/\\d|\\w/.test(i) && (i = j(i, this.params));
        if (b.length > 0) {
            d[i] = d[i] || {};
            return this.insert(a, b, c, d[i]);
        }
        {
            if (!!i || !!b.length || d !== this.routes) {
                f = typeof d[i], g = Array.isArray(d[i]);
                if (d[i] && !g && f == "object") {
                    e = typeof d[i][a];
                    switch (e) {
                      case "function":
                        d[i][a] = [ d[i][a], c ];
                        return;

                      case "object":
                        d[i][a].push(c);
                        return;

                      case "undefined":
                        d[i][a] = c;
                        return;
                    }
                } else if (f == "undefined") {
                    h = {}, h[a] = c, d[i] = h;
                    return;
                }
                throw new Error("Invalid route context: " + f);
            }
            e = typeof d[a];
            switch (e) {
              case "function":
                d[a] = [ d[a], c ];
                return;

              case "object":
                d[a].push(c);
                return;

              case "undefined":
                d[a] = c;
                return;
            }
        }
    }, e.prototype.extend = function(a) {
        function e(a) {
            b._methods[a] = !0, b[a] = function() {
                var c = arguments.length === 1 ? [ a, "" ] : [ a ];
                b.on.apply(b, c.concat(Array.prototype.slice.call(arguments)));
            };
        }
        var b = this, c = a.length, d;
        for (d = 0; d < c; d++) e(a[d]);
    }, e.prototype.runlist = function(a) {
        var b = this.every && this.every.before ? [ this.every.before ].concat(g(a)) : g(a);
        this.every && this.every.on && b.push(this.every.on), b.captures = a.captures, b.source = a.source;
        return b;
    }, e.prototype.mount = function(a, b) {
        function d(b, d) {
            var e = b, f = b.split(c.delimiter), g = typeof a[b], h = f[0] === "" || !c._methods[f[0]], i = h ? "on" : e;
            h && (e = e.slice((e.match(new RegExp("^" + c.delimiter)) || [ "" ])[0].length), 
            f.shift());
            h && g === "object" && !Array.isArray(a[b]) ? (d = d.concat(f), c.mount(a[b], d)) : (h && (d = d.concat(e.split(c.delimiter)), 
            d = k(d, c.delimiter)), c.insert(i, d, a[b]));
        }
        if (!!a && typeof a == "object" && !Array.isArray(a)) {
            var c = this;
            b = b || [], Array.isArray(b) || (b = b.split(c.delimiter));
            for (var e in a) a.hasOwnProperty(e) && d(e, b.slice(0));
        }
    };
})(typeof exports == "object" ? exports : window);

function require(a) {
    return window.less[a.split("/")[1]];
}

function log(a, b) {
    "development" == less.env && "undefined" != typeof console && less.logLevel >= b && console.log("less: " + a);
}

function extractId(a) {
    return a.replace(/^[a-z-]+:\/+?[^\/]+/, "").replace(/^\//, "").replace(/\.[a-zA-Z]+$/, "").replace(/[^\.\w-]+/g, "-").replace(/\./g, ":");
}

function errorConsole(a, b) {
    var c = "{line} {content}", d = a.filename || b, e = [], f = (a.type || "Syntax") + "Error: " + (a.message || "There is an error in your .less file") + " in " + d + " ", g = function(a, b, d) {
        void 0 !== a.extract[b] && e.push(c.replace(/\{line\}/, (parseInt(a.line, 10) || 0) + (b - 1)).replace(/\{class\}/, d).replace(/\{content\}/, a.extract[b]));
    };
    a.extract ? (g(a, 0, ""), g(a, 1, "line"), g(a, 2, ""), f += "on line " + a.line + ", column " + (a.column + 1) + ":\n" + e.join("\n")) : a.stack && (f += a.stack), 
    log(f, logLevel.errors);
}

function createCSS(a, b, c) {
    var d = b.href || "", e = "less:" + (b.title || extractId(d)), f = document.getElementById(e), g = !1, h = document.createElement("style");
    if (h.setAttribute("type", "text/css"), b.media && h.setAttribute("media", b.media), 
    h.id = e, h.styleSheet) try {
        h.styleSheet.cssText = a;
    } catch (i) {
        throw new Error("Couldn't reassign styleSheet.cssText.");
    } else h.appendChild(document.createTextNode(a)), g = null !== f && f.childNodes.length > 0 && h.childNodes.length > 0 && f.firstChild.nodeValue === h.firstChild.nodeValue;
    var j = document.getElementsByTagName("head")[0];
    if (null === f || g === !1) {
        var k = b && b.nextSibling || null;
        k ? k.parentNode.insertBefore(h, k) : j.appendChild(h);
    }
    if (f && g === !1 && f.parentNode.removeChild(f), c && cache) {
        log("saving " + d + " to cache.", logLevel.info);
        try {
            cache.setItem(d, a), cache.setItem(d + ":timestamp", c);
        } catch (i) {
            log("failed to save", logLevel.errors);
        }
    }
}

function errorHTML(a, b) {
    var c, d, e = "less-error-message:" + extractId(b || ""), f = '<li><label>{line}</label><pre class="{class}">{content}</pre></li>', g = document.createElement("div"), h = [], i = a.filename || b, j = i.match(/([^\/]+(\?.*)?)$/)[1];
    g.id = e, g.className = "less-error-message", d = "<h3>" + (a.type || "Syntax") + "Error: " + (a.message || "There is an error in your .less file") + "</h3>" + '<p>in <a href="' + i + '">' + j + "</a> ";
    var k = function(a, b, c) {
        void 0 !== a.extract[b] && h.push(f.replace(/\{line\}/, (parseInt(a.line, 10) || 0) + (b - 1)).replace(/\{class\}/, c).replace(/\{content\}/, a.extract[b]));
    };
    a.extract ? (k(a, 0, ""), k(a, 1, "line"), k(a, 2, ""), d += "on line " + a.line + ", column " + (a.column + 1) + ":</p>" + "<ul>" + h.join("") + "</ul>") : a.stack && (d += "<br/>" + a.stack.split("\n").slice(1).join("<br/>")), 
    g.innerHTML = d, createCSS([ ".less-error-message ul, .less-error-message li {", "list-style-type: none;", "margin-right: 15px;", "padding: 4px 0;", "margin: 0;", "}", ".less-error-message label {", "font-size: 12px;", "margin-right: 15px;", "padding: 4px 0;", "color: #cc7777;", "}", ".less-error-message pre {", "color: #dd6666;", "padding: 4px 0;", "margin: 0;", "display: inline-block;", "}", ".less-error-message pre.line {", "color: #ff0000;", "}", ".less-error-message h3 {", "font-size: 20px;", "font-weight: bold;", "padding: 15px 0 5px 0;", "margin: 0;", "}", ".less-error-message a {", "color: #10a", "}", ".less-error-message .error {", "color: red;", "font-weight: bold;", "padding-bottom: 2px;", "border-bottom: 1px dashed red;", "}" ].join("\n"), {
        title: "error-message"
    }), g.style.cssText = [ "font-family: Arial, sans-serif", "border: 1px solid #e00", "background-color: #eee", "border-radius: 5px", "-webkit-border-radius: 5px", "-moz-border-radius: 5px", "color: #e00", "padding: 15px", "margin-bottom: 15px" ].join(";"), 
    "development" == less.env && (c = setInterval(function() {
        document.body && (document.getElementById(e) ? document.body.replaceChild(g, document.getElementById(e)) : document.body.insertBefore(g, document.body.firstChild), 
        clearInterval(c));
    }, 10));
}

function error(a, b) {
    less.errorReporting && "html" !== less.errorReporting ? "console" === less.errorReporting ? errorConsole(a, b) : "function" == typeof less.errorReporting && less.errorReporting("add", a, b) : errorHTML(a, b);
}

function removeErrorHTML(a) {
    var b = document.getElementById("less-error-message:" + extractId(a));
    b && b.parentNode.removeChild(b);
}

function removeErrorConsole() {}

function removeError(a) {
    less.errorReporting && "html" !== less.errorReporting ? "console" === less.errorReporting ? removeErrorConsole(a) : "function" == typeof less.errorReporting && less.errorReporting("remove", a) : removeErrorHTML(a);
}

function loadStyles(a) {
    for (var b, c = document.getElementsByTagName("style"), d = 0; d < c.length; d++) if (b = c[d], 
    b.type.match(typePattern)) {
        var e = new less.tree.parseEnv(less), f = b.innerHTML || "";
        e.filename = document.location.href.replace(/#.*$/, ""), a && (e.useFileCache = !0, 
        f += "\n" + a);
        var g = function(a) {
            return function(b, c) {
                if (b) return error(b, "inline");
                var d = c.toCSS(less);
                a.type = "text/css", a.styleSheet ? a.styleSheet.cssText = d : a.innerHTML = d;
            };
        }(b);
        new less.Parser(e).parse(f, g);
    }
}

function extractUrlParts(a, b) {
    var c, d, e = /^((?:[a-z-]+:)?\/+?(?:[^\/\?#]*\/)|([\/\\]))?((?:[^\/\\\?#]*[\/\\])*)([^\/\\\?#]*)([#\?].*)?$/i, f = a.match(e), g = {}, h = [];
    if (!f) throw new Error("Could not parse sheet href - '" + a + "'");
    if (!f[1] || f[2]) {
        if (d = b.match(e), !d) throw new Error("Could not parse page url - '" + b + "'");
        f[1] = f[1] || d[1] || "", f[2] || (f[3] = d[3] + f[3]);
    }
    if (f[3]) {
        for (h = f[3].replace(/\\/g, "/").split("/"), c = 0; c < h.length; c++) "." === h[c] && (h.splice(c, 1), 
        c -= 1);
        for (c = 0; c < h.length; c++) ".." === h[c] && c > 0 && (h.splice(c - 1, 2), c -= 2);
    }
    return g.hostPart = f[1], g.directories = h, g.path = f[1] + h.join("/"), g.fileUrl = g.path + (f[4] || ""), 
    g.url = g.fileUrl + (f[5] || ""), g;
}

function pathDiff(a, b) {
    var c, d, e, f, g = extractUrlParts(a), h = extractUrlParts(b), i = "";
    if (g.hostPart !== h.hostPart) return "";
    for (d = Math.max(h.directories.length, g.directories.length), c = 0; d > c && h.directories[c] === g.directories[c]; c++) ;
    for (f = h.directories.slice(c), e = g.directories.slice(c), c = 0; c < f.length - 1; c++) i += "../";
    for (c = 0; c < e.length - 1; c++) i += e[c] + "/";
    return i;
}

function getXMLHttpRequest() {
    if (window.XMLHttpRequest) return new XMLHttpRequest();
    try {
        return new ActiveXObject("MSXML2.XMLHTTP.3.0");
    } catch (a) {
        return log("browser doesn't support AJAX.", logLevel.errors), null;
    }
}

function doXHR(a, b, c, d) {
    function e(b, c, d) {
        b.status >= 200 && b.status < 300 ? c(b.responseText, b.getResponseHeader("Last-Modified")) : "function" == typeof d && d(b.status, a);
    }
    var f = getXMLHttpRequest(), g = isFileProtocol ? less.fileAsync : less.async;
    "function" == typeof f.overrideMimeType && f.overrideMimeType("text/css"), log("XHR: Getting '" + a + "'", logLevel.info), 
    f.open("GET", a, g), f.setRequestHeader("Accept", b || "text/x-less, text/css; q=0.9, */*; q=0.5"), 
    f.send(null), isFileProtocol && !less.fileAsync ? 0 === f.status || f.status >= 200 && f.status < 300 ? c(f.responseText) : d(f.status, a) : g ? f.onreadystatechange = function() {
        4 == f.readyState && e(f, c, d);
    } : e(f, c, d);
}

function loadFile(a, b, c, d, e) {
    b && b.currentDirectory && !/^([a-z-]+:)?\//.test(a) && (a = b.currentDirectory + a);
    var f = extractUrlParts(a, window.location.href), g = f.url, h = {
        currentDirectory: f.path,
        filename: g
    };
    if (b ? (h.entryPath = b.entryPath, h.rootpath = b.rootpath, h.rootFilename = b.rootFilename, 
    h.relativeUrls = b.relativeUrls) : (h.entryPath = f.path, h.rootpath = less.rootpath || f.path, 
    h.rootFilename = g, h.relativeUrls = d.relativeUrls), h.relativeUrls && (h.rootpath = d.rootpath ? extractUrlParts(d.rootpath + pathDiff(f.path, h.entryPath)).path : f.path), 
    d.useFileCache && fileCache[g]) try {
        var i = fileCache[g];
        e && (i += "\n" + e), c(null, i, g, h, {
            lastModified: new Date()
        });
    } catch (j) {
        c(j, null, g);
    } else doXHR(g, d.mime, function(a, b) {
        fileCache[g] = a;
        try {
            c(null, a, g, h, {
                lastModified: b
            });
        } catch (d) {
            c(d, null, g);
        }
    }, function(a, b) {
        c({
            type: "File",
            message: "'" + b + "' wasn't found (" + a + ")"
        }, null, g);
    });
}

function loadStyleSheet(a, b, c, d, e) {
    var f = new less.tree.parseEnv(less);
    f.mime = a.type, e && (f.useFileCache = !0), loadFile(a.href, null, function(e, g, h, i, j) {
        if (j) {
            j.remaining = d;
            var k = cache && cache.getItem(h), l = cache && cache.getItem(h + ":timestamp");
            if (!c && l && j.lastModified && new Date(j.lastModified).valueOf() === new Date(l).valueOf()) return createCSS(k, a), 
            j.local = !0, b(null, null, g, a, j, h), void 0;
        }
        removeError(h), g ? (f.currentFileInfo = i, new less.Parser(f).parse(g, function(c, d) {
            if (c) return b(c, null, null, a);
            try {
                b(c, d, g, a, j, h);
            } catch (c) {
                b(c, null, null, a);
            }
        })) : b(e, null, null, a, j, h);
    }, f, e);
}

function loadStyleSheets(a, b, c) {
    for (var d = 0; d < less.sheets.length; d++) loadStyleSheet(less.sheets[d], a, b, less.sheets.length - (d + 1), c);
}

function initRunningMode() {
    "development" === less.env ? (less.optimization = 0, less.watchTimer = setInterval(function() {
        less.watchMode && loadStyleSheets(function(a, b, c, d, e) {
            a ? error(a, d.href) : b && createCSS(b.toCSS(less), d, e.lastModified);
        });
    }, less.poll)) : less.optimization = 3;
}

("undefined" == typeof window.less || "undefined" != typeof window.less.nodeType) && (window.less = {}), 
less = window.less, tree = window.less.tree = {}, less.mode = "browser";

var less, tree;

void 0 === less && (less = exports, tree = require("./tree"), less.mode = "node"), 
less.Parser = function(a) {
    function b() {
        r = u[q], s = p, v = p;
    }
    function c() {
        u[q] = r, p = s, v = p;
    }
    function d() {
        p > v && (u[q] = u[q].slice(p - v), v = p);
    }
    function e(a) {
        var b = a.charCodeAt(0);
        return 32 === b || 10 === b || 9 === b;
    }
    function f(a) {
        var b, c;
        if (a instanceof Function) return a.call(w.parsers);
        if ("string" == typeof a) b = o.charAt(p) === a ? a : null, c = 1, d(); else {
            if (d(), !(b = a.exec(u[q]))) return null;
            c = b[0].length;
        }
        return b ? (g(c), "string" == typeof b ? b : 1 === b.length ? b[0] : b) : void 0;
    }
    function g(a) {
        for (var b = p, c = q, d = p + u[q].length, f = p += a; d > p && e(o.charAt(p)); ) p++;
        return u[q] = u[q].slice(a + (p - f)), v = p, 0 === u[q].length && q < u.length - 1 && q++, 
        b !== p || c !== q;
    }
    function h(a, b) {
        var c = f(a);
        return c ? c : (i(b || ("string" == typeof a ? "expected '" + a + "' got '" + o.charAt(p) + "'" : "unexpected token")), 
        void 0);
    }
    function i(a, b) {
        var c = new Error(a);
        throw c.index = p, c.type = b || "Syntax", c;
    }
    function j(a) {
        return "string" == typeof a ? o.charAt(p) === a : a.test(u[q]);
    }
    function k(a, b) {
        return a.filename && b.currentFileInfo.filename && a.filename !== b.currentFileInfo.filename ? w.imports.contents[a.filename] : o;
    }
    function l(a, b) {
        for (var c = a + 1, d = null, e = -1; --c >= 0 && "\n" !== b.charAt(c); ) e++;
        return "number" == typeof a && (d = (b.slice(0, a).match(/\n/g) || "").length), 
        {
            line: d,
            column: e
        };
    }
    function m(a, b, c) {
        var d = c.currentFileInfo.filename;
        return "browser" !== less.mode && "rhino" !== less.mode && (d = require("path").resolve(d)), 
        {
            lineNumber: l(a, b).line + 1,
            fileName: d
        };
    }
    function n(a, b) {
        var c = k(a, b), d = l(a.index, c), e = d.line, f = d.column, g = a.call && l(a.call, c).line, h = c.split("\n");
        this.type = a.type || "Syntax", this.message = a.message, this.filename = a.filename || b.currentFileInfo.filename, 
        this.index = a.index, this.line = "number" == typeof e ? e + 1 : null, this.callLine = g + 1, 
        this.callExtract = h[g], this.stack = a.stack, this.column = f, this.extract = [ h[e - 1], h[e], h[e + 1] ];
    }
    var o, p, q, r, s, t, u, v, w, x = a && a.filename;
    a instanceof tree.parseEnv || (a = new tree.parseEnv(a));
    var y = this.imports = {
        paths: a.paths || [],
        queue: [],
        files: a.files,
        contents: a.contents,
        mime: a.mime,
        error: null,
        push: function(b, c, d, e) {
            var f = this;
            this.queue.push(b);
            var g = function(a, c, d) {
                f.queue.splice(f.queue.indexOf(b), 1);
                var g = d in f.files || d === x;
                f.files[d] = c, a && !f.error && (f.error = a), e(a, c, g, d);
            };
            less.Parser.importer ? less.Parser.importer(b, c, g, a) : less.Parser.fileLoader(b, c, function(b, e, f, h) {
                if (b) return g(b), void 0;
                var i = new tree.parseEnv(a);
                i.currentFileInfo = h, i.processImports = !1, i.contents[f] = e, (c.reference || d.reference) && (h.reference = !0), 
                d.inline ? g(null, e, f) : new less.Parser(i).parse(e, function(a, b) {
                    g(a, b, f);
                });
            }, a);
        }
    };
    return n.prototype = new Error(), n.prototype.constructor = n, this.env = a = a || {}, 
    this.optimization = "optimization" in this.env ? this.env.optimization : 1, w = {
        imports: y,
        parse: function(b, c) {
            var d, e, g, h = null;
            if (p = q = v = t = 0, o = b.replace(/\r\n/g, "\n"), o = o.replace(/^\uFEFF/, ""), 
            w.imports.contents[a.currentFileInfo.filename] = o, u = function(b) {
                for (var c, d, e, f, g = 0, i = /(?:@\{[\w-]+\}|[^"'`\{\}\/\(\)\\])+/g, j = /\/\*(?:[^*]|\*+[^\/*])*\*+\/|\/\/.*/g, k = /"((?:[^"\\\r\n]|\\.)*)"|'((?:[^'\\\r\n]|\\.)*)'|`((?:[^`]|\\.)*)`/g, l = 0, m = b[0], p = 0; p < o.length; ) if (i.lastIndex = p, 
                (c = i.exec(o)) && c.index === p && (p += c[0].length, m.push(c[0])), e = o.charAt(p), 
                j.lastIndex = k.lastIndex = p, (c = k.exec(o)) && c.index === p) p += c[0].length, 
                m.push(c[0]); else if (d || "/" !== e || (f = o.charAt(p + 1), "/" !== f && "*" !== f || !(c = j.exec(o)) || c.index !== p)) {
                    switch (e) {
                      case "{":
                        if (!d) {
                            l++, m.push(e);
                            break;
                        }

                      case "}":
                        if (!d) {
                            l--, m.push(e), b[++g] = m = [];
                            break;
                        }

                      case "(":
                        if (!d) {
                            d = !0, m.push(e);
                            break;
                        }

                      case ")":
                        if (d) {
                            d = !1, m.push(e);
                            break;
                        }

                      default:
                        m.push(e);
                    }
                    p++;
                } else p += c[0].length, m.push(c[0]);
                return 0 !== l && (h = new n({
                    index: p - 1,
                    type: "Parse",
                    message: l > 0 ? "missing closing `}`" : "missing opening `{`",
                    filename: a.currentFileInfo.filename
                }, a)), b.map(function(a) {
                    return a.join("");
                });
            }([ [] ]), h) return c(new n(h, a));
            try {
                d = new tree.Ruleset([], f(this.parsers.primary)), d.root = !0, d.firstRoot = !0;
            } catch (i) {
                return c(new n(i, a));
            }
            if (d.toCSS = function(b) {
                return function(c, d) {
                    c = c || {};
                    var e, f, g = new tree.evalEnv(c);
                    "object" != typeof d || Array.isArray(d) || (d = Object.keys(d).map(function(a) {
                        var b = d[a];
                        return b instanceof tree.Value || (b instanceof tree.Expression || (b = new tree.Expression([ b ])), 
                        b = new tree.Value([ b ])), new tree.Rule("@" + a, b, !1, null, 0);
                    }), g.frames = [ new tree.Ruleset(null, d) ]);
                    try {
                        e = b.call(this, g), new tree.joinSelectorVisitor().run(e), new tree.processExtendsVisitor().run(e), 
                        new tree.toCSSVisitor({
                            compress: Boolean(c.compress)
                        }).run(e), c.sourceMap && (e = new tree.sourceMapOutput({
                            writeSourceMap: c.writeSourceMap,
                            rootNode: e,
                            contentsMap: w.imports.contents,
                            sourceMapFilename: c.sourceMapFilename,
                            outputFilename: c.sourceMapOutputFilename,
                            sourceMapBasepath: c.sourceMapBasepath,
                            sourceMapRootpath: c.sourceMapRootpath,
                            outputSourceFiles: c.outputSourceFiles,
                            sourceMapGenerator: c.sourceMapGenerator
                        })), f = e.toCSS({
                            compress: Boolean(c.compress),
                            dumpLineNumbers: a.dumpLineNumbers,
                            strictUnits: Boolean(c.strictUnits)
                        });
                    } catch (h) {
                        throw new n(h, a);
                    }
                    return c.cleancss && "node" === less.mode ? require("clean-css").process(f) : c.compress ? f.replace(/(^(\s)+)|((\s)+$)/g, "") : f;
                };
            }(d.eval), p < o.length - 1) {
                p = t;
                var j = l(p, o);
                g = o.split("\n"), e = j.line + 1, h = {
                    type: "Parse",
                    message: "Unrecognised input",
                    index: p,
                    filename: a.currentFileInfo.filename,
                    line: e,
                    column: j.column,
                    extract: [ g[e - 2], g[e - 1], g[e] ]
                };
            }
            var k = function(b) {
                return b = h || b || w.imports.error, b ? (b instanceof n || (b = new n(b, a)), 
                c(b)) : c(null, d);
            };
            return a.processImports === !1 ? k() : (new tree.importVisitor(this.imports, k).run(d), 
            void 0);
        },
        parsers: {
            primary: function() {
                for (var a, b = []; (a = f(this.extendRule) || f(this.mixin.definition) || f(this.rule) || f(this.ruleset) || f(this.mixin.call) || f(this.comment) || f(this.directive)) || f(/^[\s\n]+/) || f(/^;+/); ) a && b.push(a);
                return b;
            },
            comment: function() {
                var b;
                if ("/" === o.charAt(p)) return "/" === o.charAt(p + 1) ? new tree.Comment(f(/^\/\/.*/), !0, p, a.currentFileInfo) : (b = f(/^\/\*(?:[^*]|\*+[^\/*])*\*+\/\n?/)) ? new tree.Comment(b, !1, p, a.currentFileInfo) : void 0;
            },
            comments: function() {
                for (var a, b = []; a = f(this.comment); ) b.push(a);
                return b;
            },
            entities: {
                quoted: function() {
                    var b, c, d = p, e = p;
                    return "~" === o.charAt(d) && (d++, c = !0), '"' === o.charAt(d) || "'" === o.charAt(d) ? (c && f("~"), 
                    (b = f(/^"((?:[^"\\\r\n]|\\.)*)"|'((?:[^'\\\r\n]|\\.)*)'/)) ? new tree.Quoted(b[0], b[1] || b[2], c, e, a.currentFileInfo) : void 0) : void 0;
                },
                keyword: function() {
                    var a;
                    if (a = f(/^[_A-Za-z-][_A-Za-z0-9-]*/)) {
                        var b = tree.Color.fromKeyword(a);
                        return b ? b : new tree.Keyword(a);
                    }
                },
                call: function() {
                    var b, c, d, e, g = p;
                    if (b = /^([\w-]+|%|progid:[\w\.]+)\(/.exec(u[q])) {
                        if (b = b[1], c = b.toLowerCase(), "url" === c) return null;
                        if (p += b.length, "alpha" === c && (e = f(this.alpha), "undefined" != typeof e)) return e;
                        if (f("("), d = f(this.entities.arguments), f(")")) return b ? new tree.Call(b, d, g, a.currentFileInfo) : void 0;
                    }
                },
                arguments: function() {
                    for (var a, b = []; (a = f(this.entities.assignment) || f(this.expression)) && (b.push(a), 
                    f(",")); ) ;
                    return b;
                },
                literal: function() {
                    return f(this.entities.dimension) || f(this.entities.color) || f(this.entities.quoted) || f(this.entities.unicodeDescriptor);
                },
                assignment: function() {
                    var a, b;
                    return (a = f(/^\w+(?=\s?=)/i)) && f("=") && (b = f(this.entity)) ? new tree.Assignment(a, b) : void 0;
                },
                url: function() {
                    var b;
                    if ("u" === o.charAt(p) && f(/^url\(/)) return b = f(this.entities.quoted) || f(this.entities.variable) || f(/^(?:(?:\\[\(\)'"])|[^\(\)'"])+/) || "", 
                    h(")"), new tree.URL(null != b.value || b instanceof tree.Variable ? b : new tree.Anonymous(b), a.currentFileInfo);
                },
                variable: function() {
                    var b, c = p;
                    return "@" === o.charAt(p) && (b = f(/^@@?[\w-]+/)) ? new tree.Variable(b, c, a.currentFileInfo) : void 0;
                },
                variableCurly: function() {
                    var b, c = p;
                    return "@" === o.charAt(p) && (b = f(/^@\{([\w-]+)\}/)) ? new tree.Variable("@" + b[1], c, a.currentFileInfo) : void 0;
                },
                color: function() {
                    var a;
                    return "#" === o.charAt(p) && (a = f(/^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})/)) ? new tree.Color(a[1]) : void 0;
                },
                dimension: function() {
                    var a, b = o.charCodeAt(p);
                    if (!(b > 57 || 43 > b || 47 === b || 44 == b)) return (a = f(/^([+-]?\d*\.?\d+)(%|[a-z]+)?/)) ? new tree.Dimension(a[1], a[2]) : void 0;
                },
                unicodeDescriptor: function() {
                    var a;
                    return (a = f(/^U\+[0-9a-fA-F?]+(\-[0-9a-fA-F?]+)?/)) ? new tree.UnicodeDescriptor(a[0]) : void 0;
                },
                javascript: function() {
                    var b, c, d = p;
                    return "~" === o.charAt(d) && (d++, c = !0), "`" === o.charAt(d) ? (void 0 === a.javascriptEnabled || a.javascriptEnabled || i("You are using JavaScript, which has been disabled."), 
                    c && f("~"), (b = f(/^`([^`]*)`/)) ? new tree.JavaScript(b[1], p, c) : void 0) : void 0;
                }
            },
            variable: function() {
                var a;
                return "@" === o.charAt(p) && (a = f(/^(@[\w-]+)\s*:/)) ? a[1] : void 0;
            },
            extend: function(a) {
                var b, c, d, e = p, g = [];
                if (f(a ? /^&:extend\(/ : /^:extend\(/)) {
                    do {
                        for (d = null, b = []; ;) {
                            if (d = f(/^(all)(?=\s*(\)|,))/)) break;
                            if (c = f(this.element), !c) break;
                            b.push(c);
                        }
                        d = d && d[1], g.push(new tree.Extend(new tree.Selector(b), d, e));
                    } while (f(","));
                    return h(/^\)/), a && h(/^;/), g;
                }
            },
            extendRule: function() {
                return this.extend(!0);
            },
            mixin: {
                call: function() {
                    var d, e, g, i = [], k = p, l = o.charAt(p), m = !1;
                    if ("." === l || "#" === l) {
                        for (b(); d = f(/^[#.](?:[\w-]|\\(?:[A-Fa-f0-9]{1,6} ?|[^A-Fa-f0-9]))+/); ) i.push(new tree.Element(e, d, p, a.currentFileInfo)), 
                        e = f(">");
                        return f("(") && (g = this.mixin.args.call(this, !0).args, h(")")), g = g || [], 
                        f(this.important) && (m = !0), i.length > 0 && (f(";") || j("}")) ? new tree.mixin.Call(i, g, k, a.currentFileInfo, m) : (c(), 
                        void 0);
                    }
                },
                args: function(a) {
                    for (var b, c, d, e, g, j, k = [], l = [], m = [], n = {
                        args: null,
                        variadic: !1
                    }; ;) {
                        if (a) j = f(this.expression); else {
                            if (f(this.comments), "." === o.charAt(p) && f(/^\.{3}/)) {
                                n.variadic = !0, f(";") && !b && (b = !0), (b ? l : m).push({
                                    variadic: !0
                                });
                                break;
                            }
                            j = f(this.entities.variable) || f(this.entities.literal) || f(this.entities.keyword);
                        }
                        if (!j) break;
                        e = null, j.throwAwayComments && j.throwAwayComments(), g = j;
                        var q = null;
                        if (a ? 1 == j.value.length && (q = j.value[0]) : q = j, q && q instanceof tree.Variable) if (f(":")) k.length > 0 && (b && i("Cannot mix ; and , as delimiter types"), 
                        c = !0), g = h(this.expression), e = d = q.name; else {
                            if (!a && f(/^\.{3}/)) {
                                n.variadic = !0, f(";") && !b && (b = !0), (b ? l : m).push({
                                    name: j.name,
                                    variadic: !0
                                });
                                break;
                            }
                            a || (d = e = q.name, g = null);
                        }
                        g && k.push(g), m.push({
                            name: e,
                            value: g
                        }), f(",") || (f(";") || b) && (c && i("Cannot mix ; and , as delimiter types"), 
                        b = !0, k.length > 1 && (g = new tree.Value(k)), l.push({
                            name: d,
                            value: g
                        }), d = null, k = [], c = !1);
                    }
                    return n.args = b ? l : m, n;
                },
                definition: function() {
                    var a, d, e, g, i = [], k = !1;
                    if (!("." !== o.charAt(p) && "#" !== o.charAt(p) || j(/^[^{]*\}/)) && (b(), d = f(/^([#.](?:[\w-]|\\(?:[A-Fa-f0-9]{1,6} ?|[^A-Fa-f0-9]))+)\s*\(/))) {
                        a = d[1];
                        var l = this.mixin.args.call(this, !1);
                        if (i = l.args, k = l.variadic, f(")") || (t = p, c()), f(this.comments), f(/^when/) && (g = h(this.conditions, "expected condition")), 
                        e = f(this.block)) return new tree.mixin.Definition(a, i, e, g, k);
                        c();
                    }
                }
            },
            entity: function() {
                return f(this.entities.literal) || f(this.entities.variable) || f(this.entities.url) || f(this.entities.call) || f(this.entities.keyword) || f(this.entities.javascript) || f(this.comment);
            },
            end: function() {
                return f(";") || j("}");
            },
            alpha: function() {
                var a;
                if (f(/^\(opacity=/i)) return (a = f(/^\d+/) || f(this.entities.variable)) ? (h(")"), 
                new tree.Alpha(a)) : void 0;
            },
            element: function() {
                var b, c, d;
                return c = f(this.combinator), b = f(/^(?:\d+\.\d+|\d+)%/) || f(/^(?:[.#]?|:*)(?:[\w-]|[^\x00-\x9f]|\\(?:[A-Fa-f0-9]{1,6} ?|[^A-Fa-f0-9]))+/) || f("*") || f("&") || f(this.attribute) || f(/^\([^()@]+\)/) || f(/^[\.#](?=@)/) || f(this.entities.variableCurly), 
                b || f("(") && (d = f(this.selector)) && f(")") && (b = new tree.Paren(d)), b ? new tree.Element(c, b, p, a.currentFileInfo) : void 0;
            },
            combinator: function() {
                var a = o.charAt(p);
                if (">" === a || "+" === a || "~" === a || "|" === a) {
                    for (p++; o.charAt(p).match(/\s/); ) p++;
                    return new tree.Combinator(a);
                }
                return o.charAt(p - 1).match(/\s/) ? new tree.Combinator(" ") : new tree.Combinator(null);
            },
            lessSelector: function() {
                return this.selector(!0);
            },
            selector: function(b) {
                for (var c, d, e, g, j, k = [], l = []; (b && (e = f(this.extend)) || b && (g = f(/^when/)) || (c = f(this.element))) && (g ? j = h(this.conditions, "expected condition") : j ? i("CSS guard can only be used at the end of selector") : e ? l.push.apply(l, e) : (l.length && i("Extend can only be used at the end of selector"), 
                d = o.charAt(p), k.push(c), c = null), "{" !== d && "}" !== d && ";" !== d && "," !== d && ")" !== d); ) ;
                return k.length > 0 ? new tree.Selector(k, l, j, p, a.currentFileInfo) : (l.length && i("Extend must be used to extend a selector, it cannot be used on its own"), 
                void 0);
            },
            attribute: function() {
                var a, b, c;
                if (f("[")) return (a = f(this.entities.variableCurly)) || (a = h(/^(?:[_A-Za-z0-9-\*]*\|)?(?:[_A-Za-z0-9-]|\\.)+/)), 
                (c = f(/^[|~*$^]?=/)) && (b = f(this.entities.quoted) || f(/^[0-9]+%/) || f(/^[\w-]+/) || f(this.entities.variableCurly)), 
                h("]"), new tree.Attribute(a, c, b);
            },
            block: function() {
                var a;
                return f("{") && (a = f(this.primary)) && f("}") ? a : void 0;
            },
            ruleset: function() {
                var d, e, g, h = [];
                for (b(), a.dumpLineNumbers && (g = m(p, o, a)); (d = f(this.lessSelector)) && (h.push(d), 
                f(this.comments), f(",")); ) d.condition && i("Guards are only currently allowed on a single selector."), 
                f(this.comments);
                if (h.length > 0 && (e = f(this.block))) {
                    var j = new tree.Ruleset(h, e, a.strictImports);
                    return a.dumpLineNumbers && (j.debugInfo = g), j;
                }
                t = p, c();
            },
            rule: function(d) {
                var e, g, h, i = o.charAt(p), j = !1;
                if (b(), "." !== i && "#" !== i && "&" !== i && (e = f(this.variable) || f(this.ruleProperty))) {
                    if (g = d || !a.compress && "@" !== e.charAt(0) ? f(this.anonymousValue) || f(this.value) : f(this.value) || f(this.anonymousValue), 
                    h = f(this.important), "+" === e[e.length - 1] && (j = !0, e = e.substr(0, e.length - 1)), 
                    g && f(this.end)) return new tree.Rule(e, g, h, j, s, a.currentFileInfo);
                    if (t = p, c(), g && !d) return this.rule(!0);
                }
            },
            anonymousValue: function() {
                var a;
                return (a = /^([^@+\/'"*`(;{}-]*);/.exec(u[q])) ? (p += a[0].length - 1, new tree.Anonymous(a[1])) : void 0;
            },
            "import": function() {
                var d, e, g = p;
                b();
                var h = f(/^@import?\s+/), i = (h ? f(this.importOptions) : null) || {};
                return h && (d = f(this.entities.quoted) || f(this.entities.url)) && (e = f(this.mediaFeatures), 
                f(";")) ? (e = e && new tree.Value(e), new tree.Import(d, e, i, g, a.currentFileInfo)) : (c(), 
                void 0);
            },
            importOptions: function() {
                var a, b, c, d = {};
                if (!f("(")) return null;
                do if (a = f(this.importOption)) {
                    switch (b = a, c = !0, b) {
                      case "css":
                        b = "less", c = !1;
                        break;

                      case "once":
                        b = "multiple", c = !1;
                    }
                    if (d[b] = c, !f(",")) break;
                } while (a);
                return h(")"), d;
            },
            importOption: function() {
                var a = f(/^(less|css|multiple|once|inline|reference)/);
                return a ? a[1] : void 0;
            },
            mediaFeature: function() {
                var b, c, d = [];
                do if (b = f(this.entities.keyword) || f(this.entities.variable)) d.push(b); else if (f("(")) {
                    if (c = f(this.property), b = f(this.value), !f(")")) return null;
                    if (c && b) d.push(new tree.Paren(new tree.Rule(c, b, null, null, p, a.currentFileInfo, !0))); else {
                        if (!b) return null;
                        d.push(new tree.Paren(b));
                    }
                } while (b);
                return d.length > 0 ? new tree.Expression(d) : void 0;
            },
            mediaFeatures: function() {
                var a, b = [];
                do if (a = f(this.mediaFeature)) {
                    if (b.push(a), !f(",")) break;
                } else if ((a = f(this.entities.variable)) && (b.push(a), !f(","))) break; while (a);
                return b.length > 0 ? b : null;
            },
            media: function() {
                var b, c, d, e;
                return a.dumpLineNumbers && (e = m(p, o, a)), f(/^@media/) && (b = f(this.mediaFeatures), 
                c = f(this.block)) ? (d = new tree.Media(c, b, p, a.currentFileInfo), a.dumpLineNumbers && (d.debugInfo = e), 
                d) : void 0;
            },
            directive: function() {
                var d, e, g, h, i, j, k, l;
                if ("@" === o.charAt(p)) {
                    if (e = f(this["import"]) || f(this.media)) return e;
                    if (b(), d = f(/^@[a-z-]+/)) {
                        switch (h = d, "-" == d.charAt(1) && d.indexOf("-", 2) > 0 && (h = "@" + d.slice(d.indexOf("-", 2) + 1)), 
                        h) {
                          case "@font-face":
                            i = !0;
                            break;

                          case "@viewport":
                          case "@top-left":
                          case "@top-left-corner":
                          case "@top-center":
                          case "@top-right":
                          case "@top-right-corner":
                          case "@bottom-left":
                          case "@bottom-left-corner":
                          case "@bottom-center":
                          case "@bottom-right":
                          case "@bottom-right-corner":
                          case "@left-top":
                          case "@left-middle":
                          case "@left-bottom":
                          case "@right-top":
                          case "@right-middle":
                          case "@right-bottom":
                            i = !0;
                            break;

                          case "@host":
                          case "@page":
                          case "@document":
                          case "@supports":
                          case "@keyframes":
                            i = !0, j = !0;
                            break;

                          case "@namespace":
                            k = !0;
                        }
                        if (j && (l = (f(/^[^{]+/) || "").trim(), l && (d += " " + l)), i) {
                            if (g = f(this.block)) return new tree.Directive(d, g, p, a.currentFileInfo);
                        } else if ((e = k ? f(this.expression) : f(this.entity)) && f(";")) {
                            var n = new tree.Directive(d, e, p, a.currentFileInfo);
                            return a.dumpLineNumbers && (n.debugInfo = m(p, o, a)), n;
                        }
                        c();
                    }
                }
            },
            value: function() {
                for (var a, b = []; (a = f(this.expression)) && (b.push(a), f(",")); ) ;
                return b.length > 0 ? new tree.Value(b) : void 0;
            },
            important: function() {
                return "!" === o.charAt(p) ? f(/^! *important/) : void 0;
            },
            sub: function() {
                var a, b;
                return f("(") && (a = f(this.addition)) ? (b = new tree.Expression([ a ]), h(")"), 
                b.parens = !0, b) : void 0;
            },
            multiplication: function() {
                var a, b, c, d, g;
                if (a = f(this.operand)) {
                    for (g = e(o.charAt(p - 1)); !j(/^\/[*\/]/) && (c = f("/") || f("*")) && (b = f(this.operand)); ) a.parensInOp = !0, 
                    b.parensInOp = !0, d = new tree.Operation(c, [ d || a, b ], g), g = e(o.charAt(p - 1));
                    return d || a;
                }
            },
            addition: function() {
                var a, b, c, d, g;
                if (a = f(this.multiplication)) {
                    for (g = e(o.charAt(p - 1)); (c = f(/^[-+]\s+/) || !g && (f("+") || f("-"))) && (b = f(this.multiplication)); ) a.parensInOp = !0, 
                    b.parensInOp = !0, d = new tree.Operation(c, [ d || a, b ], g), g = e(o.charAt(p - 1));
                    return d || a;
                }
            },
            conditions: function() {
                var a, b, c, d = p;
                if (a = f(this.condition)) {
                    for (;j(/^,\s*(not\s*)?\(/) && f(",") && (b = f(this.condition)); ) c = new tree.Condition("or", c || a, b, d);
                    return c || a;
                }
            },
            condition: function() {
                var a, b, c, d, e = p, g = !1;
                return f(/^not/) && (g = !0), h("("), (a = f(this.addition) || f(this.entities.keyword) || f(this.entities.quoted)) ? ((d = f(/^(?:>=|<=|=<|[<=>])/)) ? (b = f(this.addition) || f(this.entities.keyword) || f(this.entities.quoted)) ? c = new tree.Condition(d, a, b, e, g) : i("expected expression") : c = new tree.Condition("=", a, new tree.Keyword("true"), e, g), 
                h(")"), f(/^and/) ? new tree.Condition("and", c, f(this.condition)) : c) : void 0;
            },
            operand: function() {
                var a, b = o.charAt(p + 1);
                "-" !== o.charAt(p) || "@" !== b && "(" !== b || (a = f("-"));
                var c = f(this.sub) || f(this.entities.dimension) || f(this.entities.color) || f(this.entities.variable) || f(this.entities.call);
                return a && (c.parensInOp = !0, c = new tree.Negative(c)), c;
            },
            expression: function() {
                for (var a, b, c = []; a = f(this.addition) || f(this.entity); ) c.push(a), !j(/^\/[\/*]/) && (b = f("/")) && c.push(new tree.Anonymous(b));
                return c.length > 0 ? new tree.Expression(c) : void 0;
            },
            property: function() {
                var a;
                return (a = f(/^(\*?-?[_a-zA-Z0-9-]+)\s*:/)) ? a[1] : void 0;
            },
            ruleProperty: function() {
                var a;
                return (a = f(/^(\*?-?[_a-zA-Z0-9-]+)\s*(\+?)\s*:/)) ? a[1] + (a[2] || "") : void 0;
            }
        }
    };
}, function(a) {
    function b(b) {
        return a.functions.hsla(b.h, b.s, b.l, b.a);
    }
    function c(b, c) {
        return b instanceof a.Dimension && b.unit.is("%") ? parseFloat(b.value * c / 100) : d(b);
    }
    function d(b) {
        if (b instanceof a.Dimension) return parseFloat(b.unit.is("%") ? b.value / 100 : b.value);
        if ("number" == typeof b) return b;
        throw {
            error: "RuntimeError",
            message: "color functions take numbers as parameters"
        };
    }
    function e(a) {
        return Math.min(1, Math.max(0, a));
    }
    a.functions = {
        rgb: function(a, b, c) {
            return this.rgba(a, b, c, 1);
        },
        rgba: function(b, e, f, g) {
            var h = [ b, e, f ].map(function(a) {
                return c(a, 256);
            });
            return g = d(g), new a.Color(h, g);
        },
        hsl: function(a, b, c) {
            return this.hsla(a, b, c, 1);
        },
        hsla: function(a, b, c, f) {
            function g(a) {
                return a = 0 > a ? a + 1 : a > 1 ? a - 1 : a, 1 > 6 * a ? i + 6 * (h - i) * a : 1 > 2 * a ? h : 2 > 3 * a ? i + 6 * (h - i) * (2 / 3 - a) : i;
            }
            a = d(a) % 360 / 360, b = e(d(b)), c = e(d(c)), f = e(d(f));
            var h = .5 >= c ? c * (b + 1) : c + b - c * b, i = 2 * c - h;
            return this.rgba(255 * g(a + 1 / 3), 255 * g(a), 255 * g(a - 1 / 3), f);
        },
        hsv: function(a, b, c) {
            return this.hsva(a, b, c, 1);
        },
        hsva: function(a, b, c, e) {
            a = 360 * (d(a) % 360 / 360), b = d(b), c = d(c), e = d(e);
            var f, g;
            f = Math.floor(a / 60 % 6), g = a / 60 - f;
            var h = [ c, c * (1 - b), c * (1 - g * b), c * (1 - (1 - g) * b) ], i = [ [ 0, 3, 1 ], [ 2, 0, 1 ], [ 1, 0, 3 ], [ 1, 2, 0 ], [ 3, 1, 0 ], [ 0, 1, 2 ] ];
            return this.rgba(255 * h[i[f][0]], 255 * h[i[f][1]], 255 * h[i[f][2]], e);
        },
        hue: function(b) {
            return new a.Dimension(Math.round(b.toHSL().h));
        },
        saturation: function(b) {
            return new a.Dimension(Math.round(100 * b.toHSL().s), "%");
        },
        lightness: function(b) {
            return new a.Dimension(Math.round(100 * b.toHSL().l), "%");
        },
        hsvhue: function(b) {
            return new a.Dimension(Math.round(b.toHSV().h));
        },
        hsvsaturation: function(b) {
            return new a.Dimension(Math.round(100 * b.toHSV().s), "%");
        },
        hsvvalue: function(b) {
            return new a.Dimension(Math.round(100 * b.toHSV().v), "%");
        },
        red: function(b) {
            return new a.Dimension(b.rgb[0]);
        },
        green: function(b) {
            return new a.Dimension(b.rgb[1]);
        },
        blue: function(b) {
            return new a.Dimension(b.rgb[2]);
        },
        alpha: function(b) {
            return new a.Dimension(b.toHSL().a);
        },
        luma: function(b) {
            return new a.Dimension(Math.round(100 * b.luma() * b.alpha), "%");
        },
        saturate: function(a, c) {
            if (!a.rgb) return null;
            var d = a.toHSL();
            return d.s += c.value / 100, d.s = e(d.s), b(d);
        },
        desaturate: function(a, c) {
            var d = a.toHSL();
            return d.s -= c.value / 100, d.s = e(d.s), b(d);
        },
        lighten: function(a, c) {
            var d = a.toHSL();
            return d.l += c.value / 100, d.l = e(d.l), b(d);
        },
        darken: function(a, c) {
            var d = a.toHSL();
            return d.l -= c.value / 100, d.l = e(d.l), b(d);
        },
        fadein: function(a, c) {
            var d = a.toHSL();
            return d.a += c.value / 100, d.a = e(d.a), b(d);
        },
        fadeout: function(a, c) {
            var d = a.toHSL();
            return d.a -= c.value / 100, d.a = e(d.a), b(d);
        },
        fade: function(a, c) {
            var d = a.toHSL();
            return d.a = c.value / 100, d.a = e(d.a), b(d);
        },
        spin: function(a, c) {
            var d = a.toHSL(), e = (d.h + c.value) % 360;
            return d.h = 0 > e ? 360 + e : e, b(d);
        },
        mix: function(b, c, d) {
            d || (d = new a.Dimension(50));
            var e = d.value / 100, f = 2 * e - 1, g = b.toHSL().a - c.toHSL().a, h = ((-1 == f * g ? f : (f + g) / (1 + f * g)) + 1) / 2, i = 1 - h, j = [ b.rgb[0] * h + c.rgb[0] * i, b.rgb[1] * h + c.rgb[1] * i, b.rgb[2] * h + c.rgb[2] * i ], k = b.alpha * e + c.alpha * (1 - e);
            return new a.Color(j, k);
        },
        greyscale: function(b) {
            return this.desaturate(b, new a.Dimension(100));
        },
        contrast: function(a, b, c, e) {
            if (!a.rgb) return null;
            if ("undefined" == typeof c && (c = this.rgba(255, 255, 255, 1)), "undefined" == typeof b && (b = this.rgba(0, 0, 0, 1)), 
            b.luma() > c.luma()) {
                var f = c;
                c = b, b = f;
            }
            return e = "undefined" == typeof e ? .43 : d(e), a.luma() * a.alpha < e ? c : b;
        },
        e: function(b) {
            return new a.Anonymous(b instanceof a.JavaScript ? b.evaluated : b);
        },
        escape: function(b) {
            return new a.Anonymous(encodeURI(b.value).replace(/=/g, "%3D").replace(/:/g, "%3A").replace(/#/g, "%23").replace(/;/g, "%3B").replace(/\(/g, "%28").replace(/\)/g, "%29"));
        },
        "%": function(b) {
            for (var c = Array.prototype.slice.call(arguments, 1), d = b.value, e = 0; e < c.length; e++) d = d.replace(/%[sda]/i, function(a) {
                var b = a.match(/s/i) ? c[e].value : c[e].toCSS();
                return a.match(/[A-Z]$/) ? encodeURIComponent(b) : b;
            });
            return d = d.replace(/%%/g, "%"), new a.Quoted('"' + d + '"', d);
        },
        unit: function(b, c) {
            if (!(b instanceof a.Dimension)) throw {
                type: "Argument",
                message: "the first argument to unit must be a number" + (b instanceof a.Operation ? ". Have you forgotten parenthesis?" : "")
            };
            return new a.Dimension(b.value, c ? c.toCSS() : "");
        },
        convert: function(a, b) {
            return a.convertTo(b.value);
        },
        round: function(a, b) {
            var c = "undefined" == typeof b ? 0 : b.value;
            return this._math(function(a) {
                return a.toFixed(c);
            }, null, a);
        },
        pi: function() {
            return new a.Dimension(Math.PI);
        },
        mod: function(b, c) {
            return new a.Dimension(b.value % c.value, b.unit);
        },
        pow: function(b, c) {
            if ("number" == typeof b && "number" == typeof c) b = new a.Dimension(b), c = new a.Dimension(c); else if (!(b instanceof a.Dimension && c instanceof a.Dimension)) throw {
                type: "Argument",
                message: "arguments must be numbers"
            };
            return new a.Dimension(Math.pow(b.value, c.value), b.unit);
        },
        _math: function(b, c, d) {
            if (d instanceof a.Dimension) return new a.Dimension(b(parseFloat(d.value)), null == c ? d.unit : c);
            if ("number" == typeof d) return b(d);
            throw {
                type: "Argument",
                message: "argument must be a number"
            };
        },
        _minmax: function(b, c) {
            switch (c = Array.prototype.slice.call(c), c.length) {
              case 0:
                throw {
                    type: "Argument",
                    message: "one or more arguments required"
                };

              case 1:
                return c[0];
            }
            var d, e, f, g, h, i, j = [], k = {};
            for (d = 0; d < c.length; d++) f = c[d], f instanceof a.Dimension ? (g = f.unify(), 
            i = g.unit.toString(), e = k[i], void 0 !== e ? (h = j[e].unify(), (b && g.value < h.value || !b && g.value > h.value) && (j[e] = f)) : (k[i] = j.length, 
            j.push(f))) : j.push(f);
            return 1 == j.length ? j[0] : (c = j.map(function(a) {
                return a.toCSS(this.env);
            }).join(this.env.compress ? "," : ", "), new a.Anonymous((b ? "min" : "max") + "(" + c + ")"));
        },
        min: function() {
            return this._minmax(!0, arguments);
        },
        max: function() {
            return this._minmax(!1, arguments);
        },
        argb: function(b) {
            return new a.Anonymous(b.toARGB());
        },
        percentage: function(b) {
            return new a.Dimension(100 * b.value, "%");
        },
        color: function(b) {
            if (b instanceof a.Quoted) {
                var c, d = b.value;
                if (c = a.Color.fromKeyword(d)) return c;
                if (/^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})/.test(d)) return new a.Color(d.slice(1));
                throw {
                    type: "Argument",
                    message: "argument must be a color keyword or 3/6 digit hex e.g. #FFF"
                };
            }
            throw {
                type: "Argument",
                message: "argument must be a string"
            };
        },
        iscolor: function(b) {
            return this._isa(b, a.Color);
        },
        isnumber: function(b) {
            return this._isa(b, a.Dimension);
        },
        isstring: function(b) {
            return this._isa(b, a.Quoted);
        },
        iskeyword: function(b) {
            return this._isa(b, a.Keyword);
        },
        isurl: function(b) {
            return this._isa(b, a.URL);
        },
        ispixel: function(a) {
            return this.isunit(a, "px");
        },
        ispercentage: function(a) {
            return this.isunit(a, "%");
        },
        isem: function(a) {
            return this.isunit(a, "em");
        },
        isunit: function(b, c) {
            return b instanceof a.Dimension && b.unit.is(c.value || c) ? a.True : a.False;
        },
        _isa: function(b, c) {
            return b instanceof c ? a.True : a.False;
        },
        multiply: function(a, b) {
            var c = a.rgb[0] * b.rgb[0] / 255, d = a.rgb[1] * b.rgb[1] / 255, e = a.rgb[2] * b.rgb[2] / 255;
            return this.rgb(c, d, e);
        },
        screen: function(a, b) {
            var c = 255 - (255 - a.rgb[0]) * (255 - b.rgb[0]) / 255, d = 255 - (255 - a.rgb[1]) * (255 - b.rgb[1]) / 255, e = 255 - (255 - a.rgb[2]) * (255 - b.rgb[2]) / 255;
            return this.rgb(c, d, e);
        },
        overlay: function(a, b) {
            var c = a.rgb[0] < 128 ? 2 * a.rgb[0] * b.rgb[0] / 255 : 255 - 2 * (255 - a.rgb[0]) * (255 - b.rgb[0]) / 255, d = a.rgb[1] < 128 ? 2 * a.rgb[1] * b.rgb[1] / 255 : 255 - 2 * (255 - a.rgb[1]) * (255 - b.rgb[1]) / 255, e = a.rgb[2] < 128 ? 2 * a.rgb[2] * b.rgb[2] / 255 : 255 - 2 * (255 - a.rgb[2]) * (255 - b.rgb[2]) / 255;
            return this.rgb(c, d, e);
        },
        softlight: function(a, b) {
            var c = b.rgb[0] * a.rgb[0] / 255, d = c + a.rgb[0] * (255 - (255 - a.rgb[0]) * (255 - b.rgb[0]) / 255 - c) / 255;
            c = b.rgb[1] * a.rgb[1] / 255;
            var e = c + a.rgb[1] * (255 - (255 - a.rgb[1]) * (255 - b.rgb[1]) / 255 - c) / 255;
            c = b.rgb[2] * a.rgb[2] / 255;
            var f = c + a.rgb[2] * (255 - (255 - a.rgb[2]) * (255 - b.rgb[2]) / 255 - c) / 255;
            return this.rgb(d, e, f);
        },
        hardlight: function(a, b) {
            var c = b.rgb[0] < 128 ? 2 * b.rgb[0] * a.rgb[0] / 255 : 255 - 2 * (255 - b.rgb[0]) * (255 - a.rgb[0]) / 255, d = b.rgb[1] < 128 ? 2 * b.rgb[1] * a.rgb[1] / 255 : 255 - 2 * (255 - b.rgb[1]) * (255 - a.rgb[1]) / 255, e = b.rgb[2] < 128 ? 2 * b.rgb[2] * a.rgb[2] / 255 : 255 - 2 * (255 - b.rgb[2]) * (255 - a.rgb[2]) / 255;
            return this.rgb(c, d, e);
        },
        difference: function(a, b) {
            var c = Math.abs(a.rgb[0] - b.rgb[0]), d = Math.abs(a.rgb[1] - b.rgb[1]), e = Math.abs(a.rgb[2] - b.rgb[2]);
            return this.rgb(c, d, e);
        },
        exclusion: function(a, b) {
            var c = a.rgb[0] + b.rgb[0] * (255 - a.rgb[0] - a.rgb[0]) / 255, d = a.rgb[1] + b.rgb[1] * (255 - a.rgb[1] - a.rgb[1]) / 255, e = a.rgb[2] + b.rgb[2] * (255 - a.rgb[2] - a.rgb[2]) / 255;
            return this.rgb(c, d, e);
        },
        average: function(a, b) {
            var c = (a.rgb[0] + b.rgb[0]) / 2, d = (a.rgb[1] + b.rgb[1]) / 2, e = (a.rgb[2] + b.rgb[2]) / 2;
            return this.rgb(c, d, e);
        },
        negation: function(a, b) {
            var c = 255 - Math.abs(255 - b.rgb[0] - a.rgb[0]), d = 255 - Math.abs(255 - b.rgb[1] - a.rgb[1]), e = 255 - Math.abs(255 - b.rgb[2] - a.rgb[2]);
            return this.rgb(c, d, e);
        },
        tint: function(a, b) {
            return this.mix(this.rgb(255, 255, 255), a, b);
        },
        shade: function(a, b) {
            return this.mix(this.rgb(0, 0, 0), a, b);
        },
        extract: function(a, b) {
            return b = b.value - 1, Array.isArray(a.value) ? a.value[b] : Array(a)[b];
        },
        length: function(b) {
            var c = Array.isArray(b.value) ? b.value.length : 1;
            return new a.Dimension(c);
        },
        "data-uri": function(b, c) {
            if ("undefined" != typeof window) return new a.URL(c || b, this.currentFileInfo).eval(this.env);
            var d = b.value, e = c && c.value, f = require("fs"), g = require("path"), h = !1;
            if (arguments.length < 2 && (e = d), this.env.isPathRelative(e) && (e = this.currentFileInfo.relativeUrls ? g.join(this.currentFileInfo.currentDirectory, e) : g.join(this.currentFileInfo.entryPath, e)), 
            arguments.length < 2) {
                var i;
                try {
                    i = require("mime");
                } catch (j) {
                    i = a._mime;
                }
                d = i.lookup(e);
                var k = i.charsets.lookup(d);
                h = [ "US-ASCII", "UTF-8" ].indexOf(k) < 0, h && (d += ";base64");
            } else h = /;base64$/.test(d);
            var l = f.readFileSync(e), m = 32, n = parseInt(l.length / 1024, 10);
            if (n >= m && this.env.ieCompat !== !1) return this.env.silent || console.warn("Skipped data-uri embedding of %s because its size (%dKB) exceeds IE8-safe %dKB!", e, n, m), 
            new a.URL(c || b, this.currentFileInfo).eval(this.env);
            l = h ? l.toString("base64") : encodeURIComponent(l);
            var o = "'data:" + d + "," + l + "'";
            return new a.URL(new a.Anonymous(o));
        },
        "svg-gradient": function(b) {
            function c() {
                throw {
                    type: "Argument",
                    message: "svg-gradient expects direction, start_color [start_position], [color position,]..., end_color [end_position]"
                };
            }
            arguments.length < 3 && c();
            var d, e, f, g, h, i, j, k = Array.prototype.slice.call(arguments, 1), l = "linear", m = 'x="0" y="0" width="1" height="1"', n = !0, o = {
                compress: !1
            }, p = b.toCSS(o);
            switch (p) {
              case "to bottom":
                d = 'x1="0%" y1="0%" x2="0%" y2="100%"';
                break;

              case "to right":
                d = 'x1="0%" y1="0%" x2="100%" y2="0%"';
                break;

              case "to bottom right":
                d = 'x1="0%" y1="0%" x2="100%" y2="100%"';
                break;

              case "to top right":
                d = 'x1="0%" y1="100%" x2="100%" y2="0%"';
                break;

              case "ellipse":
              case "ellipse at center":
                l = "radial", d = 'cx="50%" cy="50%" r="75%"', m = 'x="-50" y="-50" width="101" height="101"';
                break;

              default:
                throw {
                    type: "Argument",
                    message: "svg-gradient direction must be 'to bottom', 'to right', 'to bottom right', 'to top right' or 'ellipse at center'"
                };
            }
            for (e = '<?xml version="1.0" ?><svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100%" viewBox="0 0 1 1" preserveAspectRatio="none"><' + l + 'Gradient id="gradient" gradientUnits="userSpaceOnUse" ' + d + ">", 
            f = 0; f < k.length; f += 1) k[f].value ? (g = k[f].value[0], h = k[f].value[1]) : (g = k[f], 
            h = void 0), g instanceof a.Color && ((0 === f || f + 1 === k.length) && void 0 === h || h instanceof a.Dimension) || c(), 
            i = h ? h.toCSS(o) : 0 === f ? "0%" : "100%", j = g.alpha, e += '<stop offset="' + i + '" stop-color="' + g.toRGB() + '"' + (1 > j ? ' stop-opacity="' + j + '"' : "") + "/>";
            if (e += "</" + l + "Gradient>" + "<rect " + m + ' fill="url(#gradient)" /></svg>', 
            n) try {
                e = new Buffer(e).toString("base64");
            } catch (q) {
                n = !1;
            }
            return e = "'data:image/svg+xml" + (n ? ";base64" : "") + "," + e + "'", new a.URL(new a.Anonymous(e));
        }
    }, a._mime = {
        _types: {
            ".htm": "text/html",
            ".html": "text/html",
            ".gif": "image/gif",
            ".jpg": "image/jpeg",
            ".jpeg": "image/jpeg",
            ".png": "image/png"
        },
        lookup: function(b) {
            var c = require("path").extname(b), d = a._mime._types[c];
            if (void 0 === d) throw new Error('Optional dependency "mime" is required for ' + c);
            return d;
        },
        charsets: {
            lookup: function(a) {
                return a && /^text\//.test(a) ? "UTF-8" : "";
            }
        }
    };
    for (var f = [ {
        name: "ceil"
    }, {
        name: "floor"
    }, {
        name: "sqrt"
    }, {
        name: "abs"
    }, {
        name: "tan",
        unit: ""
    }, {
        name: "sin",
        unit: ""
    }, {
        name: "cos",
        unit: ""
    }, {
        name: "atan",
        unit: "rad"
    }, {
        name: "asin",
        unit: "rad"
    }, {
        name: "acos",
        unit: "rad"
    } ], g = function(a, b) {
        return function(c) {
            return null != b && (c = c.unify()), this._math(Math[a], b, c);
        };
    }, h = 0; h < f.length; h++) a.functions[f[h].name] = g(f[h].name, f[h].unit);
    a.functionCall = function(a, b) {
        this.env = a, this.currentFileInfo = b;
    }, a.functionCall.prototype = a.functions;
}(require("./tree")), function(a) {
    a.colors = {
        aliceblue: "#f0f8ff",
        antiquewhite: "#faebd7",
        aqua: "#00ffff",
        aquamarine: "#7fffd4",
        azure: "#f0ffff",
        beige: "#f5f5dc",
        bisque: "#ffe4c4",
        black: "#000000",
        blanchedalmond: "#ffebcd",
        blue: "#0000ff",
        blueviolet: "#8a2be2",
        brown: "#a52a2a",
        burlywood: "#deb887",
        cadetblue: "#5f9ea0",
        chartreuse: "#7fff00",
        chocolate: "#d2691e",
        coral: "#ff7f50",
        cornflowerblue: "#6495ed",
        cornsilk: "#fff8dc",
        crimson: "#dc143c",
        cyan: "#00ffff",
        darkblue: "#00008b",
        darkcyan: "#008b8b",
        darkgoldenrod: "#b8860b",
        darkgray: "#a9a9a9",
        darkgrey: "#a9a9a9",
        darkgreen: "#006400",
        darkkhaki: "#bdb76b",
        darkmagenta: "#8b008b",
        darkolivegreen: "#556b2f",
        darkorange: "#ff8c00",
        darkorchid: "#9932cc",
        darkred: "#8b0000",
        darksalmon: "#e9967a",
        darkseagreen: "#8fbc8f",
        darkslateblue: "#483d8b",
        darkslategray: "#2f4f4f",
        darkslategrey: "#2f4f4f",
        darkturquoise: "#00ced1",
        darkviolet: "#9400d3",
        deeppink: "#ff1493",
        deepskyblue: "#00bfff",
        dimgray: "#696969",
        dimgrey: "#696969",
        dodgerblue: "#1e90ff",
        firebrick: "#b22222",
        floralwhite: "#fffaf0",
        forestgreen: "#228b22",
        fuchsia: "#ff00ff",
        gainsboro: "#dcdcdc",
        ghostwhite: "#f8f8ff",
        gold: "#ffd700",
        goldenrod: "#daa520",
        gray: "#808080",
        grey: "#808080",
        green: "#008000",
        greenyellow: "#adff2f",
        honeydew: "#f0fff0",
        hotpink: "#ff69b4",
        indianred: "#cd5c5c",
        indigo: "#4b0082",
        ivory: "#fffff0",
        khaki: "#f0e68c",
        lavender: "#e6e6fa",
        lavenderblush: "#fff0f5",
        lawngreen: "#7cfc00",
        lemonchiffon: "#fffacd",
        lightblue: "#add8e6",
        lightcoral: "#f08080",
        lightcyan: "#e0ffff",
        lightgoldenrodyellow: "#fafad2",
        lightgray: "#d3d3d3",
        lightgrey: "#d3d3d3",
        lightgreen: "#90ee90",
        lightpink: "#ffb6c1",
        lightsalmon: "#ffa07a",
        lightseagreen: "#20b2aa",
        lightskyblue: "#87cefa",
        lightslategray: "#778899",
        lightslategrey: "#778899",
        lightsteelblue: "#b0c4de",
        lightyellow: "#ffffe0",
        lime: "#00ff00",
        limegreen: "#32cd32",
        linen: "#faf0e6",
        magenta: "#ff00ff",
        maroon: "#800000",
        mediumaquamarine: "#66cdaa",
        mediumblue: "#0000cd",
        mediumorchid: "#ba55d3",
        mediumpurple: "#9370d8",
        mediumseagreen: "#3cb371",
        mediumslateblue: "#7b68ee",
        mediumspringgreen: "#00fa9a",
        mediumturquoise: "#48d1cc",
        mediumvioletred: "#c71585",
        midnightblue: "#191970",
        mintcream: "#f5fffa",
        mistyrose: "#ffe4e1",
        moccasin: "#ffe4b5",
        navajowhite: "#ffdead",
        navy: "#000080",
        oldlace: "#fdf5e6",
        olive: "#808000",
        olivedrab: "#6b8e23",
        orange: "#ffa500",
        orangered: "#ff4500",
        orchid: "#da70d6",
        palegoldenrod: "#eee8aa",
        palegreen: "#98fb98",
        paleturquoise: "#afeeee",
        palevioletred: "#d87093",
        papayawhip: "#ffefd5",
        peachpuff: "#ffdab9",
        peru: "#cd853f",
        pink: "#ffc0cb",
        plum: "#dda0dd",
        powderblue: "#b0e0e6",
        purple: "#800080",
        red: "#ff0000",
        rosybrown: "#bc8f8f",
        royalblue: "#4169e1",
        saddlebrown: "#8b4513",
        salmon: "#fa8072",
        sandybrown: "#f4a460",
        seagreen: "#2e8b57",
        seashell: "#fff5ee",
        sienna: "#a0522d",
        silver: "#c0c0c0",
        skyblue: "#87ceeb",
        slateblue: "#6a5acd",
        slategray: "#708090",
        slategrey: "#708090",
        snow: "#fffafa",
        springgreen: "#00ff7f",
        steelblue: "#4682b4",
        tan: "#d2b48c",
        teal: "#008080",
        thistle: "#d8bfd8",
        tomato: "#ff6347",
        turquoise: "#40e0d0",
        violet: "#ee82ee",
        wheat: "#f5deb3",
        white: "#ffffff",
        whitesmoke: "#f5f5f5",
        yellow: "#ffff00",
        yellowgreen: "#9acd32"
    };
}(require("./tree")), function(a) {
    a.debugInfo = function(b, c, d) {
        var e = "";
        if (b.dumpLineNumbers && !b.compress) switch (b.dumpLineNumbers) {
          case "comments":
            e = a.debugInfo.asComment(c);
            break;

          case "mediaquery":
            e = a.debugInfo.asMediaQuery(c);
            break;

          case "all":
            e = a.debugInfo.asComment(c) + (d || "") + a.debugInfo.asMediaQuery(c);
        }
        return e;
    }, a.debugInfo.asComment = function(a) {
        return "/* line " + a.debugInfo.lineNumber + ", " + a.debugInfo.fileName + " */\n";
    }, a.debugInfo.asMediaQuery = function(a) {
        return "@media -sass-debug-info{filename{font-family:" + ("file://" + a.debugInfo.fileName).replace(/([.:/\\])/g, function(a) {
            return "\\" == a && (a = "/"), "\\" + a;
        }) + "}line{font-family:\\00003" + a.debugInfo.lineNumber + "}}\n";
    }, a.find = function(a, b) {
        for (var c, d = 0; d < a.length; d++) if (c = b.call(a, a[d])) return c;
        return null;
    }, a.jsify = function(a) {
        return Array.isArray(a.value) && a.value.length > 1 ? "[" + a.value.map(function(a) {
            return a.toCSS(!1);
        }).join(", ") + "]" : a.toCSS(!1);
    }, a.toCSS = function(a) {
        var b = [];
        return this.genCSS(a, {
            add: function(a) {
                b.push(a);
            },
            isEmpty: function() {
                return 0 === b.length;
            }
        }), b.join("");
    }, a.outputRuleset = function(a, b, c) {
        b.add(a.compress ? "{" : " {\n"), a.tabLevel = (a.tabLevel || 0) + 1;
        for (var d = a.compress ? "" : Array(a.tabLevel + 1).join("  "), e = a.compress ? "" : Array(a.tabLevel).join("  "), f = 0; f < c.length; f++) b.add(d), 
        c[f].genCSS(a, b), b.add(a.compress ? "" : "\n");
        a.tabLevel--, b.add(e + "}");
    };
}(require("./tree")), function(a) {
    a.Alpha = function(a) {
        this.value = a;
    }, a.Alpha.prototype = {
        type: "Alpha",
        accept: function(a) {
            this.value = a.visit(this.value);
        },
        eval: function(b) {
            return this.value.eval ? new a.Alpha(this.value.eval(b)) : this;
        },
        genCSS: function(a, b) {
            b.add("alpha(opacity="), this.value.genCSS ? this.value.genCSS(a, b) : b.add(this.value), 
            b.add(")");
        },
        toCSS: a.toCSS
    };
}(require("../tree")), function(a) {
    a.Anonymous = function(a, b, c, d) {
        this.value = a.value || a, this.index = b, this.mapLines = d, this.currentFileInfo = c;
    }, a.Anonymous.prototype = {
        type: "Anonymous",
        eval: function() {
            return this;
        },
        compare: function(a) {
            if (!a.toCSS) return -1;
            var b = this.toCSS(), c = a.toCSS();
            return b === c ? 0 : c > b ? -1 : 1;
        },
        genCSS: function(a, b) {
            b.add(this.value, this.currentFileInfo, this.index, this.mapLines);
        },
        toCSS: a.toCSS
    };
}(require("../tree")), function(a) {
    a.Assignment = function(a, b) {
        this.key = a, this.value = b;
    }, a.Assignment.prototype = {
        type: "Assignment",
        accept: function(a) {
            this.value = a.visit(this.value);
        },
        eval: function(b) {
            return this.value.eval ? new a.Assignment(this.key, this.value.eval(b)) : this;
        },
        genCSS: function(a, b) {
            b.add(this.key + "="), this.value.genCSS ? this.value.genCSS(a, b) : b.add(this.value);
        },
        toCSS: a.toCSS
    };
}(require("../tree")), function(a) {
    a.Call = function(a, b, c, d) {
        this.name = a, this.args = b, this.index = c, this.currentFileInfo = d;
    }, a.Call.prototype = {
        type: "Call",
        accept: function(a) {
            this.args = a.visit(this.args);
        },
        eval: function(b) {
            var c, d, e = this.args.map(function(a) {
                return a.eval(b);
            }), f = this.name.toLowerCase();
            if (f in a.functions) try {
                if (d = new a.functionCall(b, this.currentFileInfo), c = d[f].apply(d, e), null != c) return c;
            } catch (g) {
                throw {
                    type: g.type || "Runtime",
                    message: "error evaluating function `" + this.name + "`" + (g.message ? ": " + g.message : ""),
                    index: this.index,
                    filename: this.currentFileInfo.filename
                };
            }
            return new a.Call(this.name, e, this.index, this.currentFileInfo);
        },
        genCSS: function(a, b) {
            b.add(this.name + "(", this.currentFileInfo, this.index);
            for (var c = 0; c < this.args.length; c++) this.args[c].genCSS(a, b), c + 1 < this.args.length && b.add(", ");
            b.add(")");
        },
        toCSS: a.toCSS
    };
}(require("../tree")), function(a) {
    a.Color = function(a, b) {
        this.rgb = Array.isArray(a) ? a : 6 == a.length ? a.match(/.{2}/g).map(function(a) {
            return parseInt(a, 16);
        }) : a.split("").map(function(a) {
            return parseInt(a + a, 16);
        }), this.alpha = "number" == typeof b ? b : 1;
    };
    var b = "transparent";
    a.Color.prototype = {
        type: "Color",
        eval: function() {
            return this;
        },
        luma: function() {
            return .2126 * this.rgb[0] / 255 + .7152 * this.rgb[1] / 255 + .0722 * this.rgb[2] / 255;
        },
        genCSS: function(a, b) {
            b.add(this.toCSS(a));
        },
        toCSS: function(a, c) {
            var d = a && a.compress && !c;
            if (this.alpha < 1) return 0 === this.alpha && this.isTransparentKeyword ? b : "rgba(" + this.rgb.map(function(a) {
                return Math.round(a);
            }).concat(this.alpha).join("," + (d ? "" : " ")) + ")";
            var e = this.toRGB();
            if (d) {
                var f = e.split("");
                f[1] === f[2] && f[3] === f[4] && f[5] === f[6] && (e = "#" + f[1] + f[3] + f[5]);
            }
            return e;
        },
        operate: function(b, c, d) {
            var e = [];
            d instanceof a.Color || (d = d.toColor());
            for (var f = 0; 3 > f; f++) e[f] = a.operate(b, c, this.rgb[f], d.rgb[f]);
            return new a.Color(e, this.alpha + d.alpha);
        },
        toRGB: function() {
            return "#" + this.rgb.map(function(a) {
                return a = Math.round(a), a = (a > 255 ? 255 : 0 > a ? 0 : a).toString(16), 1 === a.length ? "0" + a : a;
            }).join("");
        },
        toHSL: function() {
            var a, b, c = this.rgb[0] / 255, d = this.rgb[1] / 255, e = this.rgb[2] / 255, f = this.alpha, g = Math.max(c, d, e), h = Math.min(c, d, e), i = (g + h) / 2, j = g - h;
            if (g === h) a = b = 0; else {
                switch (b = i > .5 ? j / (2 - g - h) : j / (g + h), g) {
                  case c:
                    a = (d - e) / j + (e > d ? 6 : 0);
                    break;

                  case d:
                    a = (e - c) / j + 2;
                    break;

                  case e:
                    a = (c - d) / j + 4;
                }
                a /= 6;
            }
            return {
                h: 360 * a,
                s: b,
                l: i,
                a: f
            };
        },
        toHSV: function() {
            var a, b, c = this.rgb[0] / 255, d = this.rgb[1] / 255, e = this.rgb[2] / 255, f = this.alpha, g = Math.max(c, d, e), h = Math.min(c, d, e), i = g, j = g - h;
            if (b = 0 === g ? 0 : j / g, g === h) a = 0; else {
                switch (g) {
                  case c:
                    a = (d - e) / j + (e > d ? 6 : 0);
                    break;

                  case d:
                    a = (e - c) / j + 2;
                    break;

                  case e:
                    a = (c - d) / j + 4;
                }
                a /= 6;
            }
            return {
                h: 360 * a,
                s: b,
                v: i,
                a: f
            };
        },
        toARGB: function() {
            var a = [ Math.round(255 * this.alpha) ].concat(this.rgb);
            return "#" + a.map(function(a) {
                return a = Math.round(a), a = (a > 255 ? 255 : 0 > a ? 0 : a).toString(16), 1 === a.length ? "0" + a : a;
            }).join("");
        },
        compare: function(a) {
            return a.rgb ? a.rgb[0] === this.rgb[0] && a.rgb[1] === this.rgb[1] && a.rgb[2] === this.rgb[2] && a.alpha === this.alpha ? 0 : -1 : -1;
        }
    }, a.Color.fromKeyword = function(c) {
        if (a.colors.hasOwnProperty(c)) return new a.Color(a.colors[c].slice(1));
        if (c === b) {
            var d = new a.Color([ 0, 0, 0 ], 0);
            return d.isTransparentKeyword = !0, d;
        }
    };
}(require("../tree")), function(a) {
    a.Comment = function(a, b, c, d) {
        this.value = a, this.silent = !!b, this.currentFileInfo = d;
    }, a.Comment.prototype = {
        type: "Comment",
        genCSS: function(b, c) {
            this.debugInfo && c.add(a.debugInfo(b, this), this.currentFileInfo, this.index), 
            c.add(this.value.trim());
        },
        toCSS: a.toCSS,
        isSilent: function(a) {
            var b = this.currentFileInfo && this.currentFileInfo.reference && !this.isReferenced, c = a.compress && !this.value.match(/^\/\*!/);
            return this.silent || b || c;
        },
        eval: function() {
            return this;
        },
        markReferenced: function() {
            this.isReferenced = !0;
        }
    };
}(require("../tree")), function(a) {
    a.Condition = function(a, b, c, d, e) {
        this.op = a.trim(), this.lvalue = b, this.rvalue = c, this.index = d, this.negate = e;
    }, a.Condition.prototype = {
        type: "Condition",
        accept: function(a) {
            this.lvalue = a.visit(this.lvalue), this.rvalue = a.visit(this.rvalue);
        },
        eval: function(a) {
            var b, c = this.lvalue.eval(a), d = this.rvalue.eval(a), e = this.index;
            return b = function(a) {
                switch (a) {
                  case "and":
                    return c && d;

                  case "or":
                    return c || d;

                  default:
                    if (c.compare) b = c.compare(d); else {
                        if (!d.compare) throw {
                            type: "Type",
                            message: "Unable to perform comparison",
                            index: e
                        };
                        b = d.compare(c);
                    }
                    switch (b) {
                      case -1:
                        return "<" === a || "=<" === a || "<=" === a;

                      case 0:
                        return "=" === a || ">=" === a || "=<" === a || "<=" === a;

                      case 1:
                        return ">" === a || ">=" === a;
                    }
                }
            }(this.op), this.negate ? !b : b;
        }
    };
}(require("../tree")), function(a) {
    a.Dimension = function(b, c) {
        this.value = parseFloat(b), this.unit = c && c instanceof a.Unit ? c : new a.Unit(c ? [ c ] : void 0);
    }, a.Dimension.prototype = {
        type: "Dimension",
        accept: function(a) {
            this.unit = a.visit(this.unit);
        },
        eval: function() {
            return this;
        },
        toColor: function() {
            return new a.Color([ this.value, this.value, this.value ]);
        },
        genCSS: function(a, b) {
            if (a && a.strictUnits && !this.unit.isSingular()) throw new Error("Multiple units in dimension. Correct the units or use the unit function. Bad unit: " + this.unit.toString());
            var c = this.value, d = String(c);
            if (0 !== c && 1e-6 > c && c > -1e-6 && (d = c.toFixed(20).replace(/0+$/, "")), 
            a && a.compress) {
                if (0 === c && this.unit.isLength()) return b.add(d), void 0;
                c > 0 && 1 > c && (d = d.substr(1));
            }
            b.add(d), this.unit.genCSS(a, b);
        },
        toCSS: a.toCSS,
        operate: function(b, c, d) {
            var e = a.operate(b, c, this.value, d.value), f = this.unit.clone();
            if ("+" === c || "-" === c) if (0 === f.numerator.length && 0 === f.denominator.length) f.numerator = d.unit.numerator.slice(0), 
            f.denominator = d.unit.denominator.slice(0); else if (0 === d.unit.numerator.length && 0 === f.denominator.length) ; else {
                if (d = d.convertTo(this.unit.usedUnits()), b.strictUnits && d.unit.toString() !== f.toString()) throw new Error("Incompatible units. Change the units or use the unit function. Bad units: '" + f.toString() + "' and '" + d.unit.toString() + "'.");
                e = a.operate(b, c, this.value, d.value);
            } else "*" === c ? (f.numerator = f.numerator.concat(d.unit.numerator).sort(), f.denominator = f.denominator.concat(d.unit.denominator).sort(), 
            f.cancel()) : "/" === c && (f.numerator = f.numerator.concat(d.unit.denominator).sort(), 
            f.denominator = f.denominator.concat(d.unit.numerator).sort(), f.cancel());
            return new a.Dimension(e, f);
        },
        compare: function(b) {
            if (b instanceof a.Dimension) {
                var c = this.unify(), d = b.unify(), e = c.value, f = d.value;
                return f > e ? -1 : e > f ? 1 : d.unit.isEmpty() || 0 === c.unit.compare(d.unit) ? 0 : -1;
            }
            return -1;
        },
        unify: function() {
            return this.convertTo({
                length: "m",
                duration: "s",
                angle: "rad"
            });
        },
        convertTo: function(b) {
            var c, d, e, f, g, h = this.value, i = this.unit.clone(), j = {};
            if ("string" == typeof b) {
                for (c in a.UnitConversions) a.UnitConversions[c].hasOwnProperty(b) && (j = {}, 
                j[c] = b);
                b = j;
            }
            g = function(a, b) {
                return e.hasOwnProperty(a) ? (b ? h /= e[a] / e[f] : h *= e[a] / e[f], f) : a;
            };
            for (d in b) b.hasOwnProperty(d) && (f = b[d], e = a.UnitConversions[d], i.map(g));
            return i.cancel(), new a.Dimension(h, i);
        }
    }, a.UnitConversions = {
        length: {
            m: 1,
            cm: .01,
            mm: .001,
            "in": .0254,
            pt: .0254 / 72,
            pc: 12 * (.0254 / 72)
        },
        duration: {
            s: 1,
            ms: .001
        },
        angle: {
            rad: 1 / (2 * Math.PI),
            deg: 1 / 360,
            grad: .0025,
            turn: 1
        }
    }, a.Unit = function(a, b, c) {
        this.numerator = a ? a.slice(0).sort() : [], this.denominator = b ? b.slice(0).sort() : [], 
        this.backupUnit = c;
    }, a.Unit.prototype = {
        type: "Unit",
        clone: function() {
            return new a.Unit(this.numerator.slice(0), this.denominator.slice(0), this.backupUnit);
        },
        genCSS: function(a, b) {
            this.numerator.length >= 1 ? b.add(this.numerator[0]) : this.denominator.length >= 1 ? b.add(this.denominator[0]) : a && a.strictUnits || !this.backupUnit || b.add(this.backupUnit);
        },
        toCSS: a.toCSS,
        toString: function() {
            var a, b = this.numerator.join("*");
            for (a = 0; a < this.denominator.length; a++) b += "/" + this.denominator[a];
            return b;
        },
        compare: function(a) {
            return this.is(a.toString()) ? 0 : -1;
        },
        is: function(a) {
            return this.toString() === a;
        },
        isLength: function() {
            return Boolean(this.toCSS().match(/px|em|%|in|cm|mm|pc|pt|ex/));
        },
        isEmpty: function() {
            return 0 === this.numerator.length && 0 === this.denominator.length;
        },
        isSingular: function() {
            return this.numerator.length <= 1 && 0 === this.denominator.length;
        },
        map: function(a) {
            var b;
            for (b = 0; b < this.numerator.length; b++) this.numerator[b] = a(this.numerator[b], !1);
            for (b = 0; b < this.denominator.length; b++) this.denominator[b] = a(this.denominator[b], !0);
        },
        usedUnits: function() {
            var b, c, d = {};
            c = function(a) {
                return b.hasOwnProperty(a) && !d[e] && (d[e] = a), a;
            };
            for (var e in a.UnitConversions) a.UnitConversions.hasOwnProperty(e) && (b = a.UnitConversions[e], 
            this.map(c));
            return d;
        },
        cancel: function() {
            var a, b, c, d = {};
            for (b = 0; b < this.numerator.length; b++) a = this.numerator[b], c || (c = a), 
            d[a] = (d[a] || 0) + 1;
            for (b = 0; b < this.denominator.length; b++) a = this.denominator[b], c || (c = a), 
            d[a] = (d[a] || 0) - 1;
            this.numerator = [], this.denominator = [];
            for (a in d) if (d.hasOwnProperty(a)) {
                var e = d[a];
                if (e > 0) for (b = 0; e > b; b++) this.numerator.push(a); else if (0 > e) for (b = 0; -e > b; b++) this.denominator.push(a);
            }
            0 === this.numerator.length && 0 === this.denominator.length && c && (this.backupUnit = c), 
            this.numerator.sort(), this.denominator.sort();
        }
    };
}(require("../tree")), function(a) {
    a.Directive = function(b, c, d, e) {
        this.name = b, Array.isArray(c) ? (this.rules = [ new a.Ruleset([], c) ], this.rules[0].allowImports = !0) : this.value = c, 
        this.currentFileInfo = e;
    }, a.Directive.prototype = {
        type: "Directive",
        accept: function(a) {
            this.rules = a.visit(this.rules), this.value = a.visit(this.value);
        },
        genCSS: function(b, c) {
            c.add(this.name, this.currentFileInfo, this.index), this.rules ? a.outputRuleset(b, c, this.rules) : (c.add(" "), 
            this.value.genCSS(b, c), c.add(";"));
        },
        toCSS: a.toCSS,
        eval: function(b) {
            var c = this;
            return this.rules && (b.frames.unshift(this), c = new a.Directive(this.name, null, this.index, this.currentFileInfo), 
            c.rules = [ this.rules[0].eval(b) ], c.rules[0].root = !0, b.frames.shift()), c;
        },
        variable: function(b) {
            return a.Ruleset.prototype.variable.call(this.rules[0], b);
        },
        find: function() {
            return a.Ruleset.prototype.find.apply(this.rules[0], arguments);
        },
        rulesets: function() {
            return a.Ruleset.prototype.rulesets.apply(this.rules[0]);
        },
        markReferenced: function() {
            var a, b;
            if (this.isReferenced = !0, this.rules) for (b = this.rules[0].rules, a = 0; a < b.length; a++) b[a].markReferenced && b[a].markReferenced();
        }
    };
}(require("../tree")), function(a) {
    a.Element = function(b, c, d, e) {
        this.combinator = b instanceof a.Combinator ? b : new a.Combinator(b), this.value = "string" == typeof c ? c.trim() : c ? c : "", 
        this.index = d, this.currentFileInfo = e;
    }, a.Element.prototype = {
        type: "Element",
        accept: function(a) {
            this.combinator = a.visit(this.combinator), this.value = a.visit(this.value);
        },
        eval: function(b) {
            return new a.Element(this.combinator, this.value.eval ? this.value.eval(b) : this.value, this.index, this.currentFileInfo);
        },
        genCSS: function(a, b) {
            b.add(this.toCSS(a), this.currentFileInfo, this.index);
        },
        toCSS: function(a) {
            var b = this.value.toCSS ? this.value.toCSS(a) : this.value;
            return "" === b && "&" === this.combinator.value.charAt(0) ? "" : this.combinator.toCSS(a || {}) + b;
        }
    }, a.Attribute = function(a, b, c) {
        this.key = a, this.op = b, this.value = c;
    }, a.Attribute.prototype = {
        type: "Attribute",
        accept: function(a) {
            this.value = a.visit(this.value);
        },
        eval: function(b) {
            return new a.Attribute(this.key.eval ? this.key.eval(b) : this.key, this.op, this.value && this.value.eval ? this.value.eval(b) : this.value);
        },
        genCSS: function(a, b) {
            b.add(this.toCSS(a));
        },
        toCSS: function(a) {
            var b = this.key.toCSS ? this.key.toCSS(a) : this.key;
            return this.op && (b += this.op, b += this.value.toCSS ? this.value.toCSS(a) : this.value), 
            "[" + b + "]";
        }
    }, a.Combinator = function(a) {
        this.value = " " === a ? " " : a ? a.trim() : "";
    }, a.Combinator.prototype = {
        type: "Combinator",
        _outputMap: {
            "": "",
            " ": " ",
            ":": " :",
            "+": " + ",
            "~": " ~ ",
            ">": " > ",
            "|": "|"
        },
        _outputMapCompressed: {
            "": "",
            " ": " ",
            ":": " :",
            "+": "+",
            "~": "~",
            ">": ">",
            "|": "|"
        },
        genCSS: function(a, b) {
            b.add((a.compress ? this._outputMapCompressed : this._outputMap)[this.value]);
        },
        toCSS: a.toCSS
    };
}(require("../tree")), function(a) {
    a.Expression = function(a) {
        this.value = a;
    }, a.Expression.prototype = {
        type: "Expression",
        accept: function(a) {
            this.value = a.visit(this.value);
        },
        eval: function(b) {
            var c, d = this.parens && !this.parensInOp, e = !1;
            return d && b.inParenthesis(), this.value.length > 1 ? c = new a.Expression(this.value.map(function(a) {
                return a.eval(b);
            })) : 1 === this.value.length ? (this.value[0].parens && !this.value[0].parensInOp && (e = !0), 
            c = this.value[0].eval(b)) : c = this, d && b.outOfParenthesis(), this.parens && this.parensInOp && !b.isMathOn() && !e && (c = new a.Paren(c)), 
            c;
        },
        genCSS: function(a, b) {
            for (var c = 0; c < this.value.length; c++) this.value[c].genCSS(a, b), c + 1 < this.value.length && b.add(" ");
        },
        toCSS: a.toCSS,
        throwAwayComments: function() {
            this.value = this.value.filter(function(b) {
                return !(b instanceof a.Comment);
            });
        }
    };
}(require("../tree")), function(a) {
    a.Extend = function(a, b, c) {
        switch (this.selector = a, this.option = b, this.index = c, b) {
          case "all":
            this.allowBefore = !0, this.allowAfter = !0;
            break;

          default:
            this.allowBefore = !1, this.allowAfter = !1;
        }
    }, a.Extend.prototype = {
        type: "Extend",
        accept: function(a) {
            this.selector = a.visit(this.selector);
        },
        eval: function(b) {
            return new a.Extend(this.selector.eval(b), this.option, this.index);
        },
        clone: function() {
            return new a.Extend(this.selector, this.option, this.index);
        },
        findSelfSelectors: function(a) {
            var b, c, d = [];
            for (b = 0; b < a.length; b++) c = a[b].elements, b > 0 && c.length && "" === c[0].combinator.value && (c[0].combinator.value = " "), 
            d = d.concat(a[b].elements);
            this.selfSelectors = [ {
                elements: d
            } ];
        }
    };
}(require("../tree")), function(a) {
    a.Import = function(a, b, c, d, e) {
        if (this.options = c, this.index = d, this.path = a, this.features = b, this.currentFileInfo = e, 
        void 0 !== this.options.less || this.options.inline) this.css = !this.options.less || this.options.inline; else {
            var f = this.getPath();
            f && /css([\?;].*)?$/.test(f) && (this.css = !0);
        }
    }, a.Import.prototype = {
        type: "Import",
        accept: function(a) {
            this.features = a.visit(this.features), this.path = a.visit(this.path), this.options.inline || (this.root = a.visit(this.root));
        },
        genCSS: function(a, b) {
            this.css && (b.add("@import ", this.currentFileInfo, this.index), this.path.genCSS(a, b), 
            this.features && (b.add(" "), this.features.genCSS(a, b)), b.add(";"));
        },
        toCSS: a.toCSS,
        getPath: function() {
            if (this.path instanceof a.Quoted) {
                var b = this.path.value;
                return void 0 !== this.css || /(\.[a-z]*$)|([\?;].*)$/.test(b) ? b : b + ".less";
            }
            return this.path instanceof a.URL ? this.path.value.value : null;
        },
        evalForImport: function(b) {
            return new a.Import(this.path.eval(b), this.features, this.options, this.index, this.currentFileInfo);
        },
        evalPath: function(b) {
            var c = this.path.eval(b), d = this.currentFileInfo && this.currentFileInfo.rootpath;
            if (!(c instanceof a.URL)) {
                if (d) {
                    var e = c.value;
                    e && b.isPathRelative(e) && (c.value = d + e);
                }
                c.value = b.normalizePath(c.value);
            }
            return c;
        },
        eval: function(b) {
            var c, d = this.features && this.features.eval(b);
            if (this.skip) return [];
            if (this.options.inline) {
                var e = new a.Anonymous(this.root, 0, {
                    filename: this.importedFilename
                }, !0);
                return this.features ? new a.Media([ e ], this.features.value) : [ e ];
            }
            if (this.css) {
                var f = new a.Import(this.evalPath(b), d, this.options, this.index);
                if (!f.css && this.error) throw this.error;
                return f;
            }
            return c = new a.Ruleset([], this.root.rules.slice(0)), c.evalImports(b), this.features ? new a.Media(c.rules, this.features.value) : c.rules;
        }
    };
}(require("../tree")), function(a) {
    a.JavaScript = function(a, b, c) {
        this.escaped = c, this.expression = a, this.index = b;
    }, a.JavaScript.prototype = {
        type: "JavaScript",
        eval: function(b) {
            var c, d = this, e = {}, f = this.expression.replace(/@\{([\w-]+)\}/g, function(c, e) {
                return a.jsify(new a.Variable("@" + e, d.index).eval(b));
            });
            try {
                f = new Function("return (" + f + ")");
            } catch (g) {
                throw {
                    message: "JavaScript evaluation error: " + g.message + " from `" + f + "`",
                    index: this.index
                };
            }
            for (var h in b.frames[0].variables()) e[h.slice(1)] = {
                value: b.frames[0].variables()[h].value,
                toJS: function() {
                    return this.value.eval(b).toCSS();
                }
            };
            try {
                c = f.call(e);
            } catch (g) {
                throw {
                    message: "JavaScript evaluation error: '" + g.name + ": " + g.message + "'",
                    index: this.index
                };
            }
            return "string" == typeof c ? new a.Quoted('"' + c + '"', c, this.escaped, this.index) : Array.isArray(c) ? new a.Anonymous(c.join(", ")) : new a.Anonymous(c);
        }
    };
}(require("../tree")), function(a) {
    a.Keyword = function(a) {
        this.value = a;
    }, a.Keyword.prototype = {
        type: "Keyword",
        eval: function() {
            return this;
        },
        genCSS: function(a, b) {
            b.add(this.value);
        },
        toCSS: a.toCSS,
        compare: function(b) {
            return b instanceof a.Keyword ? b.value === this.value ? 0 : 1 : -1;
        }
    }, a.True = new a.Keyword("true"), a.False = new a.Keyword("false");
}(require("../tree")), function(a) {
    a.Media = function(b, c, d, e) {
        this.index = d, this.currentFileInfo = e;
        var f = this.emptySelectors();
        this.features = new a.Value(c), this.rules = [ new a.Ruleset(f, b) ], this.rules[0].allowImports = !0;
    }, a.Media.prototype = {
        type: "Media",
        accept: function(a) {
            this.features = a.visit(this.features), this.rules = a.visit(this.rules);
        },
        genCSS: function(b, c) {
            c.add("@media ", this.currentFileInfo, this.index), this.features.genCSS(b, c), 
            a.outputRuleset(b, c, this.rules);
        },
        toCSS: a.toCSS,
        eval: function(b) {
            b.mediaBlocks || (b.mediaBlocks = [], b.mediaPath = []);
            var c = new a.Media([], [], this.index, this.currentFileInfo);
            this.debugInfo && (this.rules[0].debugInfo = this.debugInfo, c.debugInfo = this.debugInfo);
            var d = !1;
            b.strictMath || (d = !0, b.strictMath = !0);
            try {
                c.features = this.features.eval(b);
            } finally {
                d && (b.strictMath = !1);
            }
            return b.mediaPath.push(c), b.mediaBlocks.push(c), b.frames.unshift(this.rules[0]), 
            c.rules = [ this.rules[0].eval(b) ], b.frames.shift(), b.mediaPath.pop(), 0 === b.mediaPath.length ? c.evalTop(b) : c.evalNested(b);
        },
        variable: function(b) {
            return a.Ruleset.prototype.variable.call(this.rules[0], b);
        },
        find: function() {
            return a.Ruleset.prototype.find.apply(this.rules[0], arguments);
        },
        rulesets: function() {
            return a.Ruleset.prototype.rulesets.apply(this.rules[0]);
        },
        emptySelectors: function() {
            var b = new a.Element("", "&", this.index, this.currentFileInfo);
            return [ new a.Selector([ b ], null, null, this.index, this.currentFileInfo) ];
        },
        markReferenced: function() {
            var a, b = this.rules[0].rules;
            for (this.isReferenced = !0, a = 0; a < b.length; a++) b[a].markReferenced && b[a].markReferenced();
        },
        evalTop: function(b) {
            var c = this;
            if (b.mediaBlocks.length > 1) {
                var d = this.emptySelectors();
                c = new a.Ruleset(d, b.mediaBlocks), c.multiMedia = !0;
            }
            return delete b.mediaBlocks, delete b.mediaPath, c;
        },
        evalNested: function(b) {
            var c, d, e = b.mediaPath.concat([ this ]);
            for (c = 0; c < e.length; c++) d = e[c].features instanceof a.Value ? e[c].features.value : e[c].features, 
            e[c] = Array.isArray(d) ? d : [ d ];
            return this.features = new a.Value(this.permute(e).map(function(b) {
                for (b = b.map(function(b) {
                    return b.toCSS ? b : new a.Anonymous(b);
                }), c = b.length - 1; c > 0; c--) b.splice(c, 0, new a.Anonymous("and"));
                return new a.Expression(b);
            })), new a.Ruleset([], []);
        },
        permute: function(a) {
            if (0 === a.length) return [];
            if (1 === a.length) return a[0];
            for (var b = [], c = this.permute(a.slice(1)), d = 0; d < c.length; d++) for (var e = 0; e < a[0].length; e++) b.push([ a[0][e] ].concat(c[d]));
            return b;
        },
        bubbleSelectors: function(b) {
            this.rules = [ new a.Ruleset(b.slice(0), [ this.rules[0] ]) ];
        }
    };
}(require("../tree")), function(a) {
    a.mixin = {}, a.mixin.Call = function(b, c, d, e, f) {
        this.selector = new a.Selector(b), this.arguments = c, this.index = d, this.currentFileInfo = e, 
        this.important = f;
    }, a.mixin.Call.prototype = {
        type: "MixinCall",
        accept: function(a) {
            this.selector = a.visit(this.selector), this.arguments = a.visit(this.arguments);
        },
        eval: function(b) {
            var c, d, e, f, g, h, i, j, k, l = [], m = !1;
            for (e = this.arguments && this.arguments.map(function(a) {
                return {
                    name: a.name,
                    value: a.value.eval(b)
                };
            }), f = 0; f < b.frames.length; f++) if ((c = b.frames[f].find(this.selector)).length > 0) {
                for (j = !0, g = 0; g < c.length; g++) {
                    for (d = c[g], i = !1, h = 0; h < b.frames.length; h++) if (!(d instanceof a.mixin.Definition) && d === (b.frames[h].originalRuleset || b.frames[h])) {
                        i = !0;
                        break;
                    }
                    if (!i && d.matchArgs(e, b)) {
                        if (!d.matchCondition || d.matchCondition(e, b)) try {
                            d instanceof a.mixin.Definition || (d = new a.mixin.Definition("", [], d.rules, null, !1), 
                            d.originalRuleset = c[g].originalRuleset || c[g]), Array.prototype.push.apply(l, d.eval(b, e, this.important).rules);
                        } catch (n) {
                            throw {
                                message: n.message,
                                index: this.index,
                                filename: this.currentFileInfo.filename,
                                stack: n.stack
                            };
                        }
                        m = !0;
                    }
                }
                if (m) {
                    if (!this.currentFileInfo || !this.currentFileInfo.reference) for (f = 0; f < l.length; f++) k = l[f], 
                    k.markReferenced && k.markReferenced();
                    return l;
                }
            }
            throw j ? {
                type: "Runtime",
                message: "No matching definition was found for `" + this.selector.toCSS().trim() + "(" + (e ? e.map(function(a) {
                    var b = "";
                    return a.name && (b += a.name + ":"), b += a.value.toCSS ? a.value.toCSS() : "???";
                }).join(", ") : "") + ")`",
                index: this.index,
                filename: this.currentFileInfo.filename
            } : {
                type: "Name",
                message: this.selector.toCSS().trim() + " is undefined",
                index: this.index,
                filename: this.currentFileInfo.filename
            };
        }
    }, a.mixin.Definition = function(b, c, d, e, f) {
        this.name = b, this.selectors = [ new a.Selector([ new a.Element(null, b, this.index, this.currentFileInfo) ]) ], 
        this.params = c, this.condition = e, this.variadic = f, this.arity = c.length, this.rules = d, 
        this._lookups = {}, this.required = c.reduce(function(a, b) {
            return !b.name || b.name && !b.value ? a + 1 : a;
        }, 0), this.parent = a.Ruleset.prototype, this.frames = [];
    }, a.mixin.Definition.prototype = {
        type: "MixinDefinition",
        accept: function(a) {
            this.params = a.visit(this.params), this.rules = a.visit(this.rules), this.condition = a.visit(this.condition);
        },
        variable: function(a) {
            return this.parent.variable.call(this, a);
        },
        variables: function() {
            return this.parent.variables.call(this);
        },
        find: function() {
            return this.parent.find.apply(this, arguments);
        },
        rulesets: function() {
            return this.parent.rulesets.apply(this);
        },
        evalParams: function(b, c, d, e) {
            var f, g, h, i, j, k, l, m, n = new a.Ruleset(null, []), o = this.params.slice(0);
            if (c = new a.evalEnv(c, [ n ].concat(c.frames)), d) for (d = d.slice(0), h = 0; h < d.length; h++) if (g = d[h], 
            k = g && g.name) {
                for (l = !1, i = 0; i < o.length; i++) if (!e[i] && k === o[i].name) {
                    e[i] = g.value.eval(b), n.rules.unshift(new a.Rule(k, g.value.eval(b))), l = !0;
                    break;
                }
                if (l) {
                    d.splice(h, 1), h--;
                    continue;
                }
                throw {
                    type: "Runtime",
                    message: "Named argument for " + this.name + " " + d[h].name + " not found"
                };
            }
            for (m = 0, h = 0; h < o.length; h++) if (!e[h]) {
                if (g = d && d[m], k = o[h].name) if (o[h].variadic && d) {
                    for (f = [], i = m; i < d.length; i++) f.push(d[i].value.eval(b));
                    n.rules.unshift(new a.Rule(k, new a.Expression(f).eval(b)));
                } else {
                    if (j = g && g.value) j = j.eval(b); else {
                        if (!o[h].value) throw {
                            type: "Runtime",
                            message: "wrong number of arguments for " + this.name + " (" + d.length + " for " + this.arity + ")"
                        };
                        j = o[h].value.eval(c), n.resetCache();
                    }
                    n.rules.unshift(new a.Rule(k, j)), e[h] = j;
                }
                if (o[h].variadic && d) for (i = m; i < d.length; i++) e[i] = d[i].value.eval(b);
                m++;
            }
            return n;
        },
        eval: function(b, c, d) {
            var e, f, g = [], h = this.frames.concat(b.frames), i = this.evalParams(b, new a.evalEnv(b, h), c, g);
            return i.rules.unshift(new a.Rule("@arguments", new a.Expression(g).eval(b))), e = this.rules.slice(0), 
            f = new a.Ruleset(null, e), f.originalRuleset = this, f = f.eval(new a.evalEnv(b, [ this, i ].concat(h))), 
            d && (f = this.parent.makeImportant.apply(f)), f;
        },
        matchCondition: function(b, c) {
            return this.condition && !this.condition.eval(new a.evalEnv(c, [ this.evalParams(c, new a.evalEnv(c, this.frames.concat(c.frames)), b, []) ].concat(this.frames).concat(c.frames))) ? !1 : !0;
        },
        matchArgs: function(a, b) {
            var c, d = a && a.length || 0;
            if (this.variadic) {
                if (d < this.required - 1) return !1;
            } else {
                if (d < this.required) return !1;
                if (d > this.params.length) return !1;
            }
            c = Math.min(d, this.arity);
            for (var e = 0; c > e; e++) if (!this.params[e].name && !this.params[e].variadic && a[e].value.eval(b).toCSS() != this.params[e].value.eval(b).toCSS()) return !1;
            return !0;
        }
    };
}(require("../tree")), function(a) {
    a.Negative = function(a) {
        this.value = a;
    }, a.Negative.prototype = {
        type: "Negative",
        accept: function(a) {
            this.value = a.visit(this.value);
        },
        genCSS: function(a, b) {
            b.add("-"), this.value.genCSS(a, b);
        },
        toCSS: a.toCSS,
        eval: function(b) {
            return b.isMathOn() ? new a.Operation("*", [ new a.Dimension(-1), this.value ]).eval(b) : new a.Negative(this.value.eval(b));
        }
    };
}(require("../tree")), function(a) {
    a.Operation = function(a, b, c) {
        this.op = a.trim(), this.operands = b, this.isSpaced = c;
    }, a.Operation.prototype = {
        type: "Operation",
        accept: function(a) {
            this.operands = a.visit(this.operands);
        },
        eval: function(b) {
            var c, d = this.operands[0].eval(b), e = this.operands[1].eval(b);
            if (b.isMathOn()) {
                if (d instanceof a.Dimension && e instanceof a.Color) {
                    if ("*" !== this.op && "+" !== this.op) throw {
                        type: "Operation",
                        message: "Can't substract or divide a color from a number"
                    };
                    c = e, e = d, d = c;
                }
                if (!d.operate) throw {
                    type: "Operation",
                    message: "Operation on an invalid type"
                };
                return d.operate(b, this.op, e);
            }
            return new a.Operation(this.op, [ d, e ], this.isSpaced);
        },
        genCSS: function(a, b) {
            this.operands[0].genCSS(a, b), this.isSpaced && b.add(" "), b.add(this.op), this.isSpaced && b.add(" "), 
            this.operands[1].genCSS(a, b);
        },
        toCSS: a.toCSS
    }, a.operate = function(a, b, c, d) {
        switch (b) {
          case "+":
            return c + d;

          case "-":
            return c - d;

          case "*":
            return c * d;

          case "/":
            return c / d;
        }
    };
}(require("../tree")), function(a) {
    a.Paren = function(a) {
        this.value = a;
    }, a.Paren.prototype = {
        type: "Paren",
        accept: function(a) {
            this.value = a.visit(this.value);
        },
        genCSS: function(a, b) {
            b.add("("), this.value.genCSS(a, b), b.add(")");
        },
        toCSS: a.toCSS,
        eval: function(b) {
            return new a.Paren(this.value.eval(b));
        }
    };
}(require("../tree")), function(a) {
    a.Quoted = function(a, b, c, d, e) {
        this.escaped = c, this.value = b || "", this.quote = a.charAt(0), this.index = d, 
        this.currentFileInfo = e;
    }, a.Quoted.prototype = {
        type: "Quoted",
        genCSS: function(a, b) {
            this.escaped || b.add(this.quote, this.currentFileInfo, this.index), b.add(this.value), 
            this.escaped || b.add(this.quote);
        },
        toCSS: a.toCSS,
        eval: function(b) {
            var c = this, d = this.value.replace(/`([^`]+)`/g, function(d, e) {
                return new a.JavaScript(e, c.index, !0).eval(b).value;
            }).replace(/@\{([\w-]+)\}/g, function(d, e) {
                var f = new a.Variable("@" + e, c.index, c.currentFileInfo).eval(b, !0);
                return f instanceof a.Quoted ? f.value : f.toCSS();
            });
            return new a.Quoted(this.quote + d + this.quote, d, this.escaped, this.index, this.currentFileInfo);
        },
        compare: function(a) {
            if (!a.toCSS) return -1;
            var b = this.toCSS(), c = a.toCSS();
            return b === c ? 0 : c > b ? -1 : 1;
        }
    };
}(require("../tree")), function(a) {
    a.Rule = function(b, c, d, e, f, g, h) {
        this.name = b, this.value = c instanceof a.Value ? c : new a.Value([ c ]), this.important = d ? " " + d.trim() : "", 
        this.merge = e, this.index = f, this.currentFileInfo = g, this.inline = h || !1, 
        this.variable = "@" === b.charAt(0);
    }, a.Rule.prototype = {
        type: "Rule",
        accept: function(a) {
            this.value = a.visit(this.value);
        },
        genCSS: function(a, b) {
            b.add(this.name + (a.compress ? ":" : ": "), this.currentFileInfo, this.index);
            try {
                this.value.genCSS(a, b);
            } catch (c) {
                throw c.index = this.index, c.filename = this.currentFileInfo.filename, c;
            }
            b.add(this.important + (this.inline || a.lastRule && a.compress ? "" : ";"), this.currentFileInfo, this.index);
        },
        toCSS: a.toCSS,
        eval: function(b) {
            var c = !1;
            "font" !== this.name || b.strictMath || (c = !0, b.strictMath = !0);
            try {
                return new a.Rule(this.name, this.value.eval(b), this.important, this.merge, this.index, this.currentFileInfo, this.inline);
            } finally {
                c && (b.strictMath = !1);
            }
        },
        makeImportant: function() {
            return new a.Rule(this.name, this.value, "!important", this.merge, this.index, this.currentFileInfo, this.inline);
        }
    };
}(require("../tree")), function(a) {
    a.Ruleset = function(a, b, c) {
        this.selectors = a, this.rules = b, this._lookups = {}, this.strictImports = c;
    }, a.Ruleset.prototype = {
        type: "Ruleset",
        accept: function(a) {
            if (this.paths) for (var b = 0; b < this.paths.length; b++) this.paths[b] = a.visit(this.paths[b]); else this.selectors = a.visit(this.selectors);
            this.rules = a.visit(this.rules);
        },
        eval: function(b) {
            var c, d, e, f = this.selectors && this.selectors.map(function(a) {
                return a.eval(b);
            }), g = new a.Ruleset(f, this.rules.slice(0), this.strictImports);
            for (g.originalRuleset = this, g.root = this.root, g.firstRoot = this.firstRoot, 
            g.allowImports = this.allowImports, this.debugInfo && (g.debugInfo = this.debugInfo), 
            b.frames.unshift(g), b.selectors || (b.selectors = []), b.selectors.unshift(this.selectors), 
            (g.root || g.allowImports || !g.strictImports) && g.evalImports(b), e = 0; e < g.rules.length; e++) g.rules[e] instanceof a.mixin.Definition && (g.rules[e].frames = b.frames.slice(0));
            var h = b.mediaBlocks && b.mediaBlocks.length || 0;
            for (e = 0; e < g.rules.length; e++) g.rules[e] instanceof a.mixin.Call && (c = g.rules[e].eval(b).filter(function(b) {
                return b instanceof a.Rule && b.variable ? !g.variable(b.name) : !0;
            }), g.rules.splice.apply(g.rules, [ e, 1 ].concat(c)), e += c.length - 1, g.resetCache());
            for (e = 0; e < g.rules.length; e++) d = g.rules[e], d instanceof a.mixin.Definition || (g.rules[e] = d.eval ? d.eval(b) : d);
            if (b.frames.shift(), b.selectors.shift(), b.mediaBlocks) for (e = h; e < b.mediaBlocks.length; e++) b.mediaBlocks[e].bubbleSelectors(f);
            return g;
        },
        evalImports: function(b) {
            var c, d;
            for (c = 0; c < this.rules.length; c++) this.rules[c] instanceof a.Import && (d = this.rules[c].eval(b), 
            "number" == typeof d.length ? (this.rules.splice.apply(this.rules, [ c, 1 ].concat(d)), 
            c += d.length - 1) : this.rules.splice(c, 1, d), this.resetCache());
        },
        makeImportant: function() {
            return new a.Ruleset(this.selectors, this.rules.map(function(a) {
                return a.makeImportant ? a.makeImportant() : a;
            }), this.strictImports);
        },
        matchArgs: function(a) {
            return !a || 0 === a.length;
        },
        matchCondition: function(b, c) {
            var d = this.selectors[this.selectors.length - 1];
            return d.condition && !d.condition.eval(new a.evalEnv(c, c.frames)) ? !1 : !0;
        },
        resetCache: function() {
            this._rulesets = null, this._variables = null, this._lookups = {};
        },
        variables: function() {
            return this._variables ? this._variables : this._variables = this.rules.reduce(function(b, c) {
                return c instanceof a.Rule && c.variable === !0 && (b[c.name] = c), b;
            }, {});
        },
        variable: function(a) {
            return this.variables()[a];
        },
        rulesets: function() {
            return this.rules.filter(function(b) {
                return b instanceof a.Ruleset || b instanceof a.mixin.Definition;
            });
        },
        find: function(b, c) {
            c = c || this;
            var d, e = [], f = b.toCSS();
            return f in this._lookups ? this._lookups[f] : (this.rulesets().forEach(function(f) {
                if (f !== c) for (var g = 0; g < f.selectors.length; g++) if (d = b.match(f.selectors[g])) {
                    b.elements.length > f.selectors[g].elements.length ? Array.prototype.push.apply(e, f.find(new a.Selector(b.elements.slice(1)), c)) : e.push(f);
                    break;
                }
            }), this._lookups[f] = e);
        },
        genCSS: function(b, c) {
            var d, e, f, g, h, i = [], j = [], k = !0;
            b.tabLevel = b.tabLevel || 0, this.root || b.tabLevel++;
            var l = b.compress ? "" : Array(b.tabLevel + 1).join("  "), m = b.compress ? "" : Array(b.tabLevel).join("  ");
            for (d = 0; d < this.rules.length; d++) g = this.rules[d], g.rules || g instanceof a.Media || g instanceof a.Directive || this.root && g instanceof a.Comment ? j.push(g) : i.push(g);
            if (!this.root) {
                for (f = a.debugInfo(b, this, m), f && (c.add(f), c.add(m)), d = 0; d < this.paths.length; d++) {
                    for (h = this.paths[d], b.firstSelector = !0, e = 0; e < h.length; e++) h[e].genCSS(b, c), 
                    b.firstSelector = !1;
                    d + 1 < this.paths.length && c.add(b.compress ? "," : ",\n" + m);
                }
                c.add((b.compress ? "{" : " {\n") + l);
            }
            for (d = 0; d < i.length; d++) g = i[d], d + 1 !== i.length || this.root && 0 !== j.length && !this.firstRoot || (b.lastRule = !0), 
            g.genCSS ? g.genCSS(b, c) : g.value && c.add(g.value.toString()), b.lastRule ? b.lastRule = !1 : c.add(b.compress ? "" : "\n" + l);
            for (this.root || (c.add(b.compress ? "}" : "\n" + m + "}"), b.tabLevel--), d = 0; d < j.length; d++) i.length && k && c.add((b.compress ? "" : "\n") + (this.root ? l : m)), 
            k || c.add((b.compress ? "" : "\n") + (this.root ? l : m)), k = !1, j[d].genCSS(b, c);
            c.isEmpty() || b.compress || !this.firstRoot || c.add("\n");
        },
        toCSS: a.toCSS,
        markReferenced: function() {
            for (var a = 0; a < this.selectors.length; a++) this.selectors[a].markReferenced();
        },
        joinSelectors: function(a, b, c) {
            for (var d = 0; d < c.length; d++) this.joinSelector(a, b, c[d]);
        },
        joinSelector: function(b, c, d) {
            var e, f, g, h, i, j, k, l, m, n, o, p, q, r, s;
            for (e = 0; e < d.elements.length; e++) j = d.elements[e], "&" === j.value && (h = !0);
            if (h) {
                for (r = [], i = [ [] ], e = 0; e < d.elements.length; e++) if (j = d.elements[e], 
                "&" !== j.value) r.push(j); else {
                    for (s = [], r.length > 0 && this.mergeElementsOnToSelectors(r, i), f = 0; f < i.length; f++) if (k = i[f], 
                    0 === c.length) k.length > 0 && (k[0].elements = k[0].elements.slice(0), k[0].elements.push(new a.Element(j.combinator, "", 0, j.index, j.currentFileInfo))), 
                    s.push(k); else for (g = 0; g < c.length; g++) l = c[g], m = [], n = [], p = !0, 
                    k.length > 0 ? (m = k.slice(0), q = m.pop(), o = d.createDerived(q.elements.slice(0)), 
                    p = !1) : o = d.createDerived([]), l.length > 1 && (n = n.concat(l.slice(1))), l.length > 0 && (p = !1, 
                    o.elements.push(new a.Element(j.combinator, l[0].elements[0].value, j.index, j.currentFileInfo)), 
                    o.elements = o.elements.concat(l[0].elements.slice(1))), p || m.push(o), m = m.concat(n), 
                    s.push(m);
                    i = s, r = [];
                }
                for (r.length > 0 && this.mergeElementsOnToSelectors(r, i), e = 0; e < i.length; e++) i[e].length > 0 && b.push(i[e]);
            } else if (c.length > 0) for (e = 0; e < c.length; e++) b.push(c[e].concat(d)); else b.push([ d ]);
        },
        mergeElementsOnToSelectors: function(b, c) {
            var d, e;
            if (0 === c.length) return c.push([ new a.Selector(b) ]), void 0;
            for (d = 0; d < c.length; d++) e = c[d], e.length > 0 ? e[e.length - 1] = e[e.length - 1].createDerived(e[e.length - 1].elements.concat(b)) : e.push(new a.Selector(b));
        }
    };
}(require("../tree")), function(a) {
    a.Selector = function(a, b, c, d, e, f) {
        this.elements = a, this.extendList = b || [], this.condition = c, this.currentFileInfo = e || {}, 
        this.isReferenced = f, c || (this.evaldCondition = !0);
    }, a.Selector.prototype = {
        type: "Selector",
        accept: function(a) {
            this.elements = a.visit(this.elements), this.extendList = a.visit(this.extendList), 
            this.condition = a.visit(this.condition);
        },
        createDerived: function(b, c, d) {
            d = null != d ? d : this.evaldCondition;
            var e = new a.Selector(b, c || this.extendList, this.condition, this.index, this.currentFileInfo, this.isReferenced);
            return e.evaldCondition = d, e;
        },
        match: function(a) {
            var b, c, d, e, f = this.elements, g = f.length;
            if (b = a.elements.slice(a.elements.length && "&" === a.elements[0].value ? 1 : 0), 
            c = b.length, d = Math.min(g, c), 0 === c || c > g) return !1;
            for (e = 0; d > e; e++) if (f[e].value !== b[e].value) return !1;
            return !0;
        },
        eval: function(a) {
            var b = this.condition && this.condition.eval(a);
            return this.createDerived(this.elements.map(function(b) {
                return b.eval(a);
            }), this.extendList.map(function(b) {
                return b.eval(a);
            }), b);
        },
        genCSS: function(a, b) {
            var c, d;
            if (a && a.firstSelector || "" !== this.elements[0].combinator.value || b.add(" ", this.currentFileInfo, this.index), 
            !this._css) for (c = 0; c < this.elements.length; c++) d = this.elements[c], d.genCSS(a, b);
        },
        toCSS: a.toCSS,
        markReferenced: function() {
            this.isReferenced = !0;
        },
        getIsReferenced: function() {
            return !this.currentFileInfo.reference || this.isReferenced;
        },
        getIsOutput: function() {
            return this.evaldCondition;
        }
    };
}(require("../tree")), function(a) {
    a.UnicodeDescriptor = function(a) {
        this.value = a;
    }, a.UnicodeDescriptor.prototype = {
        type: "UnicodeDescriptor",
        genCSS: function(a, b) {
            b.add(this.value);
        },
        toCSS: a.toCSS,
        eval: function() {
            return this;
        }
    };
}(require("../tree")), function(a) {
    a.URL = function(a, b) {
        this.value = a, this.currentFileInfo = b;
    }, a.URL.prototype = {
        type: "Url",
        accept: function(a) {
            this.value = a.visit(this.value);
        },
        genCSS: function(a, b) {
            b.add("url("), this.value.genCSS(a, b), b.add(")");
        },
        toCSS: a.toCSS,
        eval: function(b) {
            var c, d = this.value.eval(b);
            return c = this.currentFileInfo && this.currentFileInfo.rootpath, c && "string" == typeof d.value && b.isPathRelative(d.value) && (d.quote || (c = c.replace(/[\(\)'"\s]/g, function(a) {
                return "\\" + a;
            })), d.value = c + d.value), d.value = b.normalizePath(d.value), new a.URL(d, null);
        }
    };
}(require("../tree")), function(a) {
    a.Value = function(a) {
        this.value = a;
    }, a.Value.prototype = {
        type: "Value",
        accept: function(a) {
            this.value = a.visit(this.value);
        },
        eval: function(b) {
            return 1 === this.value.length ? this.value[0].eval(b) : new a.Value(this.value.map(function(a) {
                return a.eval(b);
            }));
        },
        genCSS: function(a, b) {
            var c;
            for (c = 0; c < this.value.length; c++) this.value[c].genCSS(a, b), c + 1 < this.value.length && b.add(a && a.compress ? "," : ", ");
        },
        toCSS: a.toCSS
    };
}(require("../tree")), function(a) {
    a.Variable = function(a, b, c) {
        this.name = a, this.index = b, this.currentFileInfo = c;
    }, a.Variable.prototype = {
        type: "Variable",
        eval: function(b) {
            var c, d, e = this.name;
            if (0 === e.indexOf("@@") && (e = "@" + new a.Variable(e.slice(1)).eval(b).value), 
            this.evaluating) throw {
                type: "Name",
                message: "Recursive variable definition for " + e,
                filename: this.currentFileInfo.file,
                index: this.index
            };
            if (this.evaluating = !0, c = a.find(b.frames, function(a) {
                return (d = a.variable(e)) ? d.value.eval(b) : void 0;
            })) return this.evaluating = !1, c;
            throw {
                type: "Name",
                message: "variable " + e + " is undefined",
                filename: this.currentFileInfo.filename,
                index: this.index
            };
        }
    };
}(require("../tree")), function(a) {
    var b = [ "paths", "optimization", "files", "contents", "relativeUrls", "rootpath", "strictImports", "insecure", "dumpLineNumbers", "compress", "processImports", "syncImport", "javascriptEnabled", "mime", "useFileCache", "currentFileInfo" ];
    a.parseEnv = function(a) {
        if (d(a, this, b), this.contents || (this.contents = {}), this.files || (this.files = {}), 
        !this.currentFileInfo) {
            var c = a && a.filename || "input", e = c.replace(/[^\/\\]*$/, "");
            a && (a.filename = null), this.currentFileInfo = {
                filename: c,
                relativeUrls: this.relativeUrls,
                rootpath: a && a.rootpath || "",
                currentDirectory: e,
                entryPath: e,
                rootFilename: c
            };
        }
    };
    var c = [ "silent", "verbose", "compress", "yuicompress", "ieCompat", "strictMath", "strictUnits", "cleancss", "sourceMap", "importMultiple" ];
    a.evalEnv = function(a, b) {
        d(a, this, c), this.frames = b || [];
    }, a.evalEnv.prototype.inParenthesis = function() {
        this.parensStack || (this.parensStack = []), this.parensStack.push(!0);
    }, a.evalEnv.prototype.outOfParenthesis = function() {
        this.parensStack.pop();
    }, a.evalEnv.prototype.isMathOn = function() {
        return this.strictMath ? this.parensStack && this.parensStack.length : !0;
    }, a.evalEnv.prototype.isPathRelative = function(a) {
        return !/^(?:[a-z-]+:|\/)/.test(a);
    }, a.evalEnv.prototype.normalizePath = function(a) {
        var b, c = a.split("/").reverse();
        for (a = []; 0 !== c.length; ) switch (b = c.pop()) {
          case ".":
            break;

          case "..":
            0 === a.length || ".." === a[a.length - 1] ? a.push(b) : a.pop();
            break;

          default:
            a.push(b);
        }
        return a.join("/");
    };
    var d = function(a, b, c) {
        if (a) for (var d = 0; d < c.length; d++) a.hasOwnProperty(c[d]) && (b[c[d]] = a[c[d]]);
    };
}(require("./tree")), function(a) {
    a.visitor = function(a) {
        this._implementation = a;
    }, a.visitor.prototype = {
        visit: function(a) {
            if (a instanceof Array) return this.visitArray(a);
            if (!a || !a.type) return a;
            var b, c, d = "visit" + a.type, e = this._implementation[d];
            return e && (b = {
                visitDeeper: !0
            }, c = e.call(this._implementation, a, b), this._implementation.isReplacing && (a = c)), 
            (!b || b.visitDeeper) && a && a.accept && a.accept(this), d += "Out", this._implementation[d] && this._implementation[d](a), 
            a;
        },
        visitArray: function(a) {
            var b, c = [];
            for (b = 0; b < a.length; b++) {
                var d = this.visit(a[b]);
                d instanceof Array ? (d = this.flatten(d), c = c.concat(d)) : c.push(d);
            }
            return this._implementation.isReplacing ? c : a;
        },
        doAccept: function(a) {
            a.accept(this);
        },
        flatten: function(a, b) {
            return a.reduce(this.flattenReduce.bind(this), b || []);
        },
        flattenReduce: function(a, b) {
            return b instanceof Array ? a = this.flatten(b, a) : a.push(b), a;
        }
    };
}(require("./tree")), function(a) {
    a.importVisitor = function(b, c, d) {
        this._visitor = new a.visitor(this), this._importer = b, this._finish = c, this.env = d || new a.evalEnv(), 
        this.importCount = 0;
    }, a.importVisitor.prototype = {
        isReplacing: !0,
        run: function(a) {
            var b;
            try {
                this._visitor.visit(a);
            } catch (c) {
                b = c;
            }
            this.isFinished = !0, 0 === this.importCount && this._finish(b);
        },
        visitImport: function(b, c) {
            var d, e = this, f = b.options.inline;
            if (!b.css || f) {
                try {
                    d = b.evalForImport(this.env);
                } catch (g) {
                    g.filename || (g.index = b.index, g.filename = b.currentFileInfo.filename), b.css = !0, 
                    b.error = g;
                }
                if (d && (!d.css || f)) {
                    b = d, this.importCount++;
                    var h = new a.evalEnv(this.env, this.env.frames.slice(0));
                    b.options.multiple && (h.importMultiple = !0), this._importer.push(b.getPath(), b.currentFileInfo, b.options, function(c, d, g, i) {
                        c && !c.filename && (c.index = b.index, c.filename = b.currentFileInfo.filename), 
                        g && !h.importMultiple && (b.skip = g);
                        var j = function(a) {
                            e.importCount--, 0 === e.importCount && e.isFinished && e._finish(a);
                        };
                        return !d || (b.root = d, b.importedFilename = i, f || b.skip) ? (j(), void 0) : (new a.importVisitor(e._importer, j, h).run(d), 
                        void 0);
                    });
                }
            }
            return c.visitDeeper = !1, b;
        },
        visitRule: function(a, b) {
            return b.visitDeeper = !1, a;
        },
        visitDirective: function(a) {
            return this.env.frames.unshift(a), a;
        },
        visitDirectiveOut: function() {
            this.env.frames.shift();
        },
        visitMixinDefinition: function(a) {
            return this.env.frames.unshift(a), a;
        },
        visitMixinDefinitionOut: function() {
            this.env.frames.shift();
        },
        visitRuleset: function(a) {
            return this.env.frames.unshift(a), a;
        },
        visitRulesetOut: function() {
            this.env.frames.shift();
        },
        visitMedia: function(a) {
            return this.env.frames.unshift(a.ruleset), a;
        },
        visitMediaOut: function() {
            this.env.frames.shift();
        }
    };
}(require("./tree")), function(a) {
    a.joinSelectorVisitor = function() {
        this.contexts = [ [] ], this._visitor = new a.visitor(this);
    }, a.joinSelectorVisitor.prototype = {
        run: function(a) {
            return this._visitor.visit(a);
        },
        visitRule: function(a, b) {
            b.visitDeeper = !1;
        },
        visitMixinDefinition: function(a, b) {
            b.visitDeeper = !1;
        },
        visitRuleset: function(a) {
            var b = this.contexts[this.contexts.length - 1], c = [];
            this.contexts.push(c), a.root || (a.selectors = a.selectors.filter(function(a) {
                return a.getIsOutput();
            }), 0 === a.selectors.length && (a.rules.length = 0), a.joinSelectors(c, b, a.selectors), 
            a.paths = c);
        },
        visitRulesetOut: function() {
            this.contexts.length = this.contexts.length - 1;
        },
        visitMedia: function(a) {
            var b = this.contexts[this.contexts.length - 1];
            a.rules[0].root = 0 === b.length || b[0].multiMedia;
        }
    };
}(require("./tree")), function(a) {
    a.toCSSVisitor = function(b) {
        this._visitor = new a.visitor(this), this._env = b;
    }, a.toCSSVisitor.prototype = {
        isReplacing: !0,
        run: function(a) {
            return this._visitor.visit(a);
        },
        visitRule: function(a) {
            return a.variable ? [] : a;
        },
        visitMixinDefinition: function() {
            return [];
        },
        visitExtend: function() {
            return [];
        },
        visitComment: function(a) {
            return a.isSilent(this._env) ? [] : a;
        },
        visitMedia: function(a, b) {
            return a.accept(this._visitor), b.visitDeeper = !1, a.rules.length ? a : [];
        },
        visitDirective: function(b) {
            if (b.currentFileInfo.reference && !b.isReferenced) return [];
            if ("@charset" === b.name) {
                if (this.charset) {
                    if (b.debugInfo) {
                        var c = new a.Comment("/* " + b.toCSS(this._env).replace(/\n/g, "") + " */\n");
                        return c.debugInfo = b.debugInfo, this._visitor.visit(c);
                    }
                    return [];
                }
                this.charset = !0;
            }
            return b;
        },
        checkPropertiesInRoot: function(b) {
            for (var c, d = 0; d < b.length; d++) if (c = b[d], c instanceof a.Rule && !c.variable) throw {
                message: "properties must be inside selector blocks, they cannot be in the root.",
                index: c.index,
                filename: c.currentFileInfo ? c.currentFileInfo.filename : null
            };
        },
        visitRuleset: function(b, c) {
            var d, e = [];
            if (b.firstRoot && this.checkPropertiesInRoot(b.rules), b.root) b.accept(this._visitor), 
            c.visitDeeper = !1, (b.firstRoot || b.rules.length > 0) && e.splice(0, 0, b); else {
                b.paths = b.paths.filter(function(b) {
                    var c;
                    for (" " === b[0].elements[0].combinator.value && (b[0].elements[0].combinator = new a.Combinator("")), 
                    c = 0; c < b.length; c++) return b[c].getIsReferenced() && b[c].getIsOutput() ? !0 : !1;
                });
                for (var f = 0; f < b.rules.length; f++) d = b.rules[f], d.rules && (e.push(this._visitor.visit(d)), 
                b.rules.splice(f, 1), f--);
                b.rules.length > 0 && b.accept(this._visitor), c.visitDeeper = !1, this._mergeRules(b.rules), 
                this._removeDuplicateRules(b.rules), b.rules.length > 0 && b.paths.length > 0 && e.splice(0, 0, b);
            }
            return 1 === e.length ? e[0] : e;
        },
        _removeDuplicateRules: function(b) {
            var c, d, e, f = {};
            for (e = b.length - 1; e >= 0; e--) if (d = b[e], d instanceof a.Rule) if (f[d.name]) {
                c = f[d.name], c instanceof a.Rule && (c = f[d.name] = [ f[d.name].toCSS(this._env) ]);
                var g = d.toCSS(this._env);
                -1 !== c.indexOf(g) ? b.splice(e, 1) : c.push(g);
            } else f[d.name] = d;
        },
        _mergeRules: function(b) {
            for (var c, d, e, f = {}, g = 0; g < b.length; g++) d = b[g], d instanceof a.Rule && d.merge && (e = [ d.name, d.important ? "!" : "" ].join(","), 
            f[e] ? b.splice(g--, 1) : c = f[e] = [], c.push(d));
            Object.keys(f).map(function(b) {
                c = f[b], c.length > 1 && (d = c[0], d.value = new a.Value(c.map(function(a) {
                    return a.value;
                })));
            });
        }
    };
}(require("./tree")), function(a) {
    a.extendFinderVisitor = function() {
        this._visitor = new a.visitor(this), this.contexts = [], this.allExtendsStack = [ [] ];
    }, a.extendFinderVisitor.prototype = {
        run: function(a) {
            return a = this._visitor.visit(a), a.allExtends = this.allExtendsStack[0], a;
        },
        visitRule: function(a, b) {
            b.visitDeeper = !1;
        },
        visitMixinDefinition: function(a, b) {
            b.visitDeeper = !1;
        },
        visitRuleset: function(b) {
            if (!b.root) {
                var c, d, e, f, g = [];
                for (c = 0; c < b.rules.length; c++) b.rules[c] instanceof a.Extend && (g.push(b.rules[c]), 
                b.extendOnEveryPath = !0);
                for (c = 0; c < b.paths.length; c++) {
                    var h = b.paths[c], i = h[h.length - 1];
                    for (f = i.extendList.slice(0).concat(g).map(function(a) {
                        return a.clone();
                    }), d = 0; d < f.length; d++) this.foundExtends = !0, e = f[d], e.findSelfSelectors(h), 
                    e.ruleset = b, 0 === d && (e.firstExtendOnThisSelectorPath = !0), this.allExtendsStack[this.allExtendsStack.length - 1].push(e);
                }
                this.contexts.push(b.selectors);
            }
        },
        visitRulesetOut: function(a) {
            a.root || (this.contexts.length = this.contexts.length - 1);
        },
        visitMedia: function(a) {
            a.allExtends = [], this.allExtendsStack.push(a.allExtends);
        },
        visitMediaOut: function() {
            this.allExtendsStack.length = this.allExtendsStack.length - 1;
        },
        visitDirective: function(a) {
            a.allExtends = [], this.allExtendsStack.push(a.allExtends);
        },
        visitDirectiveOut: function() {
            this.allExtendsStack.length = this.allExtendsStack.length - 1;
        }
    }, a.processExtendsVisitor = function() {
        this._visitor = new a.visitor(this);
    }, a.processExtendsVisitor.prototype = {
        run: function(b) {
            var c = new a.extendFinderVisitor();
            return c.run(b), c.foundExtends ? (b.allExtends = b.allExtends.concat(this.doExtendChaining(b.allExtends, b.allExtends)), 
            this.allExtendsStack = [ b.allExtends ], this._visitor.visit(b)) : b;
        },
        doExtendChaining: function(b, c, d) {
            var e, f, g, h, i, j, k, l, m = [], n = this;
            for (d = d || 0, e = 0; e < b.length; e++) for (f = 0; f < c.length; f++) j = b[e], 
            k = c[f], this.inInheritanceChain(k, j) || (i = [ k.selfSelectors[0] ], g = n.findMatch(j, i), 
            g.length && j.selfSelectors.forEach(function(b) {
                h = n.extendSelector(g, i, b), l = new a.Extend(k.selector, k.option, 0), l.selfSelectors = h, 
                h[h.length - 1].extendList = [ l ], m.push(l), l.ruleset = k.ruleset, l.parents = [ k, j ], 
                k.firstExtendOnThisSelectorPath && (l.firstExtendOnThisSelectorPath = !0, k.ruleset.paths.push(h));
            }));
            if (m.length) {
                if (this.extendChainCount++, d > 100) {
                    var o = "{unable to calculate}", p = "{unable to calculate}";
                    try {
                        o = m[0].selfSelectors[0].toCSS(), p = m[0].selector.toCSS();
                    } catch (q) {}
                    throw {
                        message: "extend circular reference detected. One of the circular extends is currently:" + o + ":extend(" + p + ")"
                    };
                }
                return m.concat(n.doExtendChaining(m, c, d + 1));
            }
            return m;
        },
        inInheritanceChain: function(a, b) {
            if (a === b) return !0;
            if (b.parents) {
                if (this.inInheritanceChain(a, b.parents[0])) return !0;
                if (this.inInheritanceChain(a, b.parents[1])) return !0;
            }
            return !1;
        },
        visitRule: function(a, b) {
            b.visitDeeper = !1;
        },
        visitMixinDefinition: function(a, b) {
            b.visitDeeper = !1;
        },
        visitSelector: function(a, b) {
            b.visitDeeper = !1;
        },
        visitRuleset: function(a) {
            if (!a.root) {
                var b, c, d, e, f = this.allExtendsStack[this.allExtendsStack.length - 1], g = [], h = this;
                for (d = 0; d < f.length; d++) for (c = 0; c < a.paths.length; c++) e = a.paths[c], 
                a.extendOnEveryPath || e[e.length - 1].extendList.length || (b = this.findMatch(f[d], e), 
                b.length && f[d].selfSelectors.forEach(function(a) {
                    g.push(h.extendSelector(b, e, a));
                }));
                a.paths = a.paths.concat(g);
            }
        },
        findMatch: function(a, b) {
            var c, d, e, f, g, h, i, j = this, k = a.selector.elements, l = [], m = [];
            for (c = 0; c < b.length; c++) for (d = b[c], e = 0; e < d.elements.length; e++) for (f = d.elements[e], 
            (a.allowBefore || 0 === c && 0 === e) && l.push({
                pathIndex: c,
                index: e,
                matched: 0,
                initialCombinator: f.combinator
            }), h = 0; h < l.length; h++) i = l[h], g = f.combinator.value, "" === g && 0 === e && (g = " "), 
            !j.isElementValuesEqual(k[i.matched].value, f.value) || i.matched > 0 && k[i.matched].combinator.value !== g ? i = null : i.matched++, 
            i && (i.finished = i.matched === k.length, i.finished && !a.allowAfter && (e + 1 < d.elements.length || c + 1 < b.length) && (i = null)), 
            i ? i.finished && (i.length = k.length, i.endPathIndex = c, i.endPathElementIndex = e + 1, 
            l.length = 0, m.push(i)) : (l.splice(h, 1), h--);
            return m;
        },
        isElementValuesEqual: function(b, c) {
            if ("string" == typeof b || "string" == typeof c) return b === c;
            if (b instanceof a.Attribute) return b.op !== c.op || b.key !== c.key ? !1 : b.value && c.value ? (b = b.value.value || b.value, 
            c = c.value.value || c.value, b === c) : b.value || c.value ? !1 : !0;
            if (b = b.value, c = c.value, b instanceof a.Selector) {
                if (!(c instanceof a.Selector) || b.elements.length !== c.elements.length) return !1;
                for (var d = 0; d < b.elements.length; d++) {
                    if (b.elements[d].combinator.value !== c.elements[d].combinator.value && (0 !== d || (b.elements[d].combinator.value || " ") !== (c.elements[d].combinator.value || " "))) return !1;
                    if (!this.isElementValuesEqual(b.elements[d].value, c.elements[d].value)) return !1;
                }
                return !0;
            }
            return !1;
        },
        extendSelector: function(b, c, d) {
            var e, f, g, h, i, j = 0, k = 0, l = [];
            for (e = 0; e < b.length; e++) h = b[e], f = c[h.pathIndex], g = new a.Element(h.initialCombinator, d.elements[0].value, d.elements[0].index, d.elements[0].currentFileInfo), 
            h.pathIndex > j && k > 0 && (l[l.length - 1].elements = l[l.length - 1].elements.concat(c[j].elements.slice(k)), 
            k = 0, j++), i = f.elements.slice(k, h.index).concat([ g ]).concat(d.elements.slice(1)), 
            j === h.pathIndex && e > 0 ? l[l.length - 1].elements = l[l.length - 1].elements.concat(i) : (l = l.concat(c.slice(j, h.pathIndex)), 
            l.push(new a.Selector(i))), j = h.endPathIndex, k = h.endPathElementIndex, k >= c[j].elements.length && (k = 0, 
            j++);
            return j < c.length && k > 0 && (l[l.length - 1].elements = l[l.length - 1].elements.concat(c[j].elements.slice(k)), 
            j++), l = l.concat(c.slice(j, c.length));
        },
        visitRulesetOut: function() {},
        visitMedia: function(a) {
            var b = a.allExtends.concat(this.allExtendsStack[this.allExtendsStack.length - 1]);
            b = b.concat(this.doExtendChaining(b, a.allExtends)), this.allExtendsStack.push(b);
        },
        visitMediaOut: function() {
            this.allExtendsStack.length = this.allExtendsStack.length - 1;
        },
        visitDirective: function(a) {
            var b = a.allExtends.concat(this.allExtendsStack[this.allExtendsStack.length - 1]);
            b = b.concat(this.doExtendChaining(b, a.allExtends)), this.allExtendsStack.push(b);
        },
        visitDirectiveOut: function() {
            this.allExtendsStack.length = this.allExtendsStack.length - 1;
        }
    };
}(require("./tree")), function(a) {
    a.sourceMapOutput = function(a) {
        this._css = [], this._rootNode = a.rootNode, this._writeSourceMap = a.writeSourceMap, 
        this._contentsMap = a.contentsMap, this._sourceMapFilename = a.sourceMapFilename, 
        this._outputFilename = a.outputFilename, this._sourceMapBasepath = a.sourceMapBasepath, 
        this._sourceMapRootpath = a.sourceMapRootpath, this._outputSourceFiles = a.outputSourceFiles, 
        this._sourceMapGeneratorConstructor = a.sourceMapGenerator || require("source-map").SourceMapGenerator, 
        this._sourceMapRootpath && "/" !== this._sourceMapRootpath.charAt(this._sourceMapRootpath.length - 1) && (this._sourceMapRootpath += "/"), 
        this._lineNumber = 0, this._column = 0;
    }, a.sourceMapOutput.prototype.normalizeFilename = function(a) {
        return this._sourceMapBasepath && 0 === a.indexOf(this._sourceMapBasepath) && (a = a.substring(this._sourceMapBasepath.length), 
        ("\\" === a.charAt(0) || "/" === a.charAt(0)) && (a = a.substring(1))), (this._sourceMapRootpath || "") + a.replace(/\\/g, "/");
    }, a.sourceMapOutput.prototype.add = function(a, b, c, d) {
        if (a) {
            var e, f, g, h, i;
            if (b) {
                var j = this._contentsMap[b.filename].substring(0, c);
                f = j.split("\n"), h = f[f.length - 1];
            }
            if (e = a.split("\n"), g = e[e.length - 1], b) if (d) for (i = 0; i < e.length; i++) this._sourceMapGenerator.addMapping({
                generated: {
                    line: this._lineNumber + i + 1,
                    column: 0 === i ? this._column : 0
                },
                original: {
                    line: f.length + i,
                    column: 0 === i ? h.length : 0
                },
                source: this.normalizeFilename(b.filename)
            }); else this._sourceMapGenerator.addMapping({
                generated: {
                    line: this._lineNumber + 1,
                    column: this._column
                },
                original: {
                    line: f.length,
                    column: h.length
                },
                source: this.normalizeFilename(b.filename)
            });
            1 === e.length ? this._column += g.length : (this._lineNumber += e.length - 1, this._column = g.length), 
            this._css.push(a);
        }
    }, a.sourceMapOutput.prototype.isEmpty = function() {
        return 0 === this._css.length;
    }, a.sourceMapOutput.prototype.toCSS = function(a) {
        if (this._sourceMapGenerator = new this._sourceMapGeneratorConstructor({
            file: this._outputFilename,
            sourceRoot: null
        }), this._outputSourceFiles) for (var b in this._contentsMap) this._sourceMapGenerator.setSourceContent(this.normalizeFilename(b), this._contentsMap[b]);
        if (this._rootNode.genCSS(a, this), this._css.length > 0) {
            var c, d = JSON.stringify(this._sourceMapGenerator.toJSON());
            this._sourceMapFilename && (c = this.normalizeFilename(this._sourceMapFilename)), 
            this._writeSourceMap ? this._writeSourceMap(d) : c = "data:application/json," + encodeURIComponent(d), 
            c && this._css.push("/*# sourceMappingURL=" + c + " */");
        }
        return this._css.join("");
    };
}(require("./tree"));

var isFileProtocol = /^(file|chrome(-extension)?|resource|qrc|app):/.test(location.protocol);

less.env = less.env || ("127.0.0.1" == location.hostname || "0.0.0.0" == location.hostname || "localhost" == location.hostname || location.port.length > 0 || isFileProtocol ? "development" : "production");

var logLevel = {
    info: 2,
    errors: 1,
    none: 0
};

if (less.logLevel = "undefined" != typeof less.logLevel ? less.logLevel : logLevel.info, 
less.async = less.async || !1, less.fileAsync = less.fileAsync || !1, less.poll = less.poll || (isFileProtocol ? 1e3 : 1500), 
less.functions) for (var func in less.functions) less.tree.functions[func] = less.functions[func];

var dumpLineNumbers = /!dumpLineNumbers:(comments|mediaquery|all)/.exec(location.hash);

dumpLineNumbers && (less.dumpLineNumbers = dumpLineNumbers[1]);

var typePattern = /^text\/(x-)?less$/, cache = null, fileCache = {};

if (less.watch = function() {
    return less.watchMode || (less.env = "development", initRunningMode()), this.watchMode = !0;
}, less.unwatch = function() {
    return clearInterval(less.watchTimer), this.watchMode = !1;
}, /!watch/.test(location.hash) && less.watch(), "development" != less.env) try {
    cache = "undefined" == typeof window.localStorage ? null : window.localStorage;
} catch (_) {}

var links = document.getElementsByTagName("link");

less.sheets = [];

for (var i = 0; i < links.length; i++) ("stylesheet/less" === links[i].rel || links[i].rel.match(/stylesheet/) && links[i].type.match(typePattern)) && less.sheets.push(links[i]);

less.modifyVars = function(a) {
    var b = "";
    for (var c in a) b += ("@" === c.slice(0, 1) ? "" : "@") + c + ": " + (";" === a[c].slice(-1) ? a[c] : a[c] + ";");
    less.refresh(!1, b);
}, less.refresh = function(a, b) {
    var c, d;
    c = d = new Date(), loadStyleSheets(function(a, b, e, f, g) {
        return a ? error(a, f.href) : (g.local ? log("loading " + f.href + " from cache.", logLevel.info) : (log("parsed " + f.href + " successfully.", logLevel.info), 
        createCSS(b.toCSS(less), f, g.lastModified)), log("css for " + f.href + " generated in " + (new Date() - d) + "ms", logLevel.info), 
        0 === g.remaining && log("css generated in " + (new Date() - c) + "ms", logLevel.info), 
        d = new Date(), void 0);
    }, a, b), loadStyles(b);
}, less.refreshStyles = loadStyles, less.Parser.fileLoader = loadFile, less.refresh("development" === less.env), 
"function" == typeof define && define.amd && define(function() {
    return less;
});

var Handlebars = function() {
    var __module3__ = function() {
        "use strict";
        var __exports__;
        function SafeString(string) {
            this.string = string;
        }
        SafeString.prototype.toString = function() {
            return "" + this.string;
        };
        __exports__ = SafeString;
        return __exports__;
    }();
    var __module2__ = function(__dependency1__) {
        "use strict";
        var __exports__ = {};
        var SafeString = __dependency1__;
        var escape = {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': "&quot;",
            "'": "&#x27;",
            "`": "&#x60;"
        };
        var badChars = /[&<>"'`]/g;
        var possible = /[&<>"'`]/;
        function escapeChar(chr) {
            return escape[chr] || "&amp;";
        }
        function extend(obj, value) {
            for (var key in value) {
                if (Object.prototype.hasOwnProperty.call(value, key)) {
                    obj[key] = value[key];
                }
            }
        }
        __exports__.extend = extend;
        var toString = Object.prototype.toString;
        __exports__.toString = toString;
        var isFunction = function(value) {
            return typeof value === "function";
        };
        if (isFunction(/x/)) {
            isFunction = function(value) {
                return typeof value === "function" && toString.call(value) === "[object Function]";
            };
        }
        var isFunction;
        __exports__.isFunction = isFunction;
        var isArray = Array.isArray || function(value) {
            return value && typeof value === "object" ? toString.call(value) === "[object Array]" : false;
        };
        __exports__.isArray = isArray;
        function escapeExpression(string) {
            if (string instanceof SafeString) {
                return string.toString();
            } else if (!string && string !== 0) {
                return "";
            }
            string = "" + string;
            if (!possible.test(string)) {
                return string;
            }
            return string.replace(badChars, escapeChar);
        }
        __exports__.escapeExpression = escapeExpression;
        function isEmpty(value) {
            if (!value && value !== 0) {
                return true;
            } else if (isArray(value) && value.length === 0) {
                return true;
            } else {
                return false;
            }
        }
        __exports__.isEmpty = isEmpty;
        return __exports__;
    }(__module3__);
    var __module4__ = function() {
        "use strict";
        var __exports__;
        var errorProps = [ "description", "fileName", "lineNumber", "message", "name", "number", "stack" ];
        function Exception(message, node) {
            var line;
            if (node && node.firstLine) {
                line = node.firstLine;
                message += " - " + line + ":" + node.firstColumn;
            }
            var tmp = Error.prototype.constructor.call(this, message);
            for (var idx = 0; idx < errorProps.length; idx++) {
                this[errorProps[idx]] = tmp[errorProps[idx]];
            }
            if (line) {
                this.lineNumber = line;
                this.column = node.firstColumn;
            }
        }
        Exception.prototype = new Error();
        __exports__ = Exception;
        return __exports__;
    }();
    var __module1__ = function(__dependency1__, __dependency2__) {
        "use strict";
        var __exports__ = {};
        var Utils = __dependency1__;
        var Exception = __dependency2__;
        var VERSION = "1.3.0";
        __exports__.VERSION = VERSION;
        var COMPILER_REVISION = 4;
        __exports__.COMPILER_REVISION = COMPILER_REVISION;
        var REVISION_CHANGES = {
            1: "<= 1.0.rc.2",
            2: "== 1.0.0-rc.3",
            3: "== 1.0.0-rc.4",
            4: ">= 1.0.0"
        };
        __exports__.REVISION_CHANGES = REVISION_CHANGES;
        var isArray = Utils.isArray, isFunction = Utils.isFunction, toString = Utils.toString, objectType = "[object Object]";
        function HandlebarsEnvironment(helpers, partials) {
            this.helpers = helpers || {};
            this.partials = partials || {};
            registerDefaultHelpers(this);
        }
        __exports__.HandlebarsEnvironment = HandlebarsEnvironment;
        HandlebarsEnvironment.prototype = {
            constructor: HandlebarsEnvironment,
            logger: logger,
            log: log,
            registerHelper: function(name, fn, inverse) {
                if (toString.call(name) === objectType) {
                    if (inverse || fn) {
                        throw new Exception("Arg not supported with multiple helpers");
                    }
                    Utils.extend(this.helpers, name);
                } else {
                    if (inverse) {
                        fn.not = inverse;
                    }
                    this.helpers[name] = fn;
                }
            },
            registerPartial: function(name, str) {
                if (toString.call(name) === objectType) {
                    Utils.extend(this.partials, name);
                } else {
                    this.partials[name] = str;
                }
            }
        };
        function registerDefaultHelpers(instance) {
            instance.registerHelper("helperMissing", function(arg) {
                if (arguments.length === 2) {
                    return undefined;
                } else {
                    throw new Exception("Missing helper: '" + arg + "'");
                }
            });
            instance.registerHelper("blockHelperMissing", function(context, options) {
                var inverse = options.inverse || function() {}, fn = options.fn;
                if (isFunction(context)) {
                    context = context.call(this);
                }
                if (context === true) {
                    return fn(this);
                } else if (context === false || context == null) {
                    return inverse(this);
                } else if (isArray(context)) {
                    if (context.length > 0) {
                        return instance.helpers.each(context, options);
                    } else {
                        return inverse(this);
                    }
                } else {
                    return fn(context);
                }
            });
            instance.registerHelper("each", function(context, options) {
                var fn = options.fn, inverse = options.inverse;
                var i = 0, ret = "", data;
                if (isFunction(context)) {
                    context = context.call(this);
                }
                if (options.data) {
                    data = createFrame(options.data);
                }
                if (context && typeof context === "object") {
                    if (isArray(context)) {
                        for (var j = context.length; i < j; i++) {
                            if (data) {
                                data.index = i;
                                data.first = i === 0;
                                data.last = i === context.length - 1;
                            }
                            ret = ret + fn(context[i], {
                                data: data
                            });
                        }
                    } else {
                        for (var key in context) {
                            if (context.hasOwnProperty(key)) {
                                if (data) {
                                    data.key = key;
                                    data.index = i;
                                    data.first = i === 0;
                                }
                                ret = ret + fn(context[key], {
                                    data: data
                                });
                                i++;
                            }
                        }
                    }
                }
                if (i === 0) {
                    ret = inverse(this);
                }
                return ret;
            });
            instance.registerHelper("if", function(conditional, options) {
                if (isFunction(conditional)) {
                    conditional = conditional.call(this);
                }
                if (!options.hash.includeZero && !conditional || Utils.isEmpty(conditional)) {
                    return options.inverse(this);
                } else {
                    return options.fn(this);
                }
            });
            instance.registerHelper("unless", function(conditional, options) {
                return instance.helpers["if"].call(this, conditional, {
                    fn: options.inverse,
                    inverse: options.fn,
                    hash: options.hash
                });
            });
            instance.registerHelper("with", function(context, options) {
                if (isFunction(context)) {
                    context = context.call(this);
                }
                if (!Utils.isEmpty(context)) return options.fn(context);
            });
            instance.registerHelper("log", function(context, options) {
                var level = options.data && options.data.level != null ? parseInt(options.data.level, 10) : 1;
                instance.log(level, context);
            });
        }
        var logger = {
            methodMap: {
                0: "debug",
                1: "info",
                2: "warn",
                3: "error"
            },
            DEBUG: 0,
            INFO: 1,
            WARN: 2,
            ERROR: 3,
            level: 3,
            log: function(level, obj) {
                if (logger.level <= level) {
                    var method = logger.methodMap[level];
                    if (typeof console !== "undefined" && console[method]) {
                        console[method].call(console, obj);
                    }
                }
            }
        };
        __exports__.logger = logger;
        function log(level, obj) {
            logger.log(level, obj);
        }
        __exports__.log = log;
        var createFrame = function(object) {
            var obj = {};
            Utils.extend(obj, object);
            return obj;
        };
        __exports__.createFrame = createFrame;
        return __exports__;
    }(__module2__, __module4__);
    var __module5__ = function(__dependency1__, __dependency2__, __dependency3__) {
        "use strict";
        var __exports__ = {};
        var Utils = __dependency1__;
        var Exception = __dependency2__;
        var COMPILER_REVISION = __dependency3__.COMPILER_REVISION;
        var REVISION_CHANGES = __dependency3__.REVISION_CHANGES;
        function checkRevision(compilerInfo) {
            var compilerRevision = compilerInfo && compilerInfo[0] || 1, currentRevision = COMPILER_REVISION;
            if (compilerRevision !== currentRevision) {
                if (compilerRevision < currentRevision) {
                    var runtimeVersions = REVISION_CHANGES[currentRevision], compilerVersions = REVISION_CHANGES[compilerRevision];
                    throw new Exception("Template was precompiled with an older version of Handlebars than the current runtime. " + "Please update your precompiler to a newer version (" + runtimeVersions + ") or downgrade your runtime to an older version (" + compilerVersions + ").");
                } else {
                    throw new Exception("Template was precompiled with a newer version of Handlebars than the current runtime. " + "Please update your runtime to a newer version (" + compilerInfo[1] + ").");
                }
            }
        }
        __exports__.checkRevision = checkRevision;
        function template(templateSpec, env) {
            if (!env) {
                throw new Exception("No environment passed to template");
            }
            var invokePartialWrapper = function(partial, name, context, helpers, partials, data) {
                var result = env.VM.invokePartial.apply(this, arguments);
                if (result != null) {
                    return result;
                }
                if (env.compile) {
                    var options = {
                        helpers: helpers,
                        partials: partials,
                        data: data
                    };
                    partials[name] = env.compile(partial, {
                        data: data !== undefined
                    }, env);
                    return partials[name](context, options);
                } else {
                    throw new Exception("The partial " + name + " could not be compiled when running in runtime-only mode");
                }
            };
            var container = {
                escapeExpression: Utils.escapeExpression,
                invokePartial: invokePartialWrapper,
                programs: [],
                program: function(i, fn, data) {
                    var programWrapper = this.programs[i];
                    if (data) {
                        programWrapper = program(i, fn, data);
                    } else if (!programWrapper) {
                        programWrapper = this.programs[i] = program(i, fn);
                    }
                    return programWrapper;
                },
                merge: function(param, common) {
                    var ret = param || common;
                    if (param && common && param !== common) {
                        ret = {};
                        Utils.extend(ret, common);
                        Utils.extend(ret, param);
                    }
                    return ret;
                },
                programWithDepth: env.VM.programWithDepth,
                noop: env.VM.noop,
                compilerInfo: null
            };
            return function(context, options) {
                options = options || {};
                var namespace = options.partial ? options : env, helpers, partials;
                if (!options.partial) {
                    helpers = options.helpers;
                    partials = options.partials;
                }
                var result = templateSpec.call(container, namespace, context, helpers, partials, options.data);
                if (!options.partial) {
                    env.VM.checkRevision(container.compilerInfo);
                }
                return result;
            };
        }
        __exports__.template = template;
        function programWithDepth(i, fn, data) {
            var args = Array.prototype.slice.call(arguments, 3);
            var prog = function(context, options) {
                options = options || {};
                return fn.apply(this, [ context, options.data || data ].concat(args));
            };
            prog.program = i;
            prog.depth = args.length;
            return prog;
        }
        __exports__.programWithDepth = programWithDepth;
        function program(i, fn, data) {
            var prog = function(context, options) {
                options = options || {};
                return fn(context, options.data || data);
            };
            prog.program = i;
            prog.depth = 0;
            return prog;
        }
        __exports__.program = program;
        function invokePartial(partial, name, context, helpers, partials, data) {
            var options = {
                partial: true,
                helpers: helpers,
                partials: partials,
                data: data
            };
            if (partial === undefined) {
                throw new Exception("The partial " + name + " could not be found");
            } else if (partial instanceof Function) {
                return partial(context, options);
            }
        }
        __exports__.invokePartial = invokePartial;
        function noop() {
            return "";
        }
        __exports__.noop = noop;
        return __exports__;
    }(__module2__, __module4__, __module1__);
    var __module0__ = function(__dependency1__, __dependency2__, __dependency3__, __dependency4__, __dependency5__) {
        "use strict";
        var __exports__;
        var base = __dependency1__;
        var SafeString = __dependency2__;
        var Exception = __dependency3__;
        var Utils = __dependency4__;
        var runtime = __dependency5__;
        var create = function() {
            var hb = new base.HandlebarsEnvironment();
            Utils.extend(hb, base);
            hb.SafeString = SafeString;
            hb.Exception = Exception;
            hb.Utils = Utils;
            hb.VM = runtime;
            hb.template = function(spec) {
                return runtime.template(spec, hb);
            };
            return hb;
        };
        var Handlebars = create();
        Handlebars.create = create;
        __exports__ = Handlebars;
        return __exports__;
    }(__module1__, __module3__, __module4__, __module2__, __module5__);
    return __module0__;
}();

if ("undefined" == typeof jQuery) throw new Error("Bootstrap's JavaScript requires jQuery");

+function(a) {
    "use strict";
    function b() {
        var a = document.createElement("bootstrap"), b = {
            WebkitTransition: "webkitTransitionEnd",
            MozTransition: "transitionend",
            OTransition: "oTransitionEnd otransitionend",
            transition: "transitionend"
        };
        for (var c in b) if (void 0 !== a.style[c]) return {
            end: b[c]
        };
        return !1;
    }
    a.fn.emulateTransitionEnd = function(b) {
        var c = !1, d = this;
        a(this).one(a.support.transition.end, function() {
            c = !0;
        });
        var e = function() {
            c || a(d).trigger(a.support.transition.end);
        };
        return setTimeout(e, b), this;
    }, a(function() {
        a.support.transition = b();
    });
}(jQuery), +function(a) {
    "use strict";
    var b = '[data-dismiss="alert"]', c = function(c) {
        a(c).on("click", b, this.close);
    };
    c.prototype.close = function(b) {
        function c() {
            f.trigger("closed.bs.alert").remove();
        }
        var d = a(this), e = d.attr("data-target");
        e || (e = d.attr("href"), e = e && e.replace(/.*(?=#[^\s]*$)/, ""));
        var f = a(e);
        b && b.preventDefault(), f.length || (f = d.hasClass("alert") ? d : d.parent()), 
        f.trigger(b = a.Event("close.bs.alert")), b.isDefaultPrevented() || (f.removeClass("in"), 
        a.support.transition && f.hasClass("fade") ? f.one(a.support.transition.end, c).emulateTransitionEnd(150) : c());
    };
    var d = a.fn.alert;
    a.fn.alert = function(b) {
        return this.each(function() {
            var d = a(this), e = d.data("bs.alert");
            e || d.data("bs.alert", e = new c(this)), "string" == typeof b && e[b].call(d);
        });
    }, a.fn.alert.Constructor = c, a.fn.alert.noConflict = function() {
        return a.fn.alert = d, this;
    }, a(document).on("click.bs.alert.data-api", b, c.prototype.close);
}(jQuery), +function(a) {
    "use strict";
    var b = function(c, d) {
        this.$element = a(c), this.options = a.extend({}, b.DEFAULTS, d), this.isLoading = !1;
    };
    b.DEFAULTS = {
        loadingText: "loading..."
    }, b.prototype.setState = function(b) {
        var c = "disabled", d = this.$element, e = d.is("input") ? "val" : "html", f = d.data();
        b += "Text", f.resetText || d.data("resetText", d[e]()), d[e](f[b] || this.options[b]), 
        setTimeout(a.proxy(function() {
            "loadingText" == b ? (this.isLoading = !0, d.addClass(c).attr(c, c)) : this.isLoading && (this.isLoading = !1, 
            d.removeClass(c).removeAttr(c));
        }, this), 0);
    }, b.prototype.toggle = function() {
        var a = !0, b = this.$element.closest('[data-toggle="buttons"]');
        if (b.length) {
            var c = this.$element.find("input");
            "radio" == c.prop("type") && (c.prop("checked") && this.$element.hasClass("active") ? a = !1 : b.find(".active").removeClass("active")), 
            a && c.prop("checked", !this.$element.hasClass("active")).trigger("change");
        }
        a && this.$element.toggleClass("active");
    };
    var c = a.fn.button;
    a.fn.button = function(c) {
        return this.each(function() {
            var d = a(this), e = d.data("bs.button"), f = "object" == typeof c && c;
            e || d.data("bs.button", e = new b(this, f)), "toggle" == c ? e.toggle() : c && e.setState(c);
        });
    }, a.fn.button.Constructor = b, a.fn.button.noConflict = function() {
        return a.fn.button = c, this;
    }, a(document).on("click.bs.button.data-api", "[data-toggle^=button]", function(b) {
        var c = a(b.target);
        c.hasClass("btn") || (c = c.closest(".btn")), c.button("toggle"), b.preventDefault();
    });
}(jQuery), +function(a) {
    "use strict";
    var b = function(b, c) {
        this.$element = a(b), this.$indicators = this.$element.find(".carousel-indicators"), 
        this.options = c, this.paused = this.sliding = this.interval = this.$active = this.$items = null, 
        "hover" == this.options.pause && this.$element.on("mouseenter", a.proxy(this.pause, this)).on("mouseleave", a.proxy(this.cycle, this));
    };
    b.DEFAULTS = {
        interval: 5e3,
        pause: "hover",
        wrap: !0
    }, b.prototype.cycle = function(b) {
        return b || (this.paused = !1), this.interval && clearInterval(this.interval), this.options.interval && !this.paused && (this.interval = setInterval(a.proxy(this.next, this), this.options.interval)), 
        this;
    }, b.prototype.getActiveIndex = function() {
        return this.$active = this.$element.find(".item.active"), this.$items = this.$active.parent().children(), 
        this.$items.index(this.$active);
    }, b.prototype.to = function(b) {
        var c = this, d = this.getActiveIndex();
        return b > this.$items.length - 1 || 0 > b ? void 0 : this.sliding ? this.$element.one("slid.bs.carousel", function() {
            c.to(b);
        }) : d == b ? this.pause().cycle() : this.slide(b > d ? "next" : "prev", a(this.$items[b]));
    }, b.prototype.pause = function(b) {
        return b || (this.paused = !0), this.$element.find(".next, .prev").length && a.support.transition && (this.$element.trigger(a.support.transition.end), 
        this.cycle(!0)), this.interval = clearInterval(this.interval), this;
    }, b.prototype.next = function() {
        return this.sliding ? void 0 : this.slide("next");
    }, b.prototype.prev = function() {
        return this.sliding ? void 0 : this.slide("prev");
    }, b.prototype.slide = function(b, c) {
        var d = this.$element.find(".item.active"), e = c || d[b](), f = this.interval, g = "next" == b ? "left" : "right", h = "next" == b ? "first" : "last", i = this;
        if (!e.length) {
            if (!this.options.wrap) return;
            e = this.$element.find(".item")[h]();
        }
        if (e.hasClass("active")) return this.sliding = !1;
        var j = a.Event("slide.bs.carousel", {
            relatedTarget: e[0],
            direction: g
        });
        return this.$element.trigger(j), j.isDefaultPrevented() ? void 0 : (this.sliding = !0, 
        f && this.pause(), this.$indicators.length && (this.$indicators.find(".active").removeClass("active"), 
        this.$element.one("slid.bs.carousel", function() {
            var b = a(i.$indicators.children()[i.getActiveIndex()]);
            b && b.addClass("active");
        })), a.support.transition && this.$element.hasClass("slide") ? (e.addClass(b), e[0].offsetWidth, 
        d.addClass(g), e.addClass(g), d.one(a.support.transition.end, function() {
            e.removeClass([ b, g ].join(" ")).addClass("active"), d.removeClass([ "active", g ].join(" ")), 
            i.sliding = !1, setTimeout(function() {
                i.$element.trigger("slid.bs.carousel");
            }, 0);
        }).emulateTransitionEnd(1e3 * d.css("transition-duration").slice(0, -1))) : (d.removeClass("active"), 
        e.addClass("active"), this.sliding = !1, this.$element.trigger("slid.bs.carousel")), 
        f && this.cycle(), this);
    };
    var c = a.fn.carousel;
    a.fn.carousel = function(c) {
        return this.each(function() {
            var d = a(this), e = d.data("bs.carousel"), f = a.extend({}, b.DEFAULTS, d.data(), "object" == typeof c && c), g = "string" == typeof c ? c : f.slide;
            e || d.data("bs.carousel", e = new b(this, f)), "number" == typeof c ? e.to(c) : g ? e[g]() : f.interval && e.pause().cycle();
        });
    }, a.fn.carousel.Constructor = b, a.fn.carousel.noConflict = function() {
        return a.fn.carousel = c, this;
    }, a(document).on("click.bs.carousel.data-api", "[data-slide], [data-slide-to]", function(b) {
        var c, d = a(this), e = a(d.attr("data-target") || (c = d.attr("href")) && c.replace(/.*(?=#[^\s]+$)/, "")), f = a.extend({}, e.data(), d.data()), g = d.attr("data-slide-to");
        g && (f.interval = !1), e.carousel(f), (g = d.attr("data-slide-to")) && e.data("bs.carousel").to(g), 
        b.preventDefault();
    }), a(window).on("load", function() {
        a('[data-ride="carousel"]').each(function() {
            var b = a(this);
            b.carousel(b.data());
        });
    });
}(jQuery), +function(a) {
    "use strict";
    var b = function(c, d) {
        this.$element = a(c), this.options = a.extend({}, b.DEFAULTS, d), this.transitioning = null, 
        this.options.parent && (this.$parent = a(this.options.parent)), this.options.toggle && this.toggle();
    };
    b.DEFAULTS = {
        toggle: !0
    }, b.prototype.dimension = function() {
        var a = this.$element.hasClass("width");
        return a ? "width" : "height";
    }, b.prototype.show = function() {
        if (!this.transitioning && !this.$element.hasClass("in")) {
            var b = a.Event("show.bs.collapse");
            if (this.$element.trigger(b), !b.isDefaultPrevented()) {
                var c = this.$parent && this.$parent.find("> .panel > .in");
                if (c && c.length) {
                    var d = c.data("bs.collapse");
                    if (d && d.transitioning) return;
                    c.collapse("hide"), d || c.data("bs.collapse", null);
                }
                var e = this.dimension();
                this.$element.removeClass("collapse").addClass("collapsing")[e](0), this.transitioning = 1;
                var f = function() {
                    this.$element.removeClass("collapsing").addClass("collapse in")[e]("auto"), this.transitioning = 0, 
                    this.$element.trigger("shown.bs.collapse");
                };
                if (!a.support.transition) return f.call(this);
                var g = a.camelCase([ "scroll", e ].join("-"));
                this.$element.one(a.support.transition.end, a.proxy(f, this)).emulateTransitionEnd(350)[e](this.$element[0][g]);
            }
        }
    }, b.prototype.hide = function() {
        if (!this.transitioning && this.$element.hasClass("in")) {
            var b = a.Event("hide.bs.collapse");
            if (this.$element.trigger(b), !b.isDefaultPrevented()) {
                var c = this.dimension();
                this.$element[c](this.$element[c]())[0].offsetHeight, this.$element.addClass("collapsing").removeClass("collapse").removeClass("in"), 
                this.transitioning = 1;
                var d = function() {
                    this.transitioning = 0, this.$element.trigger("hidden.bs.collapse").removeClass("collapsing").addClass("collapse");
                };
                return a.support.transition ? void this.$element[c](0).one(a.support.transition.end, a.proxy(d, this)).emulateTransitionEnd(350) : d.call(this);
            }
        }
    }, b.prototype.toggle = function() {
        this[this.$element.hasClass("in") ? "hide" : "show"]();
    };
    var c = a.fn.collapse;
    a.fn.collapse = function(c) {
        return this.each(function() {
            var d = a(this), e = d.data("bs.collapse"), f = a.extend({}, b.DEFAULTS, d.data(), "object" == typeof c && c);
            !e && f.toggle && "show" == c && (c = !c), e || d.data("bs.collapse", e = new b(this, f)), 
            "string" == typeof c && e[c]();
        });
    }, a.fn.collapse.Constructor = b, a.fn.collapse.noConflict = function() {
        return a.fn.collapse = c, this;
    }, a(document).on("click.bs.collapse.data-api", "[data-toggle=collapse]", function(b) {
        var c, d = a(this), e = d.attr("data-target") || b.preventDefault() || (c = d.attr("href")) && c.replace(/.*(?=#[^\s]+$)/, ""), f = a(e), g = f.data("bs.collapse"), h = g ? "toggle" : d.data(), i = d.attr("data-parent"), j = i && a(i);
        g && g.transitioning || (j && j.find('[data-toggle=collapse][data-parent="' + i + '"]').not(d).addClass("collapsed"), 
        d[f.hasClass("in") ? "addClass" : "removeClass"]("collapsed")), f.collapse(h);
    });
}(jQuery), +function(a) {
    "use strict";
    function b(b) {
        a(d).remove(), a(e).each(function() {
            var d = c(a(this)), e = {
                relatedTarget: this
            };
            d.hasClass("open") && (d.trigger(b = a.Event("hide.bs.dropdown", e)), b.isDefaultPrevented() || d.removeClass("open").trigger("hidden.bs.dropdown", e));
        });
    }
    function c(b) {
        var c = b.attr("data-target");
        c || (c = b.attr("href"), c = c && /#[A-Za-z]/.test(c) && c.replace(/.*(?=#[^\s]*$)/, ""));
        var d = c && a(c);
        return d && d.length ? d : b.parent();
    }
    var d = ".dropdown-backdrop", e = "[data-toggle=dropdown]", f = function(b) {
        a(b).on("click.bs.dropdown", this.toggle);
    };
    f.prototype.toggle = function(d) {
        var e = a(this);
        if (!e.is(".disabled, :disabled")) {
            var f = c(e), g = f.hasClass("open");
            if (b(), !g) {
                "ontouchstart" in document.documentElement && !f.closest(".navbar-nav").length && a('<div class="dropdown-backdrop"/>').insertAfter(a(this)).on("click", b);
                var h = {
                    relatedTarget: this
                };
                if (f.trigger(d = a.Event("show.bs.dropdown", h)), d.isDefaultPrevented()) return;
                f.toggleClass("open").trigger("shown.bs.dropdown", h), e.focus();
            }
            return !1;
        }
    }, f.prototype.keydown = function(b) {
        if (/(38|40|27)/.test(b.keyCode)) {
            var d = a(this);
            if (b.preventDefault(), b.stopPropagation(), !d.is(".disabled, :disabled")) {
                var f = c(d), g = f.hasClass("open");
                if (!g || g && 27 == b.keyCode) return 27 == b.which && f.find(e).focus(), d.click();
                var h = " li:not(.divider):visible a", i = f.find("[role=menu]" + h + ", [role=listbox]" + h);
                if (i.length) {
                    var j = i.index(i.filter(":focus"));
                    38 == b.keyCode && j > 0 && j--, 40 == b.keyCode && j < i.length - 1 && j++, ~j || (j = 0), 
                    i.eq(j).focus();
                }
            }
        }
    };
    var g = a.fn.dropdown;
    a.fn.dropdown = function(b) {
        return this.each(function() {
            var c = a(this), d = c.data("bs.dropdown");
            d || c.data("bs.dropdown", d = new f(this)), "string" == typeof b && d[b].call(c);
        });
    }, a.fn.dropdown.Constructor = f, a.fn.dropdown.noConflict = function() {
        return a.fn.dropdown = g, this;
    }, a(document).on("click.bs.dropdown.data-api", b).on("click.bs.dropdown.data-api", ".dropdown form", function(a) {
        a.stopPropagation();
    }).on("click.bs.dropdown.data-api", e, f.prototype.toggle).on("keydown.bs.dropdown.data-api", e + ", [role=menu], [role=listbox]", f.prototype.keydown);
}(jQuery), +function(a) {
    "use strict";
    var b = function(b, c) {
        this.options = c, this.$element = a(b), this.$backdrop = this.isShown = null, this.options.remote && this.$element.find(".modal-content").load(this.options.remote, a.proxy(function() {
            this.$element.trigger("loaded.bs.modal");
        }, this));
    };
    b.DEFAULTS = {
        backdrop: !0,
        keyboard: !0,
        show: !0
    }, b.prototype.toggle = function(a) {
        return this[this.isShown ? "hide" : "show"](a);
    }, b.prototype.show = function(b) {
        var c = this, d = a.Event("show.bs.modal", {
            relatedTarget: b
        });
        this.$element.trigger(d), this.isShown || d.isDefaultPrevented() || (this.isShown = !0, 
        this.escape(), this.$element.on("click.dismiss.bs.modal", '[data-dismiss="modal"]', a.proxy(this.hide, this)), 
        this.backdrop(function() {
            var d = a.support.transition && c.$element.hasClass("fade");
            c.$element.parent().length || c.$element.appendTo(document.body), c.$element.show().scrollTop(0), 
            d && c.$element[0].offsetWidth, c.$element.addClass("in").attr("aria-hidden", !1), 
            c.enforceFocus();
            var e = a.Event("shown.bs.modal", {
                relatedTarget: b
            });
            d ? c.$element.find(".modal-dialog").one(a.support.transition.end, function() {
                c.$element.focus().trigger(e);
            }).emulateTransitionEnd(300) : c.$element.focus().trigger(e);
        }));
    }, b.prototype.hide = function(b) {
        b && b.preventDefault(), b = a.Event("hide.bs.modal"), this.$element.trigger(b), 
        this.isShown && !b.isDefaultPrevented() && (this.isShown = !1, this.escape(), a(document).off("focusin.bs.modal"), 
        this.$element.removeClass("in").attr("aria-hidden", !0).off("click.dismiss.bs.modal"), 
        a.support.transition && this.$element.hasClass("fade") ? this.$element.one(a.support.transition.end, a.proxy(this.hideModal, this)).emulateTransitionEnd(300) : this.hideModal());
    }, b.prototype.enforceFocus = function() {
        a(document).off("focusin.bs.modal").on("focusin.bs.modal", a.proxy(function(a) {
            this.$element[0] === a.target || this.$element.has(a.target).length || this.$element.focus();
        }, this));
    }, b.prototype.escape = function() {
        this.isShown && this.options.keyboard ? this.$element.on("keyup.dismiss.bs.modal", a.proxy(function(a) {
            27 == a.which && this.hide();
        }, this)) : this.isShown || this.$element.off("keyup.dismiss.bs.modal");
    }, b.prototype.hideModal = function() {
        var a = this;
        this.$element.hide(), this.backdrop(function() {
            a.removeBackdrop(), a.$element.trigger("hidden.bs.modal");
        });
    }, b.prototype.removeBackdrop = function() {
        this.$backdrop && this.$backdrop.remove(), this.$backdrop = null;
    }, b.prototype.backdrop = function(b) {
        var c = this.$element.hasClass("fade") ? "fade" : "";
        if (this.isShown && this.options.backdrop) {
            var d = a.support.transition && c;
            if (this.$backdrop = a('<div class="modal-backdrop ' + c + '" />').appendTo(document.body), 
            this.$element.on("click.dismiss.bs.modal", a.proxy(function(a) {
                a.target === a.currentTarget && ("static" == this.options.backdrop ? this.$element[0].focus.call(this.$element[0]) : this.hide.call(this));
            }, this)), d && this.$backdrop[0].offsetWidth, this.$backdrop.addClass("in"), !b) return;
            d ? this.$backdrop.one(a.support.transition.end, b).emulateTransitionEnd(150) : b();
        } else !this.isShown && this.$backdrop ? (this.$backdrop.removeClass("in"), a.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one(a.support.transition.end, b).emulateTransitionEnd(150) : b()) : b && b();
    };
    var c = a.fn.modal;
    a.fn.modal = function(c, d) {
        return this.each(function() {
            var e = a(this), f = e.data("bs.modal"), g = a.extend({}, b.DEFAULTS, e.data(), "object" == typeof c && c);
            f || e.data("bs.modal", f = new b(this, g)), "string" == typeof c ? f[c](d) : g.show && f.show(d);
        });
    }, a.fn.modal.Constructor = b, a.fn.modal.noConflict = function() {
        return a.fn.modal = c, this;
    }, a(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function(b) {
        var c = a(this), d = c.attr("href"), e = a(c.attr("data-target") || d && d.replace(/.*(?=#[^\s]+$)/, "")), f = e.data("bs.modal") ? "toggle" : a.extend({
            remote: !/#/.test(d) && d
        }, e.data(), c.data());
        c.is("a") && b.preventDefault(), e.modal(f, this).one("hide", function() {
            c.is(":visible") && c.focus();
        });
    }), a(document).on("show.bs.modal", ".modal", function() {
        a(document.body).addClass("modal-open");
    }).on("hidden.bs.modal", ".modal", function() {
        a(document.body).removeClass("modal-open");
    });
}(jQuery), +function(a) {
    "use strict";
    var b = function(a, b) {
        this.type = this.options = this.enabled = this.timeout = this.hoverState = this.$element = null, 
        this.init("tooltip", a, b);
    };
    b.DEFAULTS = {
        animation: !0,
        placement: "top",
        selector: !1,
        template: '<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: "hover focus",
        title: "",
        delay: 0,
        html: !1,
        container: !1
    }, b.prototype.init = function(b, c, d) {
        this.enabled = !0, this.type = b, this.$element = a(c), this.options = this.getOptions(d);
        for (var e = this.options.trigger.split(" "), f = e.length; f--; ) {
            var g = e[f];
            if ("click" == g) this.$element.on("click." + this.type, this.options.selector, a.proxy(this.toggle, this)); else if ("manual" != g) {
                var h = "hover" == g ? "mouseenter" : "focusin", i = "hover" == g ? "mouseleave" : "focusout";
                this.$element.on(h + "." + this.type, this.options.selector, a.proxy(this.enter, this)), 
                this.$element.on(i + "." + this.type, this.options.selector, a.proxy(this.leave, this));
            }
        }
        this.options.selector ? this._options = a.extend({}, this.options, {
            trigger: "manual",
            selector: ""
        }) : this.fixTitle();
    }, b.prototype.getDefaults = function() {
        return b.DEFAULTS;
    }, b.prototype.getOptions = function(b) {
        return b = a.extend({}, this.getDefaults(), this.$element.data(), b), b.delay && "number" == typeof b.delay && (b.delay = {
            show: b.delay,
            hide: b.delay
        }), b;
    }, b.prototype.getDelegateOptions = function() {
        var b = {}, c = this.getDefaults();
        return this._options && a.each(this._options, function(a, d) {
            c[a] != d && (b[a] = d);
        }), b;
    }, b.prototype.enter = function(b) {
        var c = b instanceof this.constructor ? b : a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs." + this.type);
        return clearTimeout(c.timeout), c.hoverState = "in", c.options.delay && c.options.delay.show ? void (c.timeout = setTimeout(function() {
            "in" == c.hoverState && c.show();
        }, c.options.delay.show)) : c.show();
    }, b.prototype.leave = function(b) {
        var c = b instanceof this.constructor ? b : a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs." + this.type);
        return clearTimeout(c.timeout), c.hoverState = "out", c.options.delay && c.options.delay.hide ? void (c.timeout = setTimeout(function() {
            "out" == c.hoverState && c.hide();
        }, c.options.delay.hide)) : c.hide();
    }, b.prototype.show = function() {
        var b = a.Event("show.bs." + this.type);
        if (this.hasContent() && this.enabled) {
            if (this.$element.trigger(b), b.isDefaultPrevented()) return;
            var c = this, d = this.tip();
            this.setContent(), this.options.animation && d.addClass("fade");
            var e = "function" == typeof this.options.placement ? this.options.placement.call(this, d[0], this.$element[0]) : this.options.placement, f = /\s?auto?\s?/i, g = f.test(e);
            g && (e = e.replace(f, "") || "top"), d.detach().css({
                top: 0,
                left: 0,
                display: "block"
            }).addClass(e), this.options.container ? d.appendTo(this.options.container) : d.insertAfter(this.$element);
            var h = this.getPosition(), i = d[0].offsetWidth, j = d[0].offsetHeight;
            if (g) {
                var k = this.$element.parent(), l = e, m = document.documentElement.scrollTop || document.body.scrollTop, n = "body" == this.options.container ? window.innerWidth : k.outerWidth(), o = "body" == this.options.container ? window.innerHeight : k.outerHeight(), p = "body" == this.options.container ? 0 : k.offset().left;
                e = "bottom" == e && h.top + h.height + j - m > o ? "top" : "top" == e && h.top - m - j < 0 ? "bottom" : "right" == e && h.right + i > n ? "left" : "left" == e && h.left - i < p ? "right" : e, 
                d.removeClass(l).addClass(e);
            }
            var q = this.getCalculatedOffset(e, h, i, j);
            this.applyPlacement(q, e), this.hoverState = null;
            var r = function() {
                c.$element.trigger("shown.bs." + c.type);
            };
            a.support.transition && this.$tip.hasClass("fade") ? d.one(a.support.transition.end, r).emulateTransitionEnd(150) : r();
        }
    }, b.prototype.applyPlacement = function(b, c) {
        var d, e = this.tip(), f = e[0].offsetWidth, g = e[0].offsetHeight, h = parseInt(e.css("margin-top"), 10), i = parseInt(e.css("margin-left"), 10);
        isNaN(h) && (h = 0), isNaN(i) && (i = 0), b.top = b.top + h, b.left = b.left + i, 
        a.offset.setOffset(e[0], a.extend({
            using: function(a) {
                e.css({
                    top: Math.round(a.top),
                    left: Math.round(a.left)
                });
            }
        }, b), 0), e.addClass("in");
        var j = e[0].offsetWidth, k = e[0].offsetHeight;
        if ("top" == c && k != g && (d = !0, b.top = b.top + g - k), /bottom|top/.test(c)) {
            var l = 0;
            b.left < 0 && (l = -2 * b.left, b.left = 0, e.offset(b), j = e[0].offsetWidth, k = e[0].offsetHeight), 
            this.replaceArrow(l - f + j, j, "left");
        } else this.replaceArrow(k - g, k, "top");
        d && e.offset(b);
    }, b.prototype.replaceArrow = function(a, b, c) {
        this.arrow().css(c, a ? 50 * (1 - a / b) + "%" : "");
    }, b.prototype.setContent = function() {
        var a = this.tip(), b = this.getTitle();
        a.find(".tooltip-inner")[this.options.html ? "html" : "text"](b), a.removeClass("fade in top bottom left right");
    }, b.prototype.hide = function() {
        function b() {
            "in" != c.hoverState && d.detach(), c.$element.trigger("hidden.bs." + c.type);
        }
        var c = this, d = this.tip(), e = a.Event("hide.bs." + this.type);
        return this.$element.trigger(e), e.isDefaultPrevented() ? void 0 : (d.removeClass("in"), 
        a.support.transition && this.$tip.hasClass("fade") ? d.one(a.support.transition.end, b).emulateTransitionEnd(150) : b(), 
        this.hoverState = null, this);
    }, b.prototype.fixTitle = function() {
        var a = this.$element;
        (a.attr("title") || "string" != typeof a.attr("data-original-title")) && a.attr("data-original-title", a.attr("title") || "").attr("title", "");
    }, b.prototype.hasContent = function() {
        return this.getTitle();
    }, b.prototype.getPosition = function() {
        var b = this.$element[0];
        return a.extend({}, "function" == typeof b.getBoundingClientRect ? b.getBoundingClientRect() : {
            width: b.offsetWidth,
            height: b.offsetHeight
        }, this.$element.offset());
    }, b.prototype.getCalculatedOffset = function(a, b, c, d) {
        return "bottom" == a ? {
            top: b.top + b.height,
            left: b.left + b.width / 2 - c / 2
        } : "top" == a ? {
            top: b.top - d,
            left: b.left + b.width / 2 - c / 2
        } : "left" == a ? {
            top: b.top + b.height / 2 - d / 2,
            left: b.left - c
        } : {
            top: b.top + b.height / 2 - d / 2,
            left: b.left + b.width
        };
    }, b.prototype.getTitle = function() {
        var a, b = this.$element, c = this.options;
        return a = b.attr("data-original-title") || ("function" == typeof c.title ? c.title.call(b[0]) : c.title);
    }, b.prototype.tip = function() {
        return this.$tip = this.$tip || a(this.options.template);
    }, b.prototype.arrow = function() {
        return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow");
    }, b.prototype.validate = function() {
        this.$element[0].parentNode || (this.hide(), this.$element = null, this.options = null);
    }, b.prototype.enable = function() {
        this.enabled = !0;
    }, b.prototype.disable = function() {
        this.enabled = !1;
    }, b.prototype.toggleEnabled = function() {
        this.enabled = !this.enabled;
    }, b.prototype.toggle = function(b) {
        var c = b ? a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs." + this.type) : this;
        c.tip().hasClass("in") ? c.leave(c) : c.enter(c);
    }, b.prototype.destroy = function() {
        clearTimeout(this.timeout), this.hide().$element.off("." + this.type).removeData("bs." + this.type);
    };
    var c = a.fn.tooltip;
    a.fn.tooltip = function(c) {
        return this.each(function() {
            var d = a(this), e = d.data("bs.tooltip"), f = "object" == typeof c && c;
            (e || "destroy" != c) && (e || d.data("bs.tooltip", e = new b(this, f)), "string" == typeof c && e[c]());
        });
    }, a.fn.tooltip.Constructor = b, a.fn.tooltip.noConflict = function() {
        return a.fn.tooltip = c, this;
    };
}(jQuery), +function(a) {
    "use strict";
    var b = function(a, b) {
        this.init("popover", a, b);
    };
    if (!a.fn.tooltip) throw new Error("Popover requires tooltip.js");
    b.DEFAULTS = a.extend({}, a.fn.tooltip.Constructor.DEFAULTS, {
        placement: "right",
        trigger: "click",
        content: "",
        template: '<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    }), b.prototype = a.extend({}, a.fn.tooltip.Constructor.prototype), b.prototype.constructor = b, 
    b.prototype.getDefaults = function() {
        return b.DEFAULTS;
    }, b.prototype.setContent = function() {
        var a = this.tip(), b = this.getTitle(), c = this.getContent();
        a.find(".popover-title")[this.options.html ? "html" : "text"](b), a.find(".popover-content")[this.options.html ? "string" == typeof c ? "html" : "append" : "text"](c), 
        a.removeClass("fade top bottom left right in"), a.find(".popover-title").html() || a.find(".popover-title").hide();
    }, b.prototype.hasContent = function() {
        return this.getTitle() || this.getContent();
    }, b.prototype.getContent = function() {
        var a = this.$element, b = this.options;
        return a.attr("data-content") || ("function" == typeof b.content ? b.content.call(a[0]) : b.content);
    }, b.prototype.arrow = function() {
        return this.$arrow = this.$arrow || this.tip().find(".arrow");
    }, b.prototype.tip = function() {
        return this.$tip || (this.$tip = a(this.options.template)), this.$tip;
    };
    var c = a.fn.popover;
    a.fn.popover = function(c) {
        return this.each(function() {
            var d = a(this), e = d.data("bs.popover"), f = "object" == typeof c && c;
            (e || "destroy" != c) && (e || d.data("bs.popover", e = new b(this, f)), "string" == typeof c && e[c]());
        });
    }, a.fn.popover.Constructor = b, a.fn.popover.noConflict = function() {
        return a.fn.popover = c, this;
    };
}(jQuery), +function(a) {
    "use strict";
    function b(c, d) {
        var e, f = a.proxy(this.process, this);
        this.$element = a(a(c).is("body") ? window : c), this.$body = a("body"), this.$scrollElement = this.$element.on("scroll.bs.scroll-spy.data-api", f), 
        this.options = a.extend({}, b.DEFAULTS, d), this.selector = (this.options.target || (e = a(c).attr("href")) && e.replace(/.*(?=#[^\s]+$)/, "") || "") + " .nav li > a", 
        this.offsets = a([]), this.targets = a([]), this.activeTarget = null, this.refresh(), 
        this.process();
    }
    b.DEFAULTS = {
        offset: 10
    }, b.prototype.refresh = function() {
        var b = this.$element[0] == window ? "offset" : "position";
        this.offsets = a([]), this.targets = a([]);
        {
            var c = this;
            this.$body.find(this.selector).map(function() {
                var d = a(this), e = d.data("target") || d.attr("href"), f = /^#./.test(e) && a(e);
                return f && f.length && f.is(":visible") && [ [ f[b]().top + (!a.isWindow(c.$scrollElement.get(0)) && c.$scrollElement.scrollTop()), e ] ] || null;
            }).sort(function(a, b) {
                return a[0] - b[0];
            }).each(function() {
                c.offsets.push(this[0]), c.targets.push(this[1]);
            });
        }
    }, b.prototype.process = function() {
        var a, b = this.$scrollElement.scrollTop() + this.options.offset, c = this.$scrollElement[0].scrollHeight || this.$body[0].scrollHeight, d = c - this.$scrollElement.height(), e = this.offsets, f = this.targets, g = this.activeTarget;
        if (b >= d) return g != (a = f.last()[0]) && this.activate(a);
        if (g && b <= e[0]) return g != (a = f[0]) && this.activate(a);
        for (a = e.length; a--; ) g != f[a] && b >= e[a] && (!e[a + 1] || b <= e[a + 1]) && this.activate(f[a]);
    }, b.prototype.activate = function(b) {
        this.activeTarget = b, a(this.selector).parentsUntil(this.options.target, ".active").removeClass("active");
        var c = this.selector + '[data-target="' + b + '"],' + this.selector + '[href="' + b + '"]', d = a(c).parents("li").addClass("active");
        d.parent(".dropdown-menu").length && (d = d.closest("li.dropdown").addClass("active")), 
        d.trigger("activate.bs.scrollspy");
    };
    var c = a.fn.scrollspy;
    a.fn.scrollspy = function(c) {
        return this.each(function() {
            var d = a(this), e = d.data("bs.scrollspy"), f = "object" == typeof c && c;
            e || d.data("bs.scrollspy", e = new b(this, f)), "string" == typeof c && e[c]();
        });
    }, a.fn.scrollspy.Constructor = b, a.fn.scrollspy.noConflict = function() {
        return a.fn.scrollspy = c, this;
    }, a(window).on("load", function() {
        a('[data-spy="scroll"]').each(function() {
            var b = a(this);
            b.scrollspy(b.data());
        });
    });
}(jQuery), +function(a) {
    "use strict";
    var b = function(b) {
        this.element = a(b);
    };
    b.prototype.show = function() {
        var b = this.element, c = b.closest("ul:not(.dropdown-menu)"), d = b.data("target");
        if (d || (d = b.attr("href"), d = d && d.replace(/.*(?=#[^\s]*$)/, "")), !b.parent("li").hasClass("active")) {
            var e = c.find(".active:last a")[0], f = a.Event("show.bs.tab", {
                relatedTarget: e
            });
            if (b.trigger(f), !f.isDefaultPrevented()) {
                var g = a(d);
                this.activate(b.parent("li"), c), this.activate(g, g.parent(), function() {
                    b.trigger({
                        type: "shown.bs.tab",
                        relatedTarget: e
                    });
                });
            }
        }
    }, b.prototype.activate = function(b, c, d) {
        function e() {
            f.removeClass("active").find("> .dropdown-menu > .active").removeClass("active"), 
            b.addClass("active"), g ? (b[0].offsetWidth, b.addClass("in")) : b.removeClass("fade"), 
            b.parent(".dropdown-menu") && b.closest("li.dropdown").addClass("active"), d && d();
        }
        var f = c.find("> .active"), g = d && a.support.transition && f.hasClass("fade");
        g ? f.one(a.support.transition.end, e).emulateTransitionEnd(150) : e(), f.removeClass("in");
    };
    var c = a.fn.tab;
    a.fn.tab = function(c) {
        return this.each(function() {
            var d = a(this), e = d.data("bs.tab");
            e || d.data("bs.tab", e = new b(this)), "string" == typeof c && e[c]();
        });
    }, a.fn.tab.Constructor = b, a.fn.tab.noConflict = function() {
        return a.fn.tab = c, this;
    }, a(document).on("click.bs.tab.data-api", '[data-toggle="tab"], [data-toggle="pill"]', function(b) {
        b.preventDefault(), a(this).tab("show");
    });
}(jQuery), +function(a) {
    "use strict";
    var b = function(c, d) {
        this.options = a.extend({}, b.DEFAULTS, d), this.$window = a(window).on("scroll.bs.affix.data-api", a.proxy(this.checkPosition, this)).on("click.bs.affix.data-api", a.proxy(this.checkPositionWithEventLoop, this)), 
        this.$element = a(c), this.affixed = this.unpin = this.pinnedOffset = null, this.checkPosition();
    };
    b.RESET = "affix affix-top affix-bottom", b.DEFAULTS = {
        offset: 0
    }, b.prototype.getPinnedOffset = function() {
        if (this.pinnedOffset) return this.pinnedOffset;
        this.$element.removeClass(b.RESET).addClass("affix");
        var a = this.$window.scrollTop(), c = this.$element.offset();
        return this.pinnedOffset = c.top - a;
    }, b.prototype.checkPositionWithEventLoop = function() {
        setTimeout(a.proxy(this.checkPosition, this), 1);
    }, b.prototype.checkPosition = function() {
        if (this.$element.is(":visible")) {
            var c = a(document).height(), d = this.$window.scrollTop(), e = this.$element.offset(), f = this.options.offset, g = f.top, h = f.bottom;
            "top" == this.affixed && (e.top += d), "object" != typeof f && (h = g = f), "function" == typeof g && (g = f.top(this.$element)), 
            "function" == typeof h && (h = f.bottom(this.$element));
            var i = null != this.unpin && d + this.unpin <= e.top ? !1 : null != h && e.top + this.$element.height() >= c - h ? "bottom" : null != g && g >= d ? "top" : !1;
            if (this.affixed !== i) {
                this.unpin && this.$element.css("top", "");
                var j = "affix" + (i ? "-" + i : ""), k = a.Event(j + ".bs.affix");
                this.$element.trigger(k), k.isDefaultPrevented() || (this.affixed = i, this.unpin = "bottom" == i ? this.getPinnedOffset() : null, 
                this.$element.removeClass(b.RESET).addClass(j).trigger(a.Event(j.replace("affix", "affixed"))), 
                "bottom" == i && this.$element.offset({
                    top: c - h - this.$element.height()
                }));
            }
        }
    };
    var c = a.fn.affix;
    a.fn.affix = function(c) {
        return this.each(function() {
            var d = a(this), e = d.data("bs.affix"), f = "object" == typeof c && c;
            e || d.data("bs.affix", e = new b(this, f)), "string" == typeof c && e[c]();
        });
    }, a.fn.affix.Constructor = b, a.fn.affix.noConflict = function() {
        return a.fn.affix = c, this;
    }, a(window).on("load", function() {
        a('[data-spy="affix"]').each(function() {
            var b = a(this), c = b.data();
            c.offset = c.offset || {}, c.offsetBottom && (c.offset.bottom = c.offsetBottom), 
            c.offsetTop && (c.offset.top = c.offsetTop), b.affix(c);
        });
    });
}(jQuery);

!function(a, b) {
    "use strict";
    "function" == typeof define && define.amd ? define([ "jquery" ], b) : "object" == typeof exports ? module.exports = b(require("jquery")) : a.bootbox = b(a.jQuery);
}(this, function a(b, c) {
    "use strict";
    function d(a) {
        var b = q[o.locale];
        return b ? b[a] : q.en[a];
    }
    function e(a, c, d) {
        a.stopPropagation(), a.preventDefault();
        var e = b.isFunction(d) && d(a) === !1;
        e || c.modal("hide");
    }
    function f(a) {
        var b, c = 0;
        for (b in a) c++;
        return c;
    }
    function g(a, c) {
        var d = 0;
        b.each(a, function(a, b) {
            c(a, b, d++);
        });
    }
    function h(a) {
        var c, d;
        if ("object" != typeof a) throw new Error("Please supply an object of options");
        if (!a.message) throw new Error("Please specify a message");
        return a = b.extend({}, o, a), a.buttons || (a.buttons = {}), a.backdrop = a.backdrop ? "static" : !1, 
        c = a.buttons, d = f(c), g(c, function(a, e, f) {
            if (b.isFunction(e) && (e = c[a] = {
                callback: e
            }), "object" !== b.type(e)) throw new Error("button with key " + a + " must be an object");
            e.label || (e.label = a), e.className || (e.className = 2 >= d && f === d - 1 ? "btn-primary" : "btn-default");
        }), a;
    }
    function i(a, b) {
        var c = a.length, d = {};
        if (1 > c || c > 2) throw new Error("Invalid argument length");
        return 2 === c || "string" == typeof a[0] ? (d[b[0]] = a[0], d[b[1]] = a[1]) : d = a[0], 
        d;
    }
    function j(a, c, d) {
        return b.extend(!0, {}, a, i(c, d));
    }
    function k(a, b, c, d) {
        var e = {
            className: "bootbox-" + a,
            buttons: l.apply(null, b)
        };
        return m(j(e, d, c), b);
    }
    function l() {
        for (var a = {}, b = 0, c = arguments.length; c > b; b++) {
            var e = arguments[b], f = e.toLowerCase(), g = e.toUpperCase();
            a[f] = {
                label: d(g)
            };
        }
        return a;
    }
    function m(a, b) {
        var d = {};
        return g(b, function(a, b) {
            d[b] = !0;
        }), g(a.buttons, function(a) {
            if (d[a] === c) throw new Error("button key " + a + " is not allowed (options are " + b.join("\n") + ")");
        }), a;
    }
    var n = {
        dialog: "<div class='bootbox modal' tabindex='-1' role='dialog'><div class='modal-dialog'><div class='modal-content'><div class='modal-body'><div class='bootbox-body'></div></div></div></div></div>",
        header: "<div class='modal-header'><h4 class='modal-title'></h4></div>",
        footer: "<div class='modal-footer'></div>",
        closeButton: "<button type='button' class='bootbox-close-button close' data-dismiss='modal' aria-hidden='true'>&times;</button>",
        form: "<form class='bootbox-form'></form>",
        inputs: {
            text: "<input class='bootbox-input bootbox-input-text form-control' autocomplete=off type=text />",
            textarea: "<textarea class='bootbox-input bootbox-input-textarea form-control'></textarea>",
            email: "<input class='bootbox-input bootbox-input-email form-control' autocomplete='off' type='email' />",
            select: "<select class='bootbox-input bootbox-input-select form-control'></select>",
            checkbox: "<div class='checkbox'><label><input class='bootbox-input bootbox-input-checkbox' type='checkbox' /></label></div>",
            date: "<input class='bootbox-input bootbox-input-date form-control' autocomplete=off type='date' />",
            time: "<input class='bootbox-input bootbox-input-time form-control' autocomplete=off type='time' />",
            number: "<input class='bootbox-input bootbox-input-number form-control' autocomplete=off type='number' />",
            password: "<input class='bootbox-input bootbox-input-password form-control' autocomplete='off' type='password' />"
        }
    }, o = {
        locale: "en",
        backdrop: !0,
        animate: !0,
        className: null,
        closeButton: !0,
        show: !0,
        container: "body"
    }, p = {};
    p.alert = function() {
        var a;
        if (a = k("alert", [ "ok" ], [ "message", "callback" ], arguments), a.callback && !b.isFunction(a.callback)) throw new Error("alert requires callback property to be a function when provided");
        return a.buttons.ok.callback = a.onEscape = function() {
            return b.isFunction(a.callback) ? a.callback() : !0;
        }, p.dialog(a);
    }, p.confirm = function() {
        var a;
        if (a = k("confirm", [ "cancel", "confirm" ], [ "message", "callback" ], arguments), 
        a.buttons.cancel.callback = a.onEscape = function() {
            return a.callback(!1);
        }, a.buttons.confirm.callback = function() {
            return a.callback(!0);
        }, !b.isFunction(a.callback)) throw new Error("confirm requires a callback");
        return p.dialog(a);
    }, p.prompt = function() {
        var a, d, e, f, h, i, k;
        f = b(n.form), d = {
            className: "bootbox-prompt",
            buttons: l("cancel", "confirm"),
            value: "",
            inputType: "text"
        }, a = m(j(d, arguments, [ "title", "callback" ]), [ "cancel", "confirm" ]), i = a.show === c ? !0 : a.show;
        var o = [ "date", "time", "number" ], q = document.createElement("input");
        if (q.setAttribute("type", a.inputType), o[a.inputType] && (a.inputType = q.type), 
        a.message = f, a.buttons.cancel.callback = a.onEscape = function() {
            return a.callback(null);
        }, a.buttons.confirm.callback = function() {
            var c;
            switch (a.inputType) {
              case "text":
              case "textarea":
              case "email":
              case "select":
              case "date":
              case "time":
              case "number":
              case "password":
                c = h.val();
                break;

              case "checkbox":
                var d = h.find("input:checked");
                c = [], g(d, function(a, d) {
                    c.push(b(d).val());
                });
            }
            return a.callback(c);
        }, a.show = !1, !a.title) throw new Error("prompt requires a title");
        if (!b.isFunction(a.callback)) throw new Error("prompt requires a callback");
        if (!n.inputs[a.inputType]) throw new Error("invalid prompt type");
        switch (h = b(n.inputs[a.inputType]), a.inputType) {
          case "text":
          case "textarea":
          case "email":
          case "date":
          case "time":
          case "number":
          case "password":
            h.val(a.value);
            break;

          case "select":
            var r = {};
            if (k = a.inputOptions || [], !k.length) throw new Error("prompt with select requires options");
            g(k, function(a, d) {
                var e = h;
                if (d.value === c || d.text === c) throw new Error("given options in wrong format");
                d.group && (r[d.group] || (r[d.group] = b("<optgroup/>").attr("label", d.group)), 
                e = r[d.group]), e.append("<option value='" + d.value + "'>" + d.text + "</option>");
            }), g(r, function(a, b) {
                h.append(b);
            }), h.val(a.value);
            break;

          case "checkbox":
            var s = b.isArray(a.value) ? a.value : [ a.value ];
            if (k = a.inputOptions || [], !k.length) throw new Error("prompt with checkbox requires options");
            if (!k[0].value || !k[0].text) throw new Error("given options in wrong format");
            h = b("<div/>"), g(k, function(c, d) {
                var e = b(n.inputs[a.inputType]);
                e.find("input").attr("value", d.value), e.find("label").append(d.text), g(s, function(a, b) {
                    b === d.value && e.find("input").prop("checked", !0);
                }), h.append(e);
            });
        }
        return a.placeholder && h.attr("placeholder", a.placeholder), a.pattern && h.attr("pattern", a.pattern), 
        f.append(h), f.on("submit", function(a) {
            a.preventDefault(), e.find(".btn-primary").click();
        }), e = p.dialog(a), e.off("shown.bs.modal"), e.on("shown.bs.modal", function() {
            h.focus();
        }), i === !0 && e.modal("show"), e;
    }, p.dialog = function(a) {
        a = h(a);
        var c = b(n.dialog), d = c.find(".modal-body"), f = a.buttons, i = "", j = {
            onEscape: a.onEscape
        };
        if (g(f, function(a, b) {
            i += "<button data-bb-handler='" + a + "' type='button' class='btn " + b.className + "'>" + b.label + "</button>", 
            j[a] = b.callback;
        }), d.find(".bootbox-body").html(a.message), a.animate === !0 && c.addClass("fade"), 
        a.className && c.addClass(a.className), a.title && d.before(n.header), a.closeButton) {
            var k = b(n.closeButton);
            a.title ? c.find(".modal-header").prepend(k) : k.css("margin-top", "-10px").prependTo(d);
        }
        return a.title && c.find(".modal-title").html(a.title), i.length && (d.after(n.footer), 
        c.find(".modal-footer").html(i)), c.on("hidden.bs.modal", function(a) {
            a.target === this && c.remove();
        }), c.on("shown.bs.modal", function() {
            c.find(".btn-primary:first").focus();
        }), c.on("escape.close.bb", function(a) {
            j.onEscape && e(a, c, j.onEscape);
        }), c.on("click", ".modal-footer button", function(a) {
            var d = b(this).data("bb-handler");
            e(a, c, j[d]);
        }), c.on("click", ".bootbox-close-button", function(a) {
            e(a, c, j.onEscape);
        }), c.on("keyup", function(a) {
            27 === a.which && c.trigger("escape.close.bb");
        }), b(a.container).append(c), c.modal({
            backdrop: a.backdrop,
            keyboard: !1,
            show: !1
        }), a.show && c.modal("show"), c;
    }, p.setDefaults = function() {
        var a = {};
        2 === arguments.length ? a[arguments[0]] = arguments[1] : a = arguments[0], b.extend(o, a);
    }, p.hideAll = function() {
        b(".bootbox").modal("hide");
    };
    var q = {
        br: {
            OK: "OK",
            CANCEL: "Cancelar",
            CONFIRM: "Sim"
        },
        da: {
            OK: "OK",
            CANCEL: "Annuller",
            CONFIRM: "Accepter"
        },
        de: {
            OK: "OK",
            CANCEL: "Abbrechen",
            CONFIRM: "Akzeptieren"
        },
        en: {
            OK: "OK",
            CANCEL: "Cancel",
            CONFIRM: "OK"
        },
        es: {
            OK: "OK",
            CANCEL: "Cancelar",
            CONFIRM: "Aceptar"
        },
        fi: {
            OK: "OK",
            CANCEL: "Peruuta",
            CONFIRM: "OK"
        },
        fr: {
            OK: "OK",
            CANCEL: "Annuler",
            CONFIRM: "D'accord"
        },
        he: {
            OK: "אישור",
            CANCEL: "ביטול",
            CONFIRM: "אישור"
        },
        it: {
            OK: "OK",
            CANCEL: "Annulla",
            CONFIRM: "Conferma"
        },
        lt: {
            OK: "Gerai",
            CANCEL: "Atšaukti",
            CONFIRM: "Patvirtinti"
        },
        lv: {
            OK: "Labi",
            CANCEL: "Atcelt",
            CONFIRM: "Apstiprināt"
        },
        nl: {
            OK: "OK",
            CANCEL: "Annuleren",
            CONFIRM: "Accepteren"
        },
        no: {
            OK: "OK",
            CANCEL: "Avbryt",
            CONFIRM: "OK"
        },
        pl: {
            OK: "OK",
            CANCEL: "Anuluj",
            CONFIRM: "Potwierdź"
        },
        ru: {
            OK: "OK",
            CANCEL: "Отмена",
            CONFIRM: "Применить"
        },
        sv: {
            OK: "OK",
            CANCEL: "Avbryt",
            CONFIRM: "OK"
        },
        tr: {
            OK: "Tamam",
            CANCEL: "İptal",
            CONFIRM: "Onayla"
        },
        zh_CN: {
            OK: "OK",
            CANCEL: "取消",
            CONFIRM: "确认"
        },
        zh_TW: {
            OK: "OK",
            CANCEL: "取消",
            CONFIRM: "確認"
        }
    };
    return p.init = function(c) {
        return a(c || b);
    }, p;
});

(function(factory) {
    if (typeof define === "function" && define.amd) {
        define([ "jquery" ], factory);
    } else if (typeof exports === "object") {
        factory(require("jquery"));
    } else {
        factory(jQuery);
    }
})(function($) {
    var pluses = /\+/g;
    function encode(s) {
        return config.raw ? s : encodeURIComponent(s);
    }
    function decode(s) {
        return config.raw ? s : decodeURIComponent(s);
    }
    function stringifyCookieValue(value) {
        return encode(config.json ? JSON.stringify(value) : String(value));
    }
    function parseCookieValue(s) {
        if (s.indexOf('"') === 0) {
            s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, "\\");
        }
        try {
            s = decodeURIComponent(s.replace(pluses, " "));
            return config.json ? JSON.parse(s) : s;
        } catch (e) {}
    }
    function read(s, converter) {
        var value = config.raw ? s : parseCookieValue(s);
        return $.isFunction(converter) ? converter(value) : value;
    }
    var config = $.cookie = function(key, value, options) {
        if (value !== undefined && !$.isFunction(value)) {
            options = $.extend({}, config.defaults, options);
            if (typeof options.expires === "number") {
                var days = options.expires, t = options.expires = new Date();
                t.setTime(+t + days * 864e5);
            }
            return document.cookie = [ encode(key), "=", stringifyCookieValue(value), options.expires ? "; expires=" + options.expires.toUTCString() : "", options.path ? "; path=" + options.path : "", options.domain ? "; domain=" + options.domain : "", options.secure ? "; secure" : "" ].join("");
        }
        var result = key ? undefined : {};
        var cookies = document.cookie ? document.cookie.split("; ") : [];
        for (var i = 0, l = cookies.length; i < l; i++) {
            var parts = cookies[i].split("=");
            var name = decode(parts.shift());
            var cookie = parts.join("=");
            if (key && key === name) {
                result = read(cookie, value);
                break;
            }
            if (!key && (cookie = read(cookie)) !== undefined) {
                result[name] = cookie;
            }
        }
        return result;
    };
    config.defaults = {};
    $.removeCookie = function(key, options) {
        if ($.cookie(key) === undefined) {
            return false;
        }
        $.cookie(key, "", $.extend({}, options, {
            expires: -1
        }));
        return !$.cookie(key);
    };
});

!function(a) {
    a([ "jquery" ], function(a) {
        return function() {
            function b(a, b, c) {
                return m({
                    type: t.error,
                    iconClass: o().iconClasses.error,
                    message: a,
                    optionsOverride: c,
                    title: b
                });
            }
            function c(a, b, c) {
                return m({
                    type: t.info,
                    iconClass: o().iconClasses.info,
                    message: a,
                    optionsOverride: c,
                    title: b
                });
            }
            function d(a) {
                r = a;
            }
            function e(a, b, c) {
                return m({
                    type: t.success,
                    iconClass: o().iconClasses.success,
                    message: a,
                    optionsOverride: c,
                    title: b
                });
            }
            function f(a, b, c) {
                return m({
                    type: t.warning,
                    iconClass: o().iconClasses.warning,
                    message: a,
                    optionsOverride: c,
                    title: b
                });
            }
            function g(a) {
                var b = o();
                q || n(b), j(a, b) || i(b);
            }
            function h(b) {
                var c = o();
                return q || n(c), b && 0 === a(":focus", b).length ? void p(b) : void (q.children().length && q.remove());
            }
            function i(b) {
                for (var c = q.children(), d = c.length - 1; d >= 0; d--) j(a(c[d]), b);
            }
            function j(b, c) {
                return b && 0 === a(":focus", b).length ? (b[c.hideMethod]({
                    duration: c.hideDuration,
                    easing: c.hideEasing,
                    complete: function() {
                        p(b);
                    }
                }), !0) : !1;
            }
            function k() {
                return {
                    tapToDismiss: !0,
                    toastClass: "toast",
                    containerId: "toast-container",
                    debug: !1,
                    showMethod: "fadeIn",
                    showDuration: 300,
                    showEasing: "swing",
                    onShown: void 0,
                    hideMethod: "fadeOut",
                    hideDuration: 1e3,
                    hideEasing: "swing",
                    onHidden: void 0,
                    extendedTimeOut: 1e3,
                    iconClasses: {
                        error: "toast-error",
                        info: "toast-info",
                        success: "toast-success",
                        warning: "toast-warning"
                    },
                    iconClass: "toast-info",
                    positionClass: "toast-top-right",
                    timeOut: 5e3,
                    titleClass: "toast-title",
                    messageClass: "toast-message",
                    target: "body",
                    closeHtml: "<button>&times;</button>",
                    newestOnTop: !0
                };
            }
            function l(a) {
                r && r(a);
            }
            function m(b) {
                function c(b) {
                    return !a(":focus", i).length || b ? i[f.hideMethod]({
                        duration: f.hideDuration,
                        easing: f.hideEasing,
                        complete: function() {
                            p(i), f.onHidden && "hidden" !== r.state && f.onHidden(), r.state = "hidden", r.endTime = new Date(), 
                            l(r);
                        }
                    }) : void 0;
                }
                function d() {
                    (f.timeOut > 0 || f.extendedTimeOut > 0) && (h = setTimeout(c, f.extendedTimeOut));
                }
                function e() {
                    clearTimeout(h), i.stop(!0, !0)[f.showMethod]({
                        duration: f.showDuration,
                        easing: f.showEasing
                    });
                }
                var f = o(), g = b.iconClass || f.iconClass;
                "undefined" != typeof b.optionsOverride && (f = a.extend(f, b.optionsOverride), 
                g = b.optionsOverride.iconClass || g), s++, q = n(f);
                var h = null, i = a("<div/>"), j = a("<div/>"), k = a("<div/>"), m = a(f.closeHtml), r = {
                    toastId: s,
                    state: "visible",
                    startTime: new Date(),
                    options: f,
                    map: b
                };
                return b.iconClass && i.addClass(f.toastClass).addClass(g), b.title && (j.append(b.title).addClass(f.titleClass), 
                i.append(j)), b.message && (k.append(b.message).addClass(f.messageClass), i.append(k)), 
                f.closeButton && (m.addClass("toast-close-button").attr("role", "button"), i.prepend(m)), 
                i.hide(), f.newestOnTop ? q.prepend(i) : q.append(i), i[f.showMethod]({
                    duration: f.showDuration,
                    easing: f.showEasing,
                    complete: f.onShown
                }), f.timeOut > 0 && (h = setTimeout(c, f.timeOut)), i.hover(e, d), !f.onclick && f.tapToDismiss && i.click(c), 
                f.closeButton && m && m.click(function(a) {
                    a.stopPropagation ? a.stopPropagation() : void 0 !== a.cancelBubble && a.cancelBubble !== !0 && (a.cancelBubble = !0), 
                    c(!0);
                }), f.onclick && i.click(function() {
                    f.onclick(), c();
                }), l(r), f.debug && console && console.log(r), i;
            }
            function n(b) {
                return b || (b = o()), q = a("#" + b.containerId), q.length ? q : (q = a("<div/>").attr("id", b.containerId).addClass(b.positionClass).attr("aria-live", "polite").attr("role", "alert"), 
                q.appendTo(a(b.target)), q);
            }
            function o() {
                return a.extend({}, k(), u.options);
            }
            function p(a) {
                q || (q = n()), a.is(":visible") || (a.remove(), a = null, 0 === q.children().length && q.remove());
            }
            var q, r, s = 0, t = {
                error: "error",
                info: "info",
                success: "success",
                warning: "warning"
            }, u = {
                clear: g,
                remove: h,
                error: b,
                getContainer: n,
                info: c,
                options: {},
                subscribe: d,
                success: e,
                version: "2.0.2",
                warning: f
            };
            return u;
        }();
    });
}("function" == typeof define && define.amd ? define : function(a, b) {
    "undefined" != typeof module && module.exports ? module.exports = b(require("jquery")) : window.toastr = b(window.jQuery);
});

(function(window) {
    var slice = Array.prototype.slice;
    function noop() {}
    function defineBridget($) {
        if (!$) {
            return;
        }
        function addOptionMethod(PluginClass) {
            if (PluginClass.prototype.option) {
                return;
            }
            PluginClass.prototype.option = function(opts) {
                if (!$.isPlainObject(opts)) {
                    return;
                }
                this.options = $.extend(true, this.options, opts);
            };
        }
        var logError = typeof console === "undefined" ? noop : function(message) {
            console.error(message);
        };
        function bridge(namespace, PluginClass) {
            $.fn[namespace] = function(options) {
                if (typeof options === "string") {
                    var args = slice.call(arguments, 1);
                    for (var i = 0, len = this.length; i < len; i++) {
                        var elem = this[i];
                        var instance = $.data(elem, namespace);
                        if (!instance) {
                            logError("cannot call methods on " + namespace + " prior to initialization; " + "attempted to call '" + options + "'");
                            continue;
                        }
                        if (!$.isFunction(instance[options]) || options.charAt(0) === "_") {
                            logError("no such method '" + options + "' for " + namespace + " instance");
                            continue;
                        }
                        var returnValue = instance[options].apply(instance, args);
                        if (returnValue !== undefined) {
                            return returnValue;
                        }
                    }
                    return this;
                } else {
                    return this.each(function() {
                        var instance = $.data(this, namespace);
                        if (instance) {
                            instance.option(options);
                            instance._init();
                        } else {
                            instance = new PluginClass(this, options);
                            $.data(this, namespace, instance);
                        }
                    });
                }
            };
        }
        $.bridget = function(namespace, PluginClass) {
            addOptionMethod(PluginClass);
            bridge(namespace, PluginClass);
        };
        return $.bridget;
    }
    if (typeof define === "function" && define.amd) {
        define("jquery-bridget/jquery.bridget", [ "jquery" ], defineBridget);
    } else {
        defineBridget(window.jQuery);
    }
})(window);

(function(window) {
    var docElem = document.documentElement;
    var bind = function() {};
    function getIEEvent(obj) {
        var event = window.event;
        event.target = event.target || event.srcElement || obj;
        return event;
    }
    if (docElem.addEventListener) {
        bind = function(obj, type, fn) {
            obj.addEventListener(type, fn, false);
        };
    } else if (docElem.attachEvent) {
        bind = function(obj, type, fn) {
            obj[type + fn] = fn.handleEvent ? function() {
                var event = getIEEvent(obj);
                fn.handleEvent.call(fn, event);
            } : function() {
                var event = getIEEvent(obj);
                fn.call(obj, event);
            };
            obj.attachEvent("on" + type, obj[type + fn]);
        };
    }
    var unbind = function() {};
    if (docElem.removeEventListener) {
        unbind = function(obj, type, fn) {
            obj.removeEventListener(type, fn, false);
        };
    } else if (docElem.detachEvent) {
        unbind = function(obj, type, fn) {
            obj.detachEvent("on" + type, obj[type + fn]);
            try {
                delete obj[type + fn];
            } catch (err) {
                obj[type + fn] = undefined;
            }
        };
    }
    var eventie = {
        bind: bind,
        unbind: unbind
    };
    if (typeof define === "function" && define.amd) {
        define("eventie/eventie", eventie);
    } else if (typeof exports === "object") {
        module.exports = eventie;
    } else {
        window.eventie = eventie;
    }
})(this);

(function(window) {
    var document = window.document;
    var queue = [];
    function docReady(fn) {
        if (typeof fn !== "function") {
            return;
        }
        if (docReady.isReady) {
            fn();
        } else {
            queue.push(fn);
        }
    }
    docReady.isReady = false;
    function init(event) {
        var isIE8NotReady = event.type === "readystatechange" && document.readyState !== "complete";
        if (docReady.isReady || isIE8NotReady) {
            return;
        }
        docReady.isReady = true;
        for (var i = 0, len = queue.length; i < len; i++) {
            var fn = queue[i];
            fn();
        }
    }
    function defineDocReady(eventie) {
        eventie.bind(document, "DOMContentLoaded", init);
        eventie.bind(document, "readystatechange", init);
        eventie.bind(window, "load", init);
        return docReady;
    }
    if (typeof define === "function" && define.amd) {
        docReady.isReady = typeof requirejs === "function";
        define("doc-ready/doc-ready", [ "eventie/eventie" ], defineDocReady);
    } else {
        window.docReady = defineDocReady(window.eventie);
    }
})(this);

(function() {
    function EventEmitter() {}
    var proto = EventEmitter.prototype;
    var exports = this;
    var originalGlobalValue = exports.EventEmitter;
    function indexOfListener(listeners, listener) {
        var i = listeners.length;
        while (i--) {
            if (listeners[i].listener === listener) {
                return i;
            }
        }
        return -1;
    }
    function alias(name) {
        return function aliasClosure() {
            return this[name].apply(this, arguments);
        };
    }
    proto.getListeners = function getListeners(evt) {
        var events = this._getEvents();
        var response;
        var key;
        if (evt instanceof RegExp) {
            response = {};
            for (key in events) {
                if (events.hasOwnProperty(key) && evt.test(key)) {
                    response[key] = events[key];
                }
            }
        } else {
            response = events[evt] || (events[evt] = []);
        }
        return response;
    };
    proto.flattenListeners = function flattenListeners(listeners) {
        var flatListeners = [];
        var i;
        for (i = 0; i < listeners.length; i += 1) {
            flatListeners.push(listeners[i].listener);
        }
        return flatListeners;
    };
    proto.getListenersAsObject = function getListenersAsObject(evt) {
        var listeners = this.getListeners(evt);
        var response;
        if (listeners instanceof Array) {
            response = {};
            response[evt] = listeners;
        }
        return response || listeners;
    };
    proto.addListener = function addListener(evt, listener) {
        var listeners = this.getListenersAsObject(evt);
        var listenerIsWrapped = typeof listener === "object";
        var key;
        for (key in listeners) {
            if (listeners.hasOwnProperty(key) && indexOfListener(listeners[key], listener) === -1) {
                listeners[key].push(listenerIsWrapped ? listener : {
                    listener: listener,
                    once: false
                });
            }
        }
        return this;
    };
    proto.on = alias("addListener");
    proto.addOnceListener = function addOnceListener(evt, listener) {
        return this.addListener(evt, {
            listener: listener,
            once: true
        });
    };
    proto.once = alias("addOnceListener");
    proto.defineEvent = function defineEvent(evt) {
        this.getListeners(evt);
        return this;
    };
    proto.defineEvents = function defineEvents(evts) {
        for (var i = 0; i < evts.length; i += 1) {
            this.defineEvent(evts[i]);
        }
        return this;
    };
    proto.removeListener = function removeListener(evt, listener) {
        var listeners = this.getListenersAsObject(evt);
        var index;
        var key;
        for (key in listeners) {
            if (listeners.hasOwnProperty(key)) {
                index = indexOfListener(listeners[key], listener);
                if (index !== -1) {
                    listeners[key].splice(index, 1);
                }
            }
        }
        return this;
    };
    proto.off = alias("removeListener");
    proto.addListeners = function addListeners(evt, listeners) {
        return this.manipulateListeners(false, evt, listeners);
    };
    proto.removeListeners = function removeListeners(evt, listeners) {
        return this.manipulateListeners(true, evt, listeners);
    };
    proto.manipulateListeners = function manipulateListeners(remove, evt, listeners) {
        var i;
        var value;
        var single = remove ? this.removeListener : this.addListener;
        var multiple = remove ? this.removeListeners : this.addListeners;
        if (typeof evt === "object" && !(evt instanceof RegExp)) {
            for (i in evt) {
                if (evt.hasOwnProperty(i) && (value = evt[i])) {
                    if (typeof value === "function") {
                        single.call(this, i, value);
                    } else {
                        multiple.call(this, i, value);
                    }
                }
            }
        } else {
            i = listeners.length;
            while (i--) {
                single.call(this, evt, listeners[i]);
            }
        }
        return this;
    };
    proto.removeEvent = function removeEvent(evt) {
        var type = typeof evt;
        var events = this._getEvents();
        var key;
        if (type === "string") {
            delete events[evt];
        } else if (evt instanceof RegExp) {
            for (key in events) {
                if (events.hasOwnProperty(key) && evt.test(key)) {
                    delete events[key];
                }
            }
        } else {
            delete this._events;
        }
        return this;
    };
    proto.removeAllListeners = alias("removeEvent");
    proto.emitEvent = function emitEvent(evt, args) {
        var listeners = this.getListenersAsObject(evt);
        var listener;
        var i;
        var key;
        var response;
        for (key in listeners) {
            if (listeners.hasOwnProperty(key)) {
                i = listeners[key].length;
                while (i--) {
                    listener = listeners[key][i];
                    if (listener.once === true) {
                        this.removeListener(evt, listener.listener);
                    }
                    response = listener.listener.apply(this, args || []);
                    if (response === this._getOnceReturnValue()) {
                        this.removeListener(evt, listener.listener);
                    }
                }
            }
        }
        return this;
    };
    proto.trigger = alias("emitEvent");
    proto.emit = function emit(evt) {
        var args = Array.prototype.slice.call(arguments, 1);
        return this.emitEvent(evt, args);
    };
    proto.setOnceReturnValue = function setOnceReturnValue(value) {
        this._onceReturnValue = value;
        return this;
    };
    proto._getOnceReturnValue = function _getOnceReturnValue() {
        if (this.hasOwnProperty("_onceReturnValue")) {
            return this._onceReturnValue;
        } else {
            return true;
        }
    };
    proto._getEvents = function _getEvents() {
        return this._events || (this._events = {});
    };
    EventEmitter.noConflict = function noConflict() {
        exports.EventEmitter = originalGlobalValue;
        return EventEmitter;
    };
    if (typeof define === "function" && define.amd) {
        define("eventEmitter/EventEmitter", [], function() {
            return EventEmitter;
        });
    } else if (typeof module === "object" && module.exports) {
        module.exports = EventEmitter;
    } else {
        this.EventEmitter = EventEmitter;
    }
}).call(this);

(function(window) {
    var prefixes = "Webkit Moz ms Ms O".split(" ");
    var docElemStyle = document.documentElement.style;
    function getStyleProperty(propName) {
        if (!propName) {
            return;
        }
        if (typeof docElemStyle[propName] === "string") {
            return propName;
        }
        propName = propName.charAt(0).toUpperCase() + propName.slice(1);
        var prefixed;
        for (var i = 0, len = prefixes.length; i < len; i++) {
            prefixed = prefixes[i] + propName;
            if (typeof docElemStyle[prefixed] === "string") {
                return prefixed;
            }
        }
    }
    if (typeof define === "function" && define.amd) {
        define("get-style-property/get-style-property", [], function() {
            return getStyleProperty;
        });
    } else if (typeof exports === "object") {
        module.exports = getStyleProperty;
    } else {
        window.getStyleProperty = getStyleProperty;
    }
})(window);

(function(window, undefined) {
    var getComputedStyle = window.getComputedStyle;
    var getStyle = getComputedStyle ? function(elem) {
        return getComputedStyle(elem, null);
    } : function(elem) {
        return elem.currentStyle;
    };
    function getStyleSize(value) {
        var num = parseFloat(value);
        var isValid = value.indexOf("%") === -1 && !isNaN(num);
        return isValid && num;
    }
    var measurements = [ "paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth" ];
    function getZeroSize() {
        var size = {
            width: 0,
            height: 0,
            innerWidth: 0,
            innerHeight: 0,
            outerWidth: 0,
            outerHeight: 0
        };
        for (var i = 0, len = measurements.length; i < len; i++) {
            var measurement = measurements[i];
            size[measurement] = 0;
        }
        return size;
    }
    function defineGetSize(getStyleProperty) {
        var boxSizingProp = getStyleProperty("boxSizing");
        var isBoxSizeOuter;
        (function() {
            if (!boxSizingProp) {
                return;
            }
            var div = document.createElement("div");
            div.style.width = "200px";
            div.style.padding = "1px 2px 3px 4px";
            div.style.borderStyle = "solid";
            div.style.borderWidth = "1px 2px 3px 4px";
            div.style[boxSizingProp] = "border-box";
            var body = document.body || document.documentElement;
            body.appendChild(div);
            var style = getStyle(div);
            isBoxSizeOuter = getStyleSize(style.width) === 200;
            body.removeChild(div);
        })();
        function getSize(elem) {
            if (typeof elem === "string") {
                elem = document.querySelector(elem);
            }
            if (!elem || typeof elem !== "object" || !elem.nodeType) {
                return;
            }
            var style = getStyle(elem);
            if (style.display === "none") {
                return getZeroSize();
            }
            var size = {};
            size.width = elem.offsetWidth;
            size.height = elem.offsetHeight;
            var isBorderBox = size.isBorderBox = !!(boxSizingProp && style[boxSizingProp] && style[boxSizingProp] === "border-box");
            for (var i = 0, len = measurements.length; i < len; i++) {
                var measurement = measurements[i];
                var value = style[measurement];
                value = mungeNonPixel(elem, value);
                var num = parseFloat(value);
                size[measurement] = !isNaN(num) ? num : 0;
            }
            var paddingWidth = size.paddingLeft + size.paddingRight;
            var paddingHeight = size.paddingTop + size.paddingBottom;
            var marginWidth = size.marginLeft + size.marginRight;
            var marginHeight = size.marginTop + size.marginBottom;
            var borderWidth = size.borderLeftWidth + size.borderRightWidth;
            var borderHeight = size.borderTopWidth + size.borderBottomWidth;
            var isBorderBoxSizeOuter = isBorderBox && isBoxSizeOuter;
            var styleWidth = getStyleSize(style.width);
            if (styleWidth !== false) {
                size.width = styleWidth + (isBorderBoxSizeOuter ? 0 : paddingWidth + borderWidth);
            }
            var styleHeight = getStyleSize(style.height);
            if (styleHeight !== false) {
                size.height = styleHeight + (isBorderBoxSizeOuter ? 0 : paddingHeight + borderHeight);
            }
            size.innerWidth = size.width - (paddingWidth + borderWidth);
            size.innerHeight = size.height - (paddingHeight + borderHeight);
            size.outerWidth = size.width + marginWidth;
            size.outerHeight = size.height + marginHeight;
            return size;
        }
        function mungeNonPixel(elem, value) {
            if (getComputedStyle || value.indexOf("%") === -1) {
                return value;
            }
            var style = elem.style;
            var left = style.left;
            var rs = elem.runtimeStyle;
            var rsLeft = rs && rs.left;
            if (rsLeft) {
                rs.left = elem.currentStyle.left;
            }
            style.left = value;
            value = style.pixelLeft;
            style.left = left;
            if (rsLeft) {
                rs.left = rsLeft;
            }
            return value;
        }
        return getSize;
    }
    if (typeof define === "function" && define.amd) {
        define("get-size/get-size", [ "get-style-property/get-style-property" ], defineGetSize);
    } else if (typeof exports === "object") {
        module.exports = defineGetSize(require("get-style-property"));
    } else {
        window.getSize = defineGetSize(window.getStyleProperty);
    }
})(window);

(function(global, ElemProto) {
    var matchesMethod = function() {
        if (ElemProto.matchesSelector) {
            return "matchesSelector";
        }
        var prefixes = [ "webkit", "moz", "ms", "o" ];
        for (var i = 0, len = prefixes.length; i < len; i++) {
            var prefix = prefixes[i];
            var method = prefix + "MatchesSelector";
            if (ElemProto[method]) {
                return method;
            }
        }
    }();
    function match(elem, selector) {
        return elem[matchesMethod](selector);
    }
    function checkParent(elem) {
        if (elem.parentNode) {
            return;
        }
        var fragment = document.createDocumentFragment();
        fragment.appendChild(elem);
    }
    function query(elem, selector) {
        checkParent(elem);
        var elems = elem.parentNode.querySelectorAll(selector);
        for (var i = 0, len = elems.length; i < len; i++) {
            if (elems[i] === elem) {
                return true;
            }
        }
        return false;
    }
    function matchChild(elem, selector) {
        checkParent(elem);
        return match(elem, selector);
    }
    var matchesSelector;
    if (matchesMethod) {
        var div = document.createElement("div");
        var supportsOrphans = match(div, "div");
        matchesSelector = supportsOrphans ? match : matchChild;
    } else {
        matchesSelector = query;
    }
    if (typeof define === "function" && define.amd) {
        define("matches-selector/matches-selector", [], function() {
            return matchesSelector;
        });
    } else {
        window.matchesSelector = matchesSelector;
    }
})(this, Element.prototype);

(function(window) {
    var getComputedStyle = window.getComputedStyle;
    var getStyle = getComputedStyle ? function(elem) {
        return getComputedStyle(elem, null);
    } : function(elem) {
        return elem.currentStyle;
    };
    function extend(a, b) {
        for (var prop in b) {
            a[prop] = b[prop];
        }
        return a;
    }
    function isEmptyObj(obj) {
        for (var prop in obj) {
            return false;
        }
        prop = null;
        return true;
    }
    function toDash(str) {
        return str.replace(/([A-Z])/g, function($1) {
            return "-" + $1.toLowerCase();
        });
    }
    function outlayerItemDefinition(EventEmitter, getSize, getStyleProperty) {
        var transitionProperty = getStyleProperty("transition");
        var transformProperty = getStyleProperty("transform");
        var supportsCSS3 = transitionProperty && transformProperty;
        var is3d = !!getStyleProperty("perspective");
        var transitionEndEvent = {
            WebkitTransition: "webkitTransitionEnd",
            MozTransition: "transitionend",
            OTransition: "otransitionend",
            transition: "transitionend"
        }[transitionProperty];
        var prefixableProperties = [ "transform", "transition", "transitionDuration", "transitionProperty" ];
        var vendorProperties = function() {
            var cache = {};
            for (var i = 0, len = prefixableProperties.length; i < len; i++) {
                var prop = prefixableProperties[i];
                var supportedProp = getStyleProperty(prop);
                if (supportedProp && supportedProp !== prop) {
                    cache[prop] = supportedProp;
                }
            }
            return cache;
        }();
        function Item(element, layout) {
            if (!element) {
                return;
            }
            this.element = element;
            this.layout = layout;
            this.position = {
                x: 0,
                y: 0
            };
            this._create();
        }
        extend(Item.prototype, EventEmitter.prototype);
        Item.prototype._create = function() {
            this._transn = {
                ingProperties: {},
                clean: {},
                onEnd: {}
            };
            this.css({
                position: "absolute"
            });
        };
        Item.prototype.handleEvent = function(event) {
            var method = "on" + event.type;
            if (this[method]) {
                this[method](event);
            }
        };
        Item.prototype.getSize = function() {
            this.size = getSize(this.element);
        };
        Item.prototype.css = function(style) {
            var elemStyle = this.element.style;
            for (var prop in style) {
                var supportedProp = vendorProperties[prop] || prop;
                elemStyle[supportedProp] = style[prop];
            }
        };
        Item.prototype.getPosition = function() {
            var style = getStyle(this.element);
            var layoutOptions = this.layout.options;
            var isOriginLeft = layoutOptions.isOriginLeft;
            var isOriginTop = layoutOptions.isOriginTop;
            var x = parseInt(style[isOriginLeft ? "left" : "right"], 10);
            var y = parseInt(style[isOriginTop ? "top" : "bottom"], 10);
            x = isNaN(x) ? 0 : x;
            y = isNaN(y) ? 0 : y;
            var layoutSize = this.layout.size;
            x -= isOriginLeft ? layoutSize.paddingLeft : layoutSize.paddingRight;
            y -= isOriginTop ? layoutSize.paddingTop : layoutSize.paddingBottom;
            this.position.x = x;
            this.position.y = y;
        };
        Item.prototype.layoutPosition = function() {
            var layoutSize = this.layout.size;
            var layoutOptions = this.layout.options;
            var style = {};
            if (layoutOptions.isOriginLeft) {
                style.left = this.position.x + layoutSize.paddingLeft + "px";
                style.right = "";
            } else {
                style.right = this.position.x + layoutSize.paddingRight + "px";
                style.left = "";
            }
            if (layoutOptions.isOriginTop) {
                style.top = this.position.y + layoutSize.paddingTop + "px";
                style.bottom = "";
            } else {
                style.bottom = this.position.y + layoutSize.paddingBottom + "px";
                style.top = "";
            }
            this.css(style);
            this.emitEvent("layout", [ this ]);
        };
        var translate = is3d ? function(x, y) {
            return "translate3d(" + x + "px, " + y + "px, 0)";
        } : function(x, y) {
            return "translate(" + x + "px, " + y + "px)";
        };
        Item.prototype._transitionTo = function(x, y) {
            this.getPosition();
            var curX = this.position.x;
            var curY = this.position.y;
            var compareX = parseInt(x, 10);
            var compareY = parseInt(y, 10);
            var didNotMove = compareX === this.position.x && compareY === this.position.y;
            this.setPosition(x, y);
            if (didNotMove && !this.isTransitioning) {
                this.layoutPosition();
                return;
            }
            var transX = x - curX;
            var transY = y - curY;
            var transitionStyle = {};
            var layoutOptions = this.layout.options;
            transX = layoutOptions.isOriginLeft ? transX : -transX;
            transY = layoutOptions.isOriginTop ? transY : -transY;
            transitionStyle.transform = translate(transX, transY);
            this.transition({
                to: transitionStyle,
                onTransitionEnd: {
                    transform: this.layoutPosition
                },
                isCleaning: true
            });
        };
        Item.prototype.goTo = function(x, y) {
            this.setPosition(x, y);
            this.layoutPosition();
        };
        Item.prototype.moveTo = supportsCSS3 ? Item.prototype._transitionTo : Item.prototype.goTo;
        Item.prototype.setPosition = function(x, y) {
            this.position.x = parseInt(x, 10);
            this.position.y = parseInt(y, 10);
        };
        Item.prototype._nonTransition = function(args) {
            this.css(args.to);
            if (args.isCleaning) {
                this._removeStyles(args.to);
            }
            for (var prop in args.onTransitionEnd) {
                args.onTransitionEnd[prop].call(this);
            }
        };
        Item.prototype._transition = function(args) {
            if (!parseFloat(this.layout.options.transitionDuration)) {
                this._nonTransition(args);
                return;
            }
            var _transition = this._transn;
            for (var prop in args.onTransitionEnd) {
                _transition.onEnd[prop] = args.onTransitionEnd[prop];
            }
            for (prop in args.to) {
                _transition.ingProperties[prop] = true;
                if (args.isCleaning) {
                    _transition.clean[prop] = true;
                }
            }
            if (args.from) {
                this.css(args.from);
                var h = this.element.offsetHeight;
                h = null;
            }
            this.enableTransition(args.to);
            this.css(args.to);
            this.isTransitioning = true;
        };
        var itemTransitionProperties = transformProperty && toDash(transformProperty) + ",opacity";
        Item.prototype.enableTransition = function() {
            if (this.isTransitioning) {
                return;
            }
            this.css({
                transitionProperty: itemTransitionProperties,
                transitionDuration: this.layout.options.transitionDuration
            });
            this.element.addEventListener(transitionEndEvent, this, false);
        };
        Item.prototype.transition = Item.prototype[transitionProperty ? "_transition" : "_nonTransition"];
        Item.prototype.onwebkitTransitionEnd = function(event) {
            this.ontransitionend(event);
        };
        Item.prototype.onotransitionend = function(event) {
            this.ontransitionend(event);
        };
        var dashedVendorProperties = {
            "-webkit-transform": "transform",
            "-moz-transform": "transform",
            "-o-transform": "transform"
        };
        Item.prototype.ontransitionend = function(event) {
            if (event.target !== this.element) {
                return;
            }
            var _transition = this._transn;
            var propertyName = dashedVendorProperties[event.propertyName] || event.propertyName;
            delete _transition.ingProperties[propertyName];
            if (isEmptyObj(_transition.ingProperties)) {
                this.disableTransition();
            }
            if (propertyName in _transition.clean) {
                this.element.style[event.propertyName] = "";
                delete _transition.clean[propertyName];
            }
            if (propertyName in _transition.onEnd) {
                var onTransitionEnd = _transition.onEnd[propertyName];
                onTransitionEnd.call(this);
                delete _transition.onEnd[propertyName];
            }
            this.emitEvent("transitionEnd", [ this ]);
        };
        Item.prototype.disableTransition = function() {
            this.removeTransitionStyles();
            this.element.removeEventListener(transitionEndEvent, this, false);
            this.isTransitioning = false;
        };
        Item.prototype._removeStyles = function(style) {
            var cleanStyle = {};
            for (var prop in style) {
                cleanStyle[prop] = "";
            }
            this.css(cleanStyle);
        };
        var cleanTransitionStyle = {
            transitionProperty: "",
            transitionDuration: ""
        };
        Item.prototype.removeTransitionStyles = function() {
            this.css(cleanTransitionStyle);
        };
        Item.prototype.removeElem = function() {
            this.element.parentNode.removeChild(this.element);
            this.emitEvent("remove", [ this ]);
        };
        Item.prototype.remove = function() {
            if (!transitionProperty || !parseFloat(this.layout.options.transitionDuration)) {
                this.removeElem();
                return;
            }
            var _this = this;
            this.on("transitionEnd", function() {
                _this.removeElem();
                return true;
            });
            this.hide();
        };
        Item.prototype.reveal = function() {
            delete this.isHidden;
            this.css({
                display: ""
            });
            var options = this.layout.options;
            this.transition({
                from: options.hiddenStyle,
                to: options.visibleStyle,
                isCleaning: true
            });
        };
        Item.prototype.hide = function() {
            this.isHidden = true;
            this.css({
                display: ""
            });
            var options = this.layout.options;
            this.transition({
                from: options.visibleStyle,
                to: options.hiddenStyle,
                isCleaning: true,
                onTransitionEnd: {
                    opacity: function() {
                        if (this.isHidden) {
                            this.css({
                                display: "none"
                            });
                        }
                    }
                }
            });
        };
        Item.prototype.destroy = function() {
            this.css({
                position: "",
                left: "",
                right: "",
                top: "",
                bottom: "",
                transition: "",
                transform: ""
            });
        };
        return Item;
    }
    if (typeof define === "function" && define.amd) {
        define("outlayer/item", [ "eventEmitter/EventEmitter", "get-size/get-size", "get-style-property/get-style-property" ], outlayerItemDefinition);
    } else {
        window.Outlayer = {};
        window.Outlayer.Item = outlayerItemDefinition(window.EventEmitter, window.getSize, window.getStyleProperty);
    }
})(window);

(function(window) {
    var document = window.document;
    var console = window.console;
    var jQuery = window.jQuery;
    var noop = function() {};
    function extend(a, b) {
        for (var prop in b) {
            a[prop] = b[prop];
        }
        return a;
    }
    var objToString = Object.prototype.toString;
    function isArray(obj) {
        return objToString.call(obj) === "[object Array]";
    }
    function makeArray(obj) {
        var ary = [];
        if (isArray(obj)) {
            ary = obj;
        } else if (obj && typeof obj.length === "number") {
            for (var i = 0, len = obj.length; i < len; i++) {
                ary.push(obj[i]);
            }
        } else {
            ary.push(obj);
        }
        return ary;
    }
    var isElement = typeof HTMLElement === "object" ? function isElementDOM2(obj) {
        return obj instanceof HTMLElement;
    } : function isElementQuirky(obj) {
        return obj && typeof obj === "object" && obj.nodeType === 1 && typeof obj.nodeName === "string";
    };
    var indexOf = Array.prototype.indexOf ? function(ary, obj) {
        return ary.indexOf(obj);
    } : function(ary, obj) {
        for (var i = 0, len = ary.length; i < len; i++) {
            if (ary[i] === obj) {
                return i;
            }
        }
        return -1;
    };
    function removeFrom(obj, ary) {
        var index = indexOf(ary, obj);
        if (index !== -1) {
            ary.splice(index, 1);
        }
    }
    function toDashed(str) {
        return str.replace(/(.)([A-Z])/g, function(match, $1, $2) {
            return $1 + "-" + $2;
        }).toLowerCase();
    }
    function outlayerDefinition(eventie, docReady, EventEmitter, getSize, matchesSelector, Item) {
        var GUID = 0;
        var instances = {};
        function Outlayer(element, options) {
            if (typeof element === "string") {
                element = document.querySelector(element);
            }
            if (!element || !isElement(element)) {
                if (console) {
                    console.error("Bad " + this.constructor.namespace + " element: " + element);
                }
                return;
            }
            this.element = element;
            this.options = extend({}, this.constructor.defaults);
            this.option(options);
            var id = ++GUID;
            this.element.outlayerGUID = id;
            instances[id] = this;
            this._create();
            if (this.options.isInitLayout) {
                this.layout();
            }
        }
        Outlayer.namespace = "outlayer";
        Outlayer.Item = Item;
        Outlayer.defaults = {
            containerStyle: {
                position: "relative"
            },
            isInitLayout: true,
            isOriginLeft: true,
            isOriginTop: true,
            isResizeBound: true,
            isResizingContainer: true,
            transitionDuration: "0.4s",
            hiddenStyle: {
                opacity: 0,
                transform: "scale(0.001)"
            },
            visibleStyle: {
                opacity: 1,
                transform: "scale(1)"
            }
        };
        extend(Outlayer.prototype, EventEmitter.prototype);
        Outlayer.prototype.option = function(opts) {
            extend(this.options, opts);
        };
        Outlayer.prototype._create = function() {
            this.reloadItems();
            this.stamps = [];
            this.stamp(this.options.stamp);
            extend(this.element.style, this.options.containerStyle);
            if (this.options.isResizeBound) {
                this.bindResize();
            }
        };
        Outlayer.prototype.reloadItems = function() {
            this.items = this._itemize(this.element.children);
        };
        Outlayer.prototype._itemize = function(elems) {
            var itemElems = this._filterFindItemElements(elems);
            var Item = this.constructor.Item;
            var items = [];
            for (var i = 0, len = itemElems.length; i < len; i++) {
                var elem = itemElems[i];
                var item = new Item(elem, this);
                items.push(item);
            }
            return items;
        };
        Outlayer.prototype._filterFindItemElements = function(elems) {
            elems = makeArray(elems);
            var itemSelector = this.options.itemSelector;
            var itemElems = [];
            for (var i = 0, len = elems.length; i < len; i++) {
                var elem = elems[i];
                if (!isElement(elem)) {
                    continue;
                }
                if (itemSelector) {
                    if (matchesSelector(elem, itemSelector)) {
                        itemElems.push(elem);
                    }
                    var childElems = elem.querySelectorAll(itemSelector);
                    for (var j = 0, jLen = childElems.length; j < jLen; j++) {
                        itemElems.push(childElems[j]);
                    }
                } else {
                    itemElems.push(elem);
                }
            }
            return itemElems;
        };
        Outlayer.prototype.getItemElements = function() {
            var elems = [];
            for (var i = 0, len = this.items.length; i < len; i++) {
                elems.push(this.items[i].element);
            }
            return elems;
        };
        Outlayer.prototype.layout = function() {
            this._resetLayout();
            this._manageStamps();
            var isInstant = this.options.isLayoutInstant !== undefined ? this.options.isLayoutInstant : !this._isLayoutInited;
            this.layoutItems(this.items, isInstant);
            this._isLayoutInited = true;
        };
        Outlayer.prototype._init = Outlayer.prototype.layout;
        Outlayer.prototype._resetLayout = function() {
            this.getSize();
        };
        Outlayer.prototype.getSize = function() {
            this.size = getSize(this.element);
        };
        Outlayer.prototype._getMeasurement = function(measurement, size) {
            var option = this.options[measurement];
            var elem;
            if (!option) {
                this[measurement] = 0;
            } else {
                if (typeof option === "string") {
                    elem = this.element.querySelector(option);
                } else if (isElement(option)) {
                    elem = option;
                }
                this[measurement] = elem ? getSize(elem)[size] : option;
            }
        };
        Outlayer.prototype.layoutItems = function(items, isInstant) {
            items = this._getItemsForLayout(items);
            this._layoutItems(items, isInstant);
            this._postLayout();
        };
        Outlayer.prototype._getItemsForLayout = function(items) {
            var layoutItems = [];
            for (var i = 0, len = items.length; i < len; i++) {
                var item = items[i];
                if (!item.isIgnored) {
                    layoutItems.push(item);
                }
            }
            return layoutItems;
        };
        Outlayer.prototype._layoutItems = function(items, isInstant) {
            var _this = this;
            function onItemsLayout() {
                _this.emitEvent("layoutComplete", [ _this, items ]);
            }
            if (!items || !items.length) {
                onItemsLayout();
                return;
            }
            this._itemsOn(items, "layout", onItemsLayout);
            var queue = [];
            for (var i = 0, len = items.length; i < len; i++) {
                var item = items[i];
                var position = this._getItemLayoutPosition(item);
                position.item = item;
                position.isInstant = isInstant || item.isLayoutInstant;
                queue.push(position);
            }
            this._processLayoutQueue(queue);
        };
        Outlayer.prototype._getItemLayoutPosition = function() {
            return {
                x: 0,
                y: 0
            };
        };
        Outlayer.prototype._processLayoutQueue = function(queue) {
            for (var i = 0, len = queue.length; i < len; i++) {
                var obj = queue[i];
                this._positionItem(obj.item, obj.x, obj.y, obj.isInstant);
            }
        };
        Outlayer.prototype._positionItem = function(item, x, y, isInstant) {
            if (isInstant) {
                item.goTo(x, y);
            } else {
                item.moveTo(x, y);
            }
        };
        Outlayer.prototype._postLayout = function() {
            this.resizeContainer();
        };
        Outlayer.prototype.resizeContainer = function() {
            if (!this.options.isResizingContainer) {
                return;
            }
            var size = this._getContainerSize();
            if (size) {
                this._setContainerMeasure(size.width, true);
                this._setContainerMeasure(size.height, false);
            }
        };
        Outlayer.prototype._getContainerSize = noop;
        Outlayer.prototype._setContainerMeasure = function(measure, isWidth) {
            if (measure === undefined) {
                return;
            }
            var elemSize = this.size;
            if (elemSize.isBorderBox) {
                measure += isWidth ? elemSize.paddingLeft + elemSize.paddingRight + elemSize.borderLeftWidth + elemSize.borderRightWidth : elemSize.paddingBottom + elemSize.paddingTop + elemSize.borderTopWidth + elemSize.borderBottomWidth;
            }
            measure = Math.max(measure, 0);
            this.element.style[isWidth ? "width" : "height"] = measure + "px";
        };
        Outlayer.prototype._itemsOn = function(items, eventName, callback) {
            var doneCount = 0;
            var count = items.length;
            var _this = this;
            function tick() {
                doneCount++;
                if (doneCount === count) {
                    callback.call(_this);
                }
                return true;
            }
            for (var i = 0, len = items.length; i < len; i++) {
                var item = items[i];
                item.on(eventName, tick);
            }
        };
        Outlayer.prototype.ignore = function(elem) {
            var item = this.getItem(elem);
            if (item) {
                item.isIgnored = true;
            }
        };
        Outlayer.prototype.unignore = function(elem) {
            var item = this.getItem(elem);
            if (item) {
                delete item.isIgnored;
            }
        };
        Outlayer.prototype.stamp = function(elems) {
            elems = this._find(elems);
            if (!elems) {
                return;
            }
            this.stamps = this.stamps.concat(elems);
            for (var i = 0, len = elems.length; i < len; i++) {
                var elem = elems[i];
                this.ignore(elem);
            }
        };
        Outlayer.prototype.unstamp = function(elems) {
            elems = this._find(elems);
            if (!elems) {
                return;
            }
            for (var i = 0, len = elems.length; i < len; i++) {
                var elem = elems[i];
                removeFrom(elem, this.stamps);
                this.unignore(elem);
            }
        };
        Outlayer.prototype._find = function(elems) {
            if (!elems) {
                return;
            }
            if (typeof elems === "string") {
                elems = this.element.querySelectorAll(elems);
            }
            elems = makeArray(elems);
            return elems;
        };
        Outlayer.prototype._manageStamps = function() {
            if (!this.stamps || !this.stamps.length) {
                return;
            }
            this._getBoundingRect();
            for (var i = 0, len = this.stamps.length; i < len; i++) {
                var stamp = this.stamps[i];
                this._manageStamp(stamp);
            }
        };
        Outlayer.prototype._getBoundingRect = function() {
            var boundingRect = this.element.getBoundingClientRect();
            var size = this.size;
            this._boundingRect = {
                left: boundingRect.left + size.paddingLeft + size.borderLeftWidth,
                top: boundingRect.top + size.paddingTop + size.borderTopWidth,
                right: boundingRect.right - (size.paddingRight + size.borderRightWidth),
                bottom: boundingRect.bottom - (size.paddingBottom + size.borderBottomWidth)
            };
        };
        Outlayer.prototype._manageStamp = noop;
        Outlayer.prototype._getElementOffset = function(elem) {
            var boundingRect = elem.getBoundingClientRect();
            var thisRect = this._boundingRect;
            var size = getSize(elem);
            var offset = {
                left: boundingRect.left - thisRect.left - size.marginLeft,
                top: boundingRect.top - thisRect.top - size.marginTop,
                right: thisRect.right - boundingRect.right - size.marginRight,
                bottom: thisRect.bottom - boundingRect.bottom - size.marginBottom
            };
            return offset;
        };
        Outlayer.prototype.handleEvent = function(event) {
            var method = "on" + event.type;
            if (this[method]) {
                this[method](event);
            }
        };
        Outlayer.prototype.bindResize = function() {
            if (this.isResizeBound) {
                return;
            }
            eventie.bind(window, "resize", this);
            this.isResizeBound = true;
        };
        Outlayer.prototype.unbindResize = function() {
            if (this.isResizeBound) {
                eventie.unbind(window, "resize", this);
            }
            this.isResizeBound = false;
        };
        Outlayer.prototype.onresize = function() {
            if (this.resizeTimeout) {
                clearTimeout(this.resizeTimeout);
            }
            var _this = this;
            function delayed() {
                _this.resize();
                delete _this.resizeTimeout;
            }
            this.resizeTimeout = setTimeout(delayed, 100);
        };
        Outlayer.prototype.resize = function() {
            if (!this.isResizeBound || !this.needsResizeLayout()) {
                return;
            }
            this.layout();
        };
        Outlayer.prototype.needsResizeLayout = function() {
            var size = getSize(this.element);
            var hasSizes = this.size && size;
            return hasSizes && size.innerWidth !== this.size.innerWidth;
        };
        Outlayer.prototype.addItems = function(elems) {
            var items = this._itemize(elems);
            if (items.length) {
                this.items = this.items.concat(items);
            }
            return items;
        };
        Outlayer.prototype.appended = function(elems) {
            var items = this.addItems(elems);
            if (!items.length) {
                return;
            }
            this.layoutItems(items, true);
            this.reveal(items);
        };
        Outlayer.prototype.prepended = function(elems) {
            var items = this._itemize(elems);
            if (!items.length) {
                return;
            }
            var previousItems = this.items.slice(0);
            this.items = items.concat(previousItems);
            this._resetLayout();
            this._manageStamps();
            this.layoutItems(items, true);
            this.reveal(items);
            this.layoutItems(previousItems);
        };
        Outlayer.prototype.reveal = function(items) {
            var len = items && items.length;
            if (!len) {
                return;
            }
            for (var i = 0; i < len; i++) {
                var item = items[i];
                item.reveal();
            }
        };
        Outlayer.prototype.hide = function(items) {
            var len = items && items.length;
            if (!len) {
                return;
            }
            for (var i = 0; i < len; i++) {
                var item = items[i];
                item.hide();
            }
        };
        Outlayer.prototype.getItem = function(elem) {
            for (var i = 0, len = this.items.length; i < len; i++) {
                var item = this.items[i];
                if (item.element === elem) {
                    return item;
                }
            }
        };
        Outlayer.prototype.getItems = function(elems) {
            if (!elems || !elems.length) {
                return;
            }
            var items = [];
            for (var i = 0, len = elems.length; i < len; i++) {
                var elem = elems[i];
                var item = this.getItem(elem);
                if (item) {
                    items.push(item);
                }
            }
            return items;
        };
        Outlayer.prototype.remove = function(elems) {
            elems = makeArray(elems);
            var removeItems = this.getItems(elems);
            if (!removeItems || !removeItems.length) {
                return;
            }
            this._itemsOn(removeItems, "remove", function() {
                this.emitEvent("removeComplete", [ this, removeItems ]);
            });
            for (var i = 0, len = removeItems.length; i < len; i++) {
                var item = removeItems[i];
                item.remove();
                removeFrom(item, this.items);
            }
        };
        Outlayer.prototype.destroy = function() {
            var style = this.element.style;
            style.height = "";
            style.position = "";
            style.width = "";
            for (var i = 0, len = this.items.length; i < len; i++) {
                var item = this.items[i];
                item.destroy();
            }
            this.unbindResize();
            delete this.element.outlayerGUID;
            if (jQuery) {
                jQuery.removeData(this.element, this.constructor.namespace);
            }
        };
        Outlayer.data = function(elem) {
            var id = elem && elem.outlayerGUID;
            return id && instances[id];
        };
        Outlayer.create = function(namespace, options) {
            function Layout() {
                Outlayer.apply(this, arguments);
            }
            if (Object.create) {
                Layout.prototype = Object.create(Outlayer.prototype);
            } else {
                extend(Layout.prototype, Outlayer.prototype);
            }
            Layout.prototype.constructor = Layout;
            Layout.defaults = extend({}, Outlayer.defaults);
            extend(Layout.defaults, options);
            Layout.prototype.settings = {};
            Layout.namespace = namespace;
            Layout.data = Outlayer.data;
            Layout.Item = function LayoutItem() {
                Item.apply(this, arguments);
            };
            Layout.Item.prototype = new Item();
            docReady(function() {
                var dashedNamespace = toDashed(namespace);
                var elems = document.querySelectorAll(".js-" + dashedNamespace);
                var dataAttr = "data-" + dashedNamespace + "-options";
                for (var i = 0, len = elems.length; i < len; i++) {
                    var elem = elems[i];
                    var attr = elem.getAttribute(dataAttr);
                    var options;
                    try {
                        options = attr && JSON.parse(attr);
                    } catch (error) {
                        if (console) {
                            console.error("Error parsing " + dataAttr + " on " + elem.nodeName.toLowerCase() + (elem.id ? "#" + elem.id : "") + ": " + error);
                        }
                        continue;
                    }
                    var instance = new Layout(elem, options);
                    if (jQuery) {
                        jQuery.data(elem, namespace, instance);
                    }
                }
            });
            if (jQuery && jQuery.bridget) {
                jQuery.bridget(namespace, Layout);
            }
            return Layout;
        };
        Outlayer.Item = Item;
        return Outlayer;
    }
    if (typeof define === "function" && define.amd) {
        define("outlayer/outlayer", [ "eventie/eventie", "doc-ready/doc-ready", "eventEmitter/EventEmitter", "get-size/get-size", "matches-selector/matches-selector", "./item" ], outlayerDefinition);
    } else {
        window.Outlayer = outlayerDefinition(window.eventie, window.docReady, window.EventEmitter, window.getSize, window.matchesSelector, window.Outlayer.Item);
    }
})(window);

(function(window) {
    var indexOf = Array.prototype.indexOf ? function(items, value) {
        return items.indexOf(value);
    } : function(items, value) {
        for (var i = 0, len = items.length; i < len; i++) {
            var item = items[i];
            if (item === value) {
                return i;
            }
        }
        return -1;
    };
    function masonryDefinition(Outlayer, getSize) {
        var Masonry = Outlayer.create("masonry");
        Masonry.prototype._resetLayout = function() {
            this.getSize();
            this._getMeasurement("columnWidth", "outerWidth");
            this._getMeasurement("gutter", "outerWidth");
            this.measureColumns();
            var i = this.cols;
            this.colYs = [];
            while (i--) {
                this.colYs.push(0);
            }
            this.maxY = 0;
        };
        Masonry.prototype.measureColumns = function() {
            this.getContainerWidth();
            if (!this.columnWidth) {
                var firstItem = this.items[0];
                var firstItemElem = firstItem && firstItem.element;
                this.columnWidth = firstItemElem && getSize(firstItemElem).outerWidth || this.containerWidth;
            }
            this.columnWidth += this.gutter;
            this.cols = Math.floor((this.containerWidth + this.gutter) / this.columnWidth);
            this.cols = Math.max(this.cols, 1);
        };
        Masonry.prototype.getContainerWidth = function() {
            var container = this.options.isFitWidth ? this.element.parentNode : this.element;
            var size = getSize(container);
            this.containerWidth = size && size.innerWidth;
        };
        Masonry.prototype._getItemLayoutPosition = function(item) {
            item.getSize();
            var remainder = item.size.outerWidth % this.columnWidth;
            var mathMethod = remainder && remainder < 1 ? "round" : "ceil";
            var colSpan = Math[mathMethod](item.size.outerWidth / this.columnWidth);
            colSpan = Math.min(colSpan, this.cols);
            var colGroup = this._getColGroup(colSpan);
            var minimumY = Math.min.apply(Math, colGroup);
            var shortColIndex = indexOf(colGroup, minimumY);
            var position = {
                x: this.columnWidth * shortColIndex,
                y: minimumY
            };
            var setHeight = minimumY + item.size.outerHeight;
            var setSpan = this.cols + 1 - colGroup.length;
            for (var i = 0; i < setSpan; i++) {
                this.colYs[shortColIndex + i] = setHeight;
            }
            return position;
        };
        Masonry.prototype._getColGroup = function(colSpan) {
            if (colSpan < 2) {
                return this.colYs;
            }
            var colGroup = [];
            var groupCount = this.cols + 1 - colSpan;
            for (var i = 0; i < groupCount; i++) {
                var groupColYs = this.colYs.slice(i, i + colSpan);
                colGroup[i] = Math.max.apply(Math, groupColYs);
            }
            return colGroup;
        };
        Masonry.prototype._manageStamp = function(stamp) {
            var stampSize = getSize(stamp);
            var offset = this._getElementOffset(stamp);
            var firstX = this.options.isOriginLeft ? offset.left : offset.right;
            var lastX = firstX + stampSize.outerWidth;
            var firstCol = Math.floor(firstX / this.columnWidth);
            firstCol = Math.max(0, firstCol);
            var lastCol = Math.floor(lastX / this.columnWidth);
            lastCol -= lastX % this.columnWidth ? 0 : 1;
            lastCol = Math.min(this.cols - 1, lastCol);
            var stampMaxY = (this.options.isOriginTop ? offset.top : offset.bottom) + stampSize.outerHeight;
            for (var i = firstCol; i <= lastCol; i++) {
                this.colYs[i] = Math.max(stampMaxY, this.colYs[i]);
            }
        };
        Masonry.prototype._getContainerSize = function() {
            this.maxY = Math.max.apply(Math, this.colYs);
            var size = {
                height: this.maxY
            };
            if (this.options.isFitWidth) {
                size.width = this._getContainerFitWidth();
            }
            return size;
        };
        Masonry.prototype._getContainerFitWidth = function() {
            var unusedCols = 0;
            var i = this.cols;
            while (--i) {
                if (this.colYs[i] !== 0) {
                    break;
                }
                unusedCols++;
            }
            return (this.cols - unusedCols) * this.columnWidth - this.gutter;
        };
        Masonry.prototype.needsResizeLayout = function() {
            var previousWidth = this.containerWidth;
            this.getContainerWidth();
            return previousWidth !== this.containerWidth;
        };
        return Masonry;
    }
    if (typeof define === "function" && define.amd) {
        define([ "outlayer/outlayer", "get-size/get-size" ], masonryDefinition);
    } else {
        window.Masonry = masonryDefinition(window.Outlayer, window.getSize);
    }
})(window);

(function(factory) {
    "use strict";
    if (typeof define === "function" && define.amd) {
        define([ "jquery" ], factory);
    } else {
        factory(typeof jQuery != "undefined" ? jQuery : window.Zepto);
    }
})(function($) {
    "use strict";
    var feature = {};
    feature.fileapi = $("<input type='file'/>").get(0).files !== undefined;
    feature.formdata = window.FormData !== undefined;
    var hasProp = !!$.fn.prop;
    $.fn.attr2 = function() {
        if (!hasProp) {
            return this.attr.apply(this, arguments);
        }
        var val = this.prop.apply(this, arguments);
        if (val && val.jquery || typeof val === "string") {
            return val;
        }
        return this.attr.apply(this, arguments);
    };
    $.fn.ajaxSubmit = function(options) {
        if (!this.length) {
            log("ajaxSubmit: skipping submit process - no element selected");
            return this;
        }
        var method, action, url, $form = this;
        if (typeof options == "function") {
            options = {
                success: options
            };
        } else if (options === undefined) {
            options = {};
        }
        method = options.type || this.attr2("method");
        action = options.url || this.attr2("action");
        url = typeof action === "string" ? $.trim(action) : "";
        url = url || window.location.href || "";
        if (url) {
            url = (url.match(/^([^#]+)/) || [])[1];
        }
        options = $.extend(true, {
            url: url,
            success: $.ajaxSettings.success,
            type: method || $.ajaxSettings.type,
            iframeSrc: /^https/i.test(window.location.href || "") ? "javascript:false" : "about:blank"
        }, options);
        var veto = {};
        this.trigger("form-pre-serialize", [ this, options, veto ]);
        if (veto.veto) {
            log("ajaxSubmit: submit vetoed via form-pre-serialize trigger");
            return this;
        }
        if (options.beforeSerialize && options.beforeSerialize(this, options) === false) {
            log("ajaxSubmit: submit aborted via beforeSerialize callback");
            return this;
        }
        var traditional = options.traditional;
        if (traditional === undefined) {
            traditional = $.ajaxSettings.traditional;
        }
        var elements = [];
        var qx, a = this.formToArray(options.semantic, elements);
        if (options.data) {
            options.extraData = options.data;
            qx = $.param(options.data, traditional);
        }
        if (options.beforeSubmit && options.beforeSubmit(a, this, options) === false) {
            log("ajaxSubmit: submit aborted via beforeSubmit callback");
            return this;
        }
        this.trigger("form-submit-validate", [ a, this, options, veto ]);
        if (veto.veto) {
            log("ajaxSubmit: submit vetoed via form-submit-validate trigger");
            return this;
        }
        var q = $.param(a, traditional);
        if (qx) {
            q = q ? q + "&" + qx : qx;
        }
        if (options.type.toUpperCase() == "GET") {
            options.url += (options.url.indexOf("?") >= 0 ? "&" : "?") + q;
            options.data = null;
        } else {
            options.data = q;
        }
        var callbacks = [];
        if (options.resetForm) {
            callbacks.push(function() {
                $form.resetForm();
            });
        }
        if (options.clearForm) {
            callbacks.push(function() {
                $form.clearForm(options.includeHidden);
            });
        }
        if (!options.dataType && options.target) {
            var oldSuccess = options.success || function() {};
            callbacks.push(function(data) {
                var fn = options.replaceTarget ? "replaceWith" : "html";
                $(options.target)[fn](data).each(oldSuccess, arguments);
            });
        } else if (options.success) {
            callbacks.push(options.success);
        }
        options.success = function(data, status, xhr) {
            var context = options.context || this;
            for (var i = 0, max = callbacks.length; i < max; i++) {
                callbacks[i].apply(context, [ data, status, xhr || $form, $form ]);
            }
        };
        if (options.error) {
            var oldError = options.error;
            options.error = function(xhr, status, error) {
                var context = options.context || this;
                oldError.apply(context, [ xhr, status, error, $form ]);
            };
        }
        if (options.complete) {
            var oldComplete = options.complete;
            options.complete = function(xhr, status) {
                var context = options.context || this;
                oldComplete.apply(context, [ xhr, status, $form ]);
            };
        }
        var fileInputs = $("input[type=file]:enabled", this).filter(function() {
            return $(this).val() !== "";
        });
        var hasFileInputs = fileInputs.length > 0;
        var mp = "multipart/form-data";
        var multipart = $form.attr("enctype") == mp || $form.attr("encoding") == mp;
        var fileAPI = feature.fileapi && feature.formdata;
        log("fileAPI :" + fileAPI);
        var shouldUseFrame = (hasFileInputs || multipart) && !fileAPI;
        var jqxhr;
        if (options.iframe !== false && (options.iframe || shouldUseFrame)) {
            if (options.closeKeepAlive) {
                $.get(options.closeKeepAlive, function() {
                    jqxhr = fileUploadIframe(a);
                });
            } else {
                jqxhr = fileUploadIframe(a);
            }
        } else if ((hasFileInputs || multipart) && fileAPI) {
            jqxhr = fileUploadXhr(a);
        } else {
            jqxhr = $.ajax(options);
        }
        $form.removeData("jqxhr").data("jqxhr", jqxhr);
        for (var k = 0; k < elements.length; k++) {
            elements[k] = null;
        }
        this.trigger("form-submit-notify", [ this, options ]);
        return this;
        function deepSerialize(extraData) {
            var serialized = $.param(extraData, options.traditional).split("&");
            var len = serialized.length;
            var result = [];
            var i, part;
            for (i = 0; i < len; i++) {
                serialized[i] = serialized[i].replace(/\+/g, " ");
                part = serialized[i].split("=");
                result.push([ decodeURIComponent(part[0]), decodeURIComponent(part[1]) ]);
            }
            return result;
        }
        function fileUploadXhr(a) {
            var formdata = new FormData();
            for (var i = 0; i < a.length; i++) {
                formdata.append(a[i].name, a[i].value);
            }
            if (options.extraData) {
                var serializedData = deepSerialize(options.extraData);
                for (i = 0; i < serializedData.length; i++) {
                    if (serializedData[i]) {
                        formdata.append(serializedData[i][0], serializedData[i][1]);
                    }
                }
            }
            options.data = null;
            var s = $.extend(true, {}, $.ajaxSettings, options, {
                contentType: false,
                processData: false,
                cache: false,
                type: method || "POST"
            });
            if (options.uploadProgress) {
                s.xhr = function() {
                    var xhr = $.ajaxSettings.xhr();
                    if (xhr.upload) {
                        xhr.upload.addEventListener("progress", function(event) {
                            var percent = 0;
                            var position = event.loaded || event.position;
                            var total = event.total;
                            if (event.lengthComputable) {
                                percent = Math.ceil(position / total * 100);
                            }
                            options.uploadProgress(event, position, total, percent);
                        }, false);
                    }
                    return xhr;
                };
            }
            s.data = null;
            var beforeSend = s.beforeSend;
            s.beforeSend = function(xhr, o) {
                if (options.formData) {
                    o.data = options.formData;
                } else {
                    o.data = formdata;
                }
                if (beforeSend) {
                    beforeSend.call(this, xhr, o);
                }
            };
            return $.ajax(s);
        }
        function fileUploadIframe(a) {
            var form = $form[0], el, i, s, g, id, $io, io, xhr, sub, n, timedOut, timeoutHandle;
            var deferred = $.Deferred();
            deferred.abort = function(status) {
                xhr.abort(status);
            };
            if (a) {
                for (i = 0; i < elements.length; i++) {
                    el = $(elements[i]);
                    if (hasProp) {
                        el.prop("disabled", false);
                    } else {
                        el.removeAttr("disabled");
                    }
                }
            }
            s = $.extend(true, {}, $.ajaxSettings, options);
            s.context = s.context || s;
            id = "jqFormIO" + new Date().getTime();
            if (s.iframeTarget) {
                $io = $(s.iframeTarget);
                n = $io.attr2("name");
                if (!n) {
                    $io.attr2("name", id);
                } else {
                    id = n;
                }
            } else {
                $io = $('<iframe name="' + id + '" src="' + s.iframeSrc + '" />');
                $io.css({
                    position: "absolute",
                    top: "-1000px",
                    left: "-1000px"
                });
            }
            io = $io[0];
            xhr = {
                aborted: 0,
                responseText: null,
                responseXML: null,
                status: 0,
                statusText: "n/a",
                getAllResponseHeaders: function() {},
                getResponseHeader: function() {},
                setRequestHeader: function() {},
                abort: function(status) {
                    var e = status === "timeout" ? "timeout" : "aborted";
                    log("aborting upload... " + e);
                    this.aborted = 1;
                    try {
                        if (io.contentWindow.document.execCommand) {
                            io.contentWindow.document.execCommand("Stop");
                        }
                    } catch (ignore) {}
                    $io.attr("src", s.iframeSrc);
                    xhr.error = e;
                    if (s.error) {
                        s.error.call(s.context, xhr, e, status);
                    }
                    if (g) {
                        $.event.trigger("ajaxError", [ xhr, s, e ]);
                    }
                    if (s.complete) {
                        s.complete.call(s.context, xhr, e);
                    }
                }
            };
            g = s.global;
            if (g && 0 === $.active++) {
                $.event.trigger("ajaxStart");
            }
            if (g) {
                $.event.trigger("ajaxSend", [ xhr, s ]);
            }
            if (s.beforeSend && s.beforeSend.call(s.context, xhr, s) === false) {
                if (s.global) {
                    $.active--;
                }
                deferred.reject();
                return deferred;
            }
            if (xhr.aborted) {
                deferred.reject();
                return deferred;
            }
            sub = form.clk;
            if (sub) {
                n = sub.name;
                if (n && !sub.disabled) {
                    s.extraData = s.extraData || {};
                    s.extraData[n] = sub.value;
                    if (sub.type == "image") {
                        s.extraData[n + ".x"] = form.clk_x;
                        s.extraData[n + ".y"] = form.clk_y;
                    }
                }
            }
            var CLIENT_TIMEOUT_ABORT = 1;
            var SERVER_ABORT = 2;
            function getDoc(frame) {
                var doc = null;
                try {
                    if (frame.contentWindow) {
                        doc = frame.contentWindow.document;
                    }
                } catch (err) {
                    log("cannot get iframe.contentWindow document: " + err);
                }
                if (doc) {
                    return doc;
                }
                try {
                    doc = frame.contentDocument ? frame.contentDocument : frame.document;
                } catch (err) {
                    log("cannot get iframe.contentDocument: " + err);
                    doc = frame.document;
                }
                return doc;
            }
            var csrf_token = $("meta[name=csrf-token]").attr("content");
            var csrf_param = $("meta[name=csrf-param]").attr("content");
            if (csrf_param && csrf_token) {
                s.extraData = s.extraData || {};
                s.extraData[csrf_param] = csrf_token;
            }
            function doSubmit() {
                var t = $form.attr2("target"), a = $form.attr2("action"), mp = "multipart/form-data", et = $form.attr("enctype") || $form.attr("encoding") || mp;
                form.setAttribute("target", id);
                if (!method || /post/i.test(method)) {
                    form.setAttribute("method", "POST");
                }
                if (a != s.url) {
                    form.setAttribute("action", s.url);
                }
                if (!s.skipEncodingOverride && (!method || /post/i.test(method))) {
                    $form.attr({
                        encoding: "multipart/form-data",
                        enctype: "multipart/form-data"
                    });
                }
                if (s.timeout) {
                    timeoutHandle = setTimeout(function() {
                        timedOut = true;
                        cb(CLIENT_TIMEOUT_ABORT);
                    }, s.timeout);
                }
                function checkState() {
                    try {
                        var state = getDoc(io).readyState;
                        log("state = " + state);
                        if (state && state.toLowerCase() == "uninitialized") {
                            setTimeout(checkState, 50);
                        }
                    } catch (e) {
                        log("Server abort: ", e, " (", e.name, ")");
                        cb(SERVER_ABORT);
                        if (timeoutHandle) {
                            clearTimeout(timeoutHandle);
                        }
                        timeoutHandle = undefined;
                    }
                }
                var extraInputs = [];
                try {
                    if (s.extraData) {
                        for (var n in s.extraData) {
                            if (s.extraData.hasOwnProperty(n)) {
                                if ($.isPlainObject(s.extraData[n]) && s.extraData[n].hasOwnProperty("name") && s.extraData[n].hasOwnProperty("value")) {
                                    extraInputs.push($('<input type="hidden" name="' + s.extraData[n].name + '">').val(s.extraData[n].value).appendTo(form)[0]);
                                } else {
                                    extraInputs.push($('<input type="hidden" name="' + n + '">').val(s.extraData[n]).appendTo(form)[0]);
                                }
                            }
                        }
                    }
                    if (!s.iframeTarget) {
                        $io.appendTo("body");
                    }
                    if (io.attachEvent) {
                        io.attachEvent("onload", cb);
                    } else {
                        io.addEventListener("load", cb, false);
                    }
                    setTimeout(checkState, 15);
                    try {
                        form.submit();
                    } catch (err) {
                        var submitFn = document.createElement("form").submit;
                        submitFn.apply(form);
                    }
                } finally {
                    form.setAttribute("action", a);
                    form.setAttribute("enctype", et);
                    if (t) {
                        form.setAttribute("target", t);
                    } else {
                        $form.removeAttr("target");
                    }
                    $(extraInputs).remove();
                }
            }
            if (s.forceSync) {
                doSubmit();
            } else {
                setTimeout(doSubmit, 10);
            }
            var data, doc, domCheckCount = 50, callbackProcessed;
            function cb(e) {
                if (xhr.aborted || callbackProcessed) {
                    return;
                }
                doc = getDoc(io);
                if (!doc) {
                    log("cannot access response document");
                    e = SERVER_ABORT;
                }
                if (e === CLIENT_TIMEOUT_ABORT && xhr) {
                    xhr.abort("timeout");
                    deferred.reject(xhr, "timeout");
                    return;
                } else if (e == SERVER_ABORT && xhr) {
                    xhr.abort("server abort");
                    deferred.reject(xhr, "error", "server abort");
                    return;
                }
                if (!doc || doc.location.href == s.iframeSrc) {
                    if (!timedOut) {
                        return;
                    }
                }
                if (io.detachEvent) {
                    io.detachEvent("onload", cb);
                } else {
                    io.removeEventListener("load", cb, false);
                }
                var status = "success", errMsg;
                try {
                    if (timedOut) {
                        throw "timeout";
                    }
                    var isXml = s.dataType == "xml" || doc.XMLDocument || $.isXMLDoc(doc);
                    log("isXml=" + isXml);
                    if (!isXml && window.opera && (doc.body === null || !doc.body.innerHTML)) {
                        if (--domCheckCount) {
                            log("requeing onLoad callback, DOM not available");
                            setTimeout(cb, 250);
                            return;
                        }
                    }
                    var docRoot = doc.body ? doc.body : doc.documentElement;
                    xhr.responseText = docRoot ? docRoot.innerHTML : null;
                    xhr.responseXML = doc.XMLDocument ? doc.XMLDocument : doc;
                    if (isXml) {
                        s.dataType = "xml";
                    }
                    xhr.getResponseHeader = function(header) {
                        var headers = {
                            "content-type": s.dataType
                        };
                        return headers[header.toLowerCase()];
                    };
                    if (docRoot) {
                        xhr.status = Number(docRoot.getAttribute("status")) || xhr.status;
                        xhr.statusText = docRoot.getAttribute("statusText") || xhr.statusText;
                    }
                    var dt = (s.dataType || "").toLowerCase();
                    var scr = /(json|script|text)/.test(dt);
                    if (scr || s.textarea) {
                        var ta = doc.getElementsByTagName("textarea")[0];
                        if (ta) {
                            xhr.responseText = ta.value;
                            xhr.status = Number(ta.getAttribute("status")) || xhr.status;
                            xhr.statusText = ta.getAttribute("statusText") || xhr.statusText;
                        } else if (scr) {
                            var pre = doc.getElementsByTagName("pre")[0];
                            var b = doc.getElementsByTagName("body")[0];
                            if (pre) {
                                xhr.responseText = pre.textContent ? pre.textContent : pre.innerText;
                            } else if (b) {
                                xhr.responseText = b.textContent ? b.textContent : b.innerText;
                            }
                        }
                    } else if (dt == "xml" && !xhr.responseXML && xhr.responseText) {
                        xhr.responseXML = toXml(xhr.responseText);
                    }
                    try {
                        data = httpData(xhr, dt, s);
                    } catch (err) {
                        status = "parsererror";
                        xhr.error = errMsg = err || status;
                    }
                } catch (err) {
                    log("error caught: ", err);
                    status = "error";
                    xhr.error = errMsg = err || status;
                }
                if (xhr.aborted) {
                    log("upload aborted");
                    status = null;
                }
                if (xhr.status) {
                    status = xhr.status >= 200 && xhr.status < 300 || xhr.status === 304 ? "success" : "error";
                }
                if (status === "success") {
                    if (s.success) {
                        s.success.call(s.context, data, "success", xhr);
                    }
                    deferred.resolve(xhr.responseText, "success", xhr);
                    if (g) {
                        $.event.trigger("ajaxSuccess", [ xhr, s ]);
                    }
                } else if (status) {
                    if (errMsg === undefined) {
                        errMsg = xhr.statusText;
                    }
                    if (s.error) {
                        s.error.call(s.context, xhr, status, errMsg);
                    }
                    deferred.reject(xhr, "error", errMsg);
                    if (g) {
                        $.event.trigger("ajaxError", [ xhr, s, errMsg ]);
                    }
                }
                if (g) {
                    $.event.trigger("ajaxComplete", [ xhr, s ]);
                }
                if (g && !--$.active) {
                    $.event.trigger("ajaxStop");
                }
                if (s.complete) {
                    s.complete.call(s.context, xhr, status);
                }
                callbackProcessed = true;
                if (s.timeout) {
                    clearTimeout(timeoutHandle);
                }
                setTimeout(function() {
                    if (!s.iframeTarget) {
                        $io.remove();
                    } else {
                        $io.attr("src", s.iframeSrc);
                    }
                    xhr.responseXML = null;
                }, 100);
            }
            var toXml = $.parseXML || function(s, doc) {
                if (window.ActiveXObject) {
                    doc = new ActiveXObject("Microsoft.XMLDOM");
                    doc.async = "false";
                    doc.loadXML(s);
                } else {
                    doc = new DOMParser().parseFromString(s, "text/xml");
                }
                return doc && doc.documentElement && doc.documentElement.nodeName != "parsererror" ? doc : null;
            };
            var parseJSON = $.parseJSON || function(s) {
                return window["eval"]("(" + s + ")");
            };
            var httpData = function(xhr, type, s) {
                var ct = xhr.getResponseHeader("content-type") || "", xml = type === "xml" || !type && ct.indexOf("xml") >= 0, data = xml ? xhr.responseXML : xhr.responseText;
                if (xml && data.documentElement.nodeName === "parsererror") {
                    if ($.error) {
                        $.error("parsererror");
                    }
                }
                if (s && s.dataFilter) {
                    data = s.dataFilter(data, type);
                }
                if (typeof data === "string") {
                    if (type === "json" || !type && ct.indexOf("json") >= 0) {
                        data = parseJSON(data);
                    } else if (type === "script" || !type && ct.indexOf("javascript") >= 0) {
                        $.globalEval(data);
                    }
                }
                return data;
            };
            return deferred;
        }
    };
    $.fn.ajaxForm = function(options) {
        options = options || {};
        options.delegation = options.delegation && $.isFunction($.fn.on);
        if (!options.delegation && this.length === 0) {
            var o = {
                s: this.selector,
                c: this.context
            };
            if (!$.isReady && o.s) {
                log("DOM not ready, queuing ajaxForm");
                $(function() {
                    $(o.s, o.c).ajaxForm(options);
                });
                return this;
            }
            log("terminating; zero elements found by selector" + ($.isReady ? "" : " (DOM not ready)"));
            return this;
        }
        if (options.delegation) {
            $(document).off("submit.form-plugin", this.selector, doAjaxSubmit).off("click.form-plugin", this.selector, captureSubmittingElement).on("submit.form-plugin", this.selector, options, doAjaxSubmit).on("click.form-plugin", this.selector, options, captureSubmittingElement);
            return this;
        }
        return this.ajaxFormUnbind().bind("submit.form-plugin", options, doAjaxSubmit).bind("click.form-plugin", options, captureSubmittingElement);
    };
    function doAjaxSubmit(e) {
        var options = e.data;
        if (!e.isDefaultPrevented()) {
            e.preventDefault();
            $(e.target).ajaxSubmit(options);
        }
    }
    function captureSubmittingElement(e) {
        var target = e.target;
        var $el = $(target);
        if (!$el.is("[type=submit],[type=image]")) {
            var t = $el.closest("[type=submit]");
            if (t.length === 0) {
                return;
            }
            target = t[0];
        }
        var form = this;
        form.clk = target;
        if (target.type == "image") {
            if (e.offsetX !== undefined) {
                form.clk_x = e.offsetX;
                form.clk_y = e.offsetY;
            } else if (typeof $.fn.offset == "function") {
                var offset = $el.offset();
                form.clk_x = e.pageX - offset.left;
                form.clk_y = e.pageY - offset.top;
            } else {
                form.clk_x = e.pageX - target.offsetLeft;
                form.clk_y = e.pageY - target.offsetTop;
            }
        }
        setTimeout(function() {
            form.clk = form.clk_x = form.clk_y = null;
        }, 100);
    }
    $.fn.ajaxFormUnbind = function() {
        return this.unbind("submit.form-plugin click.form-plugin");
    };
    $.fn.formToArray = function(semantic, elements) {
        var a = [];
        if (this.length === 0) {
            return a;
        }
        var form = this[0];
        var formId = this.attr("id");
        var els = semantic ? form.getElementsByTagName("*") : form.elements;
        var els2;
        if (els && !/MSIE [678]/.test(navigator.userAgent)) {
            els = $(els).get();
        }
        if (formId) {
            els2 = $(":input[form=" + formId + "]").get();
            if (els2.length) {
                els = (els || []).concat(els2);
            }
        }
        if (!els || !els.length) {
            return a;
        }
        var i, j, n, v, el, max, jmax;
        for (i = 0, max = els.length; i < max; i++) {
            el = els[i];
            n = el.name;
            if (!n || el.disabled) {
                continue;
            }
            if (semantic && form.clk && el.type == "image") {
                if (form.clk == el) {
                    a.push({
                        name: n,
                        value: $(el).val(),
                        type: el.type
                    });
                    a.push({
                        name: n + ".x",
                        value: form.clk_x
                    }, {
                        name: n + ".y",
                        value: form.clk_y
                    });
                }
                continue;
            }
            v = $.fieldValue(el, true);
            if (v && v.constructor == Array) {
                if (elements) {
                    elements.push(el);
                }
                for (j = 0, jmax = v.length; j < jmax; j++) {
                    a.push({
                        name: n,
                        value: v[j]
                    });
                }
            } else if (feature.fileapi && el.type == "file") {
                if (elements) {
                    elements.push(el);
                }
                var files = el.files;
                if (files.length) {
                    for (j = 0; j < files.length; j++) {
                        a.push({
                            name: n,
                            value: files[j],
                            type: el.type
                        });
                    }
                } else {
                    a.push({
                        name: n,
                        value: "",
                        type: el.type
                    });
                }
            } else if (v !== null && typeof v != "undefined") {
                if (elements) {
                    elements.push(el);
                }
                a.push({
                    name: n,
                    value: v,
                    type: el.type,
                    required: el.required
                });
            }
        }
        if (!semantic && form.clk) {
            var $input = $(form.clk), input = $input[0];
            n = input.name;
            if (n && !input.disabled && input.type == "image") {
                a.push({
                    name: n,
                    value: $input.val()
                });
                a.push({
                    name: n + ".x",
                    value: form.clk_x
                }, {
                    name: n + ".y",
                    value: form.clk_y
                });
            }
        }
        return a;
    };
    $.fn.formSerialize = function(semantic) {
        return $.param(this.formToArray(semantic));
    };
    $.fn.fieldSerialize = function(successful) {
        var a = [];
        this.each(function() {
            var n = this.name;
            if (!n) {
                return;
            }
            var v = $.fieldValue(this, successful);
            if (v && v.constructor == Array) {
                for (var i = 0, max = v.length; i < max; i++) {
                    a.push({
                        name: n,
                        value: v[i]
                    });
                }
            } else if (v !== null && typeof v != "undefined") {
                a.push({
                    name: this.name,
                    value: v
                });
            }
        });
        return $.param(a);
    };
    $.fn.fieldValue = function(successful) {
        for (var val = [], i = 0, max = this.length; i < max; i++) {
            var el = this[i];
            var v = $.fieldValue(el, successful);
            if (v === null || typeof v == "undefined" || v.constructor == Array && !v.length) {
                continue;
            }
            if (v.constructor == Array) {
                $.merge(val, v);
            } else {
                val.push(v);
            }
        }
        return val;
    };
    $.fieldValue = function(el, successful) {
        var n = el.name, t = el.type, tag = el.tagName.toLowerCase();
        if (successful === undefined) {
            successful = true;
        }
        if (successful && (!n || el.disabled || t == "reset" || t == "button" || (t == "checkbox" || t == "radio") && !el.checked || (t == "submit" || t == "image") && el.form && el.form.clk != el || tag == "select" && el.selectedIndex == -1)) {
            return null;
        }
        if (tag == "select") {
            var index = el.selectedIndex;
            if (index < 0) {
                return null;
            }
            var a = [], ops = el.options;
            var one = t == "select-one";
            var max = one ? index + 1 : ops.length;
            for (var i = one ? index : 0; i < max; i++) {
                var op = ops[i];
                if (op.selected) {
                    var v = op.value;
                    if (!v) {
                        v = op.attributes && op.attributes.value && !op.attributes.value.specified ? op.text : op.value;
                    }
                    if (one) {
                        return v;
                    }
                    a.push(v);
                }
            }
            return a;
        }
        return $(el).val();
    };
    $.fn.clearForm = function(includeHidden) {
        return this.each(function() {
            $("input,select,textarea", this).clearFields(includeHidden);
        });
    };
    $.fn.clearFields = $.fn.clearInputs = function(includeHidden) {
        var re = /^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i;
        return this.each(function() {
            var t = this.type, tag = this.tagName.toLowerCase();
            if (re.test(t) || tag == "textarea") {
                this.value = "";
            } else if (t == "checkbox" || t == "radio") {
                this.checked = false;
            } else if (tag == "select") {
                this.selectedIndex = -1;
            } else if (t == "file") {
                if (/MSIE/.test(navigator.userAgent)) {
                    $(this).replaceWith($(this).clone(true));
                } else {
                    $(this).val("");
                }
            } else if (includeHidden) {
                if (includeHidden === true && /hidden/.test(t) || typeof includeHidden == "string" && $(this).is(includeHidden)) {
                    this.value = "";
                }
            }
        });
    };
    $.fn.resetForm = function() {
        return this.each(function() {
            if (typeof this.reset == "function" || typeof this.reset == "object" && !this.reset.nodeType) {
                this.reset();
            }
        });
    };
    $.fn.enable = function(b) {
        if (b === undefined) {
            b = true;
        }
        return this.each(function() {
            this.disabled = !b;
        });
    };
    $.fn.selected = function(select) {
        if (select === undefined) {
            select = true;
        }
        return this.each(function() {
            var t = this.type;
            if (t == "checkbox" || t == "radio") {
                this.checked = select;
            } else if (this.tagName.toLowerCase() == "option") {
                var $sel = $(this).parent("select");
                if (select && $sel[0] && $sel[0].type == "select-one") {
                    $sel.find("option").selected(false);
                }
                this.selected = select;
            }
        });
    };
    $.fn.ajaxSubmit.debug = false;
    function log() {
        if (!$.fn.ajaxSubmit.debug) {
            return;
        }
        var msg = "[jquery.form] " + Array.prototype.join.call(arguments, "");
        if (window.console && window.console.log) {
            window.console.log(msg);
        } else if (window.opera && window.opera.postError) {
            window.opera.postError(msg);
        }
    }
});

(function(a) {
    function b() {
        return {
            empty: !1,
            unusedTokens: [],
            unusedInput: [],
            overflow: -2,
            charsLeftOver: 0,
            nullInput: !1,
            invalidMonth: null,
            invalidFormat: !1,
            userInvalidated: !1,
            iso: !1
        };
    }
    function c(a, b) {
        function c() {
            ib.suppressDeprecationWarnings === !1 && "undefined" != typeof console && console.warn && console.warn("Deprecation warning: " + a);
        }
        var d = !0;
        return i(function() {
            return d && (c(), d = !1), b.apply(this, arguments);
        }, b);
    }
    function d(a, b) {
        return function(c) {
            return l(a.call(this, c), b);
        };
    }
    function e(a, b) {
        return function(c) {
            return this.lang().ordinal(a.call(this, c), b);
        };
    }
    function f() {}
    function g(a) {
        y(a), i(this, a);
    }
    function h(a) {
        var b = r(a), c = b.year || 0, d = b.quarter || 0, e = b.month || 0, f = b.week || 0, g = b.day || 0, h = b.hour || 0, i = b.minute || 0, j = b.second || 0, k = b.millisecond || 0;
        this._milliseconds = +k + 1e3 * j + 6e4 * i + 36e5 * h, this._days = +g + 7 * f, 
        this._months = +e + 3 * d + 12 * c, this._data = {}, this._bubble();
    }
    function i(a, b) {
        for (var c in b) b.hasOwnProperty(c) && (a[c] = b[c]);
        return b.hasOwnProperty("toString") && (a.toString = b.toString), b.hasOwnProperty("valueOf") && (a.valueOf = b.valueOf), 
        a;
    }
    function j(a) {
        var b, c = {};
        for (b in a) a.hasOwnProperty(b) && wb.hasOwnProperty(b) && (c[b] = a[b]);
        return c;
    }
    function k(a) {
        return 0 > a ? Math.ceil(a) : Math.floor(a);
    }
    function l(a, b, c) {
        for (var d = "" + Math.abs(a), e = a >= 0; d.length < b; ) d = "0" + d;
        return (e ? c ? "+" : "" : "-") + d;
    }
    function m(a, b, c, d) {
        var e = b._milliseconds, f = b._days, g = b._months;
        d = null == d ? !0 : d, e && a._d.setTime(+a._d + e * c), f && db(a, "Date", cb(a, "Date") + f * c), 
        g && bb(a, cb(a, "Month") + g * c), d && ib.updateOffset(a, f || g);
    }
    function n(a) {
        return "[object Array]" === Object.prototype.toString.call(a);
    }
    function o(a) {
        return "[object Date]" === Object.prototype.toString.call(a) || a instanceof Date;
    }
    function p(a, b, c) {
        var d, e = Math.min(a.length, b.length), f = Math.abs(a.length - b.length), g = 0;
        for (d = 0; e > d; d++) (c && a[d] !== b[d] || !c && t(a[d]) !== t(b[d])) && g++;
        return g + f;
    }
    function q(a) {
        if (a) {
            var b = a.toLowerCase().replace(/(.)s$/, "$1");
            a = Zb[a] || $b[b] || b;
        }
        return a;
    }
    function r(a) {
        var b, c, d = {};
        for (c in a) a.hasOwnProperty(c) && (b = q(c), b && (d[b] = a[c]));
        return d;
    }
    function s(b) {
        var c, d;
        if (0 === b.indexOf("week")) c = 7, d = "day"; else {
            if (0 !== b.indexOf("month")) return;
            c = 12, d = "month";
        }
        ib[b] = function(e, f) {
            var g, h, i = ib.fn._lang[b], j = [];
            if ("number" == typeof e && (f = e, e = a), h = function(a) {
                var b = ib().utc().set(d, a);
                return i.call(ib.fn._lang, b, e || "");
            }, null != f) return h(f);
            for (g = 0; c > g; g++) j.push(h(g));
            return j;
        };
    }
    function t(a) {
        var b = +a, c = 0;
        return 0 !== b && isFinite(b) && (c = b >= 0 ? Math.floor(b) : Math.ceil(b)), c;
    }
    function u(a, b) {
        return new Date(Date.UTC(a, b + 1, 0)).getUTCDate();
    }
    function v(a, b, c) {
        return $(ib([ a, 11, 31 + b - c ]), b, c).week;
    }
    function w(a) {
        return x(a) ? 366 : 365;
    }
    function x(a) {
        return a % 4 === 0 && a % 100 !== 0 || a % 400 === 0;
    }
    function y(a) {
        var b;
        a._a && -2 === a._pf.overflow && (b = a._a[pb] < 0 || a._a[pb] > 11 ? pb : a._a[qb] < 1 || a._a[qb] > u(a._a[ob], a._a[pb]) ? qb : a._a[rb] < 0 || a._a[rb] > 23 ? rb : a._a[sb] < 0 || a._a[sb] > 59 ? sb : a._a[tb] < 0 || a._a[tb] > 59 ? tb : a._a[ub] < 0 || a._a[ub] > 999 ? ub : -1, 
        a._pf._overflowDayOfYear && (ob > b || b > qb) && (b = qb), a._pf.overflow = b);
    }
    function z(a) {
        return null == a._isValid && (a._isValid = !isNaN(a._d.getTime()) && a._pf.overflow < 0 && !a._pf.empty && !a._pf.invalidMonth && !a._pf.nullInput && !a._pf.invalidFormat && !a._pf.userInvalidated, 
        a._strict && (a._isValid = a._isValid && 0 === a._pf.charsLeftOver && 0 === a._pf.unusedTokens.length)), 
        a._isValid;
    }
    function A(a) {
        return a ? a.toLowerCase().replace("_", "-") : a;
    }
    function B(a, b) {
        return b._isUTC ? ib(a).zone(b._offset || 0) : ib(a).local();
    }
    function C(a, b) {
        return b.abbr = a, vb[a] || (vb[a] = new f()), vb[a].set(b), vb[a];
    }
    function D(a) {
        delete vb[a];
    }
    function E(a) {
        var b, c, d, e, f = 0, g = function(a) {
            if (!vb[a] && xb) try {
                require("./lang/" + a);
            } catch (b) {}
            return vb[a];
        };
        if (!a) return ib.fn._lang;
        if (!n(a)) {
            if (c = g(a)) return c;
            a = [ a ];
        }
        for (;f < a.length; ) {
            for (e = A(a[f]).split("-"), b = e.length, d = A(a[f + 1]), d = d ? d.split("-") : null; b > 0; ) {
                if (c = g(e.slice(0, b).join("-"))) return c;
                if (d && d.length >= b && p(e, d, !0) >= b - 1) break;
                b--;
            }
            f++;
        }
        return ib.fn._lang;
    }
    function F(a) {
        return a.match(/\[[\s\S]/) ? a.replace(/^\[|\]$/g, "") : a.replace(/\\/g, "");
    }
    function G(a) {
        var b, c, d = a.match(Bb);
        for (b = 0, c = d.length; c > b; b++) d[b] = cc[d[b]] ? cc[d[b]] : F(d[b]);
        return function(e) {
            var f = "";
            for (b = 0; c > b; b++) f += d[b] instanceof Function ? d[b].call(e, a) : d[b];
            return f;
        };
    }
    function H(a, b) {
        return a.isValid() ? (b = I(b, a.lang()), _b[b] || (_b[b] = G(b)), _b[b](a)) : a.lang().invalidDate();
    }
    function I(a, b) {
        function c(a) {
            return b.longDateFormat(a) || a;
        }
        var d = 5;
        for (Cb.lastIndex = 0; d >= 0 && Cb.test(a); ) a = a.replace(Cb, c), Cb.lastIndex = 0, 
        d -= 1;
        return a;
    }
    function J(a, b) {
        var c, d = b._strict;
        switch (a) {
          case "Q":
            return Nb;

          case "DDDD":
            return Pb;

          case "YYYY":
          case "GGGG":
          case "gggg":
            return d ? Qb : Fb;

          case "Y":
          case "G":
          case "g":
            return Sb;

          case "YYYYYY":
          case "YYYYY":
          case "GGGGG":
          case "ggggg":
            return d ? Rb : Gb;

          case "S":
            if (d) return Nb;

          case "SS":
            if (d) return Ob;

          case "SSS":
            if (d) return Pb;

          case "DDD":
            return Eb;

          case "MMM":
          case "MMMM":
          case "dd":
          case "ddd":
          case "dddd":
            return Ib;

          case "a":
          case "A":
            return E(b._l)._meridiemParse;

          case "X":
            return Lb;

          case "Z":
          case "ZZ":
            return Jb;

          case "T":
            return Kb;

          case "SSSS":
            return Hb;

          case "MM":
          case "DD":
          case "YY":
          case "GG":
          case "gg":
          case "HH":
          case "hh":
          case "mm":
          case "ss":
          case "ww":
          case "WW":
            return d ? Ob : Db;

          case "M":
          case "D":
          case "d":
          case "H":
          case "h":
          case "m":
          case "s":
          case "w":
          case "W":
          case "e":
          case "E":
            return Db;

          case "Do":
            return Mb;

          default:
            return c = new RegExp(R(Q(a.replace("\\", "")), "i"));
        }
    }
    function K(a) {
        a = a || "";
        var b = a.match(Jb) || [], c = b[b.length - 1] || [], d = (c + "").match(Xb) || [ "-", 0, 0 ], e = +(60 * d[1]) + t(d[2]);
        return "+" === d[0] ? -e : e;
    }
    function L(a, b, c) {
        var d, e = c._a;
        switch (a) {
          case "Q":
            null != b && (e[pb] = 3 * (t(b) - 1));
            break;

          case "M":
          case "MM":
            null != b && (e[pb] = t(b) - 1);
            break;

          case "MMM":
          case "MMMM":
            d = E(c._l).monthsParse(b), null != d ? e[pb] = d : c._pf.invalidMonth = b;
            break;

          case "D":
          case "DD":
            null != b && (e[qb] = t(b));
            break;

          case "Do":
            null != b && (e[qb] = t(parseInt(b, 10)));
            break;

          case "DDD":
          case "DDDD":
            null != b && (c._dayOfYear = t(b));
            break;

          case "YY":
            e[ob] = ib.parseTwoDigitYear(b);
            break;

          case "YYYY":
          case "YYYYY":
          case "YYYYYY":
            e[ob] = t(b);
            break;

          case "a":
          case "A":
            c._isPm = E(c._l).isPM(b);
            break;

          case "H":
          case "HH":
          case "h":
          case "hh":
            e[rb] = t(b);
            break;

          case "m":
          case "mm":
            e[sb] = t(b);
            break;

          case "s":
          case "ss":
            e[tb] = t(b);
            break;

          case "S":
          case "SS":
          case "SSS":
          case "SSSS":
            e[ub] = t(1e3 * ("0." + b));
            break;

          case "X":
            c._d = new Date(1e3 * parseFloat(b));
            break;

          case "Z":
          case "ZZ":
            c._useUTC = !0, c._tzm = K(b);
            break;

          case "w":
          case "ww":
          case "W":
          case "WW":
          case "d":
          case "dd":
          case "ddd":
          case "dddd":
          case "e":
          case "E":
            a = a.substr(0, 1);

          case "gg":
          case "gggg":
          case "GG":
          case "GGGG":
          case "GGGGG":
            a = a.substr(0, 2), b && (c._w = c._w || {}, c._w[a] = b);
        }
    }
    function M(a) {
        var b, c, d, e, f, g, h, i, j, k, l = [];
        if (!a._d) {
            for (d = O(a), a._w && null == a._a[qb] && null == a._a[pb] && (f = function(b) {
                var c = parseInt(b, 10);
                return b ? b.length < 3 ? c > 68 ? 1900 + c : 2e3 + c : c : null == a._a[ob] ? ib().weekYear() : a._a[ob];
            }, g = a._w, null != g.GG || null != g.W || null != g.E ? h = _(f(g.GG), g.W || 1, g.E, 4, 1) : (i = E(a._l), 
            j = null != g.d ? X(g.d, i) : null != g.e ? parseInt(g.e, 10) + i._week.dow : 0, 
            k = parseInt(g.w, 10) || 1, null != g.d && j < i._week.dow && k++, h = _(f(g.gg), k, j, i._week.doy, i._week.dow)), 
            a._a[ob] = h.year, a._dayOfYear = h.dayOfYear), a._dayOfYear && (e = null == a._a[ob] ? d[ob] : a._a[ob], 
            a._dayOfYear > w(e) && (a._pf._overflowDayOfYear = !0), c = W(e, 0, a._dayOfYear), 
            a._a[pb] = c.getUTCMonth(), a._a[qb] = c.getUTCDate()), b = 0; 3 > b && null == a._a[b]; ++b) a._a[b] = l[b] = d[b];
            for (;7 > b; b++) a._a[b] = l[b] = null == a._a[b] ? 2 === b ? 1 : 0 : a._a[b];
            l[rb] += t((a._tzm || 0) / 60), l[sb] += t((a._tzm || 0) % 60), a._d = (a._useUTC ? W : V).apply(null, l);
        }
    }
    function N(a) {
        var b;
        a._d || (b = r(a._i), a._a = [ b.year, b.month, b.day, b.hour, b.minute, b.second, b.millisecond ], 
        M(a));
    }
    function O(a) {
        var b = new Date();
        return a._useUTC ? [ b.getUTCFullYear(), b.getUTCMonth(), b.getUTCDate() ] : [ b.getFullYear(), b.getMonth(), b.getDate() ];
    }
    function P(a) {
        a._a = [], a._pf.empty = !0;
        var b, c, d, e, f, g = E(a._l), h = "" + a._i, i = h.length, j = 0;
        for (d = I(a._f, g).match(Bb) || [], b = 0; b < d.length; b++) e = d[b], c = (h.match(J(e, a)) || [])[0], 
        c && (f = h.substr(0, h.indexOf(c)), f.length > 0 && a._pf.unusedInput.push(f), 
        h = h.slice(h.indexOf(c) + c.length), j += c.length), cc[e] ? (c ? a._pf.empty = !1 : a._pf.unusedTokens.push(e), 
        L(e, c, a)) : a._strict && !c && a._pf.unusedTokens.push(e);
        a._pf.charsLeftOver = i - j, h.length > 0 && a._pf.unusedInput.push(h), a._isPm && a._a[rb] < 12 && (a._a[rb] += 12), 
        a._isPm === !1 && 12 === a._a[rb] && (a._a[rb] = 0), M(a), y(a);
    }
    function Q(a) {
        return a.replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function(a, b, c, d, e) {
            return b || c || d || e;
        });
    }
    function R(a) {
        return a.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&");
    }
    function S(a) {
        var c, d, e, f, g;
        if (0 === a._f.length) return a._pf.invalidFormat = !0, void (a._d = new Date(0 / 0));
        for (f = 0; f < a._f.length; f++) g = 0, c = i({}, a), c._pf = b(), c._f = a._f[f], 
        P(c), z(c) && (g += c._pf.charsLeftOver, g += 10 * c._pf.unusedTokens.length, c._pf.score = g, 
        (null == e || e > g) && (e = g, d = c));
        i(a, d || c);
    }
    function T(a) {
        var b, c, d = a._i, e = Tb.exec(d);
        if (e) {
            for (a._pf.iso = !0, b = 0, c = Vb.length; c > b; b++) if (Vb[b][1].exec(d)) {
                a._f = Vb[b][0] + (e[6] || " ");
                break;
            }
            for (b = 0, c = Wb.length; c > b; b++) if (Wb[b][1].exec(d)) {
                a._f += Wb[b][0];
                break;
            }
            d.match(Jb) && (a._f += "Z"), P(a);
        } else ib.createFromInputFallback(a);
    }
    function U(b) {
        var c = b._i, d = yb.exec(c);
        c === a ? b._d = new Date() : d ? b._d = new Date(+d[1]) : "string" == typeof c ? T(b) : n(c) ? (b._a = c.slice(0), 
        M(b)) : o(c) ? b._d = new Date(+c) : "object" == typeof c ? N(b) : "number" == typeof c ? b._d = new Date(c) : ib.createFromInputFallback(b);
    }
    function V(a, b, c, d, e, f, g) {
        var h = new Date(a, b, c, d, e, f, g);
        return 1970 > a && h.setFullYear(a), h;
    }
    function W(a) {
        var b = new Date(Date.UTC.apply(null, arguments));
        return 1970 > a && b.setUTCFullYear(a), b;
    }
    function X(a, b) {
        if ("string" == typeof a) if (isNaN(a)) {
            if (a = b.weekdaysParse(a), "number" != typeof a) return null;
        } else a = parseInt(a, 10);
        return a;
    }
    function Y(a, b, c, d, e) {
        return e.relativeTime(b || 1, !!c, a, d);
    }
    function Z(a, b, c) {
        var d = nb(Math.abs(a) / 1e3), e = nb(d / 60), f = nb(e / 60), g = nb(f / 24), h = nb(g / 365), i = 45 > d && [ "s", d ] || 1 === e && [ "m" ] || 45 > e && [ "mm", e ] || 1 === f && [ "h" ] || 22 > f && [ "hh", f ] || 1 === g && [ "d" ] || 25 >= g && [ "dd", g ] || 45 >= g && [ "M" ] || 345 > g && [ "MM", nb(g / 30) ] || 1 === h && [ "y" ] || [ "yy", h ];
        return i[2] = b, i[3] = a > 0, i[4] = c, Y.apply({}, i);
    }
    function $(a, b, c) {
        var d, e = c - b, f = c - a.day();
        return f > e && (f -= 7), e - 7 > f && (f += 7), d = ib(a).add("d", f), {
            week: Math.ceil(d.dayOfYear() / 7),
            year: d.year()
        };
    }
    function _(a, b, c, d, e) {
        var f, g, h = W(a, 0, 1).getUTCDay();
        return c = null != c ? c : e, f = e - h + (h > d ? 7 : 0) - (e > h ? 7 : 0), g = 7 * (b - 1) + (c - e) + f + 1, 
        {
            year: g > 0 ? a : a - 1,
            dayOfYear: g > 0 ? g : w(a - 1) + g
        };
    }
    function ab(b) {
        var c = b._i, d = b._f;
        return null === c || d === a && "" === c ? ib.invalid({
            nullInput: !0
        }) : ("string" == typeof c && (b._i = c = E().preparse(c)), ib.isMoment(c) ? (b = j(c), 
        b._d = new Date(+c._d)) : d ? n(d) ? S(b) : P(b) : U(b), new g(b));
    }
    function bb(a, b) {
        var c;
        return "string" == typeof b && (b = a.lang().monthsParse(b), "number" != typeof b) ? a : (c = Math.min(a.date(), u(a.year(), b)), 
        a._d["set" + (a._isUTC ? "UTC" : "") + "Month"](b, c), a);
    }
    function cb(a, b) {
        return a._d["get" + (a._isUTC ? "UTC" : "") + b]();
    }
    function db(a, b, c) {
        return "Month" === b ? bb(a, c) : a._d["set" + (a._isUTC ? "UTC" : "") + b](c);
    }
    function eb(a, b) {
        return function(c) {
            return null != c ? (db(this, a, c), ib.updateOffset(this, b), this) : cb(this, a);
        };
    }
    function fb(a) {
        ib.duration.fn[a] = function() {
            return this._data[a];
        };
    }
    function gb(a, b) {
        ib.duration.fn["as" + a] = function() {
            return +this / b;
        };
    }
    function hb(a) {
        "undefined" == typeof ender && (jb = mb.moment, mb.moment = a ? c("Accessing Moment through the global scope is deprecated, and will be removed in an upcoming release.", ib) : ib);
    }
    for (var ib, jb, kb, lb = "2.6.0", mb = "undefined" != typeof global ? global : this, nb = Math.round, ob = 0, pb = 1, qb = 2, rb = 3, sb = 4, tb = 5, ub = 6, vb = {}, wb = {
        _isAMomentObject: null,
        _i: null,
        _f: null,
        _l: null,
        _strict: null,
        _isUTC: null,
        _offset: null,
        _pf: null,
        _lang: null
    }, xb = "undefined" != typeof module && module.exports, yb = /^\/?Date\((\-?\d+)/i, zb = /(\-)?(?:(\d*)\.)?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?)?/, Ab = /^(-)?P(?:(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?|([0-9,.]*)W)$/, Bb = /(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Q|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,4}|X|zz?|ZZ?|.)/g, Cb = /(\[[^\[]*\])|(\\)?(LT|LL?L?L?|l{1,4})/g, Db = /\d\d?/, Eb = /\d{1,3}/, Fb = /\d{1,4}/, Gb = /[+\-]?\d{1,6}/, Hb = /\d+/, Ib = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i, Jb = /Z|[\+\-]\d\d:?\d\d/gi, Kb = /T/i, Lb = /[\+\-]?\d+(\.\d{1,3})?/, Mb = /\d{1,2}/, Nb = /\d/, Ob = /\d\d/, Pb = /\d{3}/, Qb = /\d{4}/, Rb = /[+-]?\d{6}/, Sb = /[+-]?\d+/, Tb = /^\s*(?:[+-]\d{6}|\d{4})-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/, Ub = "YYYY-MM-DDTHH:mm:ssZ", Vb = [ [ "YYYYYY-MM-DD", /[+-]\d{6}-\d{2}-\d{2}/ ], [ "YYYY-MM-DD", /\d{4}-\d{2}-\d{2}/ ], [ "GGGG-[W]WW-E", /\d{4}-W\d{2}-\d/ ], [ "GGGG-[W]WW", /\d{4}-W\d{2}/ ], [ "YYYY-DDD", /\d{4}-\d{3}/ ] ], Wb = [ [ "HH:mm:ss.SSSS", /(T| )\d\d:\d\d:\d\d\.\d+/ ], [ "HH:mm:ss", /(T| )\d\d:\d\d:\d\d/ ], [ "HH:mm", /(T| )\d\d:\d\d/ ], [ "HH", /(T| )\d\d/ ] ], Xb = /([\+\-]|\d\d)/gi, Yb = ("Date|Hours|Minutes|Seconds|Milliseconds".split("|"), 
    {
        Milliseconds: 1,
        Seconds: 1e3,
        Minutes: 6e4,
        Hours: 36e5,
        Days: 864e5,
        Months: 2592e6,
        Years: 31536e6
    }), Zb = {
        ms: "millisecond",
        s: "second",
        m: "minute",
        h: "hour",
        d: "day",
        D: "date",
        w: "week",
        W: "isoWeek",
        M: "month",
        Q: "quarter",
        y: "year",
        DDD: "dayOfYear",
        e: "weekday",
        E: "isoWeekday",
        gg: "weekYear",
        GG: "isoWeekYear"
    }, $b = {
        dayofyear: "dayOfYear",
        isoweekday: "isoWeekday",
        isoweek: "isoWeek",
        weekyear: "weekYear",
        isoweekyear: "isoWeekYear"
    }, _b = {}, ac = "DDD w W M D d".split(" "), bc = "M D H h m s w W".split(" "), cc = {
        M: function() {
            return this.month() + 1;
        },
        MMM: function(a) {
            return this.lang().monthsShort(this, a);
        },
        MMMM: function(a) {
            return this.lang().months(this, a);
        },
        D: function() {
            return this.date();
        },
        DDD: function() {
            return this.dayOfYear();
        },
        d: function() {
            return this.day();
        },
        dd: function(a) {
            return this.lang().weekdaysMin(this, a);
        },
        ddd: function(a) {
            return this.lang().weekdaysShort(this, a);
        },
        dddd: function(a) {
            return this.lang().weekdays(this, a);
        },
        w: function() {
            return this.week();
        },
        W: function() {
            return this.isoWeek();
        },
        YY: function() {
            return l(this.year() % 100, 2);
        },
        YYYY: function() {
            return l(this.year(), 4);
        },
        YYYYY: function() {
            return l(this.year(), 5);
        },
        YYYYYY: function() {
            var a = this.year(), b = a >= 0 ? "+" : "-";
            return b + l(Math.abs(a), 6);
        },
        gg: function() {
            return l(this.weekYear() % 100, 2);
        },
        gggg: function() {
            return l(this.weekYear(), 4);
        },
        ggggg: function() {
            return l(this.weekYear(), 5);
        },
        GG: function() {
            return l(this.isoWeekYear() % 100, 2);
        },
        GGGG: function() {
            return l(this.isoWeekYear(), 4);
        },
        GGGGG: function() {
            return l(this.isoWeekYear(), 5);
        },
        e: function() {
            return this.weekday();
        },
        E: function() {
            return this.isoWeekday();
        },
        a: function() {
            return this.lang().meridiem(this.hours(), this.minutes(), !0);
        },
        A: function() {
            return this.lang().meridiem(this.hours(), this.minutes(), !1);
        },
        H: function() {
            return this.hours();
        },
        h: function() {
            return this.hours() % 12 || 12;
        },
        m: function() {
            return this.minutes();
        },
        s: function() {
            return this.seconds();
        },
        S: function() {
            return t(this.milliseconds() / 100);
        },
        SS: function() {
            return l(t(this.milliseconds() / 10), 2);
        },
        SSS: function() {
            return l(this.milliseconds(), 3);
        },
        SSSS: function() {
            return l(this.milliseconds(), 3);
        },
        Z: function() {
            var a = -this.zone(), b = "+";
            return 0 > a && (a = -a, b = "-"), b + l(t(a / 60), 2) + ":" + l(t(a) % 60, 2);
        },
        ZZ: function() {
            var a = -this.zone(), b = "+";
            return 0 > a && (a = -a, b = "-"), b + l(t(a / 60), 2) + l(t(a) % 60, 2);
        },
        z: function() {
            return this.zoneAbbr();
        },
        zz: function() {
            return this.zoneName();
        },
        X: function() {
            return this.unix();
        },
        Q: function() {
            return this.quarter();
        }
    }, dc = [ "months", "monthsShort", "weekdays", "weekdaysShort", "weekdaysMin" ]; ac.length; ) kb = ac.pop(), 
    cc[kb + "o"] = e(cc[kb], kb);
    for (;bc.length; ) kb = bc.pop(), cc[kb + kb] = d(cc[kb], 2);
    for (cc.DDDD = d(cc.DDD, 3), i(f.prototype, {
        set: function(a) {
            var b, c;
            for (c in a) b = a[c], "function" == typeof b ? this[c] = b : this["_" + c] = b;
        },
        _months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
        months: function(a) {
            return this._months[a.month()];
        },
        _monthsShort: "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
        monthsShort: function(a) {
            return this._monthsShort[a.month()];
        },
        monthsParse: function(a) {
            var b, c, d;
            for (this._monthsParse || (this._monthsParse = []), b = 0; 12 > b; b++) if (this._monthsParse[b] || (c = ib.utc([ 2e3, b ]), 
            d = "^" + this.months(c, "") + "|^" + this.monthsShort(c, ""), this._monthsParse[b] = new RegExp(d.replace(".", ""), "i")), 
            this._monthsParse[b].test(a)) return b;
        },
        _weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
        weekdays: function(a) {
            return this._weekdays[a.day()];
        },
        _weekdaysShort: "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
        weekdaysShort: function(a) {
            return this._weekdaysShort[a.day()];
        },
        _weekdaysMin: "Su_Mo_Tu_We_Th_Fr_Sa".split("_"),
        weekdaysMin: function(a) {
            return this._weekdaysMin[a.day()];
        },
        weekdaysParse: function(a) {
            var b, c, d;
            for (this._weekdaysParse || (this._weekdaysParse = []), b = 0; 7 > b; b++) if (this._weekdaysParse[b] || (c = ib([ 2e3, 1 ]).day(b), 
            d = "^" + this.weekdays(c, "") + "|^" + this.weekdaysShort(c, "") + "|^" + this.weekdaysMin(c, ""), 
            this._weekdaysParse[b] = new RegExp(d.replace(".", ""), "i")), this._weekdaysParse[b].test(a)) return b;
        },
        _longDateFormat: {
            LT: "h:mm A",
            L: "MM/DD/YYYY",
            LL: "MMMM D YYYY",
            LLL: "MMMM D YYYY LT",
            LLLL: "dddd, MMMM D YYYY LT"
        },
        longDateFormat: function(a) {
            var b = this._longDateFormat[a];
            return !b && this._longDateFormat[a.toUpperCase()] && (b = this._longDateFormat[a.toUpperCase()].replace(/MMMM|MM|DD|dddd/g, function(a) {
                return a.slice(1);
            }), this._longDateFormat[a] = b), b;
        },
        isPM: function(a) {
            return "p" === (a + "").toLowerCase().charAt(0);
        },
        _meridiemParse: /[ap]\.?m?\.?/i,
        meridiem: function(a, b, c) {
            return a > 11 ? c ? "pm" : "PM" : c ? "am" : "AM";
        },
        _calendar: {
            sameDay: "[Today at] LT",
            nextDay: "[Tomorrow at] LT",
            nextWeek: "dddd [at] LT",
            lastDay: "[Yesterday at] LT",
            lastWeek: "[Last] dddd [at] LT",
            sameElse: "L"
        },
        calendar: function(a, b) {
            var c = this._calendar[a];
            return "function" == typeof c ? c.apply(b) : c;
        },
        _relativeTime: {
            future: "in %s",
            past: "%s ago",
            s: "a few seconds",
            m: "a minute",
            mm: "%d minutes",
            h: "an hour",
            hh: "%d hours",
            d: "a day",
            dd: "%d days",
            M: "a month",
            MM: "%d months",
            y: "a year",
            yy: "%d years"
        },
        relativeTime: function(a, b, c, d) {
            var e = this._relativeTime[c];
            return "function" == typeof e ? e(a, b, c, d) : e.replace(/%d/i, a);
        },
        pastFuture: function(a, b) {
            var c = this._relativeTime[a > 0 ? "future" : "past"];
            return "function" == typeof c ? c(b) : c.replace(/%s/i, b);
        },
        ordinal: function(a) {
            return this._ordinal.replace("%d", a);
        },
        _ordinal: "%d",
        preparse: function(a) {
            return a;
        },
        postformat: function(a) {
            return a;
        },
        week: function(a) {
            return $(a, this._week.dow, this._week.doy).week;
        },
        _week: {
            dow: 0,
            doy: 6
        },
        _invalidDate: "Invalid date",
        invalidDate: function() {
            return this._invalidDate;
        }
    }), ib = function(c, d, e, f) {
        var g;
        return "boolean" == typeof e && (f = e, e = a), g = {}, g._isAMomentObject = !0, 
        g._i = c, g._f = d, g._l = e, g._strict = f, g._isUTC = !1, g._pf = b(), ab(g);
    }, ib.suppressDeprecationWarnings = !1, ib.createFromInputFallback = c("moment construction falls back to js Date. This is discouraged and will be removed in upcoming major release. Please refer to https://github.com/moment/moment/issues/1407 for more info.", function(a) {
        a._d = new Date(a._i);
    }), ib.utc = function(c, d, e, f) {
        var g;
        return "boolean" == typeof e && (f = e, e = a), g = {}, g._isAMomentObject = !0, 
        g._useUTC = !0, g._isUTC = !0, g._l = e, g._i = c, g._f = d, g._strict = f, g._pf = b(), 
        ab(g).utc();
    }, ib.unix = function(a) {
        return ib(1e3 * a);
    }, ib.duration = function(a, b) {
        var c, d, e, f = a, g = null;
        return ib.isDuration(a) ? f = {
            ms: a._milliseconds,
            d: a._days,
            M: a._months
        } : "number" == typeof a ? (f = {}, b ? f[b] = a : f.milliseconds = a) : (g = zb.exec(a)) ? (c = "-" === g[1] ? -1 : 1, 
        f = {
            y: 0,
            d: t(g[qb]) * c,
            h: t(g[rb]) * c,
            m: t(g[sb]) * c,
            s: t(g[tb]) * c,
            ms: t(g[ub]) * c
        }) : (g = Ab.exec(a)) && (c = "-" === g[1] ? -1 : 1, e = function(a) {
            var b = a && parseFloat(a.replace(",", "."));
            return (isNaN(b) ? 0 : b) * c;
        }, f = {
            y: e(g[2]),
            M: e(g[3]),
            d: e(g[4]),
            h: e(g[5]),
            m: e(g[6]),
            s: e(g[7]),
            w: e(g[8])
        }), d = new h(f), ib.isDuration(a) && a.hasOwnProperty("_lang") && (d._lang = a._lang), 
        d;
    }, ib.version = lb, ib.defaultFormat = Ub, ib.momentProperties = wb, ib.updateOffset = function() {}, 
    ib.lang = function(a, b) {
        var c;
        return a ? (b ? C(A(a), b) : null === b ? (D(a), a = "en") : vb[a] || E(a), c = ib.duration.fn._lang = ib.fn._lang = E(a), 
        c._abbr) : ib.fn._lang._abbr;
    }, ib.langData = function(a) {
        return a && a._lang && a._lang._abbr && (a = a._lang._abbr), E(a);
    }, ib.isMoment = function(a) {
        return a instanceof g || null != a && a.hasOwnProperty("_isAMomentObject");
    }, ib.isDuration = function(a) {
        return a instanceof h;
    }, kb = dc.length - 1; kb >= 0; --kb) s(dc[kb]);
    ib.normalizeUnits = function(a) {
        return q(a);
    }, ib.invalid = function(a) {
        var b = ib.utc(0 / 0);
        return null != a ? i(b._pf, a) : b._pf.userInvalidated = !0, b;
    }, ib.parseZone = function() {
        return ib.apply(null, arguments).parseZone();
    }, ib.parseTwoDigitYear = function(a) {
        return t(a) + (t(a) > 68 ? 1900 : 2e3);
    }, i(ib.fn = g.prototype, {
        clone: function() {
            return ib(this);
        },
        valueOf: function() {
            return +this._d + 6e4 * (this._offset || 0);
        },
        unix: function() {
            return Math.floor(+this / 1e3);
        },
        toString: function() {
            return this.clone().lang("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ");
        },
        toDate: function() {
            return this._offset ? new Date(+this) : this._d;
        },
        toISOString: function() {
            var a = ib(this).utc();
            return 0 < a.year() && a.year() <= 9999 ? H(a, "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]") : H(a, "YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]");
        },
        toArray: function() {
            var a = this;
            return [ a.year(), a.month(), a.date(), a.hours(), a.minutes(), a.seconds(), a.milliseconds() ];
        },
        isValid: function() {
            return z(this);
        },
        isDSTShifted: function() {
            return this._a ? this.isValid() && p(this._a, (this._isUTC ? ib.utc(this._a) : ib(this._a)).toArray()) > 0 : !1;
        },
        parsingFlags: function() {
            return i({}, this._pf);
        },
        invalidAt: function() {
            return this._pf.overflow;
        },
        utc: function() {
            return this.zone(0);
        },
        local: function() {
            return this.zone(0), this._isUTC = !1, this;
        },
        format: function(a) {
            var b = H(this, a || ib.defaultFormat);
            return this.lang().postformat(b);
        },
        add: function(a, b) {
            var c;
            return c = "string" == typeof a ? ib.duration(+b, a) : ib.duration(a, b), m(this, c, 1), 
            this;
        },
        subtract: function(a, b) {
            var c;
            return c = "string" == typeof a ? ib.duration(+b, a) : ib.duration(a, b), m(this, c, -1), 
            this;
        },
        diff: function(a, b, c) {
            var d, e, f = B(a, this), g = 6e4 * (this.zone() - f.zone());
            return b = q(b), "year" === b || "month" === b ? (d = 432e5 * (this.daysInMonth() + f.daysInMonth()), 
            e = 12 * (this.year() - f.year()) + (this.month() - f.month()), e += (this - ib(this).startOf("month") - (f - ib(f).startOf("month"))) / d, 
            e -= 6e4 * (this.zone() - ib(this).startOf("month").zone() - (f.zone() - ib(f).startOf("month").zone())) / d, 
            "year" === b && (e /= 12)) : (d = this - f, e = "second" === b ? d / 1e3 : "minute" === b ? d / 6e4 : "hour" === b ? d / 36e5 : "day" === b ? (d - g) / 864e5 : "week" === b ? (d - g) / 6048e5 : d), 
            c ? e : k(e);
        },
        from: function(a, b) {
            return ib.duration(this.diff(a)).lang(this.lang()._abbr).humanize(!b);
        },
        fromNow: function(a) {
            return this.from(ib(), a);
        },
        calendar: function() {
            var a = B(ib(), this).startOf("day"), b = this.diff(a, "days", !0), c = -6 > b ? "sameElse" : -1 > b ? "lastWeek" : 0 > b ? "lastDay" : 1 > b ? "sameDay" : 2 > b ? "nextDay" : 7 > b ? "nextWeek" : "sameElse";
            return this.format(this.lang().calendar(c, this));
        },
        isLeapYear: function() {
            return x(this.year());
        },
        isDST: function() {
            return this.zone() < this.clone().month(0).zone() || this.zone() < this.clone().month(5).zone();
        },
        day: function(a) {
            var b = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
            return null != a ? (a = X(a, this.lang()), this.add({
                d: a - b
            })) : b;
        },
        month: eb("Month", !0),
        startOf: function(a) {
            switch (a = q(a)) {
              case "year":
                this.month(0);

              case "quarter":
              case "month":
                this.date(1);

              case "week":
              case "isoWeek":
              case "day":
                this.hours(0);

              case "hour":
                this.minutes(0);

              case "minute":
                this.seconds(0);

              case "second":
                this.milliseconds(0);
            }
            return "week" === a ? this.weekday(0) : "isoWeek" === a && this.isoWeekday(1), "quarter" === a && this.month(3 * Math.floor(this.month() / 3)), 
            this;
        },
        endOf: function(a) {
            return a = q(a), this.startOf(a).add("isoWeek" === a ? "week" : a, 1).subtract("ms", 1);
        },
        isAfter: function(a, b) {
            return b = "undefined" != typeof b ? b : "millisecond", +this.clone().startOf(b) > +ib(a).startOf(b);
        },
        isBefore: function(a, b) {
            return b = "undefined" != typeof b ? b : "millisecond", +this.clone().startOf(b) < +ib(a).startOf(b);
        },
        isSame: function(a, b) {
            return b = b || "ms", +this.clone().startOf(b) === +B(a, this).startOf(b);
        },
        min: function(a) {
            return a = ib.apply(null, arguments), this > a ? this : a;
        },
        max: function(a) {
            return a = ib.apply(null, arguments), a > this ? this : a;
        },
        zone: function(a, b) {
            var c = this._offset || 0;
            return null == a ? this._isUTC ? c : this._d.getTimezoneOffset() : ("string" == typeof a && (a = K(a)), 
            Math.abs(a) < 16 && (a = 60 * a), this._offset = a, this._isUTC = !0, c !== a && (!b || this._changeInProgress ? m(this, ib.duration(c - a, "m"), 1, !1) : this._changeInProgress || (this._changeInProgress = !0, 
            ib.updateOffset(this, !0), this._changeInProgress = null)), this);
        },
        zoneAbbr: function() {
            return this._isUTC ? "UTC" : "";
        },
        zoneName: function() {
            return this._isUTC ? "Coordinated Universal Time" : "";
        },
        parseZone: function() {
            return this._tzm ? this.zone(this._tzm) : "string" == typeof this._i && this.zone(this._i), 
            this;
        },
        hasAlignedHourOffset: function(a) {
            return a = a ? ib(a).zone() : 0, (this.zone() - a) % 60 === 0;
        },
        daysInMonth: function() {
            return u(this.year(), this.month());
        },
        dayOfYear: function(a) {
            var b = nb((ib(this).startOf("day") - ib(this).startOf("year")) / 864e5) + 1;
            return null == a ? b : this.add("d", a - b);
        },
        quarter: function(a) {
            return null == a ? Math.ceil((this.month() + 1) / 3) : this.month(3 * (a - 1) + this.month() % 3);
        },
        weekYear: function(a) {
            var b = $(this, this.lang()._week.dow, this.lang()._week.doy).year;
            return null == a ? b : this.add("y", a - b);
        },
        isoWeekYear: function(a) {
            var b = $(this, 1, 4).year;
            return null == a ? b : this.add("y", a - b);
        },
        week: function(a) {
            var b = this.lang().week(this);
            return null == a ? b : this.add("d", 7 * (a - b));
        },
        isoWeek: function(a) {
            var b = $(this, 1, 4).week;
            return null == a ? b : this.add("d", 7 * (a - b));
        },
        weekday: function(a) {
            var b = (this.day() + 7 - this.lang()._week.dow) % 7;
            return null == a ? b : this.add("d", a - b);
        },
        isoWeekday: function(a) {
            return null == a ? this.day() || 7 : this.day(this.day() % 7 ? a : a - 7);
        },
        isoWeeksInYear: function() {
            return v(this.year(), 1, 4);
        },
        weeksInYear: function() {
            var a = this._lang._week;
            return v(this.year(), a.dow, a.doy);
        },
        get: function(a) {
            return a = q(a), this[a]();
        },
        set: function(a, b) {
            return a = q(a), "function" == typeof this[a] && this[a](b), this;
        },
        lang: function(b) {
            return b === a ? this._lang : (this._lang = E(b), this);
        }
    }), ib.fn.millisecond = ib.fn.milliseconds = eb("Milliseconds", !1), ib.fn.second = ib.fn.seconds = eb("Seconds", !1), 
    ib.fn.minute = ib.fn.minutes = eb("Minutes", !1), ib.fn.hour = ib.fn.hours = eb("Hours", !0), 
    ib.fn.date = eb("Date", !0), ib.fn.dates = c("dates accessor is deprecated. Use date instead.", eb("Date", !0)), 
    ib.fn.year = eb("FullYear", !0), ib.fn.years = c("years accessor is deprecated. Use year instead.", eb("FullYear", !0)), 
    ib.fn.days = ib.fn.day, ib.fn.months = ib.fn.month, ib.fn.weeks = ib.fn.week, ib.fn.isoWeeks = ib.fn.isoWeek, 
    ib.fn.quarters = ib.fn.quarter, ib.fn.toJSON = ib.fn.toISOString, i(ib.duration.fn = h.prototype, {
        _bubble: function() {
            var a, b, c, d, e = this._milliseconds, f = this._days, g = this._months, h = this._data;
            h.milliseconds = e % 1e3, a = k(e / 1e3), h.seconds = a % 60, b = k(a / 60), h.minutes = b % 60, 
            c = k(b / 60), h.hours = c % 24, f += k(c / 24), h.days = f % 30, g += k(f / 30), 
            h.months = g % 12, d = k(g / 12), h.years = d;
        },
        weeks: function() {
            return k(this.days() / 7);
        },
        valueOf: function() {
            return this._milliseconds + 864e5 * this._days + this._months % 12 * 2592e6 + 31536e6 * t(this._months / 12);
        },
        humanize: function(a) {
            var b = +this, c = Z(b, !a, this.lang());
            return a && (c = this.lang().pastFuture(b, c)), this.lang().postformat(c);
        },
        add: function(a, b) {
            var c = ib.duration(a, b);
            return this._milliseconds += c._milliseconds, this._days += c._days, this._months += c._months, 
            this._bubble(), this;
        },
        subtract: function(a, b) {
            var c = ib.duration(a, b);
            return this._milliseconds -= c._milliseconds, this._days -= c._days, this._months -= c._months, 
            this._bubble(), this;
        },
        get: function(a) {
            return a = q(a), this[a.toLowerCase() + "s"]();
        },
        as: function(a) {
            return a = q(a), this["as" + a.charAt(0).toUpperCase() + a.slice(1) + "s"]();
        },
        lang: ib.fn.lang,
        toIsoString: function() {
            var a = Math.abs(this.years()), b = Math.abs(this.months()), c = Math.abs(this.days()), d = Math.abs(this.hours()), e = Math.abs(this.minutes()), f = Math.abs(this.seconds() + this.milliseconds() / 1e3);
            return this.asSeconds() ? (this.asSeconds() < 0 ? "-" : "") + "P" + (a ? a + "Y" : "") + (b ? b + "M" : "") + (c ? c + "D" : "") + (d || e || f ? "T" : "") + (d ? d + "H" : "") + (e ? e + "M" : "") + (f ? f + "S" : "") : "P0D";
        }
    });
    for (kb in Yb) Yb.hasOwnProperty(kb) && (gb(kb, Yb[kb]), fb(kb.toLowerCase()));
    gb("Weeks", 6048e5), ib.duration.fn.asMonths = function() {
        return (+this - 31536e6 * this.years()) / 2592e6 + 12 * this.years();
    }, ib.lang("en", {
        ordinal: function(a) {
            var b = a % 10, c = 1 === t(a % 100 / 10) ? "th" : 1 === b ? "st" : 2 === b ? "nd" : 3 === b ? "rd" : "th";
            return a + c;
        }
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("ar-ma", {
            months: "يناير_فبراير_مارس_أبريل_ماي_يونيو_يوليوز_غشت_شتنبر_أكتوبر_نونبر_دجنبر".split("_"),
            monthsShort: "يناير_فبراير_مارس_أبريل_ماي_يونيو_يوليوز_غشت_شتنبر_أكتوبر_نونبر_دجنبر".split("_"),
            weekdays: "الأحد_الإتنين_الثلاثاء_الأربعاء_الخميس_الجمعة_السبت".split("_"),
            weekdaysShort: "احد_اتنين_ثلاثاء_اربعاء_خميس_جمعة_سبت".split("_"),
            weekdaysMin: "ح_ن_ث_ر_خ_ج_س".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd D MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[اليوم على الساعة] LT",
                nextDay: "[غدا على الساعة] LT",
                nextWeek: "dddd [على الساعة] LT",
                lastDay: "[أمس على الساعة] LT",
                lastWeek: "dddd [على الساعة] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "في %s",
                past: "منذ %s",
                s: "ثوان",
                m: "دقيقة",
                mm: "%d دقائق",
                h: "ساعة",
                hh: "%d ساعات",
                d: "يوم",
                dd: "%d أيام",
                M: "شهر",
                MM: "%d أشهر",
                y: "سنة",
                yy: "%d سنوات"
            },
            week: {
                dow: 6,
                doy: 12
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("ar", {
            months: "يناير/ كانون الثاني_فبراير/ شباط_مارس/ آذار_أبريل/ نيسان_مايو/ أيار_يونيو/ حزيران_يوليو/ تموز_أغسطس/ آب_سبتمبر/ أيلول_أكتوبر/ تشرين الأول_نوفمبر/ تشرين الثاني_ديسمبر/ كانون الأول".split("_"),
            monthsShort: "يناير/ كانون الثاني_فبراير/ شباط_مارس/ آذار_أبريل/ نيسان_مايو/ أيار_يونيو/ حزيران_يوليو/ تموز_أغسطس/ آب_سبتمبر/ أيلول_أكتوبر/ تشرين الأول_نوفمبر/ تشرين الثاني_ديسمبر/ كانون الأول".split("_"),
            weekdays: "الأحد_الإثنين_الثلاثاء_الأربعاء_الخميس_الجمعة_السبت".split("_"),
            weekdaysShort: "الأحد_الإثنين_الثلاثاء_الأربعاء_الخميس_الجمعة_السبت".split("_"),
            weekdaysMin: "ح_ن_ث_ر_خ_ج_س".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd D MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[اليوم على الساعة] LT",
                nextDay: "[غدا على الساعة] LT",
                nextWeek: "dddd [على الساعة] LT",
                lastDay: "[أمس على الساعة] LT",
                lastWeek: "dddd [على الساعة] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "في %s",
                past: "منذ %s",
                s: "ثوان",
                m: "دقيقة",
                mm: "%d دقائق",
                h: "ساعة",
                hh: "%d ساعات",
                d: "يوم",
                dd: "%d أيام",
                M: "شهر",
                MM: "%d أشهر",
                y: "سنة",
                yy: "%d سنوات"
            },
            week: {
                dow: 6,
                doy: 12
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("bg", {
            months: "януари_февруари_март_април_май_юни_юли_август_септември_октомври_ноември_декември".split("_"),
            monthsShort: "янр_фев_мар_апр_май_юни_юли_авг_сеп_окт_ное_дек".split("_"),
            weekdays: "неделя_понеделник_вторник_сряда_четвъртък_петък_събота".split("_"),
            weekdaysShort: "нед_пон_вто_сря_чет_пет_съб".split("_"),
            weekdaysMin: "нд_пн_вт_ср_чт_пт_сб".split("_"),
            longDateFormat: {
                LT: "H:mm",
                L: "D.MM.YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd, D MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[Днес в] LT",
                nextDay: "[Утре в] LT",
                nextWeek: "dddd [в] LT",
                lastDay: "[Вчера в] LT",
                lastWeek: function() {
                    switch (this.day()) {
                      case 0:
                      case 3:
                      case 6:
                        return "[В изминалата] dddd [в] LT";

                      case 1:
                      case 2:
                      case 4:
                      case 5:
                        return "[В изминалия] dddd [в] LT";
                    }
                },
                sameElse: "L"
            },
            relativeTime: {
                future: "след %s",
                past: "преди %s",
                s: "няколко секунди",
                m: "минута",
                mm: "%d минути",
                h: "час",
                hh: "%d часа",
                d: "ден",
                dd: "%d дни",
                M: "месец",
                MM: "%d месеца",
                y: "година",
                yy: "%d години"
            },
            ordinal: function(a) {
                var b = a % 10, c = a % 100;
                return 0 === a ? a + "-ев" : 0 === c ? a + "-ен" : c > 10 && 20 > c ? a + "-ти" : 1 === b ? a + "-ви" : 2 === b ? a + "-ри" : 7 === b || 8 === b ? a + "-ми" : a + "-ти";
            },
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(b) {
        function c(a, b, c) {
            var d = {
                mm: "munutenn",
                MM: "miz",
                dd: "devezh"
            };
            return a + " " + f(d[c], a);
        }
        function d(a) {
            switch (e(a)) {
              case 1:
              case 3:
              case 4:
              case 5:
              case 9:
                return a + " bloaz";

              default:
                return a + " vloaz";
            }
        }
        function e(a) {
            return a > 9 ? e(a % 10) : a;
        }
        function f(a, b) {
            return 2 === b ? g(a) : a;
        }
        function g(b) {
            var c = {
                m: "v",
                b: "v",
                d: "z"
            };
            return c[b.charAt(0)] === a ? b : c[b.charAt(0)] + b.substring(1);
        }
        return b.lang("br", {
            months: "Genver_C'hwevrer_Meurzh_Ebrel_Mae_Mezheven_Gouere_Eost_Gwengolo_Here_Du_Kerzu".split("_"),
            monthsShort: "Gen_C'hwe_Meu_Ebr_Mae_Eve_Gou_Eos_Gwe_Her_Du_Ker".split("_"),
            weekdays: "Sul_Lun_Meurzh_Merc'her_Yaou_Gwener_Sadorn".split("_"),
            weekdaysShort: "Sul_Lun_Meu_Mer_Yao_Gwe_Sad".split("_"),
            weekdaysMin: "Su_Lu_Me_Mer_Ya_Gw_Sa".split("_"),
            longDateFormat: {
                LT: "h[e]mm A",
                L: "DD/MM/YYYY",
                LL: "D [a viz] MMMM YYYY",
                LLL: "D [a viz] MMMM YYYY LT",
                LLLL: "dddd, D [a viz] MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[Hiziv da] LT",
                nextDay: "[Warc'hoazh da] LT",
                nextWeek: "dddd [da] LT",
                lastDay: "[Dec'h da] LT",
                lastWeek: "dddd [paset da] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "a-benn %s",
                past: "%s 'zo",
                s: "un nebeud segondennoù",
                m: "ur vunutenn",
                mm: c,
                h: "un eur",
                hh: "%d eur",
                d: "un devezh",
                dd: c,
                M: "ur miz",
                MM: c,
                y: "ur bloaz",
                yy: d
            },
            ordinal: function(a) {
                var b = 1 === a ? "añ" : "vet";
                return a + b;
            },
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        function b(a, b, c) {
            var d = a + " ";
            switch (c) {
              case "m":
                return b ? "jedna minuta" : "jedne minute";

              case "mm":
                return d += 1 === a ? "minuta" : 2 === a || 3 === a || 4 === a ? "minute" : "minuta";

              case "h":
                return b ? "jedan sat" : "jednog sata";

              case "hh":
                return d += 1 === a ? "sat" : 2 === a || 3 === a || 4 === a ? "sata" : "sati";

              case "dd":
                return d += 1 === a ? "dan" : "dana";

              case "MM":
                return d += 1 === a ? "mjesec" : 2 === a || 3 === a || 4 === a ? "mjeseca" : "mjeseci";

              case "yy":
                return d += 1 === a ? "godina" : 2 === a || 3 === a || 4 === a ? "godine" : "godina";
            }
        }
        return a.lang("bs", {
            months: "januar_februar_mart_april_maj_juni_juli_avgust_septembar_oktobar_novembar_decembar".split("_"),
            monthsShort: "jan._feb._mar._apr._maj._jun._jul._avg._sep._okt._nov._dec.".split("_"),
            weekdays: "nedjelja_ponedjeljak_utorak_srijeda_četvrtak_petak_subota".split("_"),
            weekdaysShort: "ned._pon._uto._sri._čet._pet._sub.".split("_"),
            weekdaysMin: "ne_po_ut_sr_če_pe_su".split("_"),
            longDateFormat: {
                LT: "H:mm",
                L: "DD. MM. YYYY",
                LL: "D. MMMM YYYY",
                LLL: "D. MMMM YYYY LT",
                LLLL: "dddd, D. MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[danas u] LT",
                nextDay: "[sutra u] LT",
                nextWeek: function() {
                    switch (this.day()) {
                      case 0:
                        return "[u] [nedjelju] [u] LT";

                      case 3:
                        return "[u] [srijedu] [u] LT";

                      case 6:
                        return "[u] [subotu] [u] LT";

                      case 1:
                      case 2:
                      case 4:
                      case 5:
                        return "[u] dddd [u] LT";
                    }
                },
                lastDay: "[jučer u] LT",
                lastWeek: function() {
                    switch (this.day()) {
                      case 0:
                      case 3:
                        return "[prošlu] dddd [u] LT";

                      case 6:
                        return "[prošle] [subote] [u] LT";

                      case 1:
                      case 2:
                      case 4:
                      case 5:
                        return "[prošli] dddd [u] LT";
                    }
                },
                sameElse: "L"
            },
            relativeTime: {
                future: "za %s",
                past: "prije %s",
                s: "par sekundi",
                m: b,
                mm: b,
                h: b,
                hh: b,
                d: "dan",
                dd: b,
                M: "mjesec",
                MM: b,
                y: "godinu",
                yy: b
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("ca", {
            months: "gener_febrer_març_abril_maig_juny_juliol_agost_setembre_octubre_novembre_desembre".split("_"),
            monthsShort: "gen._febr._mar._abr._mai._jun._jul._ag._set._oct._nov._des.".split("_"),
            weekdays: "diumenge_dilluns_dimarts_dimecres_dijous_divendres_dissabte".split("_"),
            weekdaysShort: "dg._dl._dt._dc._dj._dv._ds.".split("_"),
            weekdaysMin: "Dg_Dl_Dt_Dc_Dj_Dv_Ds".split("_"),
            longDateFormat: {
                LT: "H:mm",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd D MMMM YYYY LT"
            },
            calendar: {
                sameDay: function() {
                    return "[avui a " + (1 !== this.hours() ? "les" : "la") + "] LT";
                },
                nextDay: function() {
                    return "[demà a " + (1 !== this.hours() ? "les" : "la") + "] LT";
                },
                nextWeek: function() {
                    return "dddd [a " + (1 !== this.hours() ? "les" : "la") + "] LT";
                },
                lastDay: function() {
                    return "[ahir a " + (1 !== this.hours() ? "les" : "la") + "] LT";
                },
                lastWeek: function() {
                    return "[el] dddd [passat a " + (1 !== this.hours() ? "les" : "la") + "] LT";
                },
                sameElse: "L"
            },
            relativeTime: {
                future: "en %s",
                past: "fa %s",
                s: "uns segons",
                m: "un minut",
                mm: "%d minuts",
                h: "una hora",
                hh: "%d hores",
                d: "un dia",
                dd: "%d dies",
                M: "un mes",
                MM: "%d mesos",
                y: "un any",
                yy: "%d anys"
            },
            ordinal: "%dº",
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        function b(a) {
            return a > 1 && 5 > a && 1 !== ~~(a / 10);
        }
        function c(a, c, d, e) {
            var f = a + " ";
            switch (d) {
              case "s":
                return c || e ? "pár sekund" : "pár sekundami";

              case "m":
                return c ? "minuta" : e ? "minutu" : "minutou";

              case "mm":
                return c || e ? f + (b(a) ? "minuty" : "minut") : f + "minutami";
                break;

              case "h":
                return c ? "hodina" : e ? "hodinu" : "hodinou";

              case "hh":
                return c || e ? f + (b(a) ? "hodiny" : "hodin") : f + "hodinami";
                break;

              case "d":
                return c || e ? "den" : "dnem";

              case "dd":
                return c || e ? f + (b(a) ? "dny" : "dní") : f + "dny";
                break;

              case "M":
                return c || e ? "měsíc" : "měsícem";

              case "MM":
                return c || e ? f + (b(a) ? "měsíce" : "měsíců") : f + "měsíci";
                break;

              case "y":
                return c || e ? "rok" : "rokem";

              case "yy":
                return c || e ? f + (b(a) ? "roky" : "let") : f + "lety";
            }
        }
        var d = "leden_únor_březen_duben_květen_červen_červenec_srpen_září_říjen_listopad_prosinec".split("_"), e = "led_úno_bře_dub_kvě_čvn_čvc_srp_zář_říj_lis_pro".split("_");
        return a.lang("cs", {
            months: d,
            monthsShort: e,
            monthsParse: function(a, b) {
                var c, d = [];
                for (c = 0; 12 > c; c++) d[c] = new RegExp("^" + a[c] + "$|^" + b[c] + "$", "i");
                return d;
            }(d, e),
            weekdays: "neděle_pondělí_úterý_středa_čtvrtek_pátek_sobota".split("_"),
            weekdaysShort: "ne_po_út_st_čt_pá_so".split("_"),
            weekdaysMin: "ne_po_út_st_čt_pá_so".split("_"),
            longDateFormat: {
                LT: "H.mm",
                L: "DD. MM. YYYY",
                LL: "D. MMMM YYYY",
                LLL: "D. MMMM YYYY LT",
                LLLL: "dddd D. MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[dnes v] LT",
                nextDay: "[zítra v] LT",
                nextWeek: function() {
                    switch (this.day()) {
                      case 0:
                        return "[v neděli v] LT";

                      case 1:
                      case 2:
                        return "[v] dddd [v] LT";

                      case 3:
                        return "[ve středu v] LT";

                      case 4:
                        return "[ve čtvrtek v] LT";

                      case 5:
                        return "[v pátek v] LT";

                      case 6:
                        return "[v sobotu v] LT";
                    }
                },
                lastDay: "[včera v] LT",
                lastWeek: function() {
                    switch (this.day()) {
                      case 0:
                        return "[minulou neděli v] LT";

                      case 1:
                      case 2:
                        return "[minulé] dddd [v] LT";

                      case 3:
                        return "[minulou středu v] LT";

                      case 4:
                      case 5:
                        return "[minulý] dddd [v] LT";

                      case 6:
                        return "[minulou sobotu v] LT";
                    }
                },
                sameElse: "L"
            },
            relativeTime: {
                future: "za %s",
                past: "před %s",
                s: c,
                m: c,
                mm: c,
                h: c,
                hh: c,
                d: c,
                dd: c,
                M: c,
                MM: c,
                y: c,
                yy: c
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("cv", {
            months: "кăрлач_нарăс_пуш_ака_май_çĕртме_утă_çурла_авăн_юпа_чӳк_раштав".split("_"),
            monthsShort: "кăр_нар_пуш_ака_май_çĕр_утă_çур_ав_юпа_чӳк_раш".split("_"),
            weekdays: "вырсарникун_тунтикун_ытларикун_юнкун_кĕçнерникун_эрнекун_шăматкун".split("_"),
            weekdaysShort: "выр_тун_ытл_юн_кĕç_эрн_шăм".split("_"),
            weekdaysMin: "вр_тн_ыт_юн_кç_эр_шм".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD-MM-YYYY",
                LL: "YYYY [çулхи] MMMM [уйăхĕн] D[-мĕшĕ]",
                LLL: "YYYY [çулхи] MMMM [уйăхĕн] D[-мĕшĕ], LT",
                LLLL: "dddd, YYYY [çулхи] MMMM [уйăхĕн] D[-мĕшĕ], LT"
            },
            calendar: {
                sameDay: "[Паян] LT [сехетре]",
                nextDay: "[Ыран] LT [сехетре]",
                lastDay: "[Ĕнер] LT [сехетре]",
                nextWeek: "[Çитес] dddd LT [сехетре]",
                lastWeek: "[Иртнĕ] dddd LT [сехетре]",
                sameElse: "L"
            },
            relativeTime: {
                future: function(a) {
                    var b = /сехет$/i.exec(a) ? "рен" : /çул$/i.exec(a) ? "тан" : "ран";
                    return a + b;
                },
                past: "%s каялла",
                s: "пĕр-ик çеккунт",
                m: "пĕр минут",
                mm: "%d минут",
                h: "пĕр сехет",
                hh: "%d сехет",
                d: "пĕр кун",
                dd: "%d кун",
                M: "пĕр уйăх",
                MM: "%d уйăх",
                y: "пĕр çул",
                yy: "%d çул"
            },
            ordinal: "%d-мĕш",
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("cy", {
            months: "Ionawr_Chwefror_Mawrth_Ebrill_Mai_Mehefin_Gorffennaf_Awst_Medi_Hydref_Tachwedd_Rhagfyr".split("_"),
            monthsShort: "Ion_Chwe_Maw_Ebr_Mai_Meh_Gor_Aws_Med_Hyd_Tach_Rhag".split("_"),
            weekdays: "Dydd Sul_Dydd Llun_Dydd Mawrth_Dydd Mercher_Dydd Iau_Dydd Gwener_Dydd Sadwrn".split("_"),
            weekdaysShort: "Sul_Llun_Maw_Mer_Iau_Gwe_Sad".split("_"),
            weekdaysMin: "Su_Ll_Ma_Me_Ia_Gw_Sa".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd, D MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[Heddiw am] LT",
                nextDay: "[Yfory am] LT",
                nextWeek: "dddd [am] LT",
                lastDay: "[Ddoe am] LT",
                lastWeek: "dddd [diwethaf am] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "mewn %s",
                past: "%s yn àl",
                s: "ychydig eiliadau",
                m: "munud",
                mm: "%d munud",
                h: "awr",
                hh: "%d awr",
                d: "diwrnod",
                dd: "%d diwrnod",
                M: "mis",
                MM: "%d mis",
                y: "blwyddyn",
                yy: "%d flynedd"
            },
            ordinal: function(a) {
                var b = a, c = "", d = [ "", "af", "il", "ydd", "ydd", "ed", "ed", "ed", "fed", "fed", "fed", "eg", "fed", "eg", "eg", "fed", "eg", "eg", "fed", "eg", "fed" ];
                return b > 20 ? c = 40 === b || 50 === b || 60 === b || 80 === b || 100 === b ? "fed" : "ain" : b > 0 && (c = d[b]), 
                a + c;
            },
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("da", {
            months: "januar_februar_marts_april_maj_juni_juli_august_september_oktober_november_december".split("_"),
            monthsShort: "jan_feb_mar_apr_maj_jun_jul_aug_sep_okt_nov_dec".split("_"),
            weekdays: "søndag_mandag_tirsdag_onsdag_torsdag_fredag_lørdag".split("_"),
            weekdaysShort: "søn_man_tir_ons_tor_fre_lør".split("_"),
            weekdaysMin: "sø_ma_ti_on_to_fr_lø".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd D. MMMM, YYYY LT"
            },
            calendar: {
                sameDay: "[I dag kl.] LT",
                nextDay: "[I morgen kl.] LT",
                nextWeek: "dddd [kl.] LT",
                lastDay: "[I går kl.] LT",
                lastWeek: "[sidste] dddd [kl] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "om %s",
                past: "%s siden",
                s: "få sekunder",
                m: "et minut",
                mm: "%d minutter",
                h: "en time",
                hh: "%d timer",
                d: "en dag",
                dd: "%d dage",
                M: "en måned",
                MM: "%d måneder",
                y: "et år",
                yy: "%d år"
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        function b(a, b, c) {
            var d = {
                m: [ "eine Minute", "einer Minute" ],
                h: [ "eine Stunde", "einer Stunde" ],
                d: [ "ein Tag", "einem Tag" ],
                dd: [ a + " Tage", a + " Tagen" ],
                M: [ "ein Monat", "einem Monat" ],
                MM: [ a + " Monate", a + " Monaten" ],
                y: [ "ein Jahr", "einem Jahr" ],
                yy: [ a + " Jahre", a + " Jahren" ]
            };
            return b ? d[c][0] : d[c][1];
        }
        return a.lang("de", {
            months: "Januar_Februar_März_April_Mai_Juni_Juli_August_September_Oktober_November_Dezember".split("_"),
            monthsShort: "Jan._Febr._Mrz._Apr._Mai_Jun._Jul._Aug._Sept._Okt._Nov._Dez.".split("_"),
            weekdays: "Sonntag_Montag_Dienstag_Mittwoch_Donnerstag_Freitag_Samstag".split("_"),
            weekdaysShort: "So._Mo._Di._Mi._Do._Fr._Sa.".split("_"),
            weekdaysMin: "So_Mo_Di_Mi_Do_Fr_Sa".split("_"),
            longDateFormat: {
                LT: "HH:mm [Uhr]",
                L: "DD.MM.YYYY",
                LL: "D. MMMM YYYY",
                LLL: "D. MMMM YYYY LT",
                LLLL: "dddd, D. MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[Heute um] LT",
                sameElse: "L",
                nextDay: "[Morgen um] LT",
                nextWeek: "dddd [um] LT",
                lastDay: "[Gestern um] LT",
                lastWeek: "[letzten] dddd [um] LT"
            },
            relativeTime: {
                future: "in %s",
                past: "vor %s",
                s: "ein paar Sekunden",
                m: b,
                mm: "%d Minuten",
                h: b,
                hh: "%d Stunden",
                d: b,
                dd: b,
                M: b,
                MM: b,
                y: b,
                yy: b
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("el", {
            monthsNominativeEl: "Ιανουάριος_Φεβρουάριος_Μάρτιος_Απρίλιος_Μάιος_Ιούνιος_Ιούλιος_Αύγουστος_Σεπτέμβριος_Οκτώβριος_Νοέμβριος_Δεκέμβριος".split("_"),
            monthsGenitiveEl: "Ιανουαρίου_Φεβρουαρίου_Μαρτίου_Απριλίου_Μαΐου_Ιουνίου_Ιουλίου_Αυγούστου_Σεπτεμβρίου_Οκτωβρίου_Νοεμβρίου_Δεκεμβρίου".split("_"),
            months: function(a, b) {
                return /D/.test(b.substring(0, b.indexOf("MMMM"))) ? this._monthsGenitiveEl[a.month()] : this._monthsNominativeEl[a.month()];
            },
            monthsShort: "Ιαν_Φεβ_Μαρ_Απρ_Μαϊ_Ιουν_Ιουλ_Αυγ_Σεπ_Οκτ_Νοε_Δεκ".split("_"),
            weekdays: "Κυριακή_Δευτέρα_Τρίτη_Τετάρτη_Πέμπτη_Παρασκευή_Σάββατο".split("_"),
            weekdaysShort: "Κυρ_Δευ_Τρι_Τετ_Πεμ_Παρ_Σαβ".split("_"),
            weekdaysMin: "Κυ_Δε_Τρ_Τε_Πε_Πα_Σα".split("_"),
            meridiem: function(a, b, c) {
                return a > 11 ? c ? "μμ" : "ΜΜ" : c ? "πμ" : "ΠΜ";
            },
            longDateFormat: {
                LT: "h:mm A",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd, D MMMM YYYY LT"
            },
            calendarEl: {
                sameDay: "[Σήμερα {}] LT",
                nextDay: "[Αύριο {}] LT",
                nextWeek: "dddd [{}] LT",
                lastDay: "[Χθες {}] LT",
                lastWeek: "[την προηγούμενη] dddd [{}] LT",
                sameElse: "L"
            },
            calendar: function(a, b) {
                var c = this._calendarEl[a], d = b && b.hours();
                return c.replace("{}", d % 12 === 1 ? "στη" : "στις");
            },
            relativeTime: {
                future: "σε %s",
                past: "%s πριν",
                s: "δευτερόλεπτα",
                m: "ένα λεπτό",
                mm: "%d λεπτά",
                h: "μία ώρα",
                hh: "%d ώρες",
                d: "μία μέρα",
                dd: "%d μέρες",
                M: "ένας μήνας",
                MM: "%d μήνες",
                y: "ένας χρόνος",
                yy: "%d χρόνια"
            },
            ordinal: function(a) {
                return a + "η";
            },
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("en-au", {
            months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
            monthsShort: "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
            weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
            weekdaysShort: "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
            weekdaysMin: "Su_Mo_Tu_We_Th_Fr_Sa".split("_"),
            longDateFormat: {
                LT: "h:mm A",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd, D MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[Today at] LT",
                nextDay: "[Tomorrow at] LT",
                nextWeek: "dddd [at] LT",
                lastDay: "[Yesterday at] LT",
                lastWeek: "[Last] dddd [at] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "in %s",
                past: "%s ago",
                s: "a few seconds",
                m: "a minute",
                mm: "%d minutes",
                h: "an hour",
                hh: "%d hours",
                d: "a day",
                dd: "%d days",
                M: "a month",
                MM: "%d months",
                y: "a year",
                yy: "%d years"
            },
            ordinal: function(a) {
                var b = a % 10, c = 1 === ~~(a % 100 / 10) ? "th" : 1 === b ? "st" : 2 === b ? "nd" : 3 === b ? "rd" : "th";
                return a + c;
            },
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("en-ca", {
            months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
            monthsShort: "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
            weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
            weekdaysShort: "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
            weekdaysMin: "Su_Mo_Tu_We_Th_Fr_Sa".split("_"),
            longDateFormat: {
                LT: "h:mm A",
                L: "YYYY-MM-DD",
                LL: "D MMMM, YYYY",
                LLL: "D MMMM, YYYY LT",
                LLLL: "dddd, D MMMM, YYYY LT"
            },
            calendar: {
                sameDay: "[Today at] LT",
                nextDay: "[Tomorrow at] LT",
                nextWeek: "dddd [at] LT",
                lastDay: "[Yesterday at] LT",
                lastWeek: "[Last] dddd [at] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "in %s",
                past: "%s ago",
                s: "a few seconds",
                m: "a minute",
                mm: "%d minutes",
                h: "an hour",
                hh: "%d hours",
                d: "a day",
                dd: "%d days",
                M: "a month",
                MM: "%d months",
                y: "a year",
                yy: "%d years"
            },
            ordinal: function(a) {
                var b = a % 10, c = 1 === ~~(a % 100 / 10) ? "th" : 1 === b ? "st" : 2 === b ? "nd" : 3 === b ? "rd" : "th";
                return a + c;
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("en-gb", {
            months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
            monthsShort: "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
            weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
            weekdaysShort: "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
            weekdaysMin: "Su_Mo_Tu_We_Th_Fr_Sa".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd, D MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[Today at] LT",
                nextDay: "[Tomorrow at] LT",
                nextWeek: "dddd [at] LT",
                lastDay: "[Yesterday at] LT",
                lastWeek: "[Last] dddd [at] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "in %s",
                past: "%s ago",
                s: "a few seconds",
                m: "a minute",
                mm: "%d minutes",
                h: "an hour",
                hh: "%d hours",
                d: "a day",
                dd: "%d days",
                M: "a month",
                MM: "%d months",
                y: "a year",
                yy: "%d years"
            },
            ordinal: function(a) {
                var b = a % 10, c = 1 === ~~(a % 100 / 10) ? "th" : 1 === b ? "st" : 2 === b ? "nd" : 3 === b ? "rd" : "th";
                return a + c;
            },
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("eo", {
            months: "januaro_februaro_marto_aprilo_majo_junio_julio_aŭgusto_septembro_oktobro_novembro_decembro".split("_"),
            monthsShort: "jan_feb_mar_apr_maj_jun_jul_aŭg_sep_okt_nov_dec".split("_"),
            weekdays: "Dimanĉo_Lundo_Mardo_Merkredo_Ĵaŭdo_Vendredo_Sabato".split("_"),
            weekdaysShort: "Dim_Lun_Mard_Merk_Ĵaŭ_Ven_Sab".split("_"),
            weekdaysMin: "Di_Lu_Ma_Me_Ĵa_Ve_Sa".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "YYYY-MM-DD",
                LL: "D[-an de] MMMM, YYYY",
                LLL: "D[-an de] MMMM, YYYY LT",
                LLLL: "dddd, [la] D[-an de] MMMM, YYYY LT"
            },
            meridiem: function(a, b, c) {
                return a > 11 ? c ? "p.t.m." : "P.T.M." : c ? "a.t.m." : "A.T.M.";
            },
            calendar: {
                sameDay: "[Hodiaŭ je] LT",
                nextDay: "[Morgaŭ je] LT",
                nextWeek: "dddd [je] LT",
                lastDay: "[Hieraŭ je] LT",
                lastWeek: "[pasinta] dddd [je] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "je %s",
                past: "antaŭ %s",
                s: "sekundoj",
                m: "minuto",
                mm: "%d minutoj",
                h: "horo",
                hh: "%d horoj",
                d: "tago",
                dd: "%d tagoj",
                M: "monato",
                MM: "%d monatoj",
                y: "jaro",
                yy: "%d jaroj"
            },
            ordinal: "%da",
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        var b = "ene._feb._mar._abr._may._jun._jul._ago._sep._oct._nov._dic.".split("_"), c = "ene_feb_mar_abr_may_jun_jul_ago_sep_oct_nov_dic".split("_");
        return a.lang("es", {
            months: "enero_febrero_marzo_abril_mayo_junio_julio_agosto_septiembre_octubre_noviembre_diciembre".split("_"),
            monthsShort: function(a, d) {
                return /-MMM-/.test(d) ? c[a.month()] : b[a.month()];
            },
            weekdays: "domingo_lunes_martes_miércoles_jueves_viernes_sábado".split("_"),
            weekdaysShort: "dom._lun._mar._mié._jue._vie._sáb.".split("_"),
            weekdaysMin: "Do_Lu_Ma_Mi_Ju_Vi_Sá".split("_"),
            longDateFormat: {
                LT: "H:mm",
                L: "DD/MM/YYYY",
                LL: "D [de] MMMM [del] YYYY",
                LLL: "D [de] MMMM [del] YYYY LT",
                LLLL: "dddd, D [de] MMMM [del] YYYY LT"
            },
            calendar: {
                sameDay: function() {
                    return "[hoy a la" + (1 !== this.hours() ? "s" : "") + "] LT";
                },
                nextDay: function() {
                    return "[mañana a la" + (1 !== this.hours() ? "s" : "") + "] LT";
                },
                nextWeek: function() {
                    return "dddd [a la" + (1 !== this.hours() ? "s" : "") + "] LT";
                },
                lastDay: function() {
                    return "[ayer a la" + (1 !== this.hours() ? "s" : "") + "] LT";
                },
                lastWeek: function() {
                    return "[el] dddd [pasado a la" + (1 !== this.hours() ? "s" : "") + "] LT";
                },
                sameElse: "L"
            },
            relativeTime: {
                future: "en %s",
                past: "hace %s",
                s: "unos segundos",
                m: "un minuto",
                mm: "%d minutos",
                h: "una hora",
                hh: "%d horas",
                d: "un día",
                dd: "%d días",
                M: "un mes",
                MM: "%d meses",
                y: "un año",
                yy: "%d años"
            },
            ordinal: "%dº",
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        function b(a, b, c, d) {
            var e = {
                s: [ "mõne sekundi", "mõni sekund", "paar sekundit" ],
                m: [ "ühe minuti", "üks minut" ],
                mm: [ a + " minuti", a + " minutit" ],
                h: [ "ühe tunni", "tund aega", "üks tund" ],
                hh: [ a + " tunni", a + " tundi" ],
                d: [ "ühe päeva", "üks päev" ],
                M: [ "kuu aja", "kuu aega", "üks kuu" ],
                MM: [ a + " kuu", a + " kuud" ],
                y: [ "ühe aasta", "aasta", "üks aasta" ],
                yy: [ a + " aasta", a + " aastat" ]
            };
            return b ? e[c][2] ? e[c][2] : e[c][1] : d ? e[c][0] : e[c][1];
        }
        return a.lang("et", {
            months: "jaanuar_veebruar_märts_aprill_mai_juuni_juuli_august_september_oktoober_november_detsember".split("_"),
            monthsShort: "jaan_veebr_märts_apr_mai_juuni_juuli_aug_sept_okt_nov_dets".split("_"),
            weekdays: "pühapäev_esmaspäev_teisipäev_kolmapäev_neljapäev_reede_laupäev".split("_"),
            weekdaysShort: "P_E_T_K_N_R_L".split("_"),
            weekdaysMin: "P_E_T_K_N_R_L".split("_"),
            longDateFormat: {
                LT: "H:mm",
                L: "DD.MM.YYYY",
                LL: "D. MMMM YYYY",
                LLL: "D. MMMM YYYY LT",
                LLLL: "dddd, D. MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[Täna,] LT",
                nextDay: "[Homme,] LT",
                nextWeek: "[Järgmine] dddd LT",
                lastDay: "[Eile,] LT",
                lastWeek: "[Eelmine] dddd LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "%s pärast",
                past: "%s tagasi",
                s: b,
                m: b,
                mm: b,
                h: b,
                hh: b,
                d: b,
                dd: "%d päeva",
                M: b,
                MM: b,
                y: b,
                yy: b
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("eu", {
            months: "urtarrila_otsaila_martxoa_apirila_maiatza_ekaina_uztaila_abuztua_iraila_urria_azaroa_abendua".split("_"),
            monthsShort: "urt._ots._mar._api._mai._eka._uzt._abu._ira._urr._aza._abe.".split("_"),
            weekdays: "igandea_astelehena_asteartea_asteazkena_osteguna_ostirala_larunbata".split("_"),
            weekdaysShort: "ig._al._ar._az._og._ol._lr.".split("_"),
            weekdaysMin: "ig_al_ar_az_og_ol_lr".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "YYYY-MM-DD",
                LL: "YYYY[ko] MMMM[ren] D[a]",
                LLL: "YYYY[ko] MMMM[ren] D[a] LT",
                LLLL: "dddd, YYYY[ko] MMMM[ren] D[a] LT",
                l: "YYYY-M-D",
                ll: "YYYY[ko] MMM D[a]",
                lll: "YYYY[ko] MMM D[a] LT",
                llll: "ddd, YYYY[ko] MMM D[a] LT"
            },
            calendar: {
                sameDay: "[gaur] LT[etan]",
                nextDay: "[bihar] LT[etan]",
                nextWeek: "dddd LT[etan]",
                lastDay: "[atzo] LT[etan]",
                lastWeek: "[aurreko] dddd LT[etan]",
                sameElse: "L"
            },
            relativeTime: {
                future: "%s barru",
                past: "duela %s",
                s: "segundo batzuk",
                m: "minutu bat",
                mm: "%d minutu",
                h: "ordu bat",
                hh: "%d ordu",
                d: "egun bat",
                dd: "%d egun",
                M: "hilabete bat",
                MM: "%d hilabete",
                y: "urte bat",
                yy: "%d urte"
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        var b = {
            1: "۱",
            2: "۲",
            3: "۳",
            4: "۴",
            5: "۵",
            6: "۶",
            7: "۷",
            8: "۸",
            9: "۹",
            0: "۰"
        }, c = {
            "۱": "1",
            "۲": "2",
            "۳": "3",
            "۴": "4",
            "۵": "5",
            "۶": "6",
            "۷": "7",
            "۸": "8",
            "۹": "9",
            "۰": "0"
        };
        return a.lang("fa", {
            months: "ژانویه_فوریه_مارس_آوریل_مه_ژوئن_ژوئیه_اوت_سپتامبر_اکتبر_نوامبر_دسامبر".split("_"),
            monthsShort: "ژانویه_فوریه_مارس_آوریل_مه_ژوئن_ژوئیه_اوت_سپتامبر_اکتبر_نوامبر_دسامبر".split("_"),
            weekdays: "یک‌شنبه_دوشنبه_سه‌شنبه_چهارشنبه_پنج‌شنبه_جمعه_شنبه".split("_"),
            weekdaysShort: "یک‌شنبه_دوشنبه_سه‌شنبه_چهارشنبه_پنج‌شنبه_جمعه_شنبه".split("_"),
            weekdaysMin: "ی_د_س_چ_پ_ج_ش".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd, D MMMM YYYY LT"
            },
            meridiem: function(a) {
                return 12 > a ? "قبل از ظهر" : "بعد از ظهر";
            },
            calendar: {
                sameDay: "[امروز ساعت] LT",
                nextDay: "[فردا ساعت] LT",
                nextWeek: "dddd [ساعت] LT",
                lastDay: "[دیروز ساعت] LT",
                lastWeek: "dddd [پیش] [ساعت] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "در %s",
                past: "%s پیش",
                s: "چندین ثانیه",
                m: "یک دقیقه",
                mm: "%d دقیقه",
                h: "یک ساعت",
                hh: "%d ساعت",
                d: "یک روز",
                dd: "%d روز",
                M: "یک ماه",
                MM: "%d ماه",
                y: "یک سال",
                yy: "%d سال"
            },
            preparse: function(a) {
                return a.replace(/[۰-۹]/g, function(a) {
                    return c[a];
                }).replace(/،/g, ",");
            },
            postformat: function(a) {
                return a.replace(/\d/g, function(a) {
                    return b[a];
                }).replace(/,/g, "،");
            },
            ordinal: "%dم",
            week: {
                dow: 6,
                doy: 12
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        function b(a, b, d, e) {
            var f = "";
            switch (d) {
              case "s":
                return e ? "muutaman sekunnin" : "muutama sekunti";

              case "m":
                return e ? "minuutin" : "minuutti";

              case "mm":
                f = e ? "minuutin" : "minuuttia";
                break;

              case "h":
                return e ? "tunnin" : "tunti";

              case "hh":
                f = e ? "tunnin" : "tuntia";
                break;

              case "d":
                return e ? "päivän" : "päivä";

              case "dd":
                f = e ? "päivän" : "päivää";
                break;

              case "M":
                return e ? "kuukauden" : "kuukausi";

              case "MM":
                f = e ? "kuukauden" : "kuukautta";
                break;

              case "y":
                return e ? "vuoden" : "vuosi";

              case "yy":
                f = e ? "vuoden" : "vuotta";
            }
            return f = c(a, e) + " " + f;
        }
        function c(a, b) {
            return 10 > a ? b ? e[a] : d[a] : a;
        }
        var d = "nolla yksi kaksi kolme neljä viisi kuusi seitsemän kahdeksan yhdeksän".split(" "), e = [ "nolla", "yhden", "kahden", "kolmen", "neljän", "viiden", "kuuden", d[7], d[8], d[9] ];
        return a.lang("fi", {
            months: "tammikuu_helmikuu_maaliskuu_huhtikuu_toukokuu_kesäkuu_heinäkuu_elokuu_syyskuu_lokakuu_marraskuu_joulukuu".split("_"),
            monthsShort: "tammi_helmi_maalis_huhti_touko_kesä_heinä_elo_syys_loka_marras_joulu".split("_"),
            weekdays: "sunnuntai_maanantai_tiistai_keskiviikko_torstai_perjantai_lauantai".split("_"),
            weekdaysShort: "su_ma_ti_ke_to_pe_la".split("_"),
            weekdaysMin: "su_ma_ti_ke_to_pe_la".split("_"),
            longDateFormat: {
                LT: "HH.mm",
                L: "DD.MM.YYYY",
                LL: "Do MMMM[ta] YYYY",
                LLL: "Do MMMM[ta] YYYY, [klo] LT",
                LLLL: "dddd, Do MMMM[ta] YYYY, [klo] LT",
                l: "D.M.YYYY",
                ll: "Do MMM YYYY",
                lll: "Do MMM YYYY, [klo] LT",
                llll: "ddd, Do MMM YYYY, [klo] LT"
            },
            calendar: {
                sameDay: "[tänään] [klo] LT",
                nextDay: "[huomenna] [klo] LT",
                nextWeek: "dddd [klo] LT",
                lastDay: "[eilen] [klo] LT",
                lastWeek: "[viime] dddd[na] [klo] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "%s päästä",
                past: "%s sitten",
                s: b,
                m: b,
                mm: b,
                h: b,
                hh: b,
                d: b,
                dd: b,
                M: b,
                MM: b,
                y: b,
                yy: b
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("fo", {
            months: "januar_februar_mars_apríl_mai_juni_juli_august_september_oktober_november_desember".split("_"),
            monthsShort: "jan_feb_mar_apr_mai_jun_jul_aug_sep_okt_nov_des".split("_"),
            weekdays: "sunnudagur_mánadagur_týsdagur_mikudagur_hósdagur_fríggjadagur_leygardagur".split("_"),
            weekdaysShort: "sun_mán_týs_mik_hós_frí_ley".split("_"),
            weekdaysMin: "su_má_tý_mi_hó_fr_le".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd D. MMMM, YYYY LT"
            },
            calendar: {
                sameDay: "[Í dag kl.] LT",
                nextDay: "[Í morgin kl.] LT",
                nextWeek: "dddd [kl.] LT",
                lastDay: "[Í gjár kl.] LT",
                lastWeek: "[síðstu] dddd [kl] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "um %s",
                past: "%s síðani",
                s: "fá sekund",
                m: "ein minutt",
                mm: "%d minuttir",
                h: "ein tími",
                hh: "%d tímar",
                d: "ein dagur",
                dd: "%d dagar",
                M: "ein mánaði",
                MM: "%d mánaðir",
                y: "eitt ár",
                yy: "%d ár"
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("fr-ca", {
            months: "janvier_février_mars_avril_mai_juin_juillet_août_septembre_octobre_novembre_décembre".split("_"),
            monthsShort: "janv._févr._mars_avr._mai_juin_juil._août_sept._oct._nov._déc.".split("_"),
            weekdays: "dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi".split("_"),
            weekdaysShort: "dim._lun._mar._mer._jeu._ven._sam.".split("_"),
            weekdaysMin: "Di_Lu_Ma_Me_Je_Ve_Sa".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "YYYY-MM-DD",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd D MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[Aujourd'hui à] LT",
                nextDay: "[Demain à] LT",
                nextWeek: "dddd [à] LT",
                lastDay: "[Hier à] LT",
                lastWeek: "dddd [dernier à] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "dans %s",
                past: "il y a %s",
                s: "quelques secondes",
                m: "une minute",
                mm: "%d minutes",
                h: "une heure",
                hh: "%d heures",
                d: "un jour",
                dd: "%d jours",
                M: "un mois",
                MM: "%d mois",
                y: "un an",
                yy: "%d ans"
            },
            ordinal: function(a) {
                return a + (1 === a ? "er" : "");
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("fr", {
            months: "janvier_février_mars_avril_mai_juin_juillet_août_septembre_octobre_novembre_décembre".split("_"),
            monthsShort: "janv._févr._mars_avr._mai_juin_juil._août_sept._oct._nov._déc.".split("_"),
            weekdays: "dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi".split("_"),
            weekdaysShort: "dim._lun._mar._mer._jeu._ven._sam.".split("_"),
            weekdaysMin: "Di_Lu_Ma_Me_Je_Ve_Sa".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd D MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[Aujourd'hui à] LT",
                nextDay: "[Demain à] LT",
                nextWeek: "dddd [à] LT",
                lastDay: "[Hier à] LT",
                lastWeek: "dddd [dernier à] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "dans %s",
                past: "il y a %s",
                s: "quelques secondes",
                m: "une minute",
                mm: "%d minutes",
                h: "une heure",
                hh: "%d heures",
                d: "un jour",
                dd: "%d jours",
                M: "un mois",
                MM: "%d mois",
                y: "un an",
                yy: "%d ans"
            },
            ordinal: function(a) {
                return a + (1 === a ? "er" : "");
            },
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("gl", {
            months: "Xaneiro_Febreiro_Marzo_Abril_Maio_Xuño_Xullo_Agosto_Setembro_Outubro_Novembro_Decembro".split("_"),
            monthsShort: "Xan._Feb._Mar._Abr._Mai._Xuñ._Xul._Ago._Set._Out._Nov._Dec.".split("_"),
            weekdays: "Domingo_Luns_Martes_Mércores_Xoves_Venres_Sábado".split("_"),
            weekdaysShort: "Dom._Lun._Mar._Mér._Xov._Ven._Sáb.".split("_"),
            weekdaysMin: "Do_Lu_Ma_Mé_Xo_Ve_Sá".split("_"),
            longDateFormat: {
                LT: "H:mm",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd D MMMM YYYY LT"
            },
            calendar: {
                sameDay: function() {
                    return "[hoxe " + (1 !== this.hours() ? "ás" : "á") + "] LT";
                },
                nextDay: function() {
                    return "[mañá " + (1 !== this.hours() ? "ás" : "á") + "] LT";
                },
                nextWeek: function() {
                    return "dddd [" + (1 !== this.hours() ? "ás" : "a") + "] LT";
                },
                lastDay: function() {
                    return "[onte " + (1 !== this.hours() ? "á" : "a") + "] LT";
                },
                lastWeek: function() {
                    return "[o] dddd [pasado " + (1 !== this.hours() ? "ás" : "a") + "] LT";
                },
                sameElse: "L"
            },
            relativeTime: {
                future: function(a) {
                    return "uns segundos" === a ? "nuns segundos" : "en " + a;
                },
                past: "hai %s",
                s: "uns segundos",
                m: "un minuto",
                mm: "%d minutos",
                h: "unha hora",
                hh: "%d horas",
                d: "un día",
                dd: "%d días",
                M: "un mes",
                MM: "%d meses",
                y: "un ano",
                yy: "%d anos"
            },
            ordinal: "%dº",
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("he", {
            months: "ינואר_פברואר_מרץ_אפריל_מאי_יוני_יולי_אוגוסט_ספטמבר_אוקטובר_נובמבר_דצמבר".split("_"),
            monthsShort: "ינו׳_פבר׳_מרץ_אפר׳_מאי_יוני_יולי_אוג׳_ספט׳_אוק׳_נוב׳_דצמ׳".split("_"),
            weekdays: "ראשון_שני_שלישי_רביעי_חמישי_שישי_שבת".split("_"),
            weekdaysShort: "א׳_ב׳_ג׳_ד׳_ה׳_ו׳_ש׳".split("_"),
            weekdaysMin: "א_ב_ג_ד_ה_ו_ש".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD/MM/YYYY",
                LL: "D [ב]MMMM YYYY",
                LLL: "D [ב]MMMM YYYY LT",
                LLLL: "dddd, D [ב]MMMM YYYY LT",
                l: "D/M/YYYY",
                ll: "D MMM YYYY",
                lll: "D MMM YYYY LT",
                llll: "ddd, D MMM YYYY LT"
            },
            calendar: {
                sameDay: "[היום ב־]LT",
                nextDay: "[מחר ב־]LT",
                nextWeek: "dddd [בשעה] LT",
                lastDay: "[אתמול ב־]LT",
                lastWeek: "[ביום] dddd [האחרון בשעה] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "בעוד %s",
                past: "לפני %s",
                s: "מספר שניות",
                m: "דקה",
                mm: "%d דקות",
                h: "שעה",
                hh: function(a) {
                    return 2 === a ? "שעתיים" : a + " שעות";
                },
                d: "יום",
                dd: function(a) {
                    return 2 === a ? "יומיים" : a + " ימים";
                },
                M: "חודש",
                MM: function(a) {
                    return 2 === a ? "חודשיים" : a + " חודשים";
                },
                y: "שנה",
                yy: function(a) {
                    return 2 === a ? "שנתיים" : a + " שנים";
                }
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        var b = {
            1: "१",
            2: "२",
            3: "३",
            4: "४",
            5: "५",
            6: "६",
            7: "७",
            8: "८",
            9: "९",
            0: "०"
        }, c = {
            "१": "1",
            "२": "2",
            "३": "3",
            "४": "4",
            "५": "5",
            "६": "6",
            "७": "7",
            "८": "8",
            "९": "9",
            "०": "0"
        };
        return a.lang("hi", {
            months: "जनवरी_फ़रवरी_मार्च_अप्रैल_मई_जून_जुलाई_अगस्त_सितम्बर_अक्टूबर_नवम्बर_दिसम्बर".split("_"),
            monthsShort: "जन._फ़र._मार्च_अप्रै._मई_जून_जुल._अग._सित._अक्टू._नव._दिस.".split("_"),
            weekdays: "रविवार_सोमवार_मंगलवार_बुधवार_गुरूवार_शुक्रवार_शनिवार".split("_"),
            weekdaysShort: "रवि_सोम_मंगल_बुध_गुरू_शुक्र_शनि".split("_"),
            weekdaysMin: "र_सो_मं_बु_गु_शु_श".split("_"),
            longDateFormat: {
                LT: "A h:mm बजे",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY, LT",
                LLLL: "dddd, D MMMM YYYY, LT"
            },
            calendar: {
                sameDay: "[आज] LT",
                nextDay: "[कल] LT",
                nextWeek: "dddd, LT",
                lastDay: "[कल] LT",
                lastWeek: "[पिछले] dddd, LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "%s में",
                past: "%s पहले",
                s: "कुछ ही क्षण",
                m: "एक मिनट",
                mm: "%d मिनट",
                h: "एक घंटा",
                hh: "%d घंटे",
                d: "एक दिन",
                dd: "%d दिन",
                M: "एक महीने",
                MM: "%d महीने",
                y: "एक वर्ष",
                yy: "%d वर्ष"
            },
            preparse: function(a) {
                return a.replace(/[१२३४५६७८९०]/g, function(a) {
                    return c[a];
                });
            },
            postformat: function(a) {
                return a.replace(/\d/g, function(a) {
                    return b[a];
                });
            },
            meridiem: function(a) {
                return 4 > a ? "रात" : 10 > a ? "सुबह" : 17 > a ? "दोपहर" : 20 > a ? "शाम" : "रात";
            },
            week: {
                dow: 0,
                doy: 6
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        function b(a, b, c) {
            var d = a + " ";
            switch (c) {
              case "m":
                return b ? "jedna minuta" : "jedne minute";

              case "mm":
                return d += 1 === a ? "minuta" : 2 === a || 3 === a || 4 === a ? "minute" : "minuta";

              case "h":
                return b ? "jedan sat" : "jednog sata";

              case "hh":
                return d += 1 === a ? "sat" : 2 === a || 3 === a || 4 === a ? "sata" : "sati";

              case "dd":
                return d += 1 === a ? "dan" : "dana";

              case "MM":
                return d += 1 === a ? "mjesec" : 2 === a || 3 === a || 4 === a ? "mjeseca" : "mjeseci";

              case "yy":
                return d += 1 === a ? "godina" : 2 === a || 3 === a || 4 === a ? "godine" : "godina";
            }
        }
        return a.lang("hr", {
            months: "sječanj_veljača_ožujak_travanj_svibanj_lipanj_srpanj_kolovoz_rujan_listopad_studeni_prosinac".split("_"),
            monthsShort: "sje._vel._ožu._tra._svi._lip._srp._kol._ruj._lis._stu._pro.".split("_"),
            weekdays: "nedjelja_ponedjeljak_utorak_srijeda_četvrtak_petak_subota".split("_"),
            weekdaysShort: "ned._pon._uto._sri._čet._pet._sub.".split("_"),
            weekdaysMin: "ne_po_ut_sr_če_pe_su".split("_"),
            longDateFormat: {
                LT: "H:mm",
                L: "DD. MM. YYYY",
                LL: "D. MMMM YYYY",
                LLL: "D. MMMM YYYY LT",
                LLLL: "dddd, D. MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[danas u] LT",
                nextDay: "[sutra u] LT",
                nextWeek: function() {
                    switch (this.day()) {
                      case 0:
                        return "[u] [nedjelju] [u] LT";

                      case 3:
                        return "[u] [srijedu] [u] LT";

                      case 6:
                        return "[u] [subotu] [u] LT";

                      case 1:
                      case 2:
                      case 4:
                      case 5:
                        return "[u] dddd [u] LT";
                    }
                },
                lastDay: "[jučer u] LT",
                lastWeek: function() {
                    switch (this.day()) {
                      case 0:
                      case 3:
                        return "[prošlu] dddd [u] LT";

                      case 6:
                        return "[prošle] [subote] [u] LT";

                      case 1:
                      case 2:
                      case 4:
                      case 5:
                        return "[prošli] dddd [u] LT";
                    }
                },
                sameElse: "L"
            },
            relativeTime: {
                future: "za %s",
                past: "prije %s",
                s: "par sekundi",
                m: b,
                mm: b,
                h: b,
                hh: b,
                d: "dan",
                dd: b,
                M: "mjesec",
                MM: b,
                y: "godinu",
                yy: b
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        function b(a, b, c, d) {
            var e = a;
            switch (c) {
              case "s":
                return d || b ? "néhány másodperc" : "néhány másodperce";

              case "m":
                return "egy" + (d || b ? " perc" : " perce");

              case "mm":
                return e + (d || b ? " perc" : " perce");

              case "h":
                return "egy" + (d || b ? " óra" : " órája");

              case "hh":
                return e + (d || b ? " óra" : " órája");

              case "d":
                return "egy" + (d || b ? " nap" : " napja");

              case "dd":
                return e + (d || b ? " nap" : " napja");

              case "M":
                return "egy" + (d || b ? " hónap" : " hónapja");

              case "MM":
                return e + (d || b ? " hónap" : " hónapja");

              case "y":
                return "egy" + (d || b ? " év" : " éve");

              case "yy":
                return e + (d || b ? " év" : " éve");
            }
            return "";
        }
        function c(a) {
            return (a ? "" : "[múlt] ") + "[" + d[this.day()] + "] LT[-kor]";
        }
        var d = "vasárnap hétfőn kedden szerdán csütörtökön pénteken szombaton".split(" ");
        return a.lang("hu", {
            months: "január_február_március_április_május_június_július_augusztus_szeptember_október_november_december".split("_"),
            monthsShort: "jan_feb_márc_ápr_máj_jún_júl_aug_szept_okt_nov_dec".split("_"),
            weekdays: "vasárnap_hétfő_kedd_szerda_csütörtök_péntek_szombat".split("_"),
            weekdaysShort: "vas_hét_kedd_sze_csüt_pén_szo".split("_"),
            weekdaysMin: "v_h_k_sze_cs_p_szo".split("_"),
            longDateFormat: {
                LT: "H:mm",
                L: "YYYY.MM.DD.",
                LL: "YYYY. MMMM D.",
                LLL: "YYYY. MMMM D., LT",
                LLLL: "YYYY. MMMM D., dddd LT"
            },
            meridiem: function(a, b, c) {
                return 12 > a ? c === !0 ? "de" : "DE" : c === !0 ? "du" : "DU";
            },
            calendar: {
                sameDay: "[ma] LT[-kor]",
                nextDay: "[holnap] LT[-kor]",
                nextWeek: function() {
                    return c.call(this, !0);
                },
                lastDay: "[tegnap] LT[-kor]",
                lastWeek: function() {
                    return c.call(this, !1);
                },
                sameElse: "L"
            },
            relativeTime: {
                future: "%s múlva",
                past: "%s",
                s: b,
                m: b,
                mm: b,
                h: b,
                hh: b,
                d: b,
                dd: b,
                M: b,
                MM: b,
                y: b,
                yy: b
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        function b(a, b) {
            var c = {
                nominative: "հունվար_փետրվար_մարտ_ապրիլ_մայիս_հունիս_հուլիս_օգոստոս_սեպտեմբեր_հոկտեմբեր_նոյեմբեր_դեկտեմբեր".split("_"),
                accusative: "հունվարի_փետրվարի_մարտի_ապրիլի_մայիսի_հունիսի_հուլիսի_օգոստոսի_սեպտեմբերի_հոկտեմբերի_նոյեմբերի_դեկտեմբերի".split("_")
            }, d = /D[oD]?(\[[^\[\]]*\]|\s+)+MMMM?/.test(b) ? "accusative" : "nominative";
            return c[d][a.month()];
        }
        function c(a) {
            var b = "հնվ_փտր_մրտ_ապր_մյս_հնս_հլս_օգս_սպտ_հկտ_նմբ_դկտ".split("_");
            return b[a.month()];
        }
        function d(a) {
            var b = "կիրակի_երկուշաբթի_երեքշաբթի_չորեքշաբթի_հինգշաբթի_ուրբաթ_շաբաթ".split("_");
            return b[a.day()];
        }
        return a.lang("hy-am", {
            months: b,
            monthsShort: c,
            weekdays: d,
            weekdaysShort: "կրկ_երկ_երք_չրք_հնգ_ուրբ_շբթ".split("_"),
            weekdaysMin: "կրկ_երկ_երք_չրք_հնգ_ուրբ_շբթ".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD.MM.YYYY",
                LL: "D MMMM YYYY թ.",
                LLL: "D MMMM YYYY թ., LT",
                LLLL: "dddd, D MMMM YYYY թ., LT"
            },
            calendar: {
                sameDay: "[այսօր] LT",
                nextDay: "[վաղը] LT",
                lastDay: "[երեկ] LT",
                nextWeek: function() {
                    return "dddd [օրը ժամը] LT";
                },
                lastWeek: function() {
                    return "[անցած] dddd [օրը ժամը] LT";
                },
                sameElse: "L"
            },
            relativeTime: {
                future: "%s հետո",
                past: "%s առաջ",
                s: "մի քանի վայրկյան",
                m: "րոպե",
                mm: "%d րոպե",
                h: "ժամ",
                hh: "%d ժամ",
                d: "օր",
                dd: "%d օր",
                M: "ամիս",
                MM: "%d ամիս",
                y: "տարի",
                yy: "%d տարի"
            },
            meridiem: function(a) {
                return 4 > a ? "գիշերվա" : 12 > a ? "առավոտվա" : 17 > a ? "ցերեկվա" : "երեկոյան";
            },
            ordinal: function(a, b) {
                switch (b) {
                  case "DDD":
                  case "w":
                  case "W":
                  case "DDDo":
                    return 1 === a ? a + "-ին" : a + "-րդ";

                  default:
                    return a;
                }
            },
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("id", {
            months: "Januari_Februari_Maret_April_Mei_Juni_Juli_Agustus_September_Oktober_November_Desember".split("_"),
            monthsShort: "Jan_Feb_Mar_Apr_Mei_Jun_Jul_Ags_Sep_Okt_Nov_Des".split("_"),
            weekdays: "Minggu_Senin_Selasa_Rabu_Kamis_Jumat_Sabtu".split("_"),
            weekdaysShort: "Min_Sen_Sel_Rab_Kam_Jum_Sab".split("_"),
            weekdaysMin: "Mg_Sn_Sl_Rb_Km_Jm_Sb".split("_"),
            longDateFormat: {
                LT: "HH.mm",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY [pukul] LT",
                LLLL: "dddd, D MMMM YYYY [pukul] LT"
            },
            meridiem: function(a) {
                return 11 > a ? "pagi" : 15 > a ? "siang" : 19 > a ? "sore" : "malam";
            },
            calendar: {
                sameDay: "[Hari ini pukul] LT",
                nextDay: "[Besok pukul] LT",
                nextWeek: "dddd [pukul] LT",
                lastDay: "[Kemarin pukul] LT",
                lastWeek: "dddd [lalu pukul] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "dalam %s",
                past: "%s yang lalu",
                s: "beberapa detik",
                m: "semenit",
                mm: "%d menit",
                h: "sejam",
                hh: "%d jam",
                d: "sehari",
                dd: "%d hari",
                M: "sebulan",
                MM: "%d bulan",
                y: "setahun",
                yy: "%d tahun"
            },
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        function b(a) {
            return a % 100 === 11 ? !0 : a % 10 === 1 ? !1 : !0;
        }
        function c(a, c, d, e) {
            var f = a + " ";
            switch (d) {
              case "s":
                return c || e ? "nokkrar sekúndur" : "nokkrum sekúndum";

              case "m":
                return c ? "mínúta" : "mínútu";

              case "mm":
                return b(a) ? f + (c || e ? "mínútur" : "mínútum") : c ? f + "mínúta" : f + "mínútu";

              case "hh":
                return b(a) ? f + (c || e ? "klukkustundir" : "klukkustundum") : f + "klukkustund";

              case "d":
                return c ? "dagur" : e ? "dag" : "degi";

              case "dd":
                return b(a) ? c ? f + "dagar" : f + (e ? "daga" : "dögum") : c ? f + "dagur" : f + (e ? "dag" : "degi");

              case "M":
                return c ? "mánuður" : e ? "mánuð" : "mánuði";

              case "MM":
                return b(a) ? c ? f + "mánuðir" : f + (e ? "mánuði" : "mánuðum") : c ? f + "mánuður" : f + (e ? "mánuð" : "mánuði");

              case "y":
                return c || e ? "ár" : "ári";

              case "yy":
                return b(a) ? f + (c || e ? "ár" : "árum") : f + (c || e ? "ár" : "ári");
            }
        }
        return a.lang("is", {
            months: "janúar_febrúar_mars_apríl_maí_júní_júlí_ágúst_september_október_nóvember_desember".split("_"),
            monthsShort: "jan_feb_mar_apr_maí_jún_júl_ágú_sep_okt_nóv_des".split("_"),
            weekdays: "sunnudagur_mánudagur_þriðjudagur_miðvikudagur_fimmtudagur_föstudagur_laugardagur".split("_"),
            weekdaysShort: "sun_mán_þri_mið_fim_fös_lau".split("_"),
            weekdaysMin: "Su_Má_Þr_Mi_Fi_Fö_La".split("_"),
            longDateFormat: {
                LT: "H:mm",
                L: "DD/MM/YYYY",
                LL: "D. MMMM YYYY",
                LLL: "D. MMMM YYYY [kl.] LT",
                LLLL: "dddd, D. MMMM YYYY [kl.] LT"
            },
            calendar: {
                sameDay: "[í dag kl.] LT",
                nextDay: "[á morgun kl.] LT",
                nextWeek: "dddd [kl.] LT",
                lastDay: "[í gær kl.] LT",
                lastWeek: "[síðasta] dddd [kl.] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "eftir %s",
                past: "fyrir %s síðan",
                s: c,
                m: c,
                mm: c,
                h: "klukkustund",
                hh: c,
                d: c,
                dd: c,
                M: c,
                MM: c,
                y: c,
                yy: c
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("it", {
            months: "Gennaio_Febbraio_Marzo_Aprile_Maggio_Giugno_Luglio_Agosto_Settembre_Ottobre_Novembre_Dicembre".split("_"),
            monthsShort: "Gen_Feb_Mar_Apr_Mag_Giu_Lug_Ago_Set_Ott_Nov_Dic".split("_"),
            weekdays: "Domenica_Lunedì_Martedì_Mercoledì_Giovedì_Venerdì_Sabato".split("_"),
            weekdaysShort: "Dom_Lun_Mar_Mer_Gio_Ven_Sab".split("_"),
            weekdaysMin: "D_L_Ma_Me_G_V_S".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd, D MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[Oggi alle] LT",
                nextDay: "[Domani alle] LT",
                nextWeek: "dddd [alle] LT",
                lastDay: "[Ieri alle] LT",
                lastWeek: "[lo scorso] dddd [alle] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: function(a) {
                    return (/^[0-9].+$/.test(a) ? "tra" : "in") + " " + a;
                },
                past: "%s fa",
                s: "alcuni secondi",
                m: "un minuto",
                mm: "%d minuti",
                h: "un'ora",
                hh: "%d ore",
                d: "un giorno",
                dd: "%d giorni",
                M: "un mese",
                MM: "%d mesi",
                y: "un anno",
                yy: "%d anni"
            },
            ordinal: "%dº",
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("ja", {
            months: "1月_2月_3月_4月_5月_6月_7月_8月_9月_10月_11月_12月".split("_"),
            monthsShort: "1月_2月_3月_4月_5月_6月_7月_8月_9月_10月_11月_12月".split("_"),
            weekdays: "日曜日_月曜日_火曜日_水曜日_木曜日_金曜日_土曜日".split("_"),
            weekdaysShort: "日_月_火_水_木_金_土".split("_"),
            weekdaysMin: "日_月_火_水_木_金_土".split("_"),
            longDateFormat: {
                LT: "Ah時m分",
                L: "YYYY/MM/DD",
                LL: "YYYY年M月D日",
                LLL: "YYYY年M月D日LT",
                LLLL: "YYYY年M月D日LT dddd"
            },
            meridiem: function(a) {
                return 12 > a ? "午前" : "午後";
            },
            calendar: {
                sameDay: "[今日] LT",
                nextDay: "[明日] LT",
                nextWeek: "[来週]dddd LT",
                lastDay: "[昨日] LT",
                lastWeek: "[前週]dddd LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "%s後",
                past: "%s前",
                s: "数秒",
                m: "1分",
                mm: "%d分",
                h: "1時間",
                hh: "%d時間",
                d: "1日",
                dd: "%d日",
                M: "1ヶ月",
                MM: "%dヶ月",
                y: "1年",
                yy: "%d年"
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        function b(a, b) {
            var c = {
                nominative: "იანვარი_თებერვალი_მარტი_აპრილი_მაისი_ივნისი_ივლისი_აგვისტო_სექტემბერი_ოქტომბერი_ნოემბერი_დეკემბერი".split("_"),
                accusative: "იანვარს_თებერვალს_მარტს_აპრილის_მაისს_ივნისს_ივლისს_აგვისტს_სექტემბერს_ოქტომბერს_ნოემბერს_დეკემბერს".split("_")
            }, d = /D[oD] *MMMM?/.test(b) ? "accusative" : "nominative";
            return c[d][a.month()];
        }
        function c(a, b) {
            var c = {
                nominative: "კვირა_ორშაბათი_სამშაბათი_ოთხშაბათი_ხუთშაბათი_პარასკევი_შაბათი".split("_"),
                accusative: "კვირას_ორშაბათს_სამშაბათს_ოთხშაბათს_ხუთშაბათს_პარასკევს_შაბათს".split("_")
            }, d = /(წინა|შემდეგ)/.test(b) ? "accusative" : "nominative";
            return c[d][a.day()];
        }
        return a.lang("ka", {
            months: b,
            monthsShort: "იან_თებ_მარ_აპრ_მაი_ივნ_ივლ_აგვ_სექ_ოქტ_ნოე_დეკ".split("_"),
            weekdays: c,
            weekdaysShort: "კვი_ორშ_სამ_ოთხ_ხუთ_პარ_შაბ".split("_"),
            weekdaysMin: "კვ_ორ_სა_ოთ_ხუ_პა_შა".split("_"),
            longDateFormat: {
                LT: "h:mm A",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd, D MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[დღეს] LT[-ზე]",
                nextDay: "[ხვალ] LT[-ზე]",
                lastDay: "[გუშინ] LT[-ზე]",
                nextWeek: "[შემდეგ] dddd LT[-ზე]",
                lastWeek: "[წინა] dddd LT-ზე",
                sameElse: "L"
            },
            relativeTime: {
                future: function(a) {
                    return /(წამი|წუთი|საათი|წელი)/.test(a) ? a.replace(/ი$/, "ში") : a + "ში";
                },
                past: function(a) {
                    return /(წამი|წუთი|საათი|დღე|თვე)/.test(a) ? a.replace(/(ი|ე)$/, "ის წინ") : /წელი/.test(a) ? a.replace(/წელი$/, "წლის წინ") : void 0;
                },
                s: "რამდენიმე წამი",
                m: "წუთი",
                mm: "%d წუთი",
                h: "საათი",
                hh: "%d საათი",
                d: "დღე",
                dd: "%d დღე",
                M: "თვე",
                MM: "%d თვე",
                y: "წელი",
                yy: "%d წელი"
            },
            ordinal: function(a) {
                return 0 === a ? a : 1 === a ? a + "-ლი" : 20 > a || 100 >= a && a % 20 === 0 || a % 100 === 0 ? "მე-" + a : a + "-ე";
            },
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("km", {
            months: "មករា_កុម្ភៈ_មិនា_មេសា_ឧសភា_មិថុនា_កក្កដា_សីហា_កញ្ញា_តុលា_វិច្ឆិកា_ធ្នូ".split("_"),
            monthsShort: "មករា_កុម្ភៈ_មិនា_មេសា_ឧសភា_មិថុនា_កក្កដា_សីហា_កញ្ញា_តុលា_វិច្ឆិកា_ធ្នូ".split("_"),
            weekdays: "អាទិត្យ_ច័ន្ទ_អង្គារ_ពុធ_ព្រហស្បតិ៍_សុក្រ_សៅរ៍".split("_"),
            weekdaysShort: "អាទិត្យ_ច័ន្ទ_អង្គារ_ពុធ_ព្រហស្បតិ៍_សុក្រ_សៅរ៍".split("_"),
            weekdaysMin: "អាទិត្យ_ច័ន្ទ_អង្គារ_ពុធ_ព្រហស្បតិ៍_សុក្រ_សៅរ៍".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd, D MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[ថ្ងៃនៈ ម៉ោង] LT",
                nextDay: "[ស្អែក ម៉ោង] LT",
                nextWeek: "dddd [ម៉ោង] LT",
                lastDay: "[ម្សិលមិញ ម៉ោង] LT",
                lastWeek: "dddd [សប្តាហ៍មុន] [ម៉ោង] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "%sទៀត",
                past: "%sមុន",
                s: "ប៉ុន្មានវិនាទី",
                m: "មួយនាទី",
                mm: "%d នាទី",
                h: "មួយម៉ោង",
                hh: "%d ម៉ោង",
                d: "មួយថ្ងៃ",
                dd: "%d ថ្ងៃ",
                M: "មួយខែ",
                MM: "%d ខែ",
                y: "មួយឆ្នាំ",
                yy: "%d ឆ្នាំ"
            },
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("ko", {
            months: "1월_2월_3월_4월_5월_6월_7월_8월_9월_10월_11월_12월".split("_"),
            monthsShort: "1월_2월_3월_4월_5월_6월_7월_8월_9월_10월_11월_12월".split("_"),
            weekdays: "일요일_월요일_화요일_수요일_목요일_금요일_토요일".split("_"),
            weekdaysShort: "일_월_화_수_목_금_토".split("_"),
            weekdaysMin: "일_월_화_수_목_금_토".split("_"),
            longDateFormat: {
                LT: "A h시 mm분",
                L: "YYYY.MM.DD",
                LL: "YYYY년 MMMM D일",
                LLL: "YYYY년 MMMM D일 LT",
                LLLL: "YYYY년 MMMM D일 dddd LT"
            },
            meridiem: function(a) {
                return 12 > a ? "오전" : "오후";
            },
            calendar: {
                sameDay: "오늘 LT",
                nextDay: "내일 LT",
                nextWeek: "dddd LT",
                lastDay: "어제 LT",
                lastWeek: "지난주 dddd LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "%s 후",
                past: "%s 전",
                s: "몇초",
                ss: "%d초",
                m: "일분",
                mm: "%d분",
                h: "한시간",
                hh: "%d시간",
                d: "하루",
                dd: "%d일",
                M: "한달",
                MM: "%d달",
                y: "일년",
                yy: "%d년"
            },
            ordinal: "%d일",
            meridiemParse: /(오전|오후)/,
            isPM: function(a) {
                return "오후" === a;
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        function b(a, b, c) {
            var d = {
                m: [ "eng Minutt", "enger Minutt" ],
                h: [ "eng Stonn", "enger Stonn" ],
                d: [ "een Dag", "engem Dag" ],
                dd: [ a + " Deeg", a + " Deeg" ],
                M: [ "ee Mount", "engem Mount" ],
                MM: [ a + " Méint", a + " Méint" ],
                y: [ "ee Joer", "engem Joer" ],
                yy: [ a + " Joer", a + " Joer" ]
            };
            return b ? d[c][0] : d[c][1];
        }
        function c(a) {
            var b = a.substr(0, a.indexOf(" "));
            return g(b) ? "a " + a : "an " + a;
        }
        function d(a) {
            var b = a.substr(0, a.indexOf(" "));
            return g(b) ? "viru " + a : "virun " + a;
        }
        function e() {
            var a = this.format("d");
            return f(a) ? "[Leschte] dddd [um] LT" : "[Leschten] dddd [um] LT";
        }
        function f(a) {
            switch (a = parseInt(a, 10)) {
              case 0:
              case 1:
              case 3:
              case 5:
              case 6:
                return !0;

              default:
                return !1;
            }
        }
        function g(a) {
            if (a = parseInt(a, 10), isNaN(a)) return !1;
            if (0 > a) return !0;
            if (10 > a) return a >= 4 && 7 >= a ? !0 : !1;
            if (100 > a) {
                var b = a % 10, c = a / 10;
                return g(0 === b ? c : b);
            }
            if (1e4 > a) {
                for (;a >= 10; ) a /= 10;
                return g(a);
            }
            return a /= 1e3, g(a);
        }
        return a.lang("lb", {
            months: "Januar_Februar_Mäerz_Abrëll_Mee_Juni_Juli_August_September_Oktober_November_Dezember".split("_"),
            monthsShort: "Jan._Febr._Mrz._Abr._Mee_Jun._Jul._Aug._Sept._Okt._Nov._Dez.".split("_"),
            weekdays: "Sonndeg_Méindeg_Dënschdeg_Mëttwoch_Donneschdeg_Freideg_Samschdeg".split("_"),
            weekdaysShort: "So._Mé._Dë._Më._Do._Fr._Sa.".split("_"),
            weekdaysMin: "So_Mé_Dë_Më_Do_Fr_Sa".split("_"),
            longDateFormat: {
                LT: "H:mm [Auer]",
                L: "DD.MM.YYYY",
                LL: "D. MMMM YYYY",
                LLL: "D. MMMM YYYY LT",
                LLLL: "dddd, D. MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[Haut um] LT",
                sameElse: "L",
                nextDay: "[Muer um] LT",
                nextWeek: "dddd [um] LT",
                lastDay: "[Gëschter um] LT",
                lastWeek: e
            },
            relativeTime: {
                future: c,
                past: d,
                s: "e puer Sekonnen",
                m: b,
                mm: "%d Minutten",
                h: b,
                hh: "%d Stonnen",
                d: b,
                dd: b,
                M: b,
                MM: b,
                y: b,
                yy: b
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        function b(a, b, c, d) {
            return b ? "kelios sekundės" : d ? "kelių sekundžių" : "kelias sekundes";
        }
        function c(a, b, c, d) {
            return b ? e(c)[0] : d ? e(c)[1] : e(c)[2];
        }
        function d(a) {
            return a % 10 === 0 || a > 10 && 20 > a;
        }
        function e(a) {
            return h[a].split("_");
        }
        function f(a, b, f, g) {
            var h = a + " ";
            return 1 === a ? h + c(a, b, f[0], g) : b ? h + (d(a) ? e(f)[1] : e(f)[0]) : g ? h + e(f)[1] : h + (d(a) ? e(f)[1] : e(f)[2]);
        }
        function g(a, b) {
            var c = -1 === b.indexOf("dddd HH:mm"), d = i[a.weekday()];
            return c ? d : d.substring(0, d.length - 2) + "į";
        }
        var h = {
            m: "minutė_minutės_minutę",
            mm: "minutės_minučių_minutes",
            h: "valanda_valandos_valandą",
            hh: "valandos_valandų_valandas",
            d: "diena_dienos_dieną",
            dd: "dienos_dienų_dienas",
            M: "mėnuo_mėnesio_mėnesį",
            MM: "mėnesiai_mėnesių_mėnesius",
            y: "metai_metų_metus",
            yy: "metai_metų_metus"
        }, i = "pirmadienis_antradienis_trečiadienis_ketvirtadienis_penktadienis_šeštadienis_sekmadienis".split("_");
        return a.lang("lt", {
            months: "sausio_vasario_kovo_balandžio_gegužės_biržėlio_liepos_rugpjūčio_rugsėjo_spalio_lapkričio_gruodžio".split("_"),
            monthsShort: "sau_vas_kov_bal_geg_bir_lie_rgp_rgs_spa_lap_grd".split("_"),
            weekdays: g,
            weekdaysShort: "Sek_Pir_Ant_Tre_Ket_Pen_Šeš".split("_"),
            weekdaysMin: "S_P_A_T_K_Pn_Š".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "YYYY-MM-DD",
                LL: "YYYY [m.] MMMM D [d.]",
                LLL: "YYYY [m.] MMMM D [d.], LT [val.]",
                LLLL: "YYYY [m.] MMMM D [d.], dddd, LT [val.]",
                l: "YYYY-MM-DD",
                ll: "YYYY [m.] MMMM D [d.]",
                lll: "YYYY [m.] MMMM D [d.], LT [val.]",
                llll: "YYYY [m.] MMMM D [d.], ddd, LT [val.]"
            },
            calendar: {
                sameDay: "[Šiandien] LT",
                nextDay: "[Rytoj] LT",
                nextWeek: "dddd LT",
                lastDay: "[Vakar] LT",
                lastWeek: "[Praėjusį] dddd LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "po %s",
                past: "prieš %s",
                s: b,
                m: c,
                mm: f,
                h: c,
                hh: f,
                d: c,
                dd: f,
                M: c,
                MM: f,
                y: c,
                yy: f
            },
            ordinal: function(a) {
                return a + "-oji";
            },
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        function b(a, b, c) {
            var d = a.split("_");
            return c ? b % 10 === 1 && 11 !== b ? d[2] : d[3] : b % 10 === 1 && 11 !== b ? d[0] : d[1];
        }
        function c(a, c, e) {
            return a + " " + b(d[e], a, c);
        }
        var d = {
            mm: "minūti_minūtes_minūte_minūtes",
            hh: "stundu_stundas_stunda_stundas",
            dd: "dienu_dienas_diena_dienas",
            MM: "mēnesi_mēnešus_mēnesis_mēneši",
            yy: "gadu_gadus_gads_gadi"
        };
        return a.lang("lv", {
            months: "janvāris_februāris_marts_aprīlis_maijs_jūnijs_jūlijs_augusts_septembris_oktobris_novembris_decembris".split("_"),
            monthsShort: "jan_feb_mar_apr_mai_jūn_jūl_aug_sep_okt_nov_dec".split("_"),
            weekdays: "svētdiena_pirmdiena_otrdiena_trešdiena_ceturtdiena_piektdiena_sestdiena".split("_"),
            weekdaysShort: "Sv_P_O_T_C_Pk_S".split("_"),
            weekdaysMin: "Sv_P_O_T_C_Pk_S".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD.MM.YYYY",
                LL: "YYYY. [gada] D. MMMM",
                LLL: "YYYY. [gada] D. MMMM, LT",
                LLLL: "YYYY. [gada] D. MMMM, dddd, LT"
            },
            calendar: {
                sameDay: "[Šodien pulksten] LT",
                nextDay: "[Rīt pulksten] LT",
                nextWeek: "dddd [pulksten] LT",
                lastDay: "[Vakar pulksten] LT",
                lastWeek: "[Pagājušā] dddd [pulksten] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "%s vēlāk",
                past: "%s agrāk",
                s: "dažas sekundes",
                m: "minūti",
                mm: c,
                h: "stundu",
                hh: c,
                d: "dienu",
                dd: c,
                M: "mēnesi",
                MM: c,
                y: "gadu",
                yy: c
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("mk", {
            months: "јануари_февруари_март_април_мај_јуни_јули_август_септември_октомври_ноември_декември".split("_"),
            monthsShort: "јан_фев_мар_апр_мај_јун_јул_авг_сеп_окт_ное_дек".split("_"),
            weekdays: "недела_понеделник_вторник_среда_четврток_петок_сабота".split("_"),
            weekdaysShort: "нед_пон_вто_сре_чет_пет_саб".split("_"),
            weekdaysMin: "нe_пo_вт_ср_че_пе_сa".split("_"),
            longDateFormat: {
                LT: "H:mm",
                L: "D.MM.YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd, D MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[Денес во] LT",
                nextDay: "[Утре во] LT",
                nextWeek: "dddd [во] LT",
                lastDay: "[Вчера во] LT",
                lastWeek: function() {
                    switch (this.day()) {
                      case 0:
                      case 3:
                      case 6:
                        return "[Во изминатата] dddd [во] LT";

                      case 1:
                      case 2:
                      case 4:
                      case 5:
                        return "[Во изминатиот] dddd [во] LT";
                    }
                },
                sameElse: "L"
            },
            relativeTime: {
                future: "после %s",
                past: "пред %s",
                s: "неколку секунди",
                m: "минута",
                mm: "%d минути",
                h: "час",
                hh: "%d часа",
                d: "ден",
                dd: "%d дена",
                M: "месец",
                MM: "%d месеци",
                y: "година",
                yy: "%d години"
            },
            ordinal: function(a) {
                var b = a % 10, c = a % 100;
                return 0 === a ? a + "-ев" : 0 === c ? a + "-ен" : c > 10 && 20 > c ? a + "-ти" : 1 === b ? a + "-ви" : 2 === b ? a + "-ри" : 7 === b || 8 === b ? a + "-ми" : a + "-ти";
            },
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("ml", {
            months: "ജനുവരി_ഫെബ്രുവരി_മാർച്ച്_ഏപ്രിൽ_മേയ്_ജൂൺ_ജൂലൈ_ഓഗസ്റ്റ്_സെപ്റ്റംബർ_ഒക്ടോബർ_നവംബർ_ഡിസംബർ".split("_"),
            monthsShort: "ജനു._ഫെബ്രു._മാർ._ഏപ്രി._മേയ്_ജൂൺ_ജൂലൈ._ഓഗ._സെപ്റ്റ._ഒക്ടോ._നവം._ഡിസം.".split("_"),
            weekdays: "ഞായറാഴ്ച_തിങ്കളാഴ്ച_ചൊവ്വാഴ്ച_ബുധനാഴ്ച_വ്യാഴാഴ്ച_വെള്ളിയാഴ്ച_ശനിയാഴ്ച".split("_"),
            weekdaysShort: "ഞായർ_തിങ്കൾ_ചൊവ്വ_ബുധൻ_വ്യാഴം_വെള്ളി_ശനി".split("_"),
            weekdaysMin: "ഞാ_തി_ചൊ_ബു_വ്യാ_വെ_ശ".split("_"),
            longDateFormat: {
                LT: "A h:mm -നു",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY, LT",
                LLLL: "dddd, D MMMM YYYY, LT"
            },
            calendar: {
                sameDay: "[ഇന്ന്] LT",
                nextDay: "[നാളെ] LT",
                nextWeek: "dddd, LT",
                lastDay: "[ഇന്നലെ] LT",
                lastWeek: "[കഴിഞ്ഞ] dddd, LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "%s കഴിഞ്ഞ്",
                past: "%s മുൻപ്",
                s: "അൽപ നിമിഷങ്ങൾ",
                m: "ഒരു മിനിറ്റ്",
                mm: "%d മിനിറ്റ്",
                h: "ഒരു മണിക്കൂർ",
                hh: "%d മണിക്കൂർ",
                d: "ഒരു ദിവസം",
                dd: "%d ദിവസം",
                M: "ഒരു മാസം",
                MM: "%d മാസം",
                y: "ഒരു വർഷം",
                yy: "%d വർഷം"
            },
            meridiem: function(a) {
                return 4 > a ? "രാത്രി" : 12 > a ? "രാവിലെ" : 17 > a ? "ഉച്ച കഴിഞ്ഞ്" : 20 > a ? "വൈകുന്നേരം" : "രാത്രി";
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        var b = {
            1: "१",
            2: "२",
            3: "३",
            4: "४",
            5: "५",
            6: "६",
            7: "७",
            8: "८",
            9: "९",
            0: "०"
        }, c = {
            "१": "1",
            "२": "2",
            "३": "3",
            "४": "4",
            "५": "5",
            "६": "6",
            "७": "7",
            "८": "8",
            "९": "9",
            "०": "0"
        };
        return a.lang("mr", {
            months: "जानेवारी_फेब्रुवारी_मार्च_एप्रिल_मे_जून_जुलै_ऑगस्ट_सप्टेंबर_ऑक्टोबर_नोव्हेंबर_डिसेंबर".split("_"),
            monthsShort: "जाने._फेब्रु._मार्च._एप्रि._मे._जून._जुलै._ऑग._सप्टें._ऑक्टो._नोव्हें._डिसें.".split("_"),
            weekdays: "रविवार_सोमवार_मंगळवार_बुधवार_गुरूवार_शुक्रवार_शनिवार".split("_"),
            weekdaysShort: "रवि_सोम_मंगळ_बुध_गुरू_शुक्र_शनि".split("_"),
            weekdaysMin: "र_सो_मं_बु_गु_शु_श".split("_"),
            longDateFormat: {
                LT: "A h:mm वाजता",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY, LT",
                LLLL: "dddd, D MMMM YYYY, LT"
            },
            calendar: {
                sameDay: "[आज] LT",
                nextDay: "[उद्या] LT",
                nextWeek: "dddd, LT",
                lastDay: "[काल] LT",
                lastWeek: "[मागील] dddd, LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "%s नंतर",
                past: "%s पूर्वी",
                s: "सेकंद",
                m: "एक मिनिट",
                mm: "%d मिनिटे",
                h: "एक तास",
                hh: "%d तास",
                d: "एक दिवस",
                dd: "%d दिवस",
                M: "एक महिना",
                MM: "%d महिने",
                y: "एक वर्ष",
                yy: "%d वर्षे"
            },
            preparse: function(a) {
                return a.replace(/[१२३४५६७८९०]/g, function(a) {
                    return c[a];
                });
            },
            postformat: function(a) {
                return a.replace(/\d/g, function(a) {
                    return b[a];
                });
            },
            meridiem: function(a) {
                return 4 > a ? "रात्री" : 10 > a ? "सकाळी" : 17 > a ? "दुपारी" : 20 > a ? "सायंकाळी" : "रात्री";
            },
            week: {
                dow: 0,
                doy: 6
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("ms-my", {
            months: "Januari_Februari_Mac_April_Mei_Jun_Julai_Ogos_September_Oktober_November_Disember".split("_"),
            monthsShort: "Jan_Feb_Mac_Apr_Mei_Jun_Jul_Ogs_Sep_Okt_Nov_Dis".split("_"),
            weekdays: "Ahad_Isnin_Selasa_Rabu_Khamis_Jumaat_Sabtu".split("_"),
            weekdaysShort: "Ahd_Isn_Sel_Rab_Kha_Jum_Sab".split("_"),
            weekdaysMin: "Ah_Is_Sl_Rb_Km_Jm_Sb".split("_"),
            longDateFormat: {
                LT: "HH.mm",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY [pukul] LT",
                LLLL: "dddd, D MMMM YYYY [pukul] LT"
            },
            meridiem: function(a) {
                return 11 > a ? "pagi" : 15 > a ? "tengahari" : 19 > a ? "petang" : "malam";
            },
            calendar: {
                sameDay: "[Hari ini pukul] LT",
                nextDay: "[Esok pukul] LT",
                nextWeek: "dddd [pukul] LT",
                lastDay: "[Kelmarin pukul] LT",
                lastWeek: "dddd [lepas pukul] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "dalam %s",
                past: "%s yang lepas",
                s: "beberapa saat",
                m: "seminit",
                mm: "%d minit",
                h: "sejam",
                hh: "%d jam",
                d: "sehari",
                dd: "%d hari",
                M: "sebulan",
                MM: "%d bulan",
                y: "setahun",
                yy: "%d tahun"
            },
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("nb", {
            months: "januar_februar_mars_april_mai_juni_juli_august_september_oktober_november_desember".split("_"),
            monthsShort: "jan._feb._mars_april_mai_juni_juli_aug._sep._okt._nov._des.".split("_"),
            weekdays: "søndag_mandag_tirsdag_onsdag_torsdag_fredag_lørdag".split("_"),
            weekdaysShort: "sø._ma._ti._on._to._fr._lø.".split("_"),
            weekdaysMin: "sø_ma_ti_on_to_fr_lø".split("_"),
            longDateFormat: {
                LT: "H.mm",
                L: "DD.MM.YYYY",
                LL: "D. MMMM YYYY",
                LLL: "D. MMMM YYYY [kl.] LT",
                LLLL: "dddd D. MMMM YYYY [kl.] LT"
            },
            calendar: {
                sameDay: "[i dag kl.] LT",
                nextDay: "[i morgen kl.] LT",
                nextWeek: "dddd [kl.] LT",
                lastDay: "[i går kl.] LT",
                lastWeek: "[forrige] dddd [kl.] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "om %s",
                past: "for %s siden",
                s: "noen sekunder",
                m: "ett minutt",
                mm: "%d minutter",
                h: "en time",
                hh: "%d timer",
                d: "en dag",
                dd: "%d dager",
                M: "en måned",
                MM: "%d måneder",
                y: "ett år",
                yy: "%d år"
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        var b = {
            1: "१",
            2: "२",
            3: "३",
            4: "४",
            5: "५",
            6: "६",
            7: "७",
            8: "८",
            9: "९",
            0: "०"
        }, c = {
            "१": "1",
            "२": "2",
            "३": "3",
            "४": "4",
            "५": "5",
            "६": "6",
            "७": "7",
            "८": "8",
            "९": "9",
            "०": "0"
        };
        return a.lang("ne", {
            months: "जनवरी_फेब्रुवरी_मार्च_अप्रिल_मई_जुन_जुलाई_अगष्ट_सेप्टेम्बर_अक्टोबर_नोभेम्बर_डिसेम्बर".split("_"),
            monthsShort: "जन._फेब्रु._मार्च_अप्रि._मई_जुन_जुलाई._अग._सेप्ट._अक्टो._नोभे._डिसे.".split("_"),
            weekdays: "आइतबार_सोमबार_मङ्गलबार_बुधबार_बिहिबार_शुक्रबार_शनिबार".split("_"),
            weekdaysShort: "आइत._सोम._मङ्गल._बुध._बिहि._शुक्र._शनि.".split("_"),
            weekdaysMin: "आइ._सो._मङ्_बु._बि._शु._श.".split("_"),
            longDateFormat: {
                LT: "Aको h:mm बजे",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY, LT",
                LLLL: "dddd, D MMMM YYYY, LT"
            },
            preparse: function(a) {
                return a.replace(/[१२३४५६७८९०]/g, function(a) {
                    return c[a];
                });
            },
            postformat: function(a) {
                return a.replace(/\d/g, function(a) {
                    return b[a];
                });
            },
            meridiem: function(a) {
                return 3 > a ? "राती" : 10 > a ? "बिहान" : 15 > a ? "दिउँसो" : 18 > a ? "बेलुका" : 20 > a ? "साँझ" : "राती";
            },
            calendar: {
                sameDay: "[आज] LT",
                nextDay: "[भोली] LT",
                nextWeek: "[आउँदो] dddd[,] LT",
                lastDay: "[हिजो] LT",
                lastWeek: "[गएको] dddd[,] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "%sमा",
                past: "%s अगाडी",
                s: "केही समय",
                m: "एक मिनेट",
                mm: "%d मिनेट",
                h: "एक घण्टा",
                hh: "%d घण्टा",
                d: "एक दिन",
                dd: "%d दिन",
                M: "एक महिना",
                MM: "%d महिना",
                y: "एक बर्ष",
                yy: "%d बर्ष"
            },
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        var b = "jan._feb._mrt._apr._mei_jun._jul._aug._sep._okt._nov._dec.".split("_"), c = "jan_feb_mrt_apr_mei_jun_jul_aug_sep_okt_nov_dec".split("_");
        return a.lang("nl", {
            months: "januari_februari_maart_april_mei_juni_juli_augustus_september_oktober_november_december".split("_"),
            monthsShort: function(a, d) {
                return /-MMM-/.test(d) ? c[a.month()] : b[a.month()];
            },
            weekdays: "zondag_maandag_dinsdag_woensdag_donderdag_vrijdag_zaterdag".split("_"),
            weekdaysShort: "zo._ma._di._wo._do._vr._za.".split("_"),
            weekdaysMin: "Zo_Ma_Di_Wo_Do_Vr_Za".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD-MM-YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd D MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[vandaag om] LT",
                nextDay: "[morgen om] LT",
                nextWeek: "dddd [om] LT",
                lastDay: "[gisteren om] LT",
                lastWeek: "[afgelopen] dddd [om] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "over %s",
                past: "%s geleden",
                s: "een paar seconden",
                m: "één minuut",
                mm: "%d minuten",
                h: "één uur",
                hh: "%d uur",
                d: "één dag",
                dd: "%d dagen",
                M: "één maand",
                MM: "%d maanden",
                y: "één jaar",
                yy: "%d jaar"
            },
            ordinal: function(a) {
                return a + (1 === a || 8 === a || a >= 20 ? "ste" : "de");
            },
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("nn", {
            months: "januar_februar_mars_april_mai_juni_juli_august_september_oktober_november_desember".split("_"),
            monthsShort: "jan_feb_mar_apr_mai_jun_jul_aug_sep_okt_nov_des".split("_"),
            weekdays: "sundag_måndag_tysdag_onsdag_torsdag_fredag_laurdag".split("_"),
            weekdaysShort: "sun_mån_tys_ons_tor_fre_lau".split("_"),
            weekdaysMin: "su_må_ty_on_to_fr_lø".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD.MM.YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd D MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[I dag klokka] LT",
                nextDay: "[I morgon klokka] LT",
                nextWeek: "dddd [klokka] LT",
                lastDay: "[I går klokka] LT",
                lastWeek: "[Føregåande] dddd [klokka] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "om %s",
                past: "for %s sidan",
                s: "nokre sekund",
                m: "eit minutt",
                mm: "%d minutt",
                h: "ein time",
                hh: "%d timar",
                d: "ein dag",
                dd: "%d dagar",
                M: "ein månad",
                MM: "%d månader",
                y: "eit år",
                yy: "%d år"
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        function b(a) {
            return 5 > a % 10 && a % 10 > 1 && ~~(a / 10) % 10 !== 1;
        }
        function c(a, c, d) {
            var e = a + " ";
            switch (d) {
              case "m":
                return c ? "minuta" : "minutę";

              case "mm":
                return e + (b(a) ? "minuty" : "minut");

              case "h":
                return c ? "godzina" : "godzinę";

              case "hh":
                return e + (b(a) ? "godziny" : "godzin");

              case "MM":
                return e + (b(a) ? "miesiące" : "miesięcy");

              case "yy":
                return e + (b(a) ? "lata" : "lat");
            }
        }
        var d = "styczeń_luty_marzec_kwiecień_maj_czerwiec_lipiec_sierpień_wrzesień_październik_listopad_grudzień".split("_"), e = "stycznia_lutego_marca_kwietnia_maja_czerwca_lipca_sierpnia_września_października_listopada_grudnia".split("_");
        return a.lang("pl", {
            months: function(a, b) {
                return /D MMMM/.test(b) ? e[a.month()] : d[a.month()];
            },
            monthsShort: "sty_lut_mar_kwi_maj_cze_lip_sie_wrz_paź_lis_gru".split("_"),
            weekdays: "niedziela_poniedziałek_wtorek_środa_czwartek_piątek_sobota".split("_"),
            weekdaysShort: "nie_pon_wt_śr_czw_pt_sb".split("_"),
            weekdaysMin: "N_Pn_Wt_Śr_Cz_Pt_So".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD.MM.YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd, D MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[Dziś o] LT",
                nextDay: "[Jutro o] LT",
                nextWeek: "[W] dddd [o] LT",
                lastDay: "[Wczoraj o] LT",
                lastWeek: function() {
                    switch (this.day()) {
                      case 0:
                        return "[W zeszłą niedzielę o] LT";

                      case 3:
                        return "[W zeszłą środę o] LT";

                      case 6:
                        return "[W zeszłą sobotę o] LT";

                      default:
                        return "[W zeszły] dddd [o] LT";
                    }
                },
                sameElse: "L"
            },
            relativeTime: {
                future: "za %s",
                past: "%s temu",
                s: "kilka sekund",
                m: c,
                mm: c,
                h: c,
                hh: c,
                d: "1 dzień",
                dd: "%d dni",
                M: "miesiąc",
                MM: c,
                y: "rok",
                yy: c
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("pt-br", {
            months: "janeiro_fevereiro_março_abril_maio_junho_julho_agosto_setembro_outubro_novembro_dezembro".split("_"),
            monthsShort: "jan_fev_mar_abr_mai_jun_jul_ago_set_out_nov_dez".split("_"),
            weekdays: "domingo_segunda-feira_terça-feira_quarta-feira_quinta-feira_sexta-feira_sábado".split("_"),
            weekdaysShort: "dom_seg_ter_qua_qui_sex_sáb".split("_"),
            weekdaysMin: "dom_2ª_3ª_4ª_5ª_6ª_sáb".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD/MM/YYYY",
                LL: "D [de] MMMM [de] YYYY",
                LLL: "D [de] MMMM [de] YYYY [às] LT",
                LLLL: "dddd, D [de] MMMM [de] YYYY [às] LT"
            },
            calendar: {
                sameDay: "[Hoje às] LT",
                nextDay: "[Amanhã às] LT",
                nextWeek: "dddd [às] LT",
                lastDay: "[Ontem às] LT",
                lastWeek: function() {
                    return 0 === this.day() || 6 === this.day() ? "[Último] dddd [às] LT" : "[Última] dddd [às] LT";
                },
                sameElse: "L"
            },
            relativeTime: {
                future: "em %s",
                past: "%s atrás",
                s: "segundos",
                m: "um minuto",
                mm: "%d minutos",
                h: "uma hora",
                hh: "%d horas",
                d: "um dia",
                dd: "%d dias",
                M: "um mês",
                MM: "%d meses",
                y: "um ano",
                yy: "%d anos"
            },
            ordinal: "%dº"
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("pt", {
            months: "janeiro_fevereiro_março_abril_maio_junho_julho_agosto_setembro_outubro_novembro_dezembro".split("_"),
            monthsShort: "jan_fev_mar_abr_mai_jun_jul_ago_set_out_nov_dez".split("_"),
            weekdays: "domingo_segunda-feira_terça-feira_quarta-feira_quinta-feira_sexta-feira_sábado".split("_"),
            weekdaysShort: "dom_seg_ter_qua_qui_sex_sáb".split("_"),
            weekdaysMin: "dom_2ª_3ª_4ª_5ª_6ª_sáb".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD/MM/YYYY",
                LL: "D [de] MMMM [de] YYYY",
                LLL: "D [de] MMMM [de] YYYY LT",
                LLLL: "dddd, D [de] MMMM [de] YYYY LT"
            },
            calendar: {
                sameDay: "[Hoje às] LT",
                nextDay: "[Amanhã às] LT",
                nextWeek: "dddd [às] LT",
                lastDay: "[Ontem às] LT",
                lastWeek: function() {
                    return 0 === this.day() || 6 === this.day() ? "[Último] dddd [às] LT" : "[Última] dddd [às] LT";
                },
                sameElse: "L"
            },
            relativeTime: {
                future: "em %s",
                past: "%s atrás",
                s: "segundos",
                m: "um minuto",
                mm: "%d minutos",
                h: "uma hora",
                hh: "%d horas",
                d: "um dia",
                dd: "%d dias",
                M: "um mês",
                MM: "%d meses",
                y: "um ano",
                yy: "%d anos"
            },
            ordinal: "%dº",
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        function b(a, b, c) {
            var d = {
                mm: "minute",
                hh: "ore",
                dd: "zile",
                MM: "luni",
                yy: "ani"
            }, e = " ";
            return (a % 100 >= 20 || a >= 100 && a % 100 === 0) && (e = " de "), a + e + d[c];
        }
        return a.lang("ro", {
            months: "ianuarie_februarie_martie_aprilie_mai_iunie_iulie_august_septembrie_octombrie_noiembrie_decembrie".split("_"),
            monthsShort: "ian._febr._mart._apr._mai_iun._iul._aug._sept._oct._nov._dec.".split("_"),
            weekdays: "duminică_luni_marți_miercuri_joi_vineri_sâmbătă".split("_"),
            weekdaysShort: "Dum_Lun_Mar_Mie_Joi_Vin_Sâm".split("_"),
            weekdaysMin: "Du_Lu_Ma_Mi_Jo_Vi_Sâ".split("_"),
            longDateFormat: {
                LT: "H:mm",
                L: "DD.MM.YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY H:mm",
                LLLL: "dddd, D MMMM YYYY H:mm"
            },
            calendar: {
                sameDay: "[azi la] LT",
                nextDay: "[mâine la] LT",
                nextWeek: "dddd [la] LT",
                lastDay: "[ieri la] LT",
                lastWeek: "[fosta] dddd [la] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "peste %s",
                past: "%s în urmă",
                s: "câteva secunde",
                m: "un minut",
                mm: b,
                h: "o oră",
                hh: b,
                d: "o zi",
                dd: b,
                M: "o lună",
                MM: b,
                y: "un an",
                yy: b
            },
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        function b(a, b) {
            var c = a.split("_");
            return b % 10 === 1 && b % 100 !== 11 ? c[0] : b % 10 >= 2 && 4 >= b % 10 && (10 > b % 100 || b % 100 >= 20) ? c[1] : c[2];
        }
        function c(a, c, d) {
            var e = {
                mm: c ? "минута_минуты_минут" : "минуту_минуты_минут",
                hh: "час_часа_часов",
                dd: "день_дня_дней",
                MM: "месяц_месяца_месяцев",
                yy: "год_года_лет"
            };
            return "m" === d ? c ? "минута" : "минуту" : a + " " + b(e[d], +a);
        }
        function d(a, b) {
            var c = {
                nominative: "январь_февраль_март_апрель_май_июнь_июль_август_сентябрь_октябрь_ноябрь_декабрь".split("_"),
                accusative: "января_февраля_марта_апреля_мая_июня_июля_августа_сентября_октября_ноября_декабря".split("_")
            }, d = /D[oD]?(\[[^\[\]]*\]|\s+)+MMMM?/.test(b) ? "accusative" : "nominative";
            return c[d][a.month()];
        }
        function e(a, b) {
            var c = {
                nominative: "янв_фев_мар_апр_май_июнь_июль_авг_сен_окт_ноя_дек".split("_"),
                accusative: "янв_фев_мар_апр_мая_июня_июля_авг_сен_окт_ноя_дек".split("_")
            }, d = /D[oD]?(\[[^\[\]]*\]|\s+)+MMMM?/.test(b) ? "accusative" : "nominative";
            return c[d][a.month()];
        }
        function f(a, b) {
            var c = {
                nominative: "воскресенье_понедельник_вторник_среда_четверг_пятница_суббота".split("_"),
                accusative: "воскресенье_понедельник_вторник_среду_четверг_пятницу_субботу".split("_")
            }, d = /\[ ?[Вв] ?(?:прошлую|следующую)? ?\] ?dddd/.test(b) ? "accusative" : "nominative";
            return c[d][a.day()];
        }
        return a.lang("ru", {
            months: d,
            monthsShort: e,
            weekdays: f,
            weekdaysShort: "вс_пн_вт_ср_чт_пт_сб".split("_"),
            weekdaysMin: "вс_пн_вт_ср_чт_пт_сб".split("_"),
            monthsParse: [ /^янв/i, /^фев/i, /^мар/i, /^апр/i, /^ма[й|я]/i, /^июн/i, /^июл/i, /^авг/i, /^сен/i, /^окт/i, /^ноя/i, /^дек/i ],
            longDateFormat: {
                LT: "HH:mm",
                L: "DD.MM.YYYY",
                LL: "D MMMM YYYY г.",
                LLL: "D MMMM YYYY г., LT",
                LLLL: "dddd, D MMMM YYYY г., LT"
            },
            calendar: {
                sameDay: "[Сегодня в] LT",
                nextDay: "[Завтра в] LT",
                lastDay: "[Вчера в] LT",
                nextWeek: function() {
                    return 2 === this.day() ? "[Во] dddd [в] LT" : "[В] dddd [в] LT";
                },
                lastWeek: function() {
                    switch (this.day()) {
                      case 0:
                        return "[В прошлое] dddd [в] LT";

                      case 1:
                      case 2:
                      case 4:
                        return "[В прошлый] dddd [в] LT";

                      case 3:
                      case 5:
                      case 6:
                        return "[В прошлую] dddd [в] LT";
                    }
                },
                sameElse: "L"
            },
            relativeTime: {
                future: "через %s",
                past: "%s назад",
                s: "несколько секунд",
                m: c,
                mm: c,
                h: "час",
                hh: c,
                d: "день",
                dd: c,
                M: "месяц",
                MM: c,
                y: "год",
                yy: c
            },
            meridiem: function(a) {
                return 4 > a ? "ночи" : 12 > a ? "утра" : 17 > a ? "дня" : "вечера";
            },
            ordinal: function(a, b) {
                switch (b) {
                  case "M":
                  case "d":
                  case "DDD":
                    return a + "-й";

                  case "D":
                    return a + "-го";

                  case "w":
                  case "W":
                    return a + "-я";

                  default:
                    return a;
                }
            },
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        function b(a) {
            return a > 1 && 5 > a;
        }
        function c(a, c, d, e) {
            var f = a + " ";
            switch (d) {
              case "s":
                return c || e ? "pár sekúnd" : "pár sekundami";

              case "m":
                return c ? "minúta" : e ? "minútu" : "minútou";

              case "mm":
                return c || e ? f + (b(a) ? "minúty" : "minút") : f + "minútami";
                break;

              case "h":
                return c ? "hodina" : e ? "hodinu" : "hodinou";

              case "hh":
                return c || e ? f + (b(a) ? "hodiny" : "hodín") : f + "hodinami";
                break;

              case "d":
                return c || e ? "deň" : "dňom";

              case "dd":
                return c || e ? f + (b(a) ? "dni" : "dní") : f + "dňami";
                break;

              case "M":
                return c || e ? "mesiac" : "mesiacom";

              case "MM":
                return c || e ? f + (b(a) ? "mesiace" : "mesiacov") : f + "mesiacmi";
                break;

              case "y":
                return c || e ? "rok" : "rokom";

              case "yy":
                return c || e ? f + (b(a) ? "roky" : "rokov") : f + "rokmi";
            }
        }
        var d = "január_február_marec_apríl_máj_jún_júl_august_september_október_november_december".split("_"), e = "jan_feb_mar_apr_máj_jún_júl_aug_sep_okt_nov_dec".split("_");
        return a.lang("sk", {
            months: d,
            monthsShort: e,
            monthsParse: function(a, b) {
                var c, d = [];
                for (c = 0; 12 > c; c++) d[c] = new RegExp("^" + a[c] + "$|^" + b[c] + "$", "i");
                return d;
            }(d, e),
            weekdays: "nedeľa_pondelok_utorok_streda_štvrtok_piatok_sobota".split("_"),
            weekdaysShort: "ne_po_ut_st_št_pi_so".split("_"),
            weekdaysMin: "ne_po_ut_st_št_pi_so".split("_"),
            longDateFormat: {
                LT: "H:mm",
                L: "DD.MM.YYYY",
                LL: "D. MMMM YYYY",
                LLL: "D. MMMM YYYY LT",
                LLLL: "dddd D. MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[dnes o] LT",
                nextDay: "[zajtra o] LT",
                nextWeek: function() {
                    switch (this.day()) {
                      case 0:
                        return "[v nedeľu o] LT";

                      case 1:
                      case 2:
                        return "[v] dddd [o] LT";

                      case 3:
                        return "[v stredu o] LT";

                      case 4:
                        return "[vo štvrtok o] LT";

                      case 5:
                        return "[v piatok o] LT";

                      case 6:
                        return "[v sobotu o] LT";
                    }
                },
                lastDay: "[včera o] LT",
                lastWeek: function() {
                    switch (this.day()) {
                      case 0:
                        return "[minulú nedeľu o] LT";

                      case 1:
                      case 2:
                        return "[minulý] dddd [o] LT";

                      case 3:
                        return "[minulú stredu o] LT";

                      case 4:
                      case 5:
                        return "[minulý] dddd [o] LT";

                      case 6:
                        return "[minulú sobotu o] LT";
                    }
                },
                sameElse: "L"
            },
            relativeTime: {
                future: "za %s",
                past: "pred %s",
                s: c,
                m: c,
                mm: c,
                h: c,
                hh: c,
                d: c,
                dd: c,
                M: c,
                MM: c,
                y: c,
                yy: c
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        function b(a, b, c) {
            var d = a + " ";
            switch (c) {
              case "m":
                return b ? "ena minuta" : "eno minuto";

              case "mm":
                return d += 1 === a ? "minuta" : 2 === a ? "minuti" : 3 === a || 4 === a ? "minute" : "minut";

              case "h":
                return b ? "ena ura" : "eno uro";

              case "hh":
                return d += 1 === a ? "ura" : 2 === a ? "uri" : 3 === a || 4 === a ? "ure" : "ur";

              case "dd":
                return d += 1 === a ? "dan" : "dni";

              case "MM":
                return d += 1 === a ? "mesec" : 2 === a ? "meseca" : 3 === a || 4 === a ? "mesece" : "mesecev";

              case "yy":
                return d += 1 === a ? "leto" : 2 === a ? "leti" : 3 === a || 4 === a ? "leta" : "let";
            }
        }
        return a.lang("sl", {
            months: "januar_februar_marec_april_maj_junij_julij_avgust_september_oktober_november_december".split("_"),
            monthsShort: "jan._feb._mar._apr._maj._jun._jul._avg._sep._okt._nov._dec.".split("_"),
            weekdays: "nedelja_ponedeljek_torek_sreda_četrtek_petek_sobota".split("_"),
            weekdaysShort: "ned._pon._tor._sre._čet._pet._sob.".split("_"),
            weekdaysMin: "ne_po_to_sr_če_pe_so".split("_"),
            longDateFormat: {
                LT: "H:mm",
                L: "DD. MM. YYYY",
                LL: "D. MMMM YYYY",
                LLL: "D. MMMM YYYY LT",
                LLLL: "dddd, D. MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[danes ob] LT",
                nextDay: "[jutri ob] LT",
                nextWeek: function() {
                    switch (this.day()) {
                      case 0:
                        return "[v] [nedeljo] [ob] LT";

                      case 3:
                        return "[v] [sredo] [ob] LT";

                      case 6:
                        return "[v] [soboto] [ob] LT";

                      case 1:
                      case 2:
                      case 4:
                      case 5:
                        return "[v] dddd [ob] LT";
                    }
                },
                lastDay: "[včeraj ob] LT",
                lastWeek: function() {
                    switch (this.day()) {
                      case 0:
                      case 3:
                      case 6:
                        return "[prejšnja] dddd [ob] LT";

                      case 1:
                      case 2:
                      case 4:
                      case 5:
                        return "[prejšnji] dddd [ob] LT";
                    }
                },
                sameElse: "L"
            },
            relativeTime: {
                future: "čez %s",
                past: "%s nazaj",
                s: "nekaj sekund",
                m: b,
                mm: b,
                h: b,
                hh: b,
                d: "en dan",
                dd: b,
                M: "en mesec",
                MM: b,
                y: "eno leto",
                yy: b
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("sq", {
            months: "Janar_Shkurt_Mars_Prill_Maj_Qershor_Korrik_Gusht_Shtator_Tetor_Nëntor_Dhjetor".split("_"),
            monthsShort: "Jan_Shk_Mar_Pri_Maj_Qer_Kor_Gus_Sht_Tet_Nën_Dhj".split("_"),
            weekdays: "E Diel_E Hënë_E Martë_E Mërkurë_E Enjte_E Premte_E Shtunë".split("_"),
            weekdaysShort: "Die_Hën_Mar_Mër_Enj_Pre_Sht".split("_"),
            weekdaysMin: "D_H_Ma_Më_E_P_Sh".split("_"),
            meridiem: function(a) {
                return 12 > a ? "PD" : "MD";
            },
            longDateFormat: {
                LT: "HH:mm",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd, D MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[Sot në] LT",
                nextDay: "[Nesër në] LT",
                nextWeek: "dddd [në] LT",
                lastDay: "[Dje në] LT",
                lastWeek: "dddd [e kaluar në] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "në %s",
                past: "%s më parë",
                s: "disa sekonda",
                m: "një minutë",
                mm: "%d minuta",
                h: "një orë",
                hh: "%d orë",
                d: "një ditë",
                dd: "%d ditë",
                M: "një muaj",
                MM: "%d muaj",
                y: "një vit",
                yy: "%d vite"
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        var b = {
            words: {
                m: [ "један минут", "једне минуте" ],
                mm: [ "минут", "минуте", "минута" ],
                h: [ "један сат", "једног сата" ],
                hh: [ "сат", "сата", "сати" ],
                dd: [ "дан", "дана", "дана" ],
                MM: [ "месец", "месеца", "месеци" ],
                yy: [ "година", "године", "година" ]
            },
            correctGrammaticalCase: function(a, b) {
                return 1 === a ? b[0] : a >= 2 && 4 >= a ? b[1] : b[2];
            },
            translate: function(a, c, d) {
                var e = b.words[d];
                return 1 === d.length ? c ? e[0] : e[1] : a + " " + b.correctGrammaticalCase(a, e);
            }
        };
        return a.lang("sr-cyr", {
            months: [ "јануар", "фебруар", "март", "април", "мај", "јун", "јул", "август", "септембар", "октобар", "новембар", "децембар" ],
            monthsShort: [ "јан.", "феб.", "мар.", "апр.", "мај", "јун", "јул", "авг.", "сеп.", "окт.", "нов.", "дец." ],
            weekdays: [ "недеља", "понедељак", "уторак", "среда", "четвртак", "петак", "субота" ],
            weekdaysShort: [ "нед.", "пон.", "уто.", "сре.", "чет.", "пет.", "суб." ],
            weekdaysMin: [ "не", "по", "ут", "ср", "че", "пе", "су" ],
            longDateFormat: {
                LT: "H:mm",
                L: "DD. MM. YYYY",
                LL: "D. MMMM YYYY",
                LLL: "D. MMMM YYYY LT",
                LLLL: "dddd, D. MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[данас у] LT",
                nextDay: "[сутра у] LT",
                nextWeek: function() {
                    switch (this.day()) {
                      case 0:
                        return "[у] [недељу] [у] LT";

                      case 3:
                        return "[у] [среду] [у] LT";

                      case 6:
                        return "[у] [суботу] [у] LT";

                      case 1:
                      case 2:
                      case 4:
                      case 5:
                        return "[у] dddd [у] LT";
                    }
                },
                lastDay: "[јуче у] LT",
                lastWeek: function() {
                    var a = [ "[прошле] [недеље] [у] LT", "[прошлог] [понедељка] [у] LT", "[прошлог] [уторка] [у] LT", "[прошле] [среде] [у] LT", "[прошлог] [четвртка] [у] LT", "[прошлог] [петка] [у] LT", "[прошле] [суботе] [у] LT" ];
                    return a[this.day()];
                },
                sameElse: "L"
            },
            relativeTime: {
                future: "за %s",
                past: "пре %s",
                s: "неколико секунди",
                m: b.translate,
                mm: b.translate,
                h: b.translate,
                hh: b.translate,
                d: "дан",
                dd: b.translate,
                M: "месец",
                MM: b.translate,
                y: "годину",
                yy: b.translate
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        var b = {
            words: {
                m: [ "jedan minut", "jedne minute" ],
                mm: [ "minut", "minute", "minuta" ],
                h: [ "jedan sat", "jednog sata" ],
                hh: [ "sat", "sata", "sati" ],
                dd: [ "dan", "dana", "dana" ],
                MM: [ "mesec", "meseca", "meseci" ],
                yy: [ "godina", "godine", "godina" ]
            },
            correctGrammaticalCase: function(a, b) {
                return 1 === a ? b[0] : a >= 2 && 4 >= a ? b[1] : b[2];
            },
            translate: function(a, c, d) {
                var e = b.words[d];
                return 1 === d.length ? c ? e[0] : e[1] : a + " " + b.correctGrammaticalCase(a, e);
            }
        };
        return a.lang("sr", {
            months: [ "januar", "februar", "mart", "april", "maj", "jun", "jul", "avgust", "septembar", "oktobar", "novembar", "decembar" ],
            monthsShort: [ "jan.", "feb.", "mar.", "apr.", "maj", "jun", "jul", "avg.", "sep.", "okt.", "nov.", "dec." ],
            weekdays: [ "nedelja", "ponedeljak", "utorak", "sreda", "četvrtak", "petak", "subota" ],
            weekdaysShort: [ "ned.", "pon.", "uto.", "sre.", "čet.", "pet.", "sub." ],
            weekdaysMin: [ "ne", "po", "ut", "sr", "če", "pe", "su" ],
            longDateFormat: {
                LT: "H:mm",
                L: "DD. MM. YYYY",
                LL: "D. MMMM YYYY",
                LLL: "D. MMMM YYYY LT",
                LLLL: "dddd, D. MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[danas u] LT",
                nextDay: "[sutra u] LT",
                nextWeek: function() {
                    switch (this.day()) {
                      case 0:
                        return "[u] [nedelju] [u] LT";

                      case 3:
                        return "[u] [sredu] [u] LT";

                      case 6:
                        return "[u] [subotu] [u] LT";

                      case 1:
                      case 2:
                      case 4:
                      case 5:
                        return "[u] dddd [u] LT";
                    }
                },
                lastDay: "[juče u] LT",
                lastWeek: function() {
                    var a = [ "[prošle] [nedelje] [u] LT", "[prošlog] [ponedeljka] [u] LT", "[prošlog] [utorka] [u] LT", "[prošle] [srede] [u] LT", "[prošlog] [četvrtka] [u] LT", "[prošlog] [petka] [u] LT", "[prošle] [subote] [u] LT" ];
                    return a[this.day()];
                },
                sameElse: "L"
            },
            relativeTime: {
                future: "za %s",
                past: "pre %s",
                s: "nekoliko sekundi",
                m: b.translate,
                mm: b.translate,
                h: b.translate,
                hh: b.translate,
                d: "dan",
                dd: b.translate,
                M: "mesec",
                MM: b.translate,
                y: "godinu",
                yy: b.translate
            },
            ordinal: "%d.",
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("sv", {
            months: "januari_februari_mars_april_maj_juni_juli_augusti_september_oktober_november_december".split("_"),
            monthsShort: "jan_feb_mar_apr_maj_jun_jul_aug_sep_okt_nov_dec".split("_"),
            weekdays: "söndag_måndag_tisdag_onsdag_torsdag_fredag_lördag".split("_"),
            weekdaysShort: "sön_mån_tis_ons_tor_fre_lör".split("_"),
            weekdaysMin: "sö_må_ti_on_to_fr_lö".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "YYYY-MM-DD",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd D MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[Idag] LT",
                nextDay: "[Imorgon] LT",
                lastDay: "[Igår] LT",
                nextWeek: "dddd LT",
                lastWeek: "[Förra] dddd[en] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "om %s",
                past: "för %s sedan",
                s: "några sekunder",
                m: "en minut",
                mm: "%d minuter",
                h: "en timme",
                hh: "%d timmar",
                d: "en dag",
                dd: "%d dagar",
                M: "en månad",
                MM: "%d månader",
                y: "ett år",
                yy: "%d år"
            },
            ordinal: function(a) {
                var b = a % 10, c = 1 === ~~(a % 100 / 10) ? "e" : 1 === b ? "a" : 2 === b ? "a" : 3 === b ? "e" : "e";
                return a + c;
            },
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("ta", {
            months: "ஜனவரி_பிப்ரவரி_மார்ச்_ஏப்ரல்_மே_ஜூன்_ஜூலை_ஆகஸ்ட்_செப்டெம்பர்_அக்டோபர்_நவம்பர்_டிசம்பர்".split("_"),
            monthsShort: "ஜனவரி_பிப்ரவரி_மார்ச்_ஏப்ரல்_மே_ஜூன்_ஜூலை_ஆகஸ்ட்_செப்டெம்பர்_அக்டோபர்_நவம்பர்_டிசம்பர்".split("_"),
            weekdays: "ஞாயிற்றுக்கிழமை_திங்கட்கிழமை_செவ்வாய்கிழமை_புதன்கிழமை_வியாழக்கிழமை_வெள்ளிக்கிழமை_சனிக்கிழமை".split("_"),
            weekdaysShort: "ஞாயிறு_திங்கள்_செவ்வாய்_புதன்_வியாழன்_வெள்ளி_சனி".split("_"),
            weekdaysMin: "ஞா_தி_செ_பு_வி_வெ_ச".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY, LT",
                LLLL: "dddd, D MMMM YYYY, LT"
            },
            calendar: {
                sameDay: "[இன்று] LT",
                nextDay: "[நாளை] LT",
                nextWeek: "dddd, LT",
                lastDay: "[நேற்று] LT",
                lastWeek: "[கடந்த வாரம்] dddd, LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "%s இல்",
                past: "%s முன்",
                s: "ஒரு சில விநாடிகள்",
                m: "ஒரு நிமிடம்",
                mm: "%d நிமிடங்கள்",
                h: "ஒரு மணி நேரம்",
                hh: "%d மணி நேரம்",
                d: "ஒரு நாள்",
                dd: "%d நாட்கள்",
                M: "ஒரு மாதம்",
                MM: "%d மாதங்கள்",
                y: "ஒரு வருடம்",
                yy: "%d ஆண்டுகள்"
            },
            ordinal: function(a) {
                return a + "வது";
            },
            meridiem: function(a) {
                return a >= 6 && 10 >= a ? " காலை" : a >= 10 && 14 >= a ? " நண்பகல்" : a >= 14 && 18 >= a ? " எற்பாடு" : a >= 18 && 20 >= a ? " மாலை" : a >= 20 && 24 >= a ? " இரவு" : a >= 0 && 6 >= a ? " வைகறை" : void 0;
            },
            week: {
                dow: 0,
                doy: 6
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("th", {
            months: "มกราคม_กุมภาพันธ์_มีนาคม_เมษายน_พฤษภาคม_มิถุนายน_กรกฎาคม_สิงหาคม_กันยายน_ตุลาคม_พฤศจิกายน_ธันวาคม".split("_"),
            monthsShort: "มกรา_กุมภา_มีนา_เมษา_พฤษภา_มิถุนา_กรกฎา_สิงหา_กันยา_ตุลา_พฤศจิกา_ธันวา".split("_"),
            weekdays: "อาทิตย์_จันทร์_อังคาร_พุธ_พฤหัสบดี_ศุกร์_เสาร์".split("_"),
            weekdaysShort: "อาทิตย์_จันทร์_อังคาร_พุธ_พฤหัส_ศุกร์_เสาร์".split("_"),
            weekdaysMin: "อา._จ._อ._พ._พฤ._ศ._ส.".split("_"),
            longDateFormat: {
                LT: "H นาฬิกา m นาที",
                L: "YYYY/MM/DD",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY เวลา LT",
                LLLL: "วันddddที่ D MMMM YYYY เวลา LT"
            },
            meridiem: function(a) {
                return 12 > a ? "ก่อนเที่ยง" : "หลังเที่ยง";
            },
            calendar: {
                sameDay: "[วันนี้ เวลา] LT",
                nextDay: "[พรุ่งนี้ เวลา] LT",
                nextWeek: "dddd[หน้า เวลา] LT",
                lastDay: "[เมื่อวานนี้ เวลา] LT",
                lastWeek: "[วัน]dddd[ที่แล้ว เวลา] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "อีก %s",
                past: "%sที่แล้ว",
                s: "ไม่กี่วินาที",
                m: "1 นาที",
                mm: "%d นาที",
                h: "1 ชั่วโมง",
                hh: "%d ชั่วโมง",
                d: "1 วัน",
                dd: "%d วัน",
                M: "1 เดือน",
                MM: "%d เดือน",
                y: "1 ปี",
                yy: "%d ปี"
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("tl-ph", {
            months: "Enero_Pebrero_Marso_Abril_Mayo_Hunyo_Hulyo_Agosto_Setyembre_Oktubre_Nobyembre_Disyembre".split("_"),
            monthsShort: "Ene_Peb_Mar_Abr_May_Hun_Hul_Ago_Set_Okt_Nob_Dis".split("_"),
            weekdays: "Linggo_Lunes_Martes_Miyerkules_Huwebes_Biyernes_Sabado".split("_"),
            weekdaysShort: "Lin_Lun_Mar_Miy_Huw_Biy_Sab".split("_"),
            weekdaysMin: "Li_Lu_Ma_Mi_Hu_Bi_Sab".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "MM/D/YYYY",
                LL: "MMMM D, YYYY",
                LLL: "MMMM D, YYYY LT",
                LLLL: "dddd, MMMM DD, YYYY LT"
            },
            calendar: {
                sameDay: "[Ngayon sa] LT",
                nextDay: "[Bukas sa] LT",
                nextWeek: "dddd [sa] LT",
                lastDay: "[Kahapon sa] LT",
                lastWeek: "dddd [huling linggo] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "sa loob ng %s",
                past: "%s ang nakalipas",
                s: "ilang segundo",
                m: "isang minuto",
                mm: "%d minuto",
                h: "isang oras",
                hh: "%d oras",
                d: "isang araw",
                dd: "%d araw",
                M: "isang buwan",
                MM: "%d buwan",
                y: "isang taon",
                yy: "%d taon"
            },
            ordinal: function(a) {
                return a;
            },
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        var b = {
            1: "'inci",
            5: "'inci",
            8: "'inci",
            70: "'inci",
            80: "'inci",
            2: "'nci",
            7: "'nci",
            20: "'nci",
            50: "'nci",
            3: "'üncü",
            4: "'üncü",
            100: "'üncü",
            6: "'ncı",
            9: "'uncu",
            10: "'uncu",
            30: "'uncu",
            60: "'ıncı",
            90: "'ıncı"
        };
        return a.lang("tr", {
            months: "Ocak_Şubat_Mart_Nisan_Mayıs_Haziran_Temmuz_Ağustos_Eylül_Ekim_Kasım_Aralık".split("_"),
            monthsShort: "Oca_Şub_Mar_Nis_May_Haz_Tem_Ağu_Eyl_Eki_Kas_Ara".split("_"),
            weekdays: "Pazar_Pazartesi_Salı_Çarşamba_Perşembe_Cuma_Cumartesi".split("_"),
            weekdaysShort: "Paz_Pts_Sal_Çar_Per_Cum_Cts".split("_"),
            weekdaysMin: "Pz_Pt_Sa_Ça_Pe_Cu_Ct".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD.MM.YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd, D MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[bugün saat] LT",
                nextDay: "[yarın saat] LT",
                nextWeek: "[haftaya] dddd [saat] LT",
                lastDay: "[dün] LT",
                lastWeek: "[geçen hafta] dddd [saat] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "%s sonra",
                past: "%s önce",
                s: "birkaç saniye",
                m: "bir dakika",
                mm: "%d dakika",
                h: "bir saat",
                hh: "%d saat",
                d: "bir gün",
                dd: "%d gün",
                M: "bir ay",
                MM: "%d ay",
                y: "bir yıl",
                yy: "%d yıl"
            },
            ordinal: function(a) {
                if (0 === a) return a + "'ıncı";
                var c = a % 10, d = a % 100 - c, e = a >= 100 ? 100 : null;
                return a + (b[c] || b[d] || b[e]);
            },
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("tzm-la", {
            months: "innayr_brˤayrˤ_marˤsˤ_ibrir_mayyw_ywnyw_ywlywz_ɣwšt_šwtanbir_ktˤwbrˤ_nwwanbir_dwjnbir".split("_"),
            monthsShort: "innayr_brˤayrˤ_marˤsˤ_ibrir_mayyw_ywnyw_ywlywz_ɣwšt_šwtanbir_ktˤwbrˤ_nwwanbir_dwjnbir".split("_"),
            weekdays: "asamas_aynas_asinas_akras_akwas_asimwas_asiḍyas".split("_"),
            weekdaysShort: "asamas_aynas_asinas_akras_akwas_asimwas_asiḍyas".split("_"),
            weekdaysMin: "asamas_aynas_asinas_akras_akwas_asimwas_asiḍyas".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd D MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[asdkh g] LT",
                nextDay: "[aska g] LT",
                nextWeek: "dddd [g] LT",
                lastDay: "[assant g] LT",
                lastWeek: "dddd [g] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "dadkh s yan %s",
                past: "yan %s",
                s: "imik",
                m: "minuḍ",
                mm: "%d minuḍ",
                h: "saɛa",
                hh: "%d tassaɛin",
                d: "ass",
                dd: "%d ossan",
                M: "ayowr",
                MM: "%d iyyirn",
                y: "asgas",
                yy: "%d isgasn"
            },
            week: {
                dow: 6,
                doy: 12
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("tzm", {
            months: "ⵉⵏⵏⴰⵢⵔ_ⴱⵕⴰⵢⵕ_ⵎⴰⵕⵚ_ⵉⴱⵔⵉⵔ_ⵎⴰⵢⵢⵓ_ⵢⵓⵏⵢⵓ_ⵢⵓⵍⵢⵓⵣ_ⵖⵓⵛⵜ_ⵛⵓⵜⴰⵏⴱⵉⵔ_ⴽⵟⵓⴱⵕ_ⵏⵓⵡⴰⵏⴱⵉⵔ_ⴷⵓⵊⵏⴱⵉⵔ".split("_"),
            monthsShort: "ⵉⵏⵏⴰⵢⵔ_ⴱⵕⴰⵢⵕ_ⵎⴰⵕⵚ_ⵉⴱⵔⵉⵔ_ⵎⴰⵢⵢⵓ_ⵢⵓⵏⵢⵓ_ⵢⵓⵍⵢⵓⵣ_ⵖⵓⵛⵜ_ⵛⵓⵜⴰⵏⴱⵉⵔ_ⴽⵟⵓⴱⵕ_ⵏⵓⵡⴰⵏⴱⵉⵔ_ⴷⵓⵊⵏⴱⵉⵔ".split("_"),
            weekdays: "ⴰⵙⴰⵎⴰⵙ_ⴰⵢⵏⴰⵙ_ⴰⵙⵉⵏⴰⵙ_ⴰⴽⵔⴰⵙ_ⴰⴽⵡⴰⵙ_ⴰⵙⵉⵎⵡⴰⵙ_ⴰⵙⵉⴹⵢⴰⵙ".split("_"),
            weekdaysShort: "ⴰⵙⴰⵎⴰⵙ_ⴰⵢⵏⴰⵙ_ⴰⵙⵉⵏⴰⵙ_ⴰⴽⵔⴰⵙ_ⴰⴽⵡⴰⵙ_ⴰⵙⵉⵎⵡⴰⵙ_ⴰⵙⵉⴹⵢⴰⵙ".split("_"),
            weekdaysMin: "ⴰⵙⴰⵎⴰⵙ_ⴰⵢⵏⴰⵙ_ⴰⵙⵉⵏⴰⵙ_ⴰⴽⵔⴰⵙ_ⴰⴽⵡⴰⵙ_ⴰⵙⵉⵎⵡⴰⵙ_ⴰⵙⵉⴹⵢⴰⵙ".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "dddd D MMMM YYYY LT"
            },
            calendar: {
                sameDay: "[ⴰⵙⴷⵅ ⴴ] LT",
                nextDay: "[ⴰⵙⴽⴰ ⴴ] LT",
                nextWeek: "dddd [ⴴ] LT",
                lastDay: "[ⴰⵚⴰⵏⵜ ⴴ] LT",
                lastWeek: "dddd [ⴴ] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "ⴷⴰⴷⵅ ⵙ ⵢⴰⵏ %s",
                past: "ⵢⴰⵏ %s",
                s: "ⵉⵎⵉⴽ",
                m: "ⵎⵉⵏⵓⴺ",
                mm: "%d ⵎⵉⵏⵓⴺ",
                h: "ⵙⴰⵄⴰ",
                hh: "%d ⵜⴰⵙⵙⴰⵄⵉⵏ",
                d: "ⴰⵙⵙ",
                dd: "%d oⵙⵙⴰⵏ",
                M: "ⴰⵢoⵓⵔ",
                MM: "%d ⵉⵢⵢⵉⵔⵏ",
                y: "ⴰⵙⴳⴰⵙ",
                yy: "%d ⵉⵙⴳⴰⵙⵏ"
            },
            week: {
                dow: 6,
                doy: 12
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        function b(a, b) {
            var c = a.split("_");
            return b % 10 === 1 && b % 100 !== 11 ? c[0] : b % 10 >= 2 && 4 >= b % 10 && (10 > b % 100 || b % 100 >= 20) ? c[1] : c[2];
        }
        function c(a, c, d) {
            var e = {
                mm: "хвилина_хвилини_хвилин",
                hh: "година_години_годин",
                dd: "день_дні_днів",
                MM: "місяць_місяці_місяців",
                yy: "рік_роки_років"
            };
            return "m" === d ? c ? "хвилина" : "хвилину" : "h" === d ? c ? "година" : "годину" : a + " " + b(e[d], +a);
        }
        function d(a, b) {
            var c = {
                nominative: "січень_лютий_березень_квітень_травень_червень_липень_серпень_вересень_жовтень_листопад_грудень".split("_"),
                accusative: "січня_лютого_березня_квітня_травня_червня_липня_серпня_вересня_жовтня_листопада_грудня".split("_")
            }, d = /D[oD]? *MMMM?/.test(b) ? "accusative" : "nominative";
            return c[d][a.month()];
        }
        function e(a, b) {
            var c = {
                nominative: "неділя_понеділок_вівторок_середа_четвер_п’ятниця_субота".split("_"),
                accusative: "неділю_понеділок_вівторок_середу_четвер_п’ятницю_суботу".split("_"),
                genitive: "неділі_понеділка_вівторка_середи_четверга_п’ятниці_суботи".split("_")
            }, d = /(\[[ВвУу]\]) ?dddd/.test(b) ? "accusative" : /\[?(?:минулої|наступної)? ?\] ?dddd/.test(b) ? "genitive" : "nominative";
            return c[d][a.day()];
        }
        function f(a) {
            return function() {
                return a + "о" + (11 === this.hours() ? "б" : "") + "] LT";
            };
        }
        return a.lang("uk", {
            months: d,
            monthsShort: "січ_лют_бер_квіт_трав_черв_лип_серп_вер_жовт_лист_груд".split("_"),
            weekdays: e,
            weekdaysShort: "нд_пн_вт_ср_чт_пт_сб".split("_"),
            weekdaysMin: "нд_пн_вт_ср_чт_пт_сб".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD.MM.YYYY",
                LL: "D MMMM YYYY р.",
                LLL: "D MMMM YYYY р., LT",
                LLLL: "dddd, D MMMM YYYY р., LT"
            },
            calendar: {
                sameDay: f("[Сьогодні "),
                nextDay: f("[Завтра "),
                lastDay: f("[Вчора "),
                nextWeek: f("[У] dddd ["),
                lastWeek: function() {
                    switch (this.day()) {
                      case 0:
                      case 3:
                      case 5:
                      case 6:
                        return f("[Минулої] dddd [").call(this);

                      case 1:
                      case 2:
                      case 4:
                        return f("[Минулого] dddd [").call(this);
                    }
                },
                sameElse: "L"
            },
            relativeTime: {
                future: "за %s",
                past: "%s тому",
                s: "декілька секунд",
                m: c,
                mm: c,
                h: "годину",
                hh: c,
                d: "день",
                dd: c,
                M: "місяць",
                MM: c,
                y: "рік",
                yy: c
            },
            meridiem: function(a) {
                return 4 > a ? "ночі" : 12 > a ? "ранку" : 17 > a ? "дня" : "вечора";
            },
            ordinal: function(a, b) {
                switch (b) {
                  case "M":
                  case "d":
                  case "DDD":
                  case "w":
                  case "W":
                    return a + "-й";

                  case "D":
                    return a + "-го";

                  default:
                    return a;
                }
            },
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("uz", {
            months: "январь_февраль_март_апрель_май_июнь_июль_август_сентябрь_октябрь_ноябрь_декабрь".split("_"),
            monthsShort: "янв_фев_мар_апр_май_июн_июл_авг_сен_окт_ноя_дек".split("_"),
            weekdays: "Якшанба_Душанба_Сешанба_Чоршанба_Пайшанба_Жума_Шанба".split("_"),
            weekdaysShort: "Якш_Душ_Сеш_Чор_Пай_Жум_Шан".split("_"),
            weekdaysMin: "Як_Ду_Се_Чо_Па_Жу_Ша".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD/MM/YYYY",
                LL: "D MMMM YYYY",
                LLL: "D MMMM YYYY LT",
                LLLL: "D MMMM YYYY, dddd LT"
            },
            calendar: {
                sameDay: "[Бугун соат] LT [да]",
                nextDay: "[Эртага] LT [да]",
                nextWeek: "dddd [куни соат] LT [да]",
                lastDay: "[Кеча соат] LT [да]",
                lastWeek: "[Утган] dddd [куни соат] LT [да]",
                sameElse: "L"
            },
            relativeTime: {
                future: "Якин %s ичида",
                past: "Бир неча %s олдин",
                s: "фурсат",
                m: "бир дакика",
                mm: "%d дакика",
                h: "бир соат",
                hh: "%d соат",
                d: "бир кун",
                dd: "%d кун",
                M: "бир ой",
                MM: "%d ой",
                y: "бир йил",
                yy: "%d йил"
            },
            week: {
                dow: 1,
                doy: 7
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("vi", {
            months: "tháng 1_tháng 2_tháng 3_tháng 4_tháng 5_tháng 6_tháng 7_tháng 8_tháng 9_tháng 10_tháng 11_tháng 12".split("_"),
            monthsShort: "Th01_Th02_Th03_Th04_Th05_Th06_Th07_Th08_Th09_Th10_Th11_Th12".split("_"),
            weekdays: "chủ nhật_thứ hai_thứ ba_thứ tư_thứ năm_thứ sáu_thứ bảy".split("_"),
            weekdaysShort: "CN_T2_T3_T4_T5_T6_T7".split("_"),
            weekdaysMin: "CN_T2_T3_T4_T5_T6_T7".split("_"),
            longDateFormat: {
                LT: "HH:mm",
                L: "DD/MM/YYYY",
                LL: "D MMMM [năm] YYYY",
                LLL: "D MMMM [năm] YYYY LT",
                LLLL: "dddd, D MMMM [năm] YYYY LT",
                l: "DD/M/YYYY",
                ll: "D MMM YYYY",
                lll: "D MMM YYYY LT",
                llll: "ddd, D MMM YYYY LT"
            },
            calendar: {
                sameDay: "[Hôm nay lúc] LT",
                nextDay: "[Ngày mai lúc] LT",
                nextWeek: "dddd [tuần tới lúc] LT",
                lastDay: "[Hôm qua lúc] LT",
                lastWeek: "dddd [tuần rồi lúc] LT",
                sameElse: "L"
            },
            relativeTime: {
                future: "%s tới",
                past: "%s trước",
                s: "vài giây",
                m: "một phút",
                mm: "%d phút",
                h: "một giờ",
                hh: "%d giờ",
                d: "một ngày",
                dd: "%d ngày",
                M: "một tháng",
                MM: "%d tháng",
                y: "một năm",
                yy: "%d năm"
            },
            ordinal: function(a) {
                return a;
            },
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("zh-cn", {
            months: "一月_二月_三月_四月_五月_六月_七月_八月_九月_十月_十一月_十二月".split("_"),
            monthsShort: "1月_2月_3月_4月_5月_6月_7月_8月_9月_10月_11月_12月".split("_"),
            weekdays: "星期日_星期一_星期二_星期三_星期四_星期五_星期六".split("_"),
            weekdaysShort: "周日_周一_周二_周三_周四_周五_周六".split("_"),
            weekdaysMin: "日_一_二_三_四_五_六".split("_"),
            longDateFormat: {
                LT: "Ah点mm",
                L: "YYYY-MM-DD",
                LL: "YYYY年MMMD日",
                LLL: "YYYY年MMMD日LT",
                LLLL: "YYYY年MMMD日ddddLT",
                l: "YYYY-MM-DD",
                ll: "YYYY年MMMD日",
                lll: "YYYY年MMMD日LT",
                llll: "YYYY年MMMD日ddddLT"
            },
            meridiem: function(a, b) {
                var c = 100 * a + b;
                return 600 > c ? "凌晨" : 900 > c ? "早上" : 1130 > c ? "上午" : 1230 > c ? "中午" : 1800 > c ? "下午" : "晚上";
            },
            calendar: {
                sameDay: function() {
                    return 0 === this.minutes() ? "[今天]Ah[点整]" : "[今天]LT";
                },
                nextDay: function() {
                    return 0 === this.minutes() ? "[明天]Ah[点整]" : "[明天]LT";
                },
                lastDay: function() {
                    return 0 === this.minutes() ? "[昨天]Ah[点整]" : "[昨天]LT";
                },
                nextWeek: function() {
                    var b, c;
                    return b = a().startOf("week"), c = this.unix() - b.unix() >= 604800 ? "[下]" : "[本]", 
                    0 === this.minutes() ? c + "dddAh点整" : c + "dddAh点mm";
                },
                lastWeek: function() {
                    var b, c;
                    return b = a().startOf("week"), c = this.unix() < b.unix() ? "[上]" : "[本]", 0 === this.minutes() ? c + "dddAh点整" : c + "dddAh点mm";
                },
                sameElse: "LL"
            },
            ordinal: function(a, b) {
                switch (b) {
                  case "d":
                  case "D":
                  case "DDD":
                    return a + "日";

                  case "M":
                    return a + "月";

                  case "w":
                  case "W":
                    return a + "周";

                  default:
                    return a;
                }
            },
            relativeTime: {
                future: "%s内",
                past: "%s前",
                s: "几秒",
                m: "1分钟",
                mm: "%d分钟",
                h: "1小时",
                hh: "%d小时",
                d: "1天",
                dd: "%d天",
                M: "1个月",
                MM: "%d个月",
                y: "1年",
                yy: "%d年"
            },
            week: {
                dow: 1,
                doy: 4
            }
        });
    }), function(a) {
        a(ib);
    }(function(a) {
        return a.lang("zh-tw", {
            months: "一月_二月_三月_四月_五月_六月_七月_八月_九月_十月_十一月_十二月".split("_"),
            monthsShort: "1月_2月_3月_4月_5月_6月_7月_8月_9月_10月_11月_12月".split("_"),
            weekdays: "星期日_星期一_星期二_星期三_星期四_星期五_星期六".split("_"),
            weekdaysShort: "週日_週一_週二_週三_週四_週五_週六".split("_"),
            weekdaysMin: "日_一_二_三_四_五_六".split("_"),
            longDateFormat: {
                LT: "Ah點mm",
                L: "YYYY年MMMD日",
                LL: "YYYY年MMMD日",
                LLL: "YYYY年MMMD日LT",
                LLLL: "YYYY年MMMD日ddddLT",
                l: "YYYY年MMMD日",
                ll: "YYYY年MMMD日",
                lll: "YYYY年MMMD日LT",
                llll: "YYYY年MMMD日ddddLT"
            },
            meridiem: function(a, b) {
                var c = 100 * a + b;
                return 900 > c ? "早上" : 1130 > c ? "上午" : 1230 > c ? "中午" : 1800 > c ? "下午" : "晚上";
            },
            calendar: {
                sameDay: "[今天]LT",
                nextDay: "[明天]LT",
                nextWeek: "[下]ddddLT",
                lastDay: "[昨天]LT",
                lastWeek: "[上]ddddLT",
                sameElse: "L"
            },
            ordinal: function(a, b) {
                switch (b) {
                  case "d":
                  case "D":
                  case "DDD":
                    return a + "日";

                  case "M":
                    return a + "月";

                  case "w":
                  case "W":
                    return a + "週";

                  default:
                    return a;
                }
            },
            relativeTime: {
                future: "%s內",
                past: "%s前",
                s: "幾秒",
                m: "一分鐘",
                mm: "%d分鐘",
                h: "一小時",
                hh: "%d小時",
                d: "一天",
                dd: "%d天",
                M: "一個月",
                MM: "%d個月",
                y: "一年",
                yy: "%d年"
            }
        });
    }), ib.lang("en"), xb ? module.exports = ib : "function" == typeof define && define.amd ? (define("moment", function(a, b, c) {
        return c.config && c.config() && c.config().noGlobal === !0 && (mb.moment = jb), 
        ib;
    }), hb(!0)) : hb();
}).call(this);

(function(factory) {
    if (typeof module === "function") {
        module.exports = factory(this.jQuery || require("jquery"));
    } else {
        this.NProgress = factory(this.jQuery);
    }
})(function($) {
    var NProgress = {};
    NProgress.version = "0.1.2";
    var Settings = NProgress.settings = {
        minimum: .08,
        easing: "ease",
        positionUsing: "",
        speed: 200,
        trickle: true,
        trickleRate: .02,
        trickleSpeed: 800,
        showSpinner: true,
        template: '<div class="bar" role="bar"><div class="peg"></div></div><div class="spinner" role="spinner"><div class="spinner-icon"></div></div>'
    };
    NProgress.configure = function(options) {
        $.extend(Settings, options);
        return this;
    };
    NProgress.status = null;
    NProgress.set = function(n) {
        var started = NProgress.isStarted();
        n = clamp(n, Settings.minimum, 1);
        NProgress.status = n === 1 ? null : n;
        var $progress = NProgress.render(!started), $bar = $progress.find('[role="bar"]'), speed = Settings.speed, ease = Settings.easing;
        $progress[0].offsetWidth;
        $progress.queue(function(next) {
            if (Settings.positionUsing === "") Settings.positionUsing = NProgress.getPositioningCSS();
            $bar.css(barPositionCSS(n, speed, ease));
            if (n === 1) {
                $progress.css({
                    transition: "none",
                    opacity: 1
                });
                $progress[0].offsetWidth;
                setTimeout(function() {
                    $progress.css({
                        transition: "all " + speed + "ms linear",
                        opacity: 0
                    });
                    setTimeout(function() {
                        NProgress.remove();
                        next();
                    }, speed);
                }, speed);
            } else {
                setTimeout(next, speed);
            }
        });
        return this;
    };
    NProgress.isStarted = function() {
        return typeof NProgress.status === "number";
    };
    NProgress.start = function() {
        if (!NProgress.status) NProgress.set(0);
        var work = function() {
            setTimeout(function() {
                if (!NProgress.status) return;
                NProgress.trickle();
                work();
            }, Settings.trickleSpeed);
        };
        if (Settings.trickle) work();
        return this;
    };
    NProgress.done = function(force) {
        if (!force && !NProgress.status) return this;
        return NProgress.inc(.3 + .5 * Math.random()).set(1);
    };
    NProgress.inc = function(amount) {
        var n = NProgress.status;
        if (!n) {
            return NProgress.start();
        } else {
            if (typeof amount !== "number") {
                amount = (1 - n) * clamp(Math.random() * n, .1, .95);
            }
            n = clamp(n + amount, 0, .994);
            return NProgress.set(n);
        }
    };
    NProgress.trickle = function() {
        return NProgress.inc(Math.random() * Settings.trickleRate);
    };
    NProgress.render = function(fromStart) {
        if (NProgress.isRendered()) return $("#nprogress");
        $("html").addClass("nprogress-busy");
        var $el = $("<div id='nprogress'>").html(Settings.template);
        var perc = fromStart ? "-100" : toBarPerc(NProgress.status || 0);
        $el.find('[role="bar"]').css({
            transition: "all 0 linear",
            transform: "translate3d(" + perc + "%,0,0)"
        });
        if (!Settings.showSpinner) $el.find('[role="spinner"]').remove();
        $el.appendTo(document.body);
        return $el;
    };
    NProgress.remove = function() {
        $("html").removeClass("nprogress-busy");
        $("#nprogress").remove();
    };
    NProgress.isRendered = function() {
        return $("#nprogress").length > 0;
    };
    NProgress.getPositioningCSS = function() {
        var bodyStyle = document.body.style;
        var vendorPrefix = "WebkitTransform" in bodyStyle ? "Webkit" : "MozTransform" in bodyStyle ? "Moz" : "msTransform" in bodyStyle ? "ms" : "OTransform" in bodyStyle ? "O" : "";
        if (vendorPrefix + "Perspective" in bodyStyle) {
            return "translate3d";
        } else if (vendorPrefix + "Transform" in bodyStyle) {
            return "translate";
        } else {
            return "margin";
        }
    };
    function clamp(n, min, max) {
        if (n < min) return min;
        if (n > max) return max;
        return n;
    }
    function toBarPerc(n) {
        return (-1 + n) * 100;
    }
    function barPositionCSS(n, speed, ease) {
        var barCSS;
        if (Settings.positionUsing === "translate3d") {
            barCSS = {
                transform: "translate3d(" + toBarPerc(n) + "%,0,0)"
            };
        } else if (Settings.positionUsing === "translate") {
            barCSS = {
                transform: "translate(" + toBarPerc(n) + "%,0)"
            };
        } else {
            barCSS = {
                "margin-left": toBarPerc(n) + "%"
            };
        }
        barCSS.transition = "all " + speed + "ms " + ease;
        return barCSS;
    }
    return NProgress;
});

(function($) {
    var smartPhone = window.orientation != undefined;
    var DateTimePicker = function(element, options) {
        this.id = dpgId++;
        this.init(element, options);
    };
    var dateToDate = function(dt) {
        if (typeof dt === "string") {
            return new Date(dt);
        }
        return dt;
    };
    DateTimePicker.prototype = {
        constructor: DateTimePicker,
        init: function(element, options) {
            var icon;
            if (!(options.pickTime || options.pickDate)) throw new Error("Must choose at least one picker");
            this.options = options;
            this.$element = $(element);
            this.language = options.language in dates ? options.language : "en";
            this.pickDate = options.pickDate;
            this.pickTime = options.pickTime;
            this.isInput = this.$element.is("input");
            this.component = false;
            if (this.$element.find(".input-append") || this.$element.find(".input-prepend")) this.component = this.$element.find(".add-on");
            this.format = options.format;
            if (!this.format) {
                if (this.isInput) this.format = this.$element.data("format"); else this.format = this.$element.find("input").data("format");
                if (!this.format) this.format = "MM/dd/yyyy";
            }
            this._compileFormat();
            if (this.component) {
                icon = this.component.find("i");
            }
            if (this.pickTime) {
                if (icon && icon.length) this.timeIcon = icon.data("time-icon");
                if (!this.timeIcon) this.timeIcon = "icon-time";
                icon.addClass(this.timeIcon);
            }
            if (this.pickDate) {
                if (icon && icon.length) this.dateIcon = icon.data("date-icon");
                if (!this.dateIcon) this.dateIcon = "icon-calendar";
                icon.removeClass(this.timeIcon);
                icon.addClass(this.dateIcon);
            }
            this.widget = $(getTemplate(this.timeIcon, options.pickDate, options.pickTime, options.pick12HourFormat, options.pickSeconds, options.collapse)).appendTo("body");
            this.minViewMode = options.minViewMode || this.$element.data("date-minviewmode") || 0;
            if (typeof this.minViewMode === "string") {
                switch (this.minViewMode) {
                  case "months":
                    this.minViewMode = 1;
                    break;

                  case "years":
                    this.minViewMode = 2;
                    break;

                  default:
                    this.minViewMode = 0;
                    break;
                }
            }
            this.viewMode = options.viewMode || this.$element.data("date-viewmode") || 0;
            if (typeof this.viewMode === "string") {
                switch (this.viewMode) {
                  case "months":
                    this.viewMode = 1;
                    break;

                  case "years":
                    this.viewMode = 2;
                    break;

                  default:
                    this.viewMode = 0;
                    break;
                }
            }
            this.startViewMode = this.viewMode;
            this.weekStart = options.weekStart || this.$element.data("date-weekstart") || 0;
            this.weekEnd = this.weekStart === 0 ? 6 : this.weekStart - 1;
            this.setStartDate(options.startDate || this.$element.data("date-startdate"));
            this.setEndDate(options.endDate || this.$element.data("date-enddate"));
            this.fillDow();
            this.fillMonths();
            this.fillHours();
            this.fillMinutes();
            this.fillSeconds();
            this.update();
            this.showMode();
            this._attachDatePickerEvents();
        },
        show: function(e) {
            this.widget.show();
            this.height = this.component ? this.component.outerHeight() : this.$element.outerHeight();
            this.place();
            this.$element.trigger({
                type: "show",
                date: this._date
            });
            this._attachDatePickerGlobalEvents();
            if (e) {
                e.stopPropagation();
                e.preventDefault();
            }
        },
        disable: function() {
            this.$element.find("input").prop("disabled", true);
            this._detachDatePickerEvents();
        },
        enable: function() {
            this.$element.find("input").prop("disabled", false);
            this._attachDatePickerEvents();
        },
        hide: function() {
            var collapse = this.widget.find(".collapse");
            for (var i = 0; i < collapse.length; i++) {
                var collapseData = collapse.eq(i).data("collapse");
                if (collapseData && collapseData.transitioning) return;
            }
            this.widget.hide();
            this.viewMode = this.startViewMode;
            this.showMode();
            this.set();
            this.$element.trigger({
                type: "hide",
                date: this._date
            });
            this._detachDatePickerGlobalEvents();
        },
        set: function() {
            var formatted = "";
            if (!this._unset) formatted = this.formatDate(this._date);
            if (!this.isInput) {
                if (this.component) {
                    var input = this.$element.find("input");
                    input.val(formatted);
                    this._resetMaskPos(input);
                }
                this.$element.data("date", formatted);
            } else {
                this.$element.val(formatted);
                this._resetMaskPos(this.$element);
            }
        },
        setValue: function(newDate) {
            if (!newDate) {
                this._unset = true;
            } else {
                this._unset = false;
            }
            if (typeof newDate === "string") {
                this._date = this.parseDate(newDate);
            } else if (newDate) {
                this._date = new Date(newDate);
            }
            this.set();
            this.viewDate = UTCDate(this._date.getUTCFullYear(), this._date.getUTCMonth(), 1, 0, 0, 0, 0);
            this.fillDate();
            this.fillTime();
        },
        getDate: function() {
            if (this._unset) return null;
            return new Date(this._date.valueOf());
        },
        setDate: function(date) {
            if (!date) this.setValue(null); else this.setValue(date.valueOf());
        },
        setStartDate: function(date) {
            if (date instanceof Date) {
                this.startDate = date;
            } else if (typeof date === "string") {
                this.startDate = new UTCDate(date);
                if (!this.startDate.getUTCFullYear()) {
                    this.startDate = -Infinity;
                }
            } else {
                this.startDate = -Infinity;
            }
            if (this.viewDate) {
                this.update();
            }
        },
        setEndDate: function(date) {
            if (date instanceof Date) {
                this.endDate = date;
            } else if (typeof date === "string") {
                this.endDate = new UTCDate(date);
                if (!this.endDate.getUTCFullYear()) {
                    this.endDate = Infinity;
                }
            } else {
                this.endDate = Infinity;
            }
            if (this.viewDate) {
                this.update();
            }
        },
        getLocalDate: function() {
            if (this._unset) return null;
            var d = this._date;
            return new Date(d.getUTCFullYear(), d.getUTCMonth(), d.getUTCDate(), d.getUTCHours(), d.getUTCMinutes(), d.getUTCSeconds(), d.getUTCMilliseconds());
        },
        setLocalDate: function(localDate) {
            if (!localDate) this.setValue(null); else this.setValue(Date.UTC(localDate.getFullYear(), localDate.getMonth(), localDate.getDate(), localDate.getHours(), localDate.getMinutes(), localDate.getSeconds(), localDate.getMilliseconds()));
        },
        place: function() {
            var position = "absolute";
            var offset = this.component ? this.component.offset() : this.$element.offset();
            this.width = this.component ? this.component.outerWidth() : this.$element.outerWidth();
            offset.top = offset.top + this.height;
            var $window = $(window);
            if (this.options.width != undefined) {
                this.widget.width(this.options.width);
            }
            if (this.options.orientation == "left") {
                this.widget.addClass("left-oriented");
                offset.left = offset.left - this.widget.width() + 20;
            }
            if (this._isInFixed()) {
                position = "fixed";
                offset.top -= $window.scrollTop();
                offset.left -= $window.scrollLeft();
            }
            if ($window.width() < offset.left + this.widget.outerWidth()) {
                offset.right = $window.width() - offset.left - this.width;
                offset.left = "auto";
                this.widget.addClass("pull-right");
            } else {
                offset.right = "auto";
                this.widget.removeClass("pull-right");
            }
            this.widget.css({
                position: position,
                top: offset.top,
                left: offset.left,
                right: offset.right
            });
        },
        notifyChange: function() {
            this.$element.trigger({
                type: "changeDate",
                date: this.getDate(),
                localDate: this.getLocalDate()
            });
        },
        update: function(newDate) {
            var dateStr = newDate;
            if (!dateStr) {
                if (this.isInput) {
                    dateStr = this.$element.val();
                } else {
                    dateStr = this.$element.find("input").val();
                }
                if (dateStr) {
                    this._date = this.parseDate(dateStr);
                }
                if (!this._date) {
                    var tmp = new Date();
                    this._date = UTCDate(tmp.getFullYear(), tmp.getMonth(), tmp.getDate(), tmp.getHours(), tmp.getMinutes(), tmp.getSeconds(), tmp.getMilliseconds());
                }
            }
            this.viewDate = UTCDate(this._date.getUTCFullYear(), this._date.getUTCMonth(), 1, 0, 0, 0, 0);
            this.fillDate();
            this.fillTime();
        },
        fillDow: function() {
            var dowCnt = this.weekStart;
            var html = $("<tr>");
            while (dowCnt < this.weekStart + 7) {
                html.append('<th class="dow">' + dates[this.language].daysMin[dowCnt++ % 7] + "</th>");
            }
            this.widget.find(".datepicker-days thead").append(html);
        },
        fillMonths: function() {
            var html = "";
            var i = 0;
            while (i < 12) {
                html += '<span class="month">' + dates[this.language].monthsShort[i++] + "</span>";
            }
            this.widget.find(".datepicker-months td").append(html);
        },
        fillDate: function() {
            var year = this.viewDate.getUTCFullYear();
            var month = this.viewDate.getUTCMonth();
            var currentDate = UTCDate(this._date.getUTCFullYear(), this._date.getUTCMonth(), this._date.getUTCDate(), 0, 0, 0, 0);
            var startYear = typeof this.startDate === "object" ? this.startDate.getUTCFullYear() : -Infinity;
            var startMonth = typeof this.startDate === "object" ? this.startDate.getUTCMonth() : -1;
            var endYear = typeof this.endDate === "object" ? this.endDate.getUTCFullYear() : Infinity;
            var endMonth = typeof this.endDate === "object" ? this.endDate.getUTCMonth() : 12;
            this.widget.find(".datepicker-days").find(".disabled").removeClass("disabled");
            this.widget.find(".datepicker-months").find(".disabled").removeClass("disabled");
            this.widget.find(".datepicker-years").find(".disabled").removeClass("disabled");
            this.widget.find(".datepicker-days th:eq(1)").text(dates[this.language].months[month] + " " + year);
            var prevMonth = UTCDate(year, month - 1, 28, 0, 0, 0, 0);
            var day = DPGlobal.getDaysInMonth(prevMonth.getUTCFullYear(), prevMonth.getUTCMonth());
            prevMonth.setUTCDate(day);
            prevMonth.setUTCDate(day - (prevMonth.getUTCDay() - this.weekStart + 7) % 7);
            if (year == startYear && month <= startMonth || year < startYear) {
                this.widget.find(".datepicker-days th:eq(0)").addClass("disabled");
            }
            if (year == endYear && month >= endMonth || year > endYear) {
                this.widget.find(".datepicker-days th:eq(2)").addClass("disabled");
            }
            var nextMonth = new Date(prevMonth.valueOf());
            nextMonth.setUTCDate(nextMonth.getUTCDate() + 42);
            nextMonth = nextMonth.valueOf();
            var html = [];
            var row;
            var clsName;
            while (prevMonth.valueOf() < nextMonth) {
                if (prevMonth.getUTCDay() === this.weekStart) {
                    row = $("<tr>");
                    html.push(row);
                }
                clsName = "";
                if (prevMonth.getUTCFullYear() < year || prevMonth.getUTCFullYear() == year && prevMonth.getUTCMonth() < month) {
                    clsName += " old";
                } else if (prevMonth.getUTCFullYear() > year || prevMonth.getUTCFullYear() == year && prevMonth.getUTCMonth() > month) {
                    clsName += " new";
                }
                if (prevMonth.valueOf() === currentDate.valueOf()) {
                    clsName += " active";
                }
                if (prevMonth.valueOf() + 864e5 <= this.startDate) {
                    clsName += " disabled";
                }
                if (prevMonth.valueOf() > this.endDate) {
                    clsName += " disabled";
                }
                row.append('<td class="day' + clsName + '">' + prevMonth.getUTCDate() + "</td>");
                prevMonth.setUTCDate(prevMonth.getUTCDate() + 1);
            }
            this.widget.find(".datepicker-days tbody").empty().append(html);
            var currentYear = this._date.getUTCFullYear();
            var months = this.widget.find(".datepicker-months").find("th:eq(1)").text(year).end().find("span").removeClass("active");
            if (currentYear === year) {
                months.eq(this._date.getUTCMonth()).addClass("active");
            }
            if (currentYear - 1 < startYear) {
                this.widget.find(".datepicker-months th:eq(0)").addClass("disabled");
            }
            if (currentYear + 1 > endYear) {
                this.widget.find(".datepicker-months th:eq(2)").addClass("disabled");
            }
            for (var i = 0; i < 12; i++) {
                if (year == startYear && startMonth > i || year < startYear) {
                    $(months[i]).addClass("disabled");
                } else if (year == endYear && endMonth < i || year > endYear) {
                    $(months[i]).addClass("disabled");
                }
            }
            html = "";
            year = parseInt(year / 10, 10) * 10;
            var yearCont = this.widget.find(".datepicker-years").find("th:eq(1)").text(year + "-" + (year + 9)).end().find("td");
            this.widget.find(".datepicker-years").find("th").removeClass("disabled");
            if (startYear > year) {
                this.widget.find(".datepicker-years").find("th:eq(0)").addClass("disabled");
            }
            if (endYear < year + 9) {
                this.widget.find(".datepicker-years").find("th:eq(2)").addClass("disabled");
            }
            year -= 1;
            for (var i = -1; i < 11; i++) {
                html += '<span class="year' + (i === -1 || i === 10 ? " old" : "") + (currentYear === year ? " active" : "") + (year < startYear || year > endYear ? " disabled" : "") + '">' + year + "</span>";
                year += 1;
            }
            yearCont.html(html);
        },
        fillHours: function() {
            var table = this.widget.find(".timepicker .timepicker-hours table");
            table.parent().hide();
            var html = "";
            if (this.options.pick12HourFormat) {
                var current = 1;
                for (var i = 0; i < 3; i += 1) {
                    html += "<tr>";
                    for (var j = 0; j < 4; j += 1) {
                        var c = current.toString();
                        html += '<td class="hour">' + padLeft(c, 2, "0") + "</td>";
                        current++;
                    }
                    html += "</tr>";
                }
            } else {
                var current = 0;
                for (var i = 0; i < 6; i += 1) {
                    html += "<tr>";
                    for (var j = 0; j < 4; j += 1) {
                        var c = current.toString();
                        html += '<td class="hour">' + padLeft(c, 2, "0") + "</td>";
                        current++;
                    }
                    html += "</tr>";
                }
            }
            table.html(html);
        },
        fillMinutes: function() {
            var table = this.widget.find(".timepicker .timepicker-minutes table");
            table.parent().hide();
            var html = "";
            var current = 0;
            for (var i = 0; i < 5; i++) {
                html += "<tr>";
                for (var j = 0; j < 4; j += 1) {
                    var c = current.toString();
                    html += '<td class="minute">' + padLeft(c, 2, "0") + "</td>";
                    current += 3;
                }
                html += "</tr>";
            }
            table.html(html);
        },
        fillSeconds: function() {
            var table = this.widget.find(".timepicker .timepicker-seconds table");
            table.parent().hide();
            var html = "";
            var current = 0;
            for (var i = 0; i < 5; i++) {
                html += "<tr>";
                for (var j = 0; j < 4; j += 1) {
                    var c = current.toString();
                    html += '<td class="second">' + padLeft(c, 2, "0") + "</td>";
                    current += 3;
                }
                html += "</tr>";
            }
            table.html(html);
        },
        fillTime: function() {
            if (!this._date) return;
            var timeComponents = this.widget.find(".timepicker span[data-time-component]");
            var table = timeComponents.closest("table");
            var is12HourFormat = this.options.pick12HourFormat;
            var hour = this._date.getUTCHours();
            var period = "AM";
            if (is12HourFormat) {
                if (hour >= 12) period = "PM";
                if (hour === 0) hour = 12; else if (hour != 12) hour = hour % 12;
                this.widget.find(".timepicker [data-action=togglePeriod]").text(period);
            }
            hour = padLeft(hour.toString(), 2, "0");
            var minute = padLeft(this._date.getUTCMinutes().toString(), 2, "0");
            var second = padLeft(this._date.getUTCSeconds().toString(), 2, "0");
            timeComponents.filter("[data-time-component=hours]").text(hour);
            timeComponents.filter("[data-time-component=minutes]").text(minute);
            timeComponents.filter("[data-time-component=seconds]").text(second);
        },
        click: function(e) {
            e.stopPropagation();
            e.preventDefault();
            this._unset = false;
            var target = $(e.target).closest("span, td, th");
            if (target.length === 1) {
                if (!target.is(".disabled")) {
                    switch (target[0].nodeName.toLowerCase()) {
                      case "th":
                        switch (target[0].className) {
                          case "switch":
                            this.showMode(1);
                            break;

                          case "prev":
                          case "next":
                            var vd = this.viewDate;
                            var navFnc = DPGlobal.modes[this.viewMode].navFnc;
                            var step = DPGlobal.modes[this.viewMode].navStep;
                            if (target[0].className === "prev") step = step * -1;
                            vd["set" + navFnc](vd["get" + navFnc]() + step);
                            this.fillDate();
                            this.set();
                            break;
                        }
                        break;

                      case "span":
                        if (target.is(".month")) {
                            var month = target.parent().find("span").index(target);
                            this.viewDate.setUTCMonth(month);
                        } else {
                            var year = parseInt(target.text(), 10) || 0;
                            this.viewDate.setUTCFullYear(year);
                        }
                        if (this.viewMode !== 0) {
                            this._date = UTCDate(this.viewDate.getUTCFullYear(), this.viewDate.getUTCMonth(), this.viewDate.getUTCDate(), this._date.getUTCHours(), this._date.getUTCMinutes(), this._date.getUTCSeconds(), this._date.getUTCMilliseconds());
                            this.notifyChange();
                        }
                        this.showMode(-1);
                        this.fillDate();
                        this.set();
                        break;

                      case "td":
                        if (target.is(".day")) {
                            var day = parseInt(target.text(), 10) || 1;
                            var month = this.viewDate.getUTCMonth();
                            var year = this.viewDate.getUTCFullYear();
                            if (target.is(".old")) {
                                if (month === 0) {
                                    month = 11;
                                    year -= 1;
                                } else {
                                    month -= 1;
                                }
                            } else if (target.is(".new")) {
                                if (month == 11) {
                                    month = 0;
                                    year += 1;
                                } else {
                                    month += 1;
                                }
                            }
                            this._date = UTCDate(year, month, day, this._date.getUTCHours(), this._date.getUTCMinutes(), this._date.getUTCSeconds(), this._date.getUTCMilliseconds());
                            this.viewDate = UTCDate(year, month, Math.min(28, day), 0, 0, 0, 0);
                            this.fillDate();
                            this.set();
                            this.notifyChange();
                        }
                        break;
                    }
                }
            }
        },
        actions: {
            incrementHours: function(e) {
                this._date.setUTCHours(this._date.getUTCHours() + 1);
            },
            incrementMinutes: function(e) {
                this._date.setUTCMinutes(this._date.getUTCMinutes() + 1);
            },
            incrementSeconds: function(e) {
                this._date.setUTCSeconds(this._date.getUTCSeconds() + 1);
            },
            decrementHours: function(e) {
                this._date.setUTCHours(this._date.getUTCHours() - 1);
            },
            decrementMinutes: function(e) {
                this._date.setUTCMinutes(this._date.getUTCMinutes() - 1);
            },
            decrementSeconds: function(e) {
                this._date.setUTCSeconds(this._date.getUTCSeconds() - 1);
            },
            togglePeriod: function(e) {
                var hour = this._date.getUTCHours();
                if (hour >= 12) hour -= 12; else hour += 12;
                this._date.setUTCHours(hour);
            },
            showPicker: function() {
                this.widget.find(".timepicker > div:not(.timepicker-picker)").hide();
                this.widget.find(".timepicker .timepicker-picker").show();
            },
            showHours: function() {
                this.widget.find(".timepicker .timepicker-picker").hide();
                this.widget.find(".timepicker .timepicker-hours").show();
            },
            showMinutes: function() {
                this.widget.find(".timepicker .timepicker-picker").hide();
                this.widget.find(".timepicker .timepicker-minutes").show();
            },
            showSeconds: function() {
                this.widget.find(".timepicker .timepicker-picker").hide();
                this.widget.find(".timepicker .timepicker-seconds").show();
            },
            selectHour: function(e) {
                var tgt = $(e.target);
                var value = parseInt(tgt.text(), 10);
                if (this.options.pick12HourFormat) {
                    var current = this._date.getUTCHours();
                    if (current >= 12) {
                        if (value != 12) value = (value + 12) % 24;
                    } else {
                        if (value === 12) value = 0; else value = value % 12;
                    }
                }
                this._date.setUTCHours(value);
                this.actions.showPicker.call(this);
            },
            selectMinute: function(e) {
                var tgt = $(e.target);
                var value = parseInt(tgt.text(), 10);
                this._date.setUTCMinutes(value);
                this.actions.showPicker.call(this);
            },
            selectSecond: function(e) {
                var tgt = $(e.target);
                var value = parseInt(tgt.text(), 10);
                this._date.setUTCSeconds(value);
                this.actions.showPicker.call(this);
            }
        },
        doAction: function(e) {
            e.stopPropagation();
            e.preventDefault();
            if (!this._date) this._date = UTCDate(1970, 0, 0, 0, 0, 0, 0);
            var action = $(e.currentTarget).data("action");
            var rv = this.actions[action].apply(this, arguments);
            this.set();
            this.fillTime();
            this.notifyChange();
            return rv;
        },
        stopEvent: function(e) {
            e.stopPropagation();
            e.preventDefault();
        },
        keydown: function(e) {
            var self = this, k = e.which, input = $(e.target);
            if (k == 8 || k == 46) {
                setTimeout(function() {
                    self._resetMaskPos(input);
                });
            }
        },
        keypress: function(e) {
            var k = e.which;
            if (k == 8 || k == 46) {
                return;
            }
            var input = $(e.target);
            var c = String.fromCharCode(k);
            var val = input.val() || "";
            val += c;
            var mask = this._mask[this._maskPos];
            if (!mask) {
                return false;
            }
            if (mask.end != val.length) {
                return;
            }
            if (!mask.pattern.test(val.slice(mask.start))) {
                val = val.slice(0, val.length - 1);
                while ((mask = this._mask[this._maskPos]) && mask.character) {
                    val += mask.character;
                    this._maskPos++;
                }
                val += c;
                if (mask.end != val.length) {
                    input.val(val);
                    return false;
                } else {
                    if (!mask.pattern.test(val.slice(mask.start))) {
                        input.val(val.slice(0, mask.start));
                        return false;
                    } else {
                        input.val(val);
                        this._maskPos++;
                        return false;
                    }
                }
            } else {
                this._maskPos++;
            }
        },
        change: function(e) {
            var input = $(e.target);
            var val = input.val();
            if (this._formatPattern.test(val)) {
                this.update();
                this.setValue(this._date.getTime());
                this.notifyChange();
                this.set();
            } else if (val && val.trim()) {
                this.setValue(this._date.getTime());
                if (this._date) this.set(); else input.val("");
            } else {
                if (this._date) {
                    this.setValue(null);
                    this.notifyChange();
                    this._unset = true;
                }
            }
            this._resetMaskPos(input);
        },
        showMode: function(dir) {
            if (dir) {
                this.viewMode = Math.max(this.minViewMode, Math.min(2, this.viewMode + dir));
            }
            this.widget.find(".datepicker > div").hide().filter(".datepicker-" + DPGlobal.modes[this.viewMode].clsName).show();
        },
        destroy: function() {
            this._detachDatePickerEvents();
            this._detachDatePickerGlobalEvents();
            this.widget.remove();
            this.$element.removeData("datetimepicker");
            this.component.removeData("datetimepicker");
        },
        formatDate: function(d) {
            return this.format.replace(formatReplacer, function(match) {
                var methodName, property, rv, len = match.length;
                if (match === "ms") len = 1;
                property = dateFormatComponents[match].property;
                if (property === "Hours12") {
                    rv = d.getUTCHours();
                    if (rv === 0) rv = 12; else if (rv !== 12) rv = rv % 12;
                } else if (property === "Period12") {
                    if (d.getUTCHours() >= 12) return "PM"; else return "AM";
                } else {
                    methodName = "get" + property;
                    rv = d[methodName]();
                }
                if (methodName === "getUTCMonth") rv = rv + 1;
                if (methodName === "getUTCYear") rv = rv + 1900 - 2e3;
                return padLeft(rv.toString(), len, "0");
            });
        },
        parseDate: function(str) {
            var match, i, property, methodName, value, parsed = {};
            if (!(match = this._formatPattern.exec(str))) return null;
            for (i = 1; i < match.length; i++) {
                property = this._propertiesByIndex[i];
                if (!property) continue;
                value = match[i];
                if (/^\d+$/.test(value)) value = parseInt(value, 10);
                parsed[property] = value;
            }
            return this._finishParsingDate(parsed);
        },
        _resetMaskPos: function(input) {
            var val = input.val();
            for (var i = 0; i < this._mask.length; i++) {
                if (this._mask[i].end > val.length) {
                    this._maskPos = i;
                    break;
                } else if (this._mask[i].end === val.length) {
                    this._maskPos = i + 1;
                    break;
                }
            }
        },
        _finishParsingDate: function(parsed) {
            var year, month, date, hours, minutes, seconds, milliseconds;
            year = parsed.UTCFullYear;
            if (parsed.UTCYear) year = 2e3 + parsed.UTCYear;
            if (!year) year = 1970;
            if (parsed.UTCMonth) month = parsed.UTCMonth - 1; else month = 0;
            date = parsed.UTCDate || 1;
            hours = parsed.UTCHours || 0;
            minutes = parsed.UTCMinutes || 0;
            seconds = parsed.UTCSeconds || 0;
            milliseconds = parsed.UTCMilliseconds || 0;
            if (parsed.Hours12) {
                hours = parsed.Hours12;
            }
            if (parsed.Period12) {
                if (/pm/i.test(parsed.Period12)) {
                    if (hours != 12) hours = (hours + 12) % 24;
                } else {
                    hours = hours % 12;
                }
            }
            return UTCDate(year, month, date, hours, minutes, seconds, milliseconds);
        },
        _compileFormat: function() {
            var match, component, components = [], mask = [], str = this.format, propertiesByIndex = {}, i = 0, pos = 0;
            while (match = formatComponent.exec(str)) {
                component = match[0];
                if (component in dateFormatComponents) {
                    i++;
                    propertiesByIndex[i] = dateFormatComponents[component].property;
                    components.push("\\s*" + dateFormatComponents[component].getPattern(this) + "\\s*");
                    mask.push({
                        pattern: new RegExp(dateFormatComponents[component].getPattern(this)),
                        property: dateFormatComponents[component].property,
                        start: pos,
                        end: pos += component.length
                    });
                } else {
                    components.push(escapeRegExp(component));
                    mask.push({
                        pattern: new RegExp(escapeRegExp(component)),
                        character: component,
                        start: pos,
                        end: ++pos
                    });
                }
                str = str.slice(component.length);
            }
            this._mask = mask;
            this._maskPos = 0;
            this._formatPattern = new RegExp("^\\s*" + components.join("") + "\\s*$");
            this._propertiesByIndex = propertiesByIndex;
        },
        _attachDatePickerEvents: function() {
            var self = this;
            this.widget.on("click", ".datepicker *", $.proxy(this.click, this));
            this.widget.on("click", "[data-action]", $.proxy(this.doAction, this));
            this.widget.on("mousedown", $.proxy(this.stopEvent, this));
            if (this.pickDate && this.pickTime) {
                this.widget.on("click.togglePicker", ".accordion-toggle", function(e) {
                    e.stopPropagation();
                    var $this = $(this);
                    var $parent = $this.closest("ul");
                    var expanded = $parent.find(".collapse.in");
                    var closed = $parent.find(".collapse:not(.in)");
                    if (expanded && expanded.length) {
                        var collapseData = expanded.data("collapse");
                        if (collapseData && collapseData.transitioning) return;
                        expanded.collapse("hide");
                        closed.collapse("show");
                        $this.find("i").toggleClass(self.timeIcon + " " + self.dateIcon);
                        self.$element.find(".add-on i").toggleClass(self.timeIcon + " " + self.dateIcon);
                    }
                });
            }
            if (this.isInput) {
                this.$element.on({
                    focus: $.proxy(this.show, this),
                    change: $.proxy(this.change, this)
                });
                if (this.options.maskInput) {
                    this.$element.on({
                        keydown: $.proxy(this.keydown, this),
                        keypress: $.proxy(this.keypress, this)
                    });
                }
            } else {
                this.$element.on({
                    change: $.proxy(this.change, this)
                }, "input");
                if (this.options.maskInput) {
                    this.$element.on({
                        keydown: $.proxy(this.keydown, this),
                        keypress: $.proxy(this.keypress, this)
                    }, "input");
                }
                if (this.component) {
                    this.component.on("click", $.proxy(this.show, this));
                } else {
                    this.$element.on("click", $.proxy(this.show, this));
                }
            }
        },
        _attachDatePickerGlobalEvents: function() {
            $(window).on("resize.datetimepicker" + this.id, $.proxy(this.place, this));
            if (!this.isInput) {
                $(document).on("mousedown.datetimepicker" + this.id, $.proxy(this.hide, this));
            }
        },
        _detachDatePickerEvents: function() {
            this.widget.off("click", ".datepicker *", this.click);
            this.widget.off("click", "[data-action]");
            this.widget.off("mousedown", this.stopEvent);
            if (this.pickDate && this.pickTime) {
                this.widget.off("click.togglePicker");
            }
            if (this.isInput) {
                this.$element.off({
                    focus: this.show,
                    change: this.change
                });
                if (this.options.maskInput) {
                    this.$element.off({
                        keydown: this.keydown,
                        keypress: this.keypress
                    });
                }
            } else {
                this.$element.off({
                    change: this.change
                }, "input");
                if (this.options.maskInput) {
                    this.$element.off({
                        keydown: this.keydown,
                        keypress: this.keypress
                    }, "input");
                }
                if (this.component) {
                    this.component.off("click", this.show);
                } else {
                    this.$element.off("click", this.show);
                }
            }
        },
        _detachDatePickerGlobalEvents: function() {
            $(window).off("resize.datetimepicker" + this.id);
            if (!this.isInput) {
                $(document).off("mousedown.datetimepicker" + this.id);
            }
        },
        _isInFixed: function() {
            if (this.$element) {
                var parents = this.$element.parents();
                var inFixed = false;
                for (var i = 0; i < parents.length; i++) {
                    if ($(parents[i]).css("position") == "fixed") {
                        inFixed = true;
                        break;
                    }
                }
                return inFixed;
            } else {
                return false;
            }
        }
    };
    $.fn.datetimepicker = function(option, val) {
        return this.each(function() {
            var $this = $(this), data = $this.data("datetimepicker"), options = typeof option === "object" && option;
            if (!data) {
                $this.data("datetimepicker", data = new DateTimePicker(this, $.extend({}, $.fn.datetimepicker.defaults, options)));
            }
            if (typeof option === "string") data[option](val);
        });
    };
    $.fn.datetimepicker.defaults = {
        maskInput: false,
        pickDate: true,
        pickTime: true,
        pick12HourFormat: false,
        pickSeconds: true,
        startDate: -Infinity,
        endDate: Infinity,
        collapse: true
    };
    $.fn.datetimepicker.Constructor = DateTimePicker;
    var dpgId = 0;
    var dates = $.fn.datetimepicker.dates = {
        en: {
            days: [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" ],
            daysShort: [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" ],
            daysMin: [ "Su", "Mo", "Tu", "We", "Th", "Fr", "Sa", "Su" ],
            months: [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ],
            monthsShort: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ]
        }
    };
    var dateFormatComponents = {
        dd: {
            property: "UTCDate",
            getPattern: function() {
                return "(0?[1-9]|[1-2][0-9]|3[0-1])\\b";
            }
        },
        MM: {
            property: "UTCMonth",
            getPattern: function() {
                return "(0?[1-9]|1[0-2])\\b";
            }
        },
        yy: {
            property: "UTCYear",
            getPattern: function() {
                return "(\\d{2})\\b";
            }
        },
        yyyy: {
            property: "UTCFullYear",
            getPattern: function() {
                return "(\\d{4})\\b";
            }
        },
        hh: {
            property: "UTCHours",
            getPattern: function() {
                return "(0?[0-9]|1[0-9]|2[0-3])\\b";
            }
        },
        mm: {
            property: "UTCMinutes",
            getPattern: function() {
                return "(0?[0-9]|[1-5][0-9])\\b";
            }
        },
        ss: {
            property: "UTCSeconds",
            getPattern: function() {
                return "(0?[0-9]|[1-5][0-9])\\b";
            }
        },
        ms: {
            property: "UTCMilliseconds",
            getPattern: function() {
                return "([0-9]{1,3})\\b";
            }
        },
        HH: {
            property: "Hours12",
            getPattern: function() {
                return "(0?[1-9]|1[0-2])\\b";
            }
        },
        PP: {
            property: "Period12",
            getPattern: function() {
                return "(AM|PM|am|pm|Am|aM|Pm|pM)\\b";
            }
        }
    };
    var keys = [];
    for (var k in dateFormatComponents) keys.push(k);
    keys[keys.length - 1] += "\\b";
    keys.push(".");
    var formatComponent = new RegExp(keys.join("\\b|"));
    keys.pop();
    var formatReplacer = new RegExp(keys.join("\\b|"), "g");
    function escapeRegExp(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
    }
    function padLeft(s, l, c) {
        if (l < s.length) return s; else return Array(l - s.length + 1).join(c || " ") + s;
    }
    function getTemplate(timeIcon, pickDate, pickTime, is12Hours, showSeconds, collapse) {
        if (pickDate && pickTime) {
            return '<div class="bootstrap-datetimepicker-widget dropdown-menu">' + "<ul>" + "<li" + (collapse ? ' class="collapse in"' : "") + ">" + '<div class="datepicker">' + DPGlobal.template + "</div>" + "</li>" + '<li class="picker-switch accordion-toggle"><a><i class="' + timeIcon + '"></i></a></li>' + "<li" + (collapse ? ' class="collapse"' : "") + ">" + '<div class="timepicker">' + TPGlobal.getTemplate(is12Hours, showSeconds) + "</div>" + "</li>" + "</ul>" + "</div>";
        } else if (pickTime) {
            return '<div class="bootstrap-datetimepicker-widget dropdown-menu">' + '<div class="timepicker">' + TPGlobal.getTemplate(is12Hours, showSeconds) + "</div>" + "</div>";
        } else {
            return '<div class="bootstrap-datetimepicker-widget dropdown-menu">' + '<div class="datepicker">' + DPGlobal.template + "</div>" + "</div>";
        }
    }
    function UTCDate() {
        return new Date(Date.UTC.apply(Date, arguments));
    }
    var DPGlobal = {
        modes: [ {
            clsName: "days",
            navFnc: "UTCMonth",
            navStep: 1
        }, {
            clsName: "months",
            navFnc: "UTCFullYear",
            navStep: 1
        }, {
            clsName: "years",
            navFnc: "UTCFullYear",
            navStep: 10
        } ],
        isLeapYear: function(year) {
            return year % 4 === 0 && year % 100 !== 0 || year % 400 === 0;
        },
        getDaysInMonth: function(year, month) {
            return [ 31, DPGlobal.isLeapYear(year) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ][month];
        },
        headTemplate: "<thead>" + "<tr>" + '<th class="prev">&lsaquo;</th>' + '<th colspan="5" class="switch"></th>' + '<th class="next">&rsaquo;</th>' + "</tr>" + "</thead>",
        contTemplate: '<tbody><tr><td colspan="7"></td></tr></tbody>'
    };
    DPGlobal.template = '<div class="datepicker-days">' + '<table class="table-condensed">' + DPGlobal.headTemplate + "<tbody></tbody>" + "</table>" + "</div>" + '<div class="datepicker-months">' + '<table class="table-condensed">' + DPGlobal.headTemplate + DPGlobal.contTemplate + "</table>" + "</div>" + '<div class="datepicker-years">' + '<table class="table-condensed">' + DPGlobal.headTemplate + DPGlobal.contTemplate + "</table>" + "</div>";
    var TPGlobal = {
        hourTemplate: '<span data-action="showHours" data-time-component="hours" class="timepicker-hour"></span>',
        minuteTemplate: '<span data-action="showMinutes" data-time-component="minutes" class="timepicker-minute"></span>',
        secondTemplate: '<span data-action="showSeconds" data-time-component="seconds" class="timepicker-second"></span>'
    };
    TPGlobal.getTemplate = function(is12Hours, showSeconds) {
        return '<div class="timepicker-picker">' + '<table class="table-condensed"' + (is12Hours ? ' data-hour-format="12"' : "") + ">" + "<tr>" + '<td><a href="#" class="btn" data-action="incrementHours"><i class="icon-chevron-up"></i></a></td>' + '<td class="separator"></td>' + '<td><a href="#" class="btn" data-action="incrementMinutes"><i class="icon-chevron-up"></i></a></td>' + (showSeconds ? '<td class="separator"></td>' + '<td><a href="#" class="btn" data-action="incrementSeconds"><i class="icon-chevron-up"></i></a></td>' : "") + (is12Hours ? '<td class="separator"></td>' : "") + "</tr>" + "<tr>" + "<td>" + TPGlobal.hourTemplate + "</td> " + '<td class="separator">:</td>' + "<td>" + TPGlobal.minuteTemplate + "</td> " + (showSeconds ? '<td class="separator">:</td>' + "<td>" + TPGlobal.secondTemplate + "</td>" : "") + (is12Hours ? '<td class="separator"></td>' + "<td>" + '<button type="button" class="btn btn-primary" data-action="togglePeriod"></button>' + "</td>" : "") + "</tr>" + "<tr>" + '<td><a href="#" class="btn" data-action="decrementHours"><i class="icon-chevron-down"></i></a></td>' + '<td class="separator"></td>' + '<td><a href="#" class="btn" data-action="decrementMinutes"><i class="icon-chevron-down"></i></a></td>' + (showSeconds ? '<td class="separator"></td>' + '<td><a href="#" class="btn" data-action="decrementSeconds"><i class="icon-chevron-down"></i></a></td>' : "") + (is12Hours ? '<td class="separator"></td>' : "") + "</tr>" + "</table>" + "</div>" + '<div class="timepicker-hours" data-action="selectHour">' + '<table class="table-condensed">' + "</table>" + "</div>" + '<div class="timepicker-minutes" data-action="selectMinute">' + '<table class="table-condensed">' + "</table>" + "</div>" + (showSeconds ? '<div class="timepicker-seconds" data-action="selectSecond">' + '<table class="table-condensed">' + "</table>" + "</div>" : "");
    };
})(window.jQuery);

(function(e, t) {
    function h(e) {
        return e && e.toLowerCase ? e.toLowerCase() : e;
    }
    function p(e, t) {
        for (var r = 0, i = e.length; r < i; r++) if (e[r] == t) return !n;
        return n;
    }
    var n = !1, r = null, i = parseFloat, s = Math.min, o = /(-?\d+\.?\d*)$/g, u = /(\d+\.?\d*)$/g, a = [], f = [], l = function(e) {
        return typeof e == "string";
    }, c = Array.prototype.indexOf || function(e) {
        var t = this.length, n = Number(arguments[1]) || 0;
        n = n < 0 ? Math.ceil(n) : Math.floor(n);
        if (n < 0) n += t;
        for (;n < t; n++) {
            if (n in this && this[n] === e) return n;
        }
        return -1;
    };
    e.tinysort = {
        id: "TinySort",
        version: "1.5.2",
        copyright: "Copyright (c) 2008-2013 Ron Valstar",
        uri: "http://tinysort.sjeiti.com/",
        licensed: {
            MIT: "http://www.opensource.org/licenses/mit-license.php",
            GPL: "http://www.gnu.org/licenses/gpl.html"
        },
        plugin: function() {
            var e = function(e, t) {
                a.push(e);
                f.push(t);
            };
            e.indexOf = c;
            return e;
        }(),
        defaults: {
            order: "asc",
            attr: r,
            data: r,
            useVal: n,
            place: "start",
            returns: n,
            cases: n,
            forceStrings: n,
            ignoreDashes: n,
            sortFunction: r
        }
    };
    e.fn.extend({
        tinysort: function() {
            var d, v, m = this, g = [], y = [], b = [], w = [], E = 0, S, x = [], T = [], N = function(t) {
                e.each(a, function(e, n) {
                    n.call(n, t);
                });
            }, C = function(t, r) {
                var s = 0;
                if (E !== 0) E = 0;
                while (s === 0 && E < S) {
                    var a = w[E], c = a.oSettings, p = c.ignoreDashes ? u : o;
                    N(c);
                    if (c.sortFunction) {
                        s = c.sortFunction(t, r);
                    } else if (c.order == "rand") {
                        s = Math.random() < .5 ? 1 : -1;
                    } else {
                        var d = n, v = !c.cases ? h(t.s[E]) : t.s[E], m = !c.cases ? h(r.s[E]) : r.s[E];
                        v = v.replace(/^\s*/i, "").replace(/\s*$/i, "");
                        m = m.replace(/^\s*/i, "").replace(/\s*$/i, "");
                        if (!A.forceStrings) {
                            var g = l(v) ? v && v.match(p) : n, y = l(m) ? m && m.match(p) : n;
                            if (g && y) {
                                var b = v.substr(0, v.length - g[0].length), x = m.substr(0, m.length - y[0].length);
                                if (b == x) {
                                    d = !n;
                                    v = i(g[0]);
                                    m = i(y[0]);
                                }
                            }
                        }
                        s = a.iAsc * (v < m ? -1 : v > m ? 1 : 0);
                    }
                    e.each(f, function(e, t) {
                        s = t.call(t, d, v, m, s);
                    });
                    if (s === 0) E++;
                }
                return s;
            };
            for (d = 0, v = arguments.length; d < v; d++) {
                var k = arguments[d];
                if (l(k)) {
                    if (x.push(k) - 1 > T.length) T.length = x.length - 1;
                } else {
                    if (T.push(k) > x.length) x.length = T.length;
                }
            }
            if (x.length > T.length) T.length = x.length;
            S = x.length;
            if (S === 0) {
                S = x.length = 1;
                T.push({});
            }
            for (d = 0, v = S; d < v; d++) {
                var L = x[d], A = e.extend({}, e.tinysort.defaults, T[d]), O = !(!L || L == ""), M = O && L[0] == ":";
                w.push({
                    sFind: L,
                    oSettings: A,
                    bFind: O,
                    bAttr: !(A.attr === r || A.attr == ""),
                    bData: A.data !== r,
                    bFilter: M,
                    $Filter: M ? m.filter(L) : m,
                    fnSort: A.sortFunction,
                    iAsc: A.order == "asc" ? 1 : -1
                });
            }
            m.each(function(n, r) {
                var i = e(r), s = i.parent().get(0), o, u = [];
                for (j = 0; j < S; j++) {
                    var a = w[j], f = a.bFind ? a.bFilter ? a.$Filter.filter(r) : i.find(a.sFind) : i;
                    u.push(a.bData ? f.data(a.oSettings.data) : a.bAttr ? f.attr(a.oSettings.attr) : a.oSettings.useVal ? f.val() : f.text());
                    if (o === t) o = f;
                }
                var l = c.call(b, s);
                if (l < 0) {
                    l = b.push(s) - 1;
                    y[l] = {
                        s: [],
                        n: []
                    };
                }
                if (o.length > 0) y[l].s.push({
                    s: u,
                    e: i,
                    n: n
                }); else y[l].n.push({
                    e: i,
                    n: n
                });
            });
            e.each(y, function(e, t) {
                t.s.sort(C);
            });
            e.each(y, function(t, r) {
                var i = r.s.length, o = [], u = i, a = [ 0, 0 ];
                switch (A.place) {
                  case "first":
                    e.each(r.s, function(e, t) {
                        u = s(u, t.n);
                    });
                    break;

                  case "org":
                    e.each(r.s, function(e, t) {
                        o.push(t.n);
                    });
                    break;

                  case "end":
                    u = r.n.length;
                    break;

                  default:
                    u = 0;
                }
                for (d = 0; d < i; d++) {
                    var f = p(o, d) ? !n : d >= u && d < u + r.s.length, l = (f ? r.s : r.n)[a[f ? 0 : 1]].e;
                    l.parent().append(l);
                    if (f || !A.returns) g.push(l.get(0));
                    a[f ? 0 : 1]++;
                }
            });
            m.length = 0;
            Array.prototype.push.apply(m, g);
            return m;
        }
    });
    e.fn.TinySort = e.fn.Tinysort = e.fn.tsort = e.fn.tinysort;
})(jQuery);

(function($) {
    var $document = $(document), bsSort = [], lastSort, signClass;
    $.bootstrapSortable = function(applyLast, sign) {
        var momentJsAvailable = typeof moment !== "undefined";
        signClass = !sign ? "arrow" : sign;
        $("table.sortable").each(function() {
            var $this = $(this);
            applyLast = applyLast === true;
            $this.find("span.sign").remove();
            $this.find("thead tr").each(function(rowIndex) {
                var columnsSkipped = 0;
                $(this).find("th").each(function(columnIndex) {
                    var $this = $(this);
                    $this.attr("data-sortcolumn", columnIndex + columnsSkipped);
                    $this.attr("data-sortkey", columnIndex + "-" + rowIndex);
                    if ($this.attr("colspan") !== undefined) {
                        columnsSkipped += parseInt($this.attr("colspan")) - 1;
                    }
                });
            });
            $this.find("td").each(function() {
                var $this = $(this);
                if ($this.attr("data-dateformat") != undefined && momentJsAvailable) {
                    $this.attr("data-value", moment($this.text(), $this.attr("data-dateformat")).format("YYYY/MM/DD/HH/mm/ss"));
                } else {
                    $this.attr("data-value") === undefined && $this.attr("data-value", $this.text());
                }
            });
            $this.find('thead th[data-defaultsort!="disabled"]').each(function(index) {
                var $this = $(this);
                var $sortTable = $this.closest("table.sortable");
                $this.data("sortTable", $sortTable);
                var sortKey = $this.attr("data-sortkey");
                var thisLastSort = applyLast ? lastSort : -1;
                bsSort[sortKey] = applyLast ? bsSort[sortKey] : $this.attr("data-defaultsort");
                if (bsSort[sortKey] != null && applyLast == (sortKey == thisLastSort)) {
                    bsSort[sortKey] = bsSort[sortKey] == "asc" ? "desc" : "asc";
                    doSort($this, $sortTable);
                }
            });
            $this.trigger("sorted");
        });
    };
    $document.on("click", 'table.sortable thead th[data-defaultsort!="disabled"]', function(e) {
        var $this = $(this), $table = $this.data("sortTable") || $this.closest("table.sortable");
        doSort($this, $table);
        $table.trigger("sorted");
    });
    function doSort($this, $table) {
        var sortColumn = $this.attr("data-sortcolumn");
        var colspan = $this.attr("colspan");
        if (colspan) {
            var selector;
            for (var i = parseFloat(sortColumn); i < parseFloat(sortColumn) + parseFloat(colspan); i++) {
                selector = selector + ', [data-sortcolumn="' + i + '"]';
            }
            var subHeader = $(selector).not("[colspan]");
            var mainSort = subHeader.filter("[data-mainsort]").eq(0);
            sortColumn = mainSort.length ? mainSort : subHeader.eq(0);
            doSort(sortColumn, $table);
            return;
        }
        var localSignClass = $this.attr("data-defaultsign") || signClass;
        if ($.browser.mozilla) {
            var moz_arrow = $table.find("div.mozilla");
            if (moz_arrow != null) {
                moz_arrow.find(".sign").remove();
                moz_arrow.parent().html(moz_arrow.html());
            }
            $this.wrapInner('<div class="mozilla"></div>');
            $this.children().eq(0).append('<span class="sign ' + localSignClass + '"></span>');
        } else {
            $table.find("span.sign").remove();
            $this.append('<span class="sign ' + localSignClass + '"></span>');
        }
        var sortKey = $this.attr("data-sortkey");
        var initialDirection = $this.attr("data-firstsort") != "desc" ? "desc" : "asc";
        lastSort = sortKey;
        bsSort[sortKey] = (bsSort[sortKey] || initialDirection) == "asc" ? "desc" : "asc";
        if (bsSort[sortKey] == "desc") {
            $this.find("span.sign").addClass("up");
        }
        var rows = $table.find("tbody tr");
        rows.tsort("td:eq(" + sortColumn + ")", {
            order: bsSort[sortKey],
            attr: "data-value"
        });
    }
    if (!$.browser) {
        $.browser = {
            chrome: false,
            mozilla: false,
            opera: false,
            msie: false,
            safari: false
        };
        var ua = navigator.userAgent;
        $.each($.browser, function(c) {
            $.browser[c] = new RegExp(c, "i").test(ua) ? true : false;
            if ($.browser.mozilla && c == "mozilla") {
                $.browser.mozilla = new RegExp("firefox", "i").test(ua) ? true : false;
            }
            if ($.browser.chrome && c == "safari") {
                $.browser.safari = false;
            }
        });
    }
    $($.bootstrapSortable);
})(jQuery);

_log = function(msg, obj) {
    console.log(msg);
    if (obj) {
        console.log(obj);
    }
};

findInArray = function(query, array, returnAll) {
    var key, value;
    for (var p in query) {
        if (query.hasOwnProperty(p)) {
            key = p;
            value = query[p];
            break;
        }
    }
    if (!array || array.length === 0) {
        return null;
    }
    var results = array.filter(function(obj) {
        return obj[key] === value;
    });
    if (results && results.length > 0) {
        if (returnAll) {
            return results;
        } else {
            return results[0];
        }
    } else {
        _log("Searching array... nothing found.");
    }
    return null;
};

num2money = function(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c), 10) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

money2num = function(value) {
    if (value.indexOf(",") !== -1 && value.indexOf(".") !== -1) {
        return parseFloat(value.replace(".", "").replace(",", "."));
    } else {
        return parseFloat(value.replace(",", "."));
    }
};

App.Module = App.Module || function() {
    var module = {
        name: "Module",
        meta: {},
        extend: function(extendedModule) {
            var m = Object.create(this);
            $.extend(m, extendedModule);
            m.start();
            return m;
        },
        start: function() {
            _log("inicijaliziram modul " + this.name);
            var el = this.rootElement;
            if (this.rootElement === "document") {
                el = document;
            }
            this.$el = $(el);
            this.meta = {};
        },
        render: function(template, data, method) {
            _log("renderiram template " + template + " u " + this.name);
            this.templates[template].fn = App.compiledTemplates[template];
            if (!this.templates[template].$el || this.templates[template].cacheElement === false) {
                this.templates[template].$el = $(this.templates[template].renderIn);
            }
            var html = this.templates[template].fn(data);
            if (method === "html") {
                return html;
            }
            switch (method) {
              case "prepend":
                this.templates[template].$el.prepend(html);
                break;

              case "clean":
                this.templates[template].$el.empty().prepend(html);
                break;

              default:
                this.templates[template].$el.append(html);
            }
        }
    };
    return module;
}();

App.Api = App.Api || function() {
    var module = {
        apiPath: "App.conf.apiPath",
        extend: function(extendedModule) {
            var m = Object.create(this);
            $.extend(m, extendedModule);
            return m;
        },
        callApi: function(type, data, callback, fail) {
            var config = {
                success: function(response) {},
                error: function(response) {},
                complete: function(response) {}
            };
            if (App.apis.defaultRequestData) {
                $.extend(data, App.apis.defaultRequestData);
            } else {}
            $.ajax({
                url: App.conf.apiPath + this.api,
                type: type,
                dataType: "JSON",
                data: data ? data : {}
            }).done(function(response) {
                module.onSuccess(callback, response);
            }).fail(function(response) {
                module.onError(fail, response, data);
            });
        },
        get: function(callback, data, fail) {
            this.callApi("GET", data, callback, fail);
        },
        post: function(callback, data, fail) {
            this.callApi("POST", data, callback, fail);
        },
        put: function(callback, data, fail) {
            this.callApi("PUT", data, callback, fail);
        },
        "delete": function(callback, data, fail) {
            this.callApi("DELETE", data, callback, fail);
        },
        onSuccess: function(callback, response) {
            if (callback) {
                callback(response);
            }
        },
        onError: function(callback, response, requestData) {
            _log("[ Error calling API ]");
            _log("[	Response: ", JSON.stringify(response));
            if (callback) {
                callback(JSON.parse(response.responseText));
            }
        }
    };
    return module;
}();

this["App"] = this["App"] || {};

this["App"]["compiledTemplates"] = this["App"]["compiledTemplates"] || {};

this["App"]["compiledTemplates"]["comments"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
    this.compilerInfo = [ 4, ">= 1.0.0" ];
    helpers = this.merge(helpers, Handlebars.helpers);
    data = data || {};
    var stack1, functionType = "function", escapeExpression = this.escapeExpression, helperMissing = helpers.helperMissing, self = this;
    function program1(depth0, data) {
        var buffer = "", stack1, stack2, options;
        buffer += '\n	<li class="comment-item" data-commentid="';
        if (stack1 = helpers._id) {
            stack1 = stack1.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            stack1 = depth0._id;
            stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1;
        }
        buffer += escapeExpression(stack1) + '">\n		<div class="comment-author">\n			' + escapeExpression((stack1 = (stack1 = depth0.author, 
        stack1 == null || stack1 === false ? stack1 : stack1.firstName), typeof stack1 === functionType ? stack1.apply(depth0) : stack1)) + " " + escapeExpression((stack1 = (stack1 = depth0.author, 
        stack1 == null || stack1 === false ? stack1 : stack1.lastName), typeof stack1 === functionType ? stack1.apply(depth0) : stack1)) + '\n		</div>\n		<span class="comment-time right" title="';
        options = {
            hash: {
                format: "DD. MMMM YYYY. HH:mm:ss"
            },
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.formatDate || depth0.formatDate, stack1 ? stack1.call(depth0, depth0.created, options) : helperMissing.call(depth0, "formatDate", depth0.created, options))) + '">';
        options = {
            hash: {
                format: "DD.MM.YYYY."
            },
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.formatDate || depth0.formatDate, stack1 ? stack1.call(depth0, depth0.created, options) : helperMissing.call(depth0, "formatDate", depth0.created, options))) + '</span>\n		<div class="comment">';
        if (stack2 = helpers.comment) {
            stack2 = stack2.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            stack2 = depth0.comment;
            stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2;
        }
        buffer += escapeExpression(stack2) + "</div>\n	</li>\n";
        return buffer;
    }
    stack1 = helpers.each.call(depth0, depth0, {
        hash: {},
        inverse: self.noop,
        fn: self.program(1, program1, data),
        data: data
    });
    if (stack1 || stack1 === 0) {
        return stack1;
    } else {
        return "";
    }
});

this["App"]["compiledTemplates"]["details.form.IRA"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
    this.compilerInfo = [ 4, ">= 1.0.0" ];
    helpers = this.merge(helpers, Handlebars.helpers);
    data = data || {};
    var buffer = "", stack1, options, functionType = "function", escapeExpression = this.escapeExpression, helperMissing = helpers.helperMissing;
    buffer += '<form class="details-form" data-id="';
    if (stack1 = helpers._id) {
        stack1 = stack1.call(depth0, {
            hash: {},
            data: data
        });
    } else {
        stack1 = depth0._id;
        stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1;
    }
    buffer += escapeExpression(stack1) + '">\n	<input class="hidden" name="documentId" type="hidden" value="' + escapeExpression((stack1 = (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.documentId), typeof stack1 === functionType ? stack1.apply(depth0) : stack1)) + '"/>\n	<input class="hidden" name="documentType" type="hidden" value="' + escapeExpression((stack1 = (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.documentType), typeof stack1 === functionType ? stack1.apply(depth0) : stack1)) + '"/>\n\n	<div class="input-field">\n		<label title="Datum dokumenta" for="documentDate">Datum dokumenta</label>\n		<div class="input-append date-input">\n			<input class="date-input-element form-control" data-format="dd.MM.yyyy." placeholder="Odaberite..." value="';
    options = {
        hash: {},
        data: data
    };
    buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", options) : helperMissing.call(depth0, "showValue", (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", options))) + '" type="text" />\n			<span class="add-on glyphicon glyphicon-calendar"></span>\n		</div>\n		<input class="hidden" name="documentDate" type="hidden" value="';
    options = {
        hash: {},
        data: data
    };
    buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", "native", options) : helperMissing.call(depth0, "showValue", (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", "native", options))) + '"/>\n	</div>\n\n	<div class="input-field">\n		<label title="Klijent" for="partner">Klijent</label>\n		<input name="partner" class="form-control" type="text" value="' + escapeExpression((stack1 = (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.partner), typeof stack1 === functionType ? stack1.apply(depth0) : stack1)) + '" placeholder="Klijent.."/>\n	</div>\n\n	<div class="input-field">\n		<label title="Ukupan iznos" for="totalAmount">Ukupan iznos</label>\n		<input name="totalAmount" class="form-control money-control" type="text" value="';
    options = {
        hash: {},
        data: data
    };
    buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.totalAmount), "currency", options) : helperMissing.call(depth0, "showValue", (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.totalAmount), "currency", options))) + '" placeholder="Utipkajte.."/>\n	</div>\n\n	<div class="input-field">\n		<label title="Iznos PDV-a" for="pdvAmount">Iznos PDV-a</label>\n		<input name="pdvAmount" class="form-control money-control" type="text" value="';
    options = {
        hash: {},
        data: data
    };
    buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.pdvAmount), "currency", options) : helperMissing.call(depth0, "showValue", (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.pdvAmount), "currency", options))) + '" placeholder="Utipkajte.."/>\n	</div>\n\n	<div class="input-field">\n		<label title="Datum dospijeća" for="paymentDate">Datum dospijeća</label>\n		<div class="input-append date-input">\n			<input class="date-input-element form-control" data-format="dd.MM.yyyy." placeholder="Odaberite..." value="';
    options = {
        hash: {},
        data: data
    };
    buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.paymentDate), "date", options) : helperMissing.call(depth0, "showValue", (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.paymentDate), "date", options))) + '" type="text" />\n			<span class="add-on glyphicon glyphicon-calendar"></span>\n		</div>\n		<input class="hidden" name="paymentDate" type="hidden" value="';
    options = {
        hash: {},
        data: data
    };
    buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.paymentDate), "date", "native", options) : helperMissing.call(depth0, "showValue", (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.paymentDate), "date", "native", options))) + '"/>\n	</div>\n\n</form>';
    return buffer;
});

this["App"]["compiledTemplates"]["details.form.URA"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
    this.compilerInfo = [ 4, ">= 1.0.0" ];
    helpers = this.merge(helpers, Handlebars.helpers);
    data = data || {};
    var buffer = "", stack1, options, functionType = "function", escapeExpression = this.escapeExpression, helperMissing = helpers.helperMissing;
    buffer += '<form class="details-form" data-id="';
    if (stack1 = helpers._id) {
        stack1 = stack1.call(depth0, {
            hash: {},
            data: data
        });
    } else {
        stack1 = depth0._id;
        stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1;
    }
    buffer += escapeExpression(stack1) + '">\n	<input class="hidden" name="documentId" type="hidden" value="' + escapeExpression((stack1 = (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.documentId), typeof stack1 === functionType ? stack1.apply(depth0) : stack1)) + '"/>\n	<input class="hidden" name="documentType" type="hidden" value="' + escapeExpression((stack1 = (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.documentType), typeof stack1 === functionType ? stack1.apply(depth0) : stack1)) + '"/>\n\n	<div class="input-field">\n		<label title="Datum dokumenta" for="documentDate">Datum dokumenta</label>\n		<div class="input-append date-input">\n			<input class="date-input-element form-control" data-format="dd.MM.yyyy." placeholder="Odaberite..." value="';
    options = {
        hash: {},
        data: data
    };
    buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", options) : helperMissing.call(depth0, "showValue", (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", options))) + '" type="text" />\n			<span class="add-on glyphicon glyphicon-calendar"></span>\n		</div>\n		<input class="hidden" name="documentDate" type="hidden" value="';
    options = {
        hash: {},
        data: data
    };
    buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", "native", options) : helperMissing.call(depth0, "showValue", (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", "native", options))) + '"/>\n	</div>\n\n	<div class="input-field">\n		<label title="Partner" for="partner">Partner</label>\n		<input name="partner" class="form-control" type="text" value="' + escapeExpression((stack1 = (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.partner), typeof stack1 === functionType ? stack1.apply(depth0) : stack1)) + '" placeholder="Partner"/>\n	</div>\n\n	<div class="input-field">\n		<label title="Ukupan iznos" for="totalAmount">Ukupan iznos</label>\n		<input name="totalAmount" class="form-control money-control" type="text" value="';
    options = {
        hash: {},
        data: data
    };
    buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.totalAmount), "currency", options) : helperMissing.call(depth0, "showValue", (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.totalAmount), "currency", options))) + '" placeholder="Utipkajte.."/>\n	</div>\n\n	<div class="input-field">\n		<label title="Iznos PDV-a" for="pdvAmount">Iznos PDV-a</label>\n		<input name="pdvAmount" class="form-control money-control" type="text" value="';
    options = {
        hash: {},
        data: data
    };
    buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.pdvAmount), "currency", options) : helperMissing.call(depth0, "showValue", (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.pdvAmount), "currency", options))) + '" placeholder="Utipkajte.."/>\n	</div>\n\n	<div class="input-field">\n		<label title="Datum dospijeća" for="paymentDate">Datum dospijeća</label>\n		<div class="input-append date-input">\n			<input class="date-input-element form-control" data-format="dd.MM.yyyy." placeholder="Odaberite..." value="';
    options = {
        hash: {},
        data: data
    };
    buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.paymentDate), "date", options) : helperMissing.call(depth0, "showValue", (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.paymentDate), "date", options))) + '" type="text" />\n			<span class="add-on glyphicon glyphicon-calendar"></span>\n		</div>\n		<input class="hidden" name="paymentDate" type="hidden" value="';
    options = {
        hash: {},
        data: data
    };
    buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.paymentDate), "date", "native", options) : helperMissing.call(depth0, "showValue", (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.paymentDate), "date", "native", options))) + '"/>\n	</div>\n\n	<div class="input-field">\n		<label title="Poziv na broj" for="pozivNaBroj">Poziv na broj</label>\n		<input name="pozivNaBroj" class="form-control money-control" type="text" value="' + escapeExpression((stack1 = (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.pozivNaBroj), typeof stack1 === functionType ? stack1.apply(depth0) : stack1)) + '" placeholder="Utipkajte.."/>\n	</div>\n\n	<div class="input-field">\n		<label title="IBAN partnera" for="IBAN">IBAN partnera</label>\n		<input name="IBAN" class="form-control" type="text" value="' + escapeExpression((stack1 = (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.IBAN), typeof stack1 === functionType ? stack1.apply(depth0) : stack1)) + '" placeholder="Utipkajte.."/>\n	</div>\n\n</form>';
    return buffer;
});

this["App"]["compiledTemplates"]["details.form.noId"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
    this.compilerInfo = [ 4, ">= 1.0.0" ];
    helpers = this.merge(helpers, Handlebars.helpers);
    data = data || {};
    var buffer = "", stack1, functionType = "function", escapeExpression = this.escapeExpression;
    buffer += '<form class="details-form" data-id="';
    if (stack1 = helpers._id) {
        stack1 = stack1.call(depth0, {
            hash: {},
            data: data
        });
    } else {
        stack1 = depth0._id;
        stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1;
    }
    buffer += escapeExpression(stack1) + '">\n	<div class="input-field">\n		<label title="Identifikator dokumenta" for="documentId">Identifikator dokumenta</label>\n		<input name="documentId" class="form-control" type="text" value="' + escapeExpression((stack1 = (stack1 = depth0.metadata, 
    stack1 == null || stack1 === false ? stack1 : stack1.documentId), typeof stack1 === functionType ? stack1.apply(depth0) : stack1)) + '" placeholder="Utipkajte.."/>\n	</div>\n</form>';
    return buffer;
});

this["App"]["compiledTemplates"]["details"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
    this.compilerInfo = [ 4, ">= 1.0.0" ];
    helpers = this.merge(helpers, Handlebars.helpers);
    data = data || {};
    var buffer = "", stack1, stack2, helperMissing = helpers.helperMissing, escapeExpression = this.escapeExpression, functionType = "function", self = this;
    function program1(depth0, data) {
        var buffer = "", stack1, options;
        buffer += '\n				<div class="document-form">\n					';
        options = {
            hash: {},
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.subtemplate || depth0.subtemplate, 
        stack1 ? stack1.call(depth0, "details.form.URA", depth0, options) : helperMissing.call(depth0, "subtemplate", "details.form.URA", depth0, options))) + "\n				</div>\n			";
        return buffer;
    }
    function program3(depth0, data) {
        var buffer = "", stack1, stack2;
        buffer += "\n				";
        stack2 = helpers["if"].call(depth0, (stack1 = depth0.metadata, stack1 == null || stack1 === false ? stack1 : stack1.documentId), {
            hash: {},
            inverse: self.program(6, program6, data),
            fn: self.program(4, program4, data),
            data: data
        });
        if (stack2 || stack2 === 0) {
            buffer += stack2;
        }
        buffer += "\n			";
        return buffer;
    }
    function program4(depth0, data) {
        var buffer = "", stack1, stack2, options;
        buffer += '\n					<div class="document-type-selector">\n						';
        options = {
            hash: {},
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.subtemplate || depth0.subtemplate, 
        stack1 ? stack1.call(depth0, "home.docTypeSelector", depth0, options) : helperMissing.call(depth0, "subtemplate", "home.docTypeSelector", depth0, options))) + '\n					</div>\n					<form class="details-form" data-id="';
        if (stack2 = helpers._id) {
            stack2 = stack2.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            stack2 = depth0._id;
            stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2;
        }
        buffer += escapeExpression(stack2) + '">\n						<input class="hidden" name="documentId" type="hidden" value="' + escapeExpression((stack1 = (stack1 = depth0.metadata, 
        stack1 == null || stack1 === false ? stack1 : stack1.documentId), typeof stack1 === functionType ? stack1.apply(depth0) : stack1)) + '"/>\n						<input class="hidden" name="documentType" type="hidden" value="' + escapeExpression((stack1 = (stack1 = depth0.metadata, 
        stack1 == null || stack1 === false ? stack1 : stack1.documentType), typeof stack1 === functionType ? stack1.apply(depth0) : stack1)) + '"/>\n					</form>\n				';
        return buffer;
    }
    function program6(depth0, data) {
        var buffer = "", stack1, options;
        buffer += '\n					<div class="document-form">\n						';
        options = {
            hash: {},
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.subtemplate || depth0.subtemplate, 
        stack1 ? stack1.call(depth0, "details.form.noId", depth0, options) : helperMissing.call(depth0, "subtemplate", "details.form.noId", depth0, options))) + "\n					</div>\n				";
        return buffer;
    }
    function program8(depth0, data) {
        var buffer = "", stack1, options;
        buffer += '\n				<div class="payment-history">\n					<h4>Povijest plaćanja</h4>\n					<table class="table table-striped table-hover">\n						<thead>\n							<tr class="active">\n								<th class="date text-center">Datum</th>\n								<th class="amount text-right">Iznos</th>\n							</tr>\n						</thead>\n						<tbody>\n							';
        options = {
            hash: {},
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.subtemplate || depth0.subtemplate, 
        stack1 ? stack1.call(depth0, "details.paymentHistory", depth0, options) : helperMissing.call(depth0, "subtemplate", "details.paymentHistory", depth0, options))) + "\n						</tbody>\n					</table>\n				</div>\n			";
        return buffer;
    }
    function program10(depth0, data) {
        var buffer = "", stack1, stack2;
        buffer += "\n				";
        stack2 = helpers["if"].call(depth0, (stack1 = depth0.currentTask, stack1 == null || stack1 === false ? stack1 : stack1.assignedToMe), {
            hash: {},
            inverse: self.noop,
            fn: self.program(11, program11, data),
            data: data
        });
        if (stack2 || stack2 === 0) {
            buffer += stack2;
        }
        buffer += "\n			";
        return buffer;
    }
    function program11(depth0, data) {
        var buffer = "", stack1, options;
        buffer += '\n					<div class="document-workflow-task" data-documentid="';
        if (stack1 = helpers._id) {
            stack1 = stack1.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            stack1 = depth0._id;
            stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1;
        }
        buffer += escapeExpression(stack1) + '">\n						';
        options = {
            hash: {},
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.subtemplate || depth0.subtemplate, 
        stack1 ? stack1.call(depth0, "home.workflow", depth0.currentTask, options) : helperMissing.call(depth0, "subtemplate", "home.workflow", depth0.currentTask, options))) + "\n					</div>\n				";
        return buffer;
    }
    function program13(depth0, data) {
        var buffer = "", stack1, options;
        buffer += "\n						";
        options = {
            hash: {},
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.subtemplate || depth0.subtemplate, 
        stack1 ? stack1.call(depth0, "comments", depth0.comments, options) : helperMissing.call(depth0, "subtemplate", "comments", depth0.comments, options))) + "\n					";
        return buffer;
    }
    buffer += '<div class="modal-header">\n	<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></button>\n	<h4 class="modal-title">Detaljni prikaz dokumenta: <span>';
    if (stack1 = helpers.title) {
        stack1 = stack1.call(depth0, {
            hash: {},
            data: data
        });
    } else {
        stack1 = depth0.title;
        stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1;
    }
    buffer += escapeExpression(stack1) + '</span></h4>\n</div>\n<div class="modal-body">\n	<div class="document-info"  data-id="';
    if (stack1 = helpers._id) {
        stack1 = stack1.call(depth0, {
            hash: {},
            data: data
        });
    } else {
        stack1 = depth0._id;
        stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1;
    }
    buffer += escapeExpression(stack1) + '">\n		<div class="document-data">\n			';
    stack2 = helpers["if"].call(depth0, (stack1 = depth0.metadata, stack1 == null || stack1 === false ? stack1 : stack1.documentType), {
        hash: {},
        inverse: self.program(3, program3, data),
        fn: self.program(1, program1, data),
        data: data
    });
    if (stack2 || stack2 === 0) {
        buffer += stack2;
    }
    buffer += "\n			";
    stack2 = helpers["if"].call(depth0, (stack1 = depth0.metadata, stack1 == null || stack1 === false ? stack1 : stack1.isPaid), {
        hash: {},
        inverse: self.program(10, program10, data),
        fn: self.program(8, program8, data),
        data: data
    });
    if (stack2 || stack2 === 0) {
        buffer += stack2;
    }
    buffer += '\n		</div>\n		<div class="document-preview">\n			<div id="docPreviewContainer">\n				<img src="http://localhost:3000/files/document/';
    if (stack2 = helpers._id) {
        stack2 = stack2.call(depth0, {
            hash: {},
            data: data
        });
    } else {
        stack2 = depth0._id;
        stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2;
    }
    buffer += escapeExpression(stack2) + '/fullsize" class="	document-image">\n			</div>\n			<div id="commentsContainer">\n				<h4>Komentari</h4>\n				<ul>\n					';
    stack2 = helpers["if"].call(depth0, depth0.comments, {
        hash: {},
        inverse: self.noop,
        fn: self.program(13, program13, data),
        data: data
    });
    if (stack2 || stack2 === 0) {
        buffer += stack2;
    }
    buffer += '\n				</ul>\n				<textarea class="comment-content form-control" placeholder="Komentiraj.."></textarea>\n				<button class="btn btn-success btn-sm send-comment displaynone" data-loading-text="Šaljem..">Pošalji</button>\n				<button class="btn btn-default btn-sm cancel-comment displaynone" >Odustani</button>\n			</div>\n		</div>\n	</div>\n</div>\n<div class="modal-footer">\n	<button type="button" class="btn btn-default" data-dismiss="modal">Zatvori</button>\n	<button type="button" class="btn btn-primary btn-save" data-loading-text="Spremam..">Spremi promjene</button>\n</div>';
    return buffer;
});

this["App"]["compiledTemplates"]["details.paymentHistory"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
    this.compilerInfo = [ 4, ">= 1.0.0" ];
    helpers = this.merge(helpers, Handlebars.helpers);
    data = data || {};
    var stack1, helperMissing = helpers.helperMissing, escapeExpression = this.escapeExpression, self = this;
    function program1(depth0, data) {
        var buffer = "", stack1, options;
        buffer += '\n	<tr>\n		<td class="date">';
        options = {
            hash: {},
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, depth0.paymentDate, "date", options) : helperMissing.call(depth0, "showValue", depth0.paymentDate, "date", options))) + '</td>\n		<td class="amount text-right">';
        options = {
            hash: {},
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, depth0.amount, "currency", options) : helperMissing.call(depth0, "showValue", depth0.amount, "currency", options))) + "</td>\n	</tr>\n";
        return buffer;
    }
    stack1 = helpers.each.call(depth0, depth0.payments, {
        hash: {},
        inverse: self.noop,
        fn: self.program(1, program1, data),
        data: data
    });
    if (stack1 || stack1 === 0) {
        return stack1;
    } else {
        return "";
    }
});

this["App"]["compiledTemplates"]["document"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
    this.compilerInfo = [ 4, ">= 1.0.0" ];
    helpers = this.merge(helpers, Handlebars.helpers);
    data = data || {};
    var stack1, functionType = "function", escapeExpression = this.escapeExpression, helperMissing = helpers.helperMissing, self = this;
    function program1(depth0, data) {
        var buffer = "", stack1, stack2, options;
        buffer += '\n	<tr class="document-item active">\n		<td><a href="#/details/';
        if (stack1 = helpers._id) {
            stack1 = stack1.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            stack1 = depth0._id;
            stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1;
        }
        buffer += escapeExpression(stack1) + '">';
        if (stack1 = helpers.documentId) {
            stack1 = stack1.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            stack1 = depth0.documentId;
            stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1;
        }
        buffer += escapeExpression(stack1) + "</a></td>\n		<td>";
        if (stack1 = helpers.documentType) {
            stack1 = stack1.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            stack1 = depth0.documentType;
            stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1;
        }
        buffer += escapeExpression(stack1) + "</td>\n		<td>";
        if (stack1 = helpers.title) {
            stack1 = stack1.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            stack1 = depth0.title;
            stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1;
        }
        buffer += escapeExpression(stack1) + "</td>\n		<td>";
        options = {
            hash: {},
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, (stack1 = depth0.metadata, 
        stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", options) : helperMissing.call(depth0, "showValue", (stack1 = depth0.metadata, 
        stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", options))) + '</td>\n		<td class="text-right">';
        options = {
            hash: {},
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, (stack1 = depth0.metadata, 
        stack1 == null || stack1 === false ? stack1 : stack1.totalAmount), "currency", options) : helperMissing.call(depth0, "showValue", (stack1 = depth0.metadata, 
        stack1 == null || stack1 === false ? stack1 : stack1.totalAmount), "currency", options))) + '</td>\n		<td class="text-right ';
        stack2 = helpers["if"].call(depth0, (stack1 = depth0.metadata, stack1 == null || stack1 === false ? stack1 : stack1.isPaid), {
            hash: {},
            inverse: self.noop,
            fn: self.program(2, program2, data),
            data: data
        });
        if (stack2 || stack2 === 0) {
            buffer += stack2;
        }
        buffer += '">';
        options = {
            hash: {},
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, (stack1 = depth0.metadata, 
        stack1 == null || stack1 === false ? stack1 : stack1.paidAmount), "currency", options) : helperMissing.call(depth0, "showValue", (stack1 = depth0.metadata, 
        stack1 == null || stack1 === false ? stack1 : stack1.paidAmount), "currency", options))) + "</td>\n	</tr>\n";
        return buffer;
    }
    function program2(depth0, data) {
        return "paid";
    }
    stack1 = helpers.each.call(depth0, depth0.documents, {
        hash: {},
        inverse: self.noop,
        fn: self.program(1, program1, data),
        data: data
    });
    if (stack1 || stack1 === 0) {
        return stack1;
    } else {
        return "";
    }
});

this["App"]["compiledTemplates"]["documents"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
    this.compilerInfo = [ 4, ">= 1.0.0" ];
    helpers = this.merge(helpers, Handlebars.helpers);
    data = data || {};
    return '<div class="documents-search-tool left">\n	<input placeholder="Pretraži..." type="text" class="form-control">\n	<span class="glyphicon glyphicon-search"></span>\n</div>\n<table class="document-list table table-striped table-hover sortable">\n	<thead>\n		<tr class="active">\n			<th>ID</th>\n			<th>Tip</th>\n			<th>Naziv</th>\n			<th>Datum</th>\n			<th>Ukupni iznos</th>\n			<th>Plaćeni iznos</th>\n		</tr>\n	</thead>\n	<tbody></tbody>\n</table>';
});

this["App"]["compiledTemplates"]["home.docTypeSelector"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
    this.compilerInfo = [ 4, ">= 1.0.0" ];
    helpers = this.merge(helpers, Handlebars.helpers);
    data = data || {};
    return '<div class="btn-group document-types right">\n	<button type="button" class="btn btn-info btn-doc-type" data-docType="URA">URA</button>\n	<button type="button" class="btn btn-info btn-doc-type" data-docType="IRA">IRA</button>\n</div>';
});

this["App"]["compiledTemplates"]["home"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
    this.compilerInfo = [ 4, ">= 1.0.0" ];
    helpers = this.merge(helpers, Handlebars.helpers);
    data = data || {};
    var stack1, functionType = "function", escapeExpression = this.escapeExpression, helperMissing = helpers.helperMissing, self = this;
    function program1(depth0, data) {
        var buffer = "", stack1, stack2;
        buffer += '\n	<div class="card panel panel-primary" data-id="';
        if (stack1 = helpers._id) {
            stack1 = stack1.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            stack1 = depth0._id;
            stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1;
        }
        buffer += escapeExpression(stack1) + '">\n		<div class="card-heading panel-heading">';
        if (stack1 = helpers.title) {
            stack1 = stack1.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            stack1 = depth0.title;
            stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1;
        }
        buffer += escapeExpression(stack1) + '</div>\n		<div class="doc-info">\n			<div class="document-type left" title="Tip dokumenta">' + escapeExpression((stack1 = (stack1 = depth0.metadata, 
        stack1 == null || stack1 === false ? stack1 : stack1.documentType), typeof stack1 === functionType ? stack1.apply(depth0) : stack1)) + '</div>\n			<div class="document-id right" title="Identifikator dokumenta">' + escapeExpression((stack1 = (stack1 = depth0.metadata, 
        stack1 == null || stack1 === false ? stack1 : stack1.documentId), typeof stack1 === functionType ? stack1.apply(depth0) : stack1)) + '</div>\n		</div>\n\n		<div class="card-content panel-body">\n			<div class="document-preview">\n				<img class="preview" src="http://localhost:3000/files/document/';
        if (stack2 = helpers._id) {
            stack2 = stack2.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            stack2 = depth0._id;
            stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2;
        }
        buffer += escapeExpression(stack2) + '/preview" />\n			</div>\n		</div>\n		';
        stack2 = helpers["if"].call(depth0, (stack1 = depth0.metadata, stack1 == null || stack1 === false ? stack1 : stack1.isPaid), {
            hash: {},
            inverse: self.program(4, program4, data),
            fn: self.program(2, program2, data),
            data: data
        });
        if (stack2 || stack2 === 0) {
            buffer += stack2;
        }
        buffer += '\n\n		<div class="card-footer panel-footer comments-container">\n			<ul class="comments-list">\n				';
        stack2 = helpers["if"].call(depth0, depth0.comments, {
            hash: {},
            inverse: self.noop,
            fn: self.program(7, program7, data),
            data: data
        });
        if (stack2 || stack2 === 0) {
            buffer += stack2;
        }
        buffer += '\n			</ul>\n			<textarea class="comment-content form-control" placeholder="Komentiraj.."></textarea>\n			<button class="btn btn-success btn-sm send-comment displaynone" data-loading-text="Šaljem..">Pošalji</button>\n			<button class="btn btn-default btn-sm cancel-comment displaynone" >Odustani</button>\n		</div>\n\n	</div>\n';
        return buffer;
    }
    function program2(depth0, data) {
        return '\n			<div class="document-paid">Plaćeno</div>\n		';
    }
    function program4(depth0, data) {
        var buffer = "", stack1, stack2;
        buffer += "\n			";
        stack2 = helpers["if"].call(depth0, (stack1 = depth0.currentTask, stack1 == null || stack1 === false ? stack1 : stack1.assignedToMe), {
            hash: {},
            inverse: self.noop,
            fn: self.program(5, program5, data),
            data: data
        });
        if (stack2 || stack2 === 0) {
            buffer += stack2;
        }
        buffer += "\n		";
        return buffer;
    }
    function program5(depth0, data) {
        var buffer = "", stack1, options;
        buffer += '\n				<div class="document-workflow-task" data-documentid="';
        if (stack1 = helpers._id) {
            stack1 = stack1.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            stack1 = depth0._id;
            stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1;
        }
        buffer += escapeExpression(stack1) + '">\n					';
        options = {
            hash: {},
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.subtemplate || depth0.subtemplate, 
        stack1 ? stack1.call(depth0, "home.workflow", depth0.currentTask, options) : helperMissing.call(depth0, "subtemplate", "home.workflow", depth0.currentTask, options))) + "\n				</div>\n			";
        return buffer;
    }
    function program7(depth0, data) {
        var buffer = "", stack1, options;
        buffer += "\n					";
        options = {
            hash: {},
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.subtemplate || depth0.subtemplate, 
        stack1 ? stack1.call(depth0, "comments", depth0.comments, options) : helperMissing.call(depth0, "subtemplate", "comments", depth0.comments, options))) + "\n				";
        return buffer;
    }
    stack1 = helpers.each.call(depth0, depth0.documents, {
        hash: {},
        inverse: self.noop,
        fn: self.program(1, program1, data),
        data: data
    });
    if (stack1 || stack1 === 0) {
        return stack1;
    } else {
        return "";
    }
});

this["App"]["compiledTemplates"]["home.workflow"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
    this.compilerInfo = [ 4, ">= 1.0.0" ];
    helpers = this.merge(helpers, Handlebars.helpers);
    data = data || {};
    var buffer = "", stack1, options, functionType = "function", escapeExpression = this.escapeExpression, helperMissing = helpers.helperMissing;
    buffer += '<div class="workflow-task">\n	<div class="task-title" data-name="';
    if (stack1 = helpers.name) {
        stack1 = stack1.call(depth0, {
            hash: {},
            data: data
        });
    } else {
        stack1 = depth0.name;
        stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1;
    }
    buffer += escapeExpression(stack1) + '">\n		<span class="glyphicon glyphicon-tasks icon left" title="Vaš zadatak"></span>\n		<div class="title">';
    if (stack1 = helpers.label) {
        stack1 = stack1.call(depth0, {
            hash: {},
            data: data
        });
    } else {
        stack1 = depth0.label;
        stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1;
    }
    buffer += escapeExpression(stack1) + '</div>\n	</div>\n	<div class="task-actions">\n		<button type="button" class="btn btn-primary complete-task right">\n			';
    options = {
        hash: {},
        data: data
    };
    buffer += escapeExpression((stack1 = helpers.getTaskName || depth0.getTaskName, 
    stack1 ? stack1.call(depth0, depth0, options) : helperMissing.call(depth0, "getTaskName", depth0, options))) + "\n		</button>\n	</div>\n</div>";
    return buffer;
});

this["App"]["compiledTemplates"]["payment"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
    this.compilerInfo = [ 4, ">= 1.0.0" ];
    helpers = this.merge(helpers, Handlebars.helpers);
    data = data || {};
    var stack1, functionType = "function", escapeExpression = this.escapeExpression, helperMissing = helpers.helperMissing, self = this;
    function program1(depth0, data) {
        var buffer = "", stack1, stack2, options;
        buffer += '\n	<tr class="document-item active" data-id="';
        if (stack1 = helpers._id) {
            stack1 = stack1.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            stack1 = depth0._id;
            stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1;
        }
        buffer += escapeExpression(stack1) + '" data-amount="' + escapeExpression((stack1 = (stack1 = depth0.metadata, 
        stack1 == null || stack1 === false ? stack1 : stack1.totalAmount), typeof stack1 === functionType ? stack1.apply(depth0) : stack1)) + '" data-maxamount="';
        options = {
            hash: {},
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.getAmountToPay || depth0.getAmountToPay, 
        stack1 ? stack1.call(depth0, depth0, options) : helperMissing.call(depth0, "getAmountToPay", depth0, options))) + '">\n		<td><a href="#/details/';
        if (stack2 = helpers._id) {
            stack2 = stack2.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            stack2 = depth0._id;
            stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2;
        }
        buffer += escapeExpression(stack2) + '" class="open-details">';
        if (stack2 = helpers.documentId) {
            stack2 = stack2.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            stack2 = depth0.documentId;
            stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2;
        }
        buffer += escapeExpression(stack2) + "</a></td>\n		<td>" + escapeExpression((stack1 = (stack1 = depth0.metadata, 
        stack1 == null || stack1 === false ? stack1 : stack1.partner), typeof stack1 === functionType ? stack1.apply(depth0) : stack1)) + "</td>\n		<td>";
        options = {
            hash: {},
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, (stack1 = depth0.metadata, 
        stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", options) : helperMissing.call(depth0, "showValue", (stack1 = depth0.metadata, 
        stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", options))) + "</td>\n		<td>";
        options = {
            hash: {},
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, (stack1 = depth0.metadata, 
        stack1 == null || stack1 === false ? stack1 : stack1.paymentDate), "date", options) : helperMissing.call(depth0, "showValue", (stack1 = depth0.metadata, 
        stack1 == null || stack1 === false ? stack1 : stack1.paymentDate), "date", options))) + '</td>\n		<td class="text-right">';
        options = {
            hash: {},
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, (stack1 = depth0.metadata, 
        stack1 == null || stack1 === false ? stack1 : stack1.totalAmount), "currency", options) : helperMissing.call(depth0, "showValue", (stack1 = depth0.metadata, 
        stack1 == null || stack1 === false ? stack1 : stack1.totalAmount), "currency", options))) + '</td>\n		<td class="text-right ';
        stack2 = helpers["if"].call(depth0, (stack1 = depth0.metadata, stack1 == null || stack1 === false ? stack1 : stack1.isPaid), {
            hash: {},
            inverse: self.noop,
            fn: self.program(2, program2, data),
            data: data
        });
        if (stack2 || stack2 === 0) {
            buffer += stack2;
        }
        buffer += '">';
        options = {
            hash: {},
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, (stack1 = depth0.metadata, 
        stack1 == null || stack1 === false ? stack1 : stack1.paidAmount), "currency", options) : helperMissing.call(depth0, "showValue", (stack1 = depth0.metadata, 
        stack1 == null || stack1 === false ? stack1 : stack1.paidAmount), "currency", options))) + '</td>\n		<td class="text-right ';
        stack2 = helpers["if"].call(depth0, (stack1 = depth0.metadata, stack1 == null || stack1 === false ? stack1 : stack1.isPaid), {
            hash: {},
            inverse: self.noop,
            fn: self.program(2, program2, data),
            data: data
        });
        if (stack2 || stack2 === 0) {
            buffer += stack2;
        }
        buffer += '">';
        options = {
            hash: {},
            data: data
        };
        buffer += escapeExpression((stack1 = helpers.getAmountToPay || depth0.getAmountToPay, 
        stack1 ? stack1.call(depth0, depth0, options) : helperMissing.call(depth0, "getAmountToPay", depth0, options))) + '</td>\n		<td class="docs-to-pay-actions">\n			';
        stack2 = helpers["if"].call(depth0, (stack1 = depth0.metadata, stack1 == null || stack1 === false ? stack1 : stack1.isPaid), {
            hash: {},
            inverse: self.program(6, program6, data),
            fn: self.program(4, program4, data),
            data: data
        });
        if (stack2 || stack2 === 0) {
            buffer += stack2;
        }
        buffer += "\n		</td>\n	</tr>\n";
        return buffer;
    }
    function program2(depth0, data) {
        return "paid";
    }
    function program4(depth0, data) {
        return "\n			";
    }
    function program6(depth0, data) {
        return '\n				<button type="button" class="btn btn-xs btn-info btn-add-payment right">Dodaj plaćanje</button>\n			';
    }
    stack1 = helpers.each.call(depth0, depth0.payments, {
        hash: {},
        inverse: self.noop,
        fn: self.program(1, program1, data),
        data: data
    });
    if (stack1 || stack1 === 0) {
        return stack1;
    } else {
        return "";
    }
});

this["App"]["compiledTemplates"]["payment.new"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
    this.compilerInfo = [ 4, ">= 1.0.0" ];
    helpers = this.merge(helpers, Handlebars.helpers);
    data = data || {};
    var buffer = "", stack1, stack2, options, helperMissing = helpers.helperMissing, escapeExpression = this.escapeExpression, functionType = "function";
    buffer += '<form class="create-payment">\n	<div class="input-field">\n		<label title="Iznos plaćanja" for="amount">Iznos</label>\n		<input name="amount" type="text" value="';
    options = {
        hash: {},
        data: data
    };
    buffer += escapeExpression((stack1 = helpers.showValue || depth0.showValue, stack1 ? stack1.call(depth0, depth0.amount, "currency", options) : helperMissing.call(depth0, "showValue", depth0.amount, "currency", options))) + '" placeholder="Unesite iznos..." class="form-control money-control input-small" />\n	</div>\n	<div class="input-field">\n		<label title="Datum plaćanja" for="paymentDate">Datum</label>\n		<div class="input-append date-input">\n			<input class="date-input-element form-control" data-format="dd.MM.yyyy." placeholder="Odaberite..." value="" type="text" />\n			<span class="add-on glyphicon glyphicon-calendar"></span>\n		</div>\n		<input class="hidden" name="paymentDate" type="hidden" value=""/>\n	</div>\n	<button class="btn btn-primary btn-save-payment" data-documentid="';
    if (stack2 = helpers.documentId) {
        stack2 = stack2.call(depth0, {
            hash: {},
            data: data
        });
    } else {
        stack2 = depth0.documentId;
        stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2;
    }
    buffer += escapeExpression(stack2) + '" data-maxamount="';
    if (stack2 = helpers.amount) {
        stack2 = stack2.call(depth0, {
            hash: {},
            data: data
        });
    } else {
        stack2 = depth0.amount;
        stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2;
    }
    buffer += escapeExpression(stack2) + '" data-amounttopay="';
    if (stack2 = helpers.amountLeftToPay) {
        stack2 = stack2.call(depth0, {
            hash: {},
            data: data
        });
    } else {
        stack2 = depth0.amountLeftToPay;
        stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2;
    }
    buffer += escapeExpression(stack2) + '" data-loading-text="Snimam...">Spremi plaćanje</button>\n	<button class="btn btn-default btn-cancel-payment">Poništi</button>\n</form>';
    return buffer;
});

this["App"]["compiledTemplates"]["payments"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
    this.compilerInfo = [ 4, ">= 1.0.0" ];
    helpers = this.merge(helpers, Handlebars.helpers);
    data = data || {};
    return '<ul class="nav nav-tabs" role="tablist">\n	<li class="active"><a href="#incoming" role="tab" data-toggle="tab">Ulazni računi</a></li>\n	<li><a href="#outgoing" role="tab" data-toggle="tab">Izlazni računi</a></li>\n</ul>\n\n<div class="tab-content">\n	<div class="tab-pane active" id="incoming">\n\n		<div class="documents-search-tool left">\n			<input placeholder="Pretraži..." type="text" class="form-control">\n			<span class="glyphicon glyphicon-search"></span>\n		</div>\n		<table class="document-list table table-striped table-hover sortable">\n			<thead>\n				<tr class="active">\n					<th>ID dokumenta</th>\n					<th>Partner</th>\n					<th>Datum</th>\n					<th>Dospijeće</th>\n					<th>Ukupan iznos</th>\n					<th>Plaćeni iznos</th>\n					<th>Za platiti</th>\n					<th>Akcije</th>\n				</tr>\n			</thead>\n			<tbody></tbody>\n		</table>\n	</div>\n	<div class="tab-pane" id="outgoing">\n		<div class="documents-search-tool left">\n			<input placeholder="Pretraži..." type="text" class="form-control">\n			<span class="glyphicon glyphicon-search"></span>\n		</div>\n		<table class="document-list table table-striped table-hover sortable">\n			<thead>\n				<tr class="active">\n					<th>ID dokumenta</th>\n					<th>Partner</th>\n					<th>Datum</th>\n					<th>Dospijeće</th>\n					<th>Ukupan iznos</th>\n					<th>Plaćeni iznos</th>\n					<th>Za platiti</th>\n					<th>Akcije</th>\n				</tr>\n			</thead>\n			<tbody></tbody>\n		</table>\n	</div>\n</div>';
});

Handlebars.registerHelper("formatDate", function(timestamp, dateFormat) {
    var format = dateFormat.hash.format || "DD. MMMM YYYY.";
    return moment.unix(timestamp).format(format);
});

Handlebars.registerHelper("subtemplate", function(template, data) {
    if (App.compiledTemplates[template]) {
        return new Handlebars.SafeString(App.compiledTemplates[template](data));
    } else {
        return "Nedostaje template '" + template + "'.";
    }
});

Handlebars.registerHelper("showValue", function(value, format, option) {
    var returnVal = value;
    if (!value) {
        return "- -";
    }
    switch (format) {
      case "as-is":
        returnVal = value;
        break;

      case "date":
        if (option === "native") {
            returnVal = value;
        } else {
            returnVal = moment.unix(value).format("DD.MM.YYYY.");
        }
        break;

      case "currency":
        returnVal = num2money(parseFloat(value));

      default:    }
    return returnVal;
});

Handlebars.registerHelper("getTaskName", function(task) {
    if (task.name === "approveOutgoingInvoice" || task.name === "approveIncomingInvoice") {
        return "Odobri!";
    } else {
        return "Jesam";
    }
});

Handlebars.registerHelper("getAmountToPay", function(doc) {
    var totalAmount = doc.metadata.totalAmount, paidAmount = doc.metadata.paidAmount;
    if (!paidAmount) {
        return num2money(totalAmount);
    } else {
        return num2money(totalAmount - paidAmount);
    }
});

App.apis.user = App.apis.user || function() {
    return App.Api.extend({
        api: "user"
    });
}();

App.apis.documents = App.apis.documents || function() {
    return App.Api.extend({
        api: "documents"
    });
}();

App.apis.payments = App.apis.payments || function() {
    return App.Api.extend({
        api: "payments"
    });
}();

App.apis.document = App.apis.document || function() {
    return App.Api.extend({
        api: "document"
    });
}();

App.apis.documentPreview = App.apis.documentPreview || function() {
    return App.Api.extend({
        api: "files/document"
    });
}();

App.apis.comments = App.apis.comments || function() {
    return App.Api.extend({
        api: "comments"
    });
}();

App.apis.getUserGroups = App.apis.getUserGroups || function() {
    return App.Api.extend({
        api: "usergroups"
    });
}();

App.apis.workflows = App.apis.workflows || function() {
    return App.Api.extend({
        api: "workflows"
    });
}();

$.extend(App, {
    constants: {
        ENTER_KEY: 13,
        ESC_KEY: 27,
        SCROLL_LOAD_NUMBER: 6
    },
    modules: {},
    run: function() {
        NProgress.start();
        App.modules.main.checkUser();
    }
});

App.templates = $.Deferred();

setTimeout(function() {
    App.templates.resolve();
}, 100);

$.when(App.templates).done(function() {
    App.run();
});

moment.lang("hr");

App.modules.main = App.modules.main || function() {
    var module = App.Module.extend({
        name: "main",
        rootElement: "document",
        templates: {
            home: {
                renderIn: "#"
            }
        },
        init: function() {
            _log("inicijaliziram aplikaciju");
            App.routes.init();
            if (!App.user && App.routes.getRoute().indexOf("login") === -1) {
                App.modules.login.meta.formerRoute = App.routes.getRoute().toString().replace(",", "/");
                toastr.warning("Morate se prijaviti!");
                $(".app-tab").hide();
                $("#header li").removeClass("active");
                App.routes.setRoute("login");
            } else if (App.user) {
                module.renderUser();
                module.getCoreData();
                toastr.success("Uspješna prijava");
                if (App.routes.getRoute()[0].length === 0) {
                    App.routes.setRoute("home");
                } else {
                    App.routes.setRoute(App.routes.getRoute().toString().replace(",", "/"));
                }
            }
            module.cacheElements();
            module.bindEvents();
            NProgress.done();
        },
        show: function() {},
        cacheElements: function() {
            module.$logout = module.$el.find(".user-info .logout");
            module.$newDocButton = module.$el.find("#newDocumentBtn");
            module.$newDocForm = module.$el.find("#documentUpload");
            module.$cancelNewDocBtn = module.$newDocForm.find("button.cancel");
            module.$registerButton = module.$el.find("#registrirajSe");
        },
        bindEvents: function() {
            module.$logout.off("click").on("click", module.logout);
            module.$newDocButton.off("click").on("click", module.showNewDocContainer);
            module.$newDocForm.off("submit").on("submit", module.saveNewDoc);
            module.$cancelNewDocBtn.off("click").on("click", module.hideNewDocContainer);
            module.$el.find("#newDocumentContainer .close").off("click").on("click", module.hideNewDocContainer);
            module.$registerButton.off("click").on("click", module.register);
        },
        navbarClick: function(e) {
            console.log("Nav bar click!!");
            $("#container .app-tab").hide();
            $("#header li").removeClass("active");
            $("#header").find('a[href="#/' + App.routes.getRoute().join("/") + '"]').parents("li").addClass("active");
        },
        renderUser: function(clear) {
            var $userInfo = $(".user-info");
            if (clear) {
                $userInfo.find(".signed-in-user").html("");
                $userInfo.addClass("displaynone");
            } else {
                var user = App.user.firstName.charAt(0) + ". " + App.user.lastName;
                $userInfo.find(".signed-in-user").html(user).attr("title", App.user.email);
                $userInfo.removeClass("displaynone");
            }
        },
        saveLocal: function() {
            $.cookie.json = true;
            $.cookie("user", App.user, {
                expires: 7
            });
        },
        getLocal: function() {
            $.cookie.json = true;
            var user = $.cookie("user");
            if (user) {
                return user;
            } else {
                return null;
            }
        },
        deleteLocalData: function() {
            App.user = null;
            App.apis.defaultRequestData = {};
            $.removeCookie("user");
            App.modules.login.meta.formerRoute = App.routes.getRoute().toString().replace(",", "/");
            $(".app-tab").hide();
            module.renderUser(true);
            $("#header li").removeClass("active");
            App.routes.setRoute("login");
        },
        logout: function() {
            var r = confirm("Želite li se odjaviti?");
            if (r == true) {
                module.deleteLocalData();
            }
        },
        checkUser: function() {
            App.user = this.getLocal();
            if (App.user) {
                module.setDefaultRequestData();
            }
            module.init();
        },
        disableNavigation: function() {
            $("#header").addClass("disabled-header");
            $("#newDocumentBtn").hide();
        },
        enableNavigation: function() {
            $("#header").removeClass("disabled-header");
            $("#newDocumentBtn").show();
        },
        showNewDocContainer: function() {
            if ($(this).hasClass("active")) {
                $("#newDocumentContainer").slideUp();
                $(this).removeClass("active");
            } else {
                $(this).addClass("active");
                $("#newDocumentContainer").slideDown();
            }
        },
        hideNewDocContainer: function() {
            module.$newDocButton.removeClass("active");
            $("#newDocumentContainer").slideUp("slow");
            document.getElementById("documentUpload").reset();
        },
        saveNewDoc: function() {
            $(this).find('input[name="userId"]').val(App.user._id);
            $(this).find('input[name="token"]').val(App.user.token);
            $(this).ajaxSubmit({
                error: function(error) {
                    _log("Pogreška! ", error);
                },
                success: function(response) {
                    _log("uspjeh!", response);
                    toastr.success("Dokument je uspješno pohranjen!");
                    module.hideNewDocContainer();
                    $(this).find('input[name="userId"]').val("");
                    $(this).find('input[name="token"]').val("");
                    if ($("#home").is(":visible")) {
                        _log("moram ga prependat");
                        var html = module.render("home", {
                            documents: [ response.output.document ]
                        }, "html");
                        $("#home").prepend(html);
                        App.modules.home.masonry.reloadItems();
                        App.modules.home.refreshMasonry();
                        $cardChanged = $("#home").find('.card[data-id="' + response.output.document._id + '"]');
                        $cardChanged.addClass("data-updated");
                        setTimeout(function() {
                            $cardChanged.removeClass("data-updated");
                        }, 4e3);
                        App.modules.home.bindEvents();
                    }
                }
            });
            return false;
        },
        setDefaultRequestData: function() {
            _log("setting default request data!");
            App.apis.defaultRequestData = {
                userId: App.user._id,
                token: App.user.token,
                email: App.user.email
            };
        },
        getCoreData: function() {
            App.apis.getUserGroups.get(function(response) {
                App.usergroups = response.output.usergroups;
            }, {}, function(error) {
                _log("error getting usergroups");
            });
        },
        register: function() {
            var $modal = $(this).parents("#register"), $form = $modal.find("#registerForm"), email = $form.find('input[name="email"]').val(), password = $form.find('input[name="password"]').val(), firstName = $form.find('input[name="firstName"]').val(), lastName = $form.find('input[name="lastName"]').val();
            if (!email || !password || !firstName || !lastName) {
                bootbox.alert("Nisu uneseni svi podaci!");
                return;
            } else if (password.length < 6) {
                bootbox.alert("Lozinka mora imati najmanje 6 znakova!");
                return;
            }
            App.apis.user.post(function(response) {
                _log("success registration");
                toastr.success("Uspješno ste se registrirali, sada se možete prijaviti!");
                $("#loginForm").find('input[name="email"]').val(response.output.user.email);
                $modal.modal("hide");
            }, {
                method: "createUser",
                email: email,
                password: password,
                firstName: firstName,
                lastName: lastName
            }, function(error) {
                _log("error registration", error);
                if (error.type === "emailExists") {
                    toastr.warning("Korisnik s ovom email adresom već postoji!");
                    $modal.modal("hide");
                } else {
                    toastr.warning("Došlo je do pogreške, pokušajte ponovno!");
                }
            });
            $modal.on("hidden.bs.modal", function(e) {
                $(this).find("input").each(function(k, v) {
                    $(v).val("");
                });
            });
        }
    });
    return module;
}();

App.modules.login = App.modules.login || function() {
    var module = App.Module.extend({
        name: "login",
        rootElement: "#login",
        show: function() {
            if (App.user) {
                _log("already signed in");
                App.routes.setRoute("home");
                return;
            }
            _log("Hello from login module!!");
            module.$el.show();
            module.cacheElements();
            module.bindEvents();
            App.modules.main.disableNavigation();
            NProgress.done();
        },
        cacheElements: function() {
            module.$button = module.$el.find("button#loginButton");
            module.$email = module.$el.find('input[name="email"]');
            module.$password = module.$el.find('input[name="password"]');
        },
        bindEvents: function() {
            module.$button.off("click").on("click", module.handleLogin);
        },
        handleLogin: function(e) {
            e.preventDefault();
            var email = module.$email.val(), password = module.$password.val();
            if (!email || !password) {
                toastr.warning("Niste unijeli sve podatke!");
            } else {
                App.apis.user.post(function(response) {
                    _log("Uspješan login!", response);
                    toastr.success("Uspješno ste se prijavili!");
                    App.user = response.output.user;
                    App.modules.main.setDefaultRequestData();
                    App.modules.main.saveLocal();
                    App.modules.main.renderUser();
                    App.modules.main.enableNavigation();
                    App.routes.setRoute(module.meta.formerRoute || "home");
                }, {
                    method: "login",
                    email: email,
                    password: password
                }, function(error) {
                    _log("neuspješan login!", error);
                    toastr.warning("Neispravni podaci!");
                });
            }
        }
    });
    return module;
}();

App.modules.home = App.modules.home || function() {
    var module = App.Module.extend({
        name: "home",
        rootElement: "#home",
        templates: {
            home: {
                renderIn: "#home"
            },
            comments: {
                renderIn: ".card .comments-list"
            }
        },
        show: function() {
            App.modules.main.navbarClick();
            module.getDocuments(function() {
                if (module.$el.is(":empty")) {
                    module.render("home", {
                        documents: App.documents
                    });
                }
                module.$el.show();
                if (!module.masonry) {
                    module.masonry = module.$el.masonry({
                        gutter: 20,
                        itemSelector: ".card"
                    }).data("masonry");
                }
                module.cacheElements();
                module.bindEvents();
                App.modules.widgets.init();
                NProgress.done(true);
            });
        },
        cacheElements: function() {},
        bindEvents: function() {
            module.$el.find(".card .panel-heading").off().on("click", module.openDetails);
            module.$el.find(".card .card-content .document-preview img").off().on("click", module.openDetails);
            module.$el.find(".card .card-footer .comment-content").off("click").on("click", module.writeNewComment);
            module.$el.find(".card .card-footer .comment-content").off("keypress").on("keypress", module.writeNewComment);
            module.$el.find(".card .card-footer .cancel-comment").off().on("click", module.cancelComment);
            module.$el.find(".card .card-footer .send-comment").off().on("click", module.sendComment);
        },
        openDetails: function(e) {
            var docId;
            if ($(this).hasClass("card")) {
                docId = $(this).attr("data-id");
            } else {
                docId = $(this).parents(".card").attr("data-id");
            }
            App.modules.details.meta.formerRoute = "home";
            App.routes.setRoute("details/" + docId);
        },
        getDocuments: function(callback) {
            App.apis.documents.get(function(response) {
                _log("success documents.get ", response);
                App.documents = response.output.documents;
                if (callback) callback();
            }, {}, function(error) {
                _log("error documents.get ", error);
            });
        },
        writeNewComment: function(e) {
            if (e.type === "click") {
                var $textarea = $(this), $comments = $textarea.parents(".comments-container");
                $comments.find("button").show();
                $textarea.addClass("active-area");
                module.refreshMasonry();
            } else {
                if (e.keyCode === 13) {
                    e.preventDefault;
                    module.sendComment(null, $(this));
                    return false;
                }
            }
        },
        cancelComment: function() {
            var $comments = $(this).parents(".comments-container"), $textarea = $comments.find("textarea");
            $textarea.removeClass("active-area");
            $textarea.val("");
            $comments.find("button").hide();
            module.refreshMasonry();
        },
        sendComment: function(e, textarea) {
            if (textarea) {
                var $comments = $(textarea).parents(".comments-container");
            } else {
                var $comments = $(this).parents(".comments-container");
            }
            var $textarea = $comments.find("textarea"), documentId = $comments.parents(".card").attr("data-id"), comment = $.trim($textarea.val()), $sendBtn = $comments.find(".send-comment");
            $sendBtn.button("loading");
            if (!comment || comment === "") {
                bootbox.alert("Niste unijeli Vaš komentar!");
                $sendBtn.button("reset");
                return;
            }
            App.apis.comments.post(function(response) {
                _log("success comments.post ", response);
                $sendBtn.button("reset");
                $textarea.val("");
                $textarea.removeClass("active-area");
                var html = module.render("comments", [ response.output.comment ], "html");
                $comments.find("ul.comments-list").append(html);
                module.refreshMasonry();
            }, {
                documentId: documentId,
                comment: comment,
                author: App.user._id
            }, function(error) {
                _log("error comments.post ", error);
            });
        },
        refreshMasonry: function() {
            module.masonry.layout();
        }
    });
    return module;
}();

App.modules.documents = App.modules.documents || function() {
    var module = App.Module.extend({
        name: "documents",
        rootElement: "#documents",
        templates: {
            documents: {
                renderIn: "#documents"
            },
            document: {
                renderIn: "#documents .document-list tbody"
            }
        },
        show: function() {
            console.log("Hello from documents module!!");
            App.modules.main.navbarClick();
            module.getDocuments(function() {
                if (module.$el.is(":empty")) {
                    module.render("documents", {});
                }
                module.$el.show();
                module.render("document", {
                    documents: App.documents
                }, "clean");
                $.bootstrapSortable(true);
                module.cacheElements();
                module.bindEvents();
                NProgress.done(true);
            });
        },
        cacheElements: function() {
            module.$linkToDocument = module.$el.find(".document-item");
            module.$searchInput = module.$el.find(".documents-search-tool input");
        },
        bindEvents: function() {
            module.$linkToDocument.off("click").on("click", module.openDetails);
            module.$searchInput.off("keyup").on("keyup", module.searchDocuments);
        },
        openDetails: function(e) {
            App.modules.details.meta.formerRoute = "documents";
        },
        getDocuments: function(callback) {
            App.apis.documents.get(function(response) {
                _log("success documents.get ", response);
                App.documents = response.output.documents;
                if (callback) callback();
            }, {}, function(error) {
                _log("error documents.get ", error);
            });
        },
        searchDocuments: function() {
            var inputVal = $(this).val();
            var table = $(this).parents("#documents").find("table");
            table.find("tbody tr").each(function(index, row) {
                var allCells = $(row).find("td");
                if (allCells.length > 0) {
                    var found = false;
                    allCells.each(function(index, td) {
                        var regExp = new RegExp(inputVal, "i");
                        if (regExp.test($(td).text())) {
                            found = true;
                            return false;
                        }
                    });
                    found ? $(row).show() : $(row).hide();
                }
            });
        }
    });
    return module;
}();

App.modules.payments = App.modules.payments || function() {
    var module = App.Module.extend({
        name: "payments",
        rootElement: "#payments",
        templates: {
            payments: {
                renderIn: "#payments"
            },
            payment: {
                renderIn: "#payments .document-list tbody"
            },
            "payment.new": {
                renderIn: "#"
            }
        },
        show: function() {
            console.log("Hello from payments module!!");
            App.modules.main.navbarClick();
            if (module.$el.is(":empty")) {
                module.render("payments", {});
            }
            module.$el.show();
            module.getDocuments("incoming", function(payments) {
                var html = module.render("payment", {
                    payments: payments
                }, "html");
                module.$el.find("#incoming .document-list tbody").html(html);
                module.getDocuments("outgoing", function(docs) {
                    var html = module.render("payment", {
                        payments: docs
                    }, "html");
                    module.$el.find("#outgoing .document-list tbody").html(html);
                    $.bootstrapSortable(true);
                    module.cacheElements();
                    module.bindEvents();
                    module.attachPopover();
                    NProgress.done(true);
                });
            });
        },
        cacheElements: function() {
            module.$linkToDocument = module.$el.find(".open-details");
            module.$searchInput = module.$el.find(".documents-search-tool input");
        },
        bindEvents: function() {
            module.$searchInput.off("keyup").on("keyup", module.searchDocuments);
            module.$linkToDocument.off("click").on("click", module.openDetails);
        },
        getDocuments: function(direction, callback) {
            App.apis.payments.get(function(response) {
                _log("success payments.get in " + direction + " direction.", response);
                if (callback) callback(response.output.payments);
            }, {
                direction: direction
            }, function(error) {
                _log("error payments.get ", error);
            });
        },
        openDetails: function() {
            App.modules.details.meta.formerRoute = "payments";
        },
        searchDocuments: function() {
            var inputVal = $(this).val();
            var table = $(this).parents(".tab-pane").find("table");
            table.find("tbody tr").each(function(index, row) {
                var allCells = $(row).find("td");
                if (allCells.length > 0) {
                    var found = false;
                    allCells.each(function(index, td) {
                        var regExp = new RegExp(inputVal, "i");
                        if (regExp.test($(td).text())) {
                            found = true;
                            return false;
                        }
                    });
                    found ? $(row).show() : $(row).hide();
                }
            });
        },
        attachPopover: function($button) {
            var attach = function($btn) {
                var $docItem = $btn.parents(".document-item"), amount = money2num($docItem.attr("data-maxamount")), docId = $docItem.attr("data-id");
                $btn.popover({
                    html: true,
                    title: function() {
                        return "Unesite podatke o plaćanju";
                    },
                    content: function() {
                        return module.render("payment.new", {
                            amount: amount,
                            documentId: docId
                        }, "html");
                    },
                    trigger: "click",
                    placement: "left"
                });
                $btn.on("shown.bs.popover", function() {
                    $btn.parent().find('.popover input[name="amount"]').focus();
                    $btn.parent().find(".popover-content .input-append.date-input").datetimepicker({
                        pickTime: true
                    }).on("changeDate", function(e) {
                        _log("date changed!");
                        var $widget = $(this), $input = $widget.find("input.date-input-element"), selectedDate = $input.val(), $hidden = $widget.parents(".input-field").find("input.hidden");
                        if (selectedDate === "") {
                            $hidden.val("");
                        } else {
                            $hidden.val(moment(selectedDate, "DD.MM.YYYY. HH:mm").unix());
                        }
                        $input.trigger("blur");
                        $(".bootstrap-datetimepicker-widget").fadeOut();
                    });
                });
                $btn.parents(".docs-to-pay-actions").on("click", ".popover-content .btn-cancel-payment", function(e) {
                    e.preventDefault();
                    $btn.popover("hide");
                }).on("click", ".popover-content .btn-save-payment", function(e) {
                    e.preventDefault();
                    $(this).attr("disabled", true);
                    var $form = $(this).parents("form"), maxAmount = money2num($(this).attr("data-maxamount")), enteredAmount = money2num($form.find('input[name="amount"]').val()), date = $form.find('input[name="paymentDate"]').val(), docId = $(this).attr("data-documentid");
                    if (!enteredAmount || !date) {
                        bootbox.alert("Nisu uneseni svi podaci!");
                        $(this).attr("disabled", false);
                        return;
                    } else if (parseFloat(enteredAmount) > parseFloat(maxAmount)) {
                        bootbox.alert("Ne možete unijeti veći iznos od ukupnog iznosa računa!");
                        $(this).attr("disabled", false);
                        return;
                    }
                    App.apis.payments.post(function(response) {
                        console.log("success payments post", response);
                        module.updateDocumentOnList(response.output.document);
                        $btn.popover("hide");
                        module.attachPopover($btn);
                    }, {
                        documentId: docId,
                        amount: enteredAmount,
                        paymentDate: date
                    }, function(error) {
                        console.log("error payments post", error);
                        toastr.warning("Došlo je do pogreške, pokušajte ponovno!");
                        $(this).attr("disabled", false);
                    });
                });
            };
            if ($button) {
                attach($button);
            } else {
                module.$el.find(".btn-add-payment").each(function(k, v) {
                    attach($(v));
                });
            }
        },
        updateDocumentOnList: function(doc) {
            var $tr = module.$el.find(".document-list tr[data-id='" + doc._id + "']"), html = module.render("payment", {
                payments: [ doc ]
            }, "html");
            $tr.replaceWith(html);
        }
    });
    return module;
}();

App.modules.details = App.modules.details || function() {
    var module = App.Module.extend({
        name: "details",
        rootElement: "#details",
        templates: {
            details: {
                renderIn: "#details .modal-content"
            },
            comments: {
                renderIn: "#"
            }
        },
        show: function(docId) {
            App.modules.main.navbarClick();
            module.$el.modal("show");
            App.apis.document.get(function(response) {
                _log("success document.get: ", response);
                module.render("details", response.output.document, "clean");
                module.doc = response.output.document;
                module.cacheElements();
                module.bindEvents();
                module.generateForm();
                App.modules.widgets.init();
                NProgress.done(true);
            }, {
                documentId: docId
            }, function(error) {
                _log("error document.get ", error);
            });
        },
        cacheElements: function() {
            module.$saveBtn = module.$el.find(".btn-save");
        },
        bindEvents: function() {
            module.$el.off("hide.bs.modal").on("hide.bs.modal", module.setFormerRoute);
            module.$saveBtn.off("click").on("click", module.saveDocumentForm);
            module.$el.find(".comment-content").off("click").on("click", module.writeNewComment);
            module.$el.find(".comment-content").off("keypress").on("keypress", module.writeNewComment);
            module.$el.find(".cancel-comment").off().on("click", module.cancelComment);
            module.$el.find(".send-comment").off().on("click", module.sendComment);
        },
        setFormerRoute: function(e) {
            if ($(e.target).hasClass("input-append")) {
                return;
            }
            _log("šaljem te na staru rutu");
            App.routes.setRoute(module.meta.formerRoute || "home");
        },
        saveDocumentForm: function(callback) {
            _log("spremam promjene");
            var properties = module.$el.find("form.details-form").serializeArray(), data = module.getFormData(properties);
            _log("evo data", data);
            App.apis.document.put(function(response) {
                toastr.success("Uspješno ste snimili promjene!");
                module.$el.modal("hide");
                if (callback && typeof callback == "function") {
                    callback();
                }
            }, {
                documentId: module.doc._id,
                properties: {
                    metadata: data
                }
            }, function(error) {
                _log("error updating document!", error);
            });
        },
        getFormData: function(data) {
            var dataToSend = {};
            for (var i = 0; i < data.length; i++) {
                if (data[i].value && data[i].value.length > 0) {
                    if (data[i].name === "totalAmount" || data[i].name === "pdvAmount") {
                        data[i].value = money2num(data[i].value);
                    }
                    dataToSend[data[i].name] = data[i].value;
                }
            }
            return dataToSend;
        },
        writeNewComment: function(e) {
            if (e.type === "click") {
                var $textarea = $(this), $comments = $textarea.parents("#commentsContainer");
                $comments.find("button").show();
                $textarea.addClass("active-area");
            } else {
                if (e.keyCode === 13) {
                    e.preventDefault;
                    module.sendComment(null, $(this));
                    return false;
                }
            }
        },
        cancelComment: function() {
            var $comments = $(this).parents("#commentsContainer"), $textarea = $comments.find("textarea");
            $textarea.removeClass("active-area");
            $textarea.val("");
            $comments.find("button").hide();
        },
        sendComment: function(e, textarea) {
            if (textarea) {
                var $comments = $(textarea).parents("#commentsContainer");
                $(textarea).blur();
            } else {
                var $comments = $(this).parents("#commentsContainer");
            }
            var $textarea = $comments.find("textarea"), documentId = $comments.parents(".document-info").attr("data-id"), comment = $.trim($textarea.val()), $sendBtn = $comments.find(".send-comment");
            $sendBtn.button("loading");
            if (!comment || comment === "") {
                bootbox.alert("Niste unijeli Vaš komentar!");
                $sendBtn.button("reset");
                return;
            }
            App.apis.comments.post(function(response) {
                _log("success comments.post ", response);
                $sendBtn.button("reset");
                $textarea.val("");
                $textarea.removeClass("active-area");
                $comments.find("button").hide();
                var html = module.render("comments", [ response.output.comment ], "html");
                $comments.find("ul").append(html);
            }, {
                documentId: documentId,
                comment: comment,
                author: App.user._id
            }, function(error) {
                _log("error comments.post ", error);
            });
        },
        generateForm: function() {
            module.$el.find(".input-append.date-input").datetimepicker({
                pickTime: true
            }).on("changeDate", function(e) {
                _log("date changed!");
                var $widget = $(this), $input = $widget.find("input.date-input-element"), selectedDate = $input.val(), $hidden = $widget.parents(".input-field").find("input.hidden");
                if (selectedDate === "") {
                    $hidden.val("");
                } else {
                    $hidden.val(moment(selectedDate, "DD.MM.YYYY. HH:mm").unix());
                }
                $input.trigger("blur");
                $(".bootstrap-datetimepicker-widget").fadeOut();
            });
        }
    });
    return module;
}();

App.modules.widgets = App.modules.widgets || function() {
    var module = App.Module.extend({
        name: "widgets",
        rootElement: "document",
        templates: {
            "home.docTypeSelector": {
                renderIn: "#"
            },
            home: {
                renderIn: "#"
            }
        },
        init: function() {
            module.bindEvents();
        },
        bindEvents: function() {
            module.$el.find(".document-type-selector button").off().on("click", module.setDocumentType);
            module.$el.find(".document-workflow-task button.complete-task").off().on("click", module.completeTask);
        },
        setDocumentType: function() {
            $(this).parents(".document-types").find("button").removeClass("active");
            $(this).addClass("active");
            module.$el.find('.details-form input[name="documentType"]').val($(this).attr("data-doctype"));
        },
        completeTask: function() {
            var that = $(this);
            var complete = function() {
                var docId = that.parents(".document-workflow-task").attr("data-documentid");
                App.apis.document.get(function(response) {
                    var doc = response.output.document;
                    App.apis.workflows.put(function(r) {
                        _log("success put workflows", r);
                        toastr.success("Zadatak uspješno završen!");
                        if ($("#details").is(":visible")) {
                            App.routes.setRoute(App.modules.details.meta.formerRoute || "home");
                        }
                        var $card = $("#home").find('.card[data-id="' + r.output.document._id + '"]'), html = module.render("home", {
                            documents: [ r.output.document ]
                        }, "html");
                        $card.replaceWith(html);
                        App.modules.home.masonry.reloadItems();
                        App.modules.home.refreshMasonry();
                        App.modules.home.bindEvents();
                    }, {
                        documentId: doc._id,
                        task: doc.currentTask.name,
                        taskCompleted: true
                    }, function(error) {
                        _log("error put workflow:", error);
                        if (error.type === "missingParameter") {
                            bootbox.alert(error.message);
                        }
                    });
                }, {
                    documentId: docId
                });
            };
            if (App.routes.getRoute().indexOf("details") !== -1) {
                App.modules.details.saveDocumentForm(complete);
            } else {
                _log("na strimu sam");
                complete();
            }
        }
    });
    return module;
}();

App.routes = App.routes || {};

$.extend(App.routes, {
    "/login": App.modules.login.show,
    "/home": App.modules.home.show,
    "/documents": App.modules.documents.show,
    "/payments": App.modules.payments.show,
    "/details/:id": App.modules.details.show
});

App.routes = new Router(App.routes);