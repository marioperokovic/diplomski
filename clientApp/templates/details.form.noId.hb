<form class="details-form" data-id="{{_id}}">
	<div class="input-field">
		<label title="Identifikator dokumenta" for="documentId">Identifikator dokumenta</label>
		<input name="documentId" class="form-control" type="text" value="{{metadata.documentId}}" placeholder="Utipkajte.."/>
	</div>
</form>