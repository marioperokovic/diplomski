<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove"></span></button>
	<h4 class="modal-title">Detaljni prikaz dokumenta: <span>{{title}}</span></h4>
</div>
<div class="modal-body">
	<div class="document-info"  data-id="{{_id}}">
		<div class="document-data">
			{{#if metadata.documentType}}
				<div class="document-form">
					{{subtemplate 'details.form.URA' this}}
				</div>
			{{else}}
				{{#if metadata.documentId}}
					<div class="document-type-selector">
						{{subtemplate 'home.docTypeSelector' this}}
					</div>
					<form class="details-form" data-id="{{_id}}">
						<input class="hidden" name="documentId" type="hidden" value="{{metadata.documentId}}"/>
						<input class="hidden" name="documentType" type="hidden" value="{{metadata.documentType}}"/>
					</form>
				{{else}}
					<div class="document-form">
						{{subtemplate 'details.form.noId' this}}
					</div>
				{{/if}}
			{{/if}}
			{{#if metadata.isPaid}}
				<div class="payment-history">
					<h4>Povijest plaćanja</h4>
					<table class="table table-striped table-hover">
						<thead>
							<tr class="active">
								<th class="date text-center">Datum</th>
								<th class="amount text-right">Iznos</th>
							</tr>
						</thead>
						<tbody>
							{{subtemplate 'details.paymentHistory' this}}
						</tbody>
					</table>
				</div>
			{{else}}
				{{#if currentTask.assignedToMe}}
					<div class="document-workflow-task" data-documentid="{{_id}}">
						{{subtemplate 'home.workflow' currentTask}}
					</div>
				{{/if}}
			{{/if}}
		</div>
		<div class="document-preview">
			<div id="docPreviewContainer">
				<img src="http://localhost:3000/files/document/{{_id}}/fullsize" class="	document-image">
			</div>
			<div id="commentsContainer">
				<h4>Komentari</h4>
				<ul>
					{{#if comments}}
						{{subtemplate 'comments' comments}}
					{{/if}}
				</ul>
				<textarea class="comment-content form-control" placeholder="Komentiraj.."></textarea>
				<button class="btn btn-success btn-sm send-comment displaynone" data-loading-text="Šaljem..">Pošalji</button>
				<button class="btn btn-default btn-sm cancel-comment displaynone" >Odustani</button>
			</div>
		</div>
	</div>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">Zatvori</button>
	<button type="button" class="btn btn-primary btn-save" data-loading-text="Spremam..">Spremi promjene</button>
</div>