{{#each payments}}
	<tr class="document-item active" data-id="{{_id}}" data-amount="{{metadata.totalAmount}}" data-maxamount="{{getAmountToPay this}}">
		<td><a href="#/details/{{_id}}" class="open-details">{{documentId}}</a></td>
		<td>{{metadata.partner}}</td>
		<td>{{showValue metadata.documentDate 'date'}}</td>
		<td>{{showValue metadata.paymentDate 'date'}}</td>
		<td class="text-right">{{showValue metadata.totalAmount 'currency'}}</td>
		<td class="text-right {{#if metadata.isPaid}}paid{{/if}}">{{showValue metadata.paidAmount 'currency'}}</td>
		<td class="text-right {{#if metadata.isPaid}}paid{{/if}}">{{getAmountToPay this}}</td>
		<td class="docs-to-pay-actions">
			{{#if metadata.isPaid}}
			{{else}}
				<button type="button" class="btn btn-xs btn-info btn-add-payment right">Dodaj plaćanje</button>
			{{/if}}
		</td>
	</tr>
{{/each}}