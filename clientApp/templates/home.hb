{{#each documents}}
	<div class="card panel panel-primary" data-id="{{_id}}">
		<div class="card-heading panel-heading">{{title}}</div>
		<div class="doc-info">
			<div class="document-type left" title="Tip dokumenta">{{metadata.documentType}}</div>
			<div class="document-id right" title="Identifikator dokumenta">{{metadata.documentId}}</div>
		</div>

		<div class="card-content panel-body">
			<div class="document-preview">
				<img class="preview" src="http://localhost:3000/files/document/{{_id}}/preview" />
			</div>
		</div>
		{{#if metadata.isPaid}}
			<div class="document-paid">Plaćeno</div>
		{{else}}
			{{#if currentTask.assignedToMe}}
				<div class="document-workflow-task" data-documentid="{{_id}}">
					{{subtemplate 'home.workflow' currentTask}}
				</div>
			{{/if}}
		{{/if}}

		<div class="card-footer panel-footer comments-container">
			<ul class="comments-list">
				{{#if comments}}
					{{subtemplate 'comments' comments}}
				{{/if}}
			</ul>
			<textarea class="comment-content form-control" placeholder="Komentiraj.."></textarea>
			<button class="btn btn-success btn-sm send-comment displaynone" data-loading-text="Šaljem..">Pošalji</button>
			<button class="btn btn-default btn-sm cancel-comment displaynone" >Odustani</button>
		</div>

	</div>
{{/each}}