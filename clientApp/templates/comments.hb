{{#each this}}
	<li class="comment-item" data-commentid="{{_id}}">
		<div class="comment-author">
			{{author.firstName}} {{author.lastName}}
		</div>
		<span class="comment-time right" title="{{formatDate created format="DD. MMMM YYYY. HH:mm:ss"}}">{{formatDate created format="DD.MM.YYYY."}}</span>
		<div class="comment">{{comment}}</div>
	</li>
{{/each}}