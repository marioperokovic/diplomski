<form class="details-form" data-id="{{_id}}">
	<input class="hidden" name="documentId" type="hidden" value="{{metadata.documentId}}"/>
	<input class="hidden" name="documentType" type="hidden" value="{{metadata.documentType}}"/>

	<div class="input-field">
		<label title="Datum dokumenta" for="documentDate">Datum dokumenta</label>
		<div class="input-append date-input">
			<input class="date-input-element form-control" data-format="dd.MM.yyyy." placeholder="Odaberite..." value="{{showValue metadata.documentDate 'date'}}" type="text" />
			<span class="add-on glyphicon glyphicon-calendar"></span>
		</div>
		<input class="hidden" name="documentDate" type="hidden" value="{{showValue metadata.documentDate 'date' 'native'}}"/>
	</div>

	<div class="input-field">
		<label title="Klijent" for="partner">Klijent</label>
		<input name="partner" class="form-control" type="text" value="{{metadata.partner}}" placeholder="Klijent.."/>
	</div>

	<div class="input-field">
		<label title="Ukupan iznos" for="totalAmount">Ukupan iznos</label>
		<input name="totalAmount" class="form-control money-control" type="text" value="{{showValue metadata.totalAmount 'currency'}}" placeholder="Utipkajte.."/>
	</div>

	<div class="input-field">
		<label title="Iznos PDV-a" for="pdvAmount">Iznos PDV-a</label>
		<input name="pdvAmount" class="form-control money-control" type="text" value="{{showValue metadata.pdvAmount 'currency'}}" placeholder="Utipkajte.."/>
	</div>

	<div class="input-field">
		<label title="Datum dospijeća" for="paymentDate">Datum dospijeća</label>
		<div class="input-append date-input">
			<input class="date-input-element form-control" data-format="dd.MM.yyyy." placeholder="Odaberite..." value="{{showValue metadata.paymentDate 'date'}}" type="text" />
			<span class="add-on glyphicon glyphicon-calendar"></span>
		</div>
		<input class="hidden" name="paymentDate" type="hidden" value="{{showValue metadata.paymentDate 'date' 'native'}}"/>
	</div>

</form>