<div class="documents-search-tool left">
	<input placeholder="Pretraži..." type="text" class="form-control">
	<span class="glyphicon glyphicon-search"></span>
</div>
<table class="document-list table table-striped table-hover sortable">
	<thead>
		<tr class="active">
			<th>ID</th>
			<th>Tip</th>
			<th>Naziv</th>
			<th>Datum</th>
			<th>Ukupni iznos</th>
			<th>Plaćeni iznos</th>
		</tr>
	</thead>
	<tbody></tbody>
</table>