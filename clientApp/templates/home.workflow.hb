<div class="workflow-task">
	<div class="task-title" data-name="{{name}}">
		<span class="glyphicon glyphicon-tasks icon left" title="Vaš zadatak"></span>
		<div class="title">{{label}}</div>
	</div>
	<div class="task-actions">
		<button type="button" class="btn btn-primary complete-task right">
			{{getTaskName this}}
		</button>
	</div>
</div>