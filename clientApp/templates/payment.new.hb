<form class="create-payment">
	<div class="input-field">
		<label title="Iznos plaćanja" for="amount">Iznos</label>
		<input name="amount" type="text" value="{{showValue amount 'currency'}}" placeholder="Unesite iznos..." class="form-control money-control input-small" />
	</div>
	<div class="input-field">
		<label title="Datum plaćanja" for="paymentDate">Datum</label>
		<div class="input-append date-input">
			<input class="date-input-element form-control" data-format="dd.MM.yyyy." placeholder="Odaberite..." value="" type="text" />
			<span class="add-on glyphicon glyphicon-calendar"></span>
		</div>
		<input class="hidden" name="paymentDate" type="hidden" value=""/>
	</div>
	<button class="btn btn-primary btn-save-payment" data-documentid="{{documentId}}" data-maxamount="{{amount}}" data-amounttopay="{{amountLeftToPay}}" data-loading-text="Snimam...">Spremi plaćanje</button>
	<button class="btn btn-default btn-cancel-payment">Poništi</button>
</form>