{{#each payments}}
	<tr>
		<td class="date">{{showValue paymentDate 'date'}}</td>
		<td class="amount text-right">{{showValue amount 'currency'}}</td>
	</tr>
{{/each}}