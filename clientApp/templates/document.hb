{{#each documents}}
	<tr class="document-item active">
		<td><a href="#/details/{{_id}}">{{documentId}}</a></td>
		<td>{{documentType}}</td>
		<td>{{title}}</td>
		<td>{{showValue metadata.documentDate 'date'}}</td>
		<td class="text-right">{{showValue metadata.totalAmount 'currency'}}</td>
		<td class="text-right {{#if metadata.isPaid}}paid{{/if}}">{{showValue metadata.paidAmount 'currency'}}</td>
	</tr>
{{/each}}