<ul class="nav nav-tabs" role="tablist">
	<li class="active"><a href="#incoming" role="tab" data-toggle="tab">Ulazni računi</a></li>
	<li><a href="#outgoing" role="tab" data-toggle="tab">Izlazni računi</a></li>
</ul>

<div class="tab-content">
	<div class="tab-pane active" id="incoming">

		<div class="documents-search-tool left">
			<input placeholder="Pretraži..." type="text" class="form-control">
			<span class="glyphicon glyphicon-search"></span>
		</div>
		<table class="document-list table table-striped table-hover sortable">
			<thead>
				<tr class="active">
					<th>ID dokumenta</th>
					<th>Partner</th>
					<th>Datum</th>
					<th>Dospijeće</th>
					<th>Ukupan iznos</th>
					<th>Plaćeni iznos</th>
					<th>Za platiti</th>
					<th>Akcije</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
	<div class="tab-pane" id="outgoing">
		<div class="documents-search-tool left">
			<input placeholder="Pretraži..." type="text" class="form-control">
			<span class="glyphicon glyphicon-search"></span>
		</div>
		<table class="document-list table table-striped table-hover sortable">
			<thead>
				<tr class="active">
					<th>ID dokumenta</th>
					<th>Partner</th>
					<th>Datum</th>
					<th>Dospijeće</th>
					<th>Ukupan iznos</th>
					<th>Plaćeni iznos</th>
					<th>Za platiti</th>
					<th>Akcije</th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</div>
</div>