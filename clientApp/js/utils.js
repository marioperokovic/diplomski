_log = function ( msg, obj ) {
	console.log( msg );
	if ( obj ) {
		console.log( obj );
	}
};

findInArray = function ( query, array, returnAll ) {
	var key, value;

	for ( var p in query ) {
		if ( query.hasOwnProperty(p) ) {
			key = p;
			value = query[p];
			break;
		}
	}

	if ( !array || array.length === 0 ) {
		return null;
	}

	var results = array.filter( function( obj ) {
		return obj[ key ] === value;
	});

	if ( results && results.length > 0 ) {
		if ( returnAll ) {
			return results;
		} else {
			return results[0];
		}
	} else {
		_log("Searching array... nothing found." );
	}

	return null;
};

/*
 *   Function for conversion number to money
 *
 *   Arguments:
 *       n - number for converstion
 *       [ c ] - decimal places (optional) - default: 2
 *       [ d ] - decimal separator (optional) - default: ,
 *       [ t ] - thousand separator (optional) - default: .
 *
 *   Example usage: App.utils.num2money( 2000.05 ) // "2.000,05"
 */
num2money = function (n, c, d, t) {
	var c = isNaN(c = Math.abs(c)) ? 2 : c,
		d = d == undefined ? "," : d,
		t = t == undefined ? "." : t,
		s = n < 0 ? "-" : "",
		i = parseInt(n = Math.abs(+n || 0).toFixed(c), 10) + "",
		j = (j = i.length) > 3 ? j % 3 : 0;
	return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

money2num = function ( value ) {
	//in case of 1.234,23
	if ( value.indexOf(',') !== -1 && value.indexOf('.') !== -1 ) {
		return parseFloat( value.replace('.','').replace(',','.') );
	} else {
		return parseFloat( value.replace(',','.') );
	}
};