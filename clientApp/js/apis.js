App.apis.user = App.apis.user || (function () {
	return App.Api.extend({
		api: 'user'
	});
})();

App.apis.documents = App.apis.documents || (function () {
	return App.Api.extend({
		api: 'documents'
	});
})();

App.apis.payments = App.apis.payments || (function () {
	return App.Api.extend({
		api: 'payments'
	});
})();

App.apis.document = App.apis.document || (function () {
	return App.Api.extend({
		api: 'document'
	});
})();

App.apis.documentPreview = App.apis.documentPreview || (function () {
	return App.Api.extend({
		api: 'files/document'
	});
})();

App.apis.comments = App.apis.comments || (function () {
	return App.Api.extend({
		api: 'comments'
	});
})();

App.apis.getUserGroups = App.apis.getUserGroups || (function () {
	return App.Api.extend({
		api: 'usergroups'
	});
})();

App.apis.workflows = App.apis.workflows || (function () {
	return App.Api.extend({
		api: 'workflows'
	});
})();