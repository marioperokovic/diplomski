this["App"] = this["App"] || {};
this["App"]["compiledTemplates"] = this["App"]["compiledTemplates"] || {};

this["App"]["compiledTemplates"]["comments"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n	<li class=\"comment-item\" data-commentid=\"";
  if (stack1 = helpers._id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0._id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n		<div class=\"comment-author\">\n			"
    + escapeExpression(((stack1 = ((stack1 = depth0.author),stack1 == null || stack1 === false ? stack1 : stack1.firstName)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + " "
    + escapeExpression(((stack1 = ((stack1 = depth0.author),stack1 == null || stack1 === false ? stack1 : stack1.lastName)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\n		</div>\n		<span class=\"comment-time right\" title=\"";
  options = {hash:{
    'format': ("DD. MMMM YYYY. HH:mm:ss")
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.formatDate || depth0.formatDate),stack1 ? stack1.call(depth0, depth0.created, options) : helperMissing.call(depth0, "formatDate", depth0.created, options)))
    + "\">";
  options = {hash:{
    'format': ("DD.MM.YYYY.")
  },data:data};
  buffer += escapeExpression(((stack1 = helpers.formatDate || depth0.formatDate),stack1 ? stack1.call(depth0, depth0.created, options) : helperMissing.call(depth0, "formatDate", depth0.created, options)))
    + "</span>\n		<div class=\"comment\">";
  if (stack2 = helpers.comment) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.comment; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "</div>\n	</li>\n";
  return buffer;
  }

  stack1 = helpers.each.call(depth0, depth0, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  });

this["App"]["compiledTemplates"]["details.form.IRA"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing;


  buffer += "<form class=\"details-form\" data-id=\"";
  if (stack1 = helpers._id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0._id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n	<input class=\"hidden\" name=\"documentId\" type=\"hidden\" value=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentId)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\"/>\n	<input class=\"hidden\" name=\"documentType\" type=\"hidden\" value=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentType)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\"/>\n\n	<div class=\"input-field\">\n		<label title=\"Datum dokumenta\" for=\"documentDate\">Datum dokumenta</label>\n		<div class=\"input-append date-input\">\n			<input class=\"date-input-element form-control\" data-format=\"dd.MM.yyyy.\" placeholder=\"Odaberite...\" value=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", options) : helperMissing.call(depth0, "showValue", ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", options)))
    + "\" type=\"text\" />\n			<span class=\"add-on glyphicon glyphicon-calendar\"></span>\n		</div>\n		<input class=\"hidden\" name=\"documentDate\" type=\"hidden\" value=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", "native", options) : helperMissing.call(depth0, "showValue", ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", "native", options)))
    + "\"/>\n	</div>\n\n	<div class=\"input-field\">\n		<label title=\"Klijent\" for=\"partner\">Klijent</label>\n		<input name=\"partner\" class=\"form-control\" type=\"text\" value=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.partner)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" placeholder=\"Klijent..\"/>\n	</div>\n\n	<div class=\"input-field\">\n		<label title=\"Ukupan iznos\" for=\"totalAmount\">Ukupan iznos</label>\n		<input name=\"totalAmount\" class=\"form-control money-control\" type=\"text\" value=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.totalAmount), "currency", options) : helperMissing.call(depth0, "showValue", ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.totalAmount), "currency", options)))
    + "\" placeholder=\"Utipkajte..\"/>\n	</div>\n\n	<div class=\"input-field\">\n		<label title=\"Iznos PDV-a\" for=\"pdvAmount\">Iznos PDV-a</label>\n		<input name=\"pdvAmount\" class=\"form-control money-control\" type=\"text\" value=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.pdvAmount), "currency", options) : helperMissing.call(depth0, "showValue", ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.pdvAmount), "currency", options)))
    + "\" placeholder=\"Utipkajte..\"/>\n	</div>\n\n	<div class=\"input-field\">\n		<label title=\"Datum dospijeća\" for=\"paymentDate\">Datum dospijeća</label>\n		<div class=\"input-append date-input\">\n			<input class=\"date-input-element form-control\" data-format=\"dd.MM.yyyy.\" placeholder=\"Odaberite...\" value=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.paymentDate), "date", options) : helperMissing.call(depth0, "showValue", ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.paymentDate), "date", options)))
    + "\" type=\"text\" />\n			<span class=\"add-on glyphicon glyphicon-calendar\"></span>\n		</div>\n		<input class=\"hidden\" name=\"paymentDate\" type=\"hidden\" value=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.paymentDate), "date", "native", options) : helperMissing.call(depth0, "showValue", ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.paymentDate), "date", "native", options)))
    + "\"/>\n	</div>\n\n</form>";
  return buffer;
  });

this["App"]["compiledTemplates"]["details.form.URA"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing;


  buffer += "<form class=\"details-form\" data-id=\"";
  if (stack1 = helpers._id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0._id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n	<input class=\"hidden\" name=\"documentId\" type=\"hidden\" value=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentId)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\"/>\n	<input class=\"hidden\" name=\"documentType\" type=\"hidden\" value=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentType)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\"/>\n\n	<div class=\"input-field\">\n		<label title=\"Datum dokumenta\" for=\"documentDate\">Datum dokumenta</label>\n		<div class=\"input-append date-input\">\n			<input class=\"date-input-element form-control\" data-format=\"dd.MM.yyyy.\" placeholder=\"Odaberite...\" value=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", options) : helperMissing.call(depth0, "showValue", ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", options)))
    + "\" type=\"text\" />\n			<span class=\"add-on glyphicon glyphicon-calendar\"></span>\n		</div>\n		<input class=\"hidden\" name=\"documentDate\" type=\"hidden\" value=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", "native", options) : helperMissing.call(depth0, "showValue", ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", "native", options)))
    + "\"/>\n	</div>\n\n	<div class=\"input-field\">\n		<label title=\"Partner\" for=\"partner\">Partner</label>\n		<input name=\"partner\" class=\"form-control\" type=\"text\" value=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.partner)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" placeholder=\"Partner\"/>\n	</div>\n\n	<div class=\"input-field\">\n		<label title=\"Ukupan iznos\" for=\"totalAmount\">Ukupan iznos</label>\n		<input name=\"totalAmount\" class=\"form-control money-control\" type=\"text\" value=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.totalAmount), "currency", options) : helperMissing.call(depth0, "showValue", ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.totalAmount), "currency", options)))
    + "\" placeholder=\"Utipkajte..\"/>\n	</div>\n\n	<div class=\"input-field\">\n		<label title=\"Iznos PDV-a\" for=\"pdvAmount\">Iznos PDV-a</label>\n		<input name=\"pdvAmount\" class=\"form-control money-control\" type=\"text\" value=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.pdvAmount), "currency", options) : helperMissing.call(depth0, "showValue", ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.pdvAmount), "currency", options)))
    + "\" placeholder=\"Utipkajte..\"/>\n	</div>\n\n	<div class=\"input-field\">\n		<label title=\"Datum dospijeća\" for=\"paymentDate\">Datum dospijeća</label>\n		<div class=\"input-append date-input\">\n			<input class=\"date-input-element form-control\" data-format=\"dd.MM.yyyy.\" placeholder=\"Odaberite...\" value=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.paymentDate), "date", options) : helperMissing.call(depth0, "showValue", ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.paymentDate), "date", options)))
    + "\" type=\"text\" />\n			<span class=\"add-on glyphicon glyphicon-calendar\"></span>\n		</div>\n		<input class=\"hidden\" name=\"paymentDate\" type=\"hidden\" value=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.paymentDate), "date", "native", options) : helperMissing.call(depth0, "showValue", ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.paymentDate), "date", "native", options)))
    + "\"/>\n	</div>\n\n	<div class=\"input-field\">\n		<label title=\"Poziv na broj\" for=\"pozivNaBroj\">Poziv na broj</label>\n		<input name=\"pozivNaBroj\" class=\"form-control money-control\" type=\"text\" value=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.pozivNaBroj)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" placeholder=\"Utipkajte..\"/>\n	</div>\n\n	<div class=\"input-field\">\n		<label title=\"IBAN partnera\" for=\"IBAN\">IBAN partnera</label>\n		<input name=\"IBAN\" class=\"form-control\" type=\"text\" value=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.IBAN)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" placeholder=\"Utipkajte..\"/>\n	</div>\n\n</form>";
  return buffer;
  });

this["App"]["compiledTemplates"]["details.form.noId"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, functionType="function", escapeExpression=this.escapeExpression;


  buffer += "<form class=\"details-form\" data-id=\"";
  if (stack1 = helpers._id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0._id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n	<div class=\"input-field\">\n		<label title=\"Identifikator dokumenta\" for=\"documentId\">Identifikator dokumenta</label>\n		<input name=\"documentId\" class=\"form-control\" type=\"text\" value=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentId)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" placeholder=\"Utipkajte..\"/>\n	</div>\n</form>";
  return buffer;
  });

this["App"]["compiledTemplates"]["details"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function", self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n				<div class=\"document-form\">\n					";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.subtemplate || depth0.subtemplate),stack1 ? stack1.call(depth0, "details.form.URA", depth0, options) : helperMissing.call(depth0, "subtemplate", "details.form.URA", depth0, options)))
    + "\n				</div>\n			";
  return buffer;
  }

function program3(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\n				";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentId), {hash:{},inverse:self.program(6, program6, data),fn:self.program(4, program4, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n			";
  return buffer;
  }
function program4(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n					<div class=\"document-type-selector\">\n						";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.subtemplate || depth0.subtemplate),stack1 ? stack1.call(depth0, "home.docTypeSelector", depth0, options) : helperMissing.call(depth0, "subtemplate", "home.docTypeSelector", depth0, options)))
    + "\n					</div>\n					<form class=\"details-form\" data-id=\"";
  if (stack2 = helpers._id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0._id; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\">\n						<input class=\"hidden\" name=\"documentId\" type=\"hidden\" value=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentId)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\"/>\n						<input class=\"hidden\" name=\"documentType\" type=\"hidden\" value=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentType)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\"/>\n					</form>\n				";
  return buffer;
  }

function program6(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n					<div class=\"document-form\">\n						";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.subtemplate || depth0.subtemplate),stack1 ? stack1.call(depth0, "details.form.noId", depth0, options) : helperMissing.call(depth0, "subtemplate", "details.form.noId", depth0, options)))
    + "\n					</div>\n				";
  return buffer;
  }

function program8(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n				<div class=\"payment-history\">\n					<h4>Povijest plaćanja</h4>\n					<table class=\"table table-striped table-hover\">\n						<thead>\n							<tr class=\"active\">\n								<th class=\"date text-center\">Datum</th>\n								<th class=\"amount text-right\">Iznos</th>\n							</tr>\n						</thead>\n						<tbody>\n							";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.subtemplate || depth0.subtemplate),stack1 ? stack1.call(depth0, "details.paymentHistory", depth0, options) : helperMissing.call(depth0, "subtemplate", "details.paymentHistory", depth0, options)))
    + "\n						</tbody>\n					</table>\n				</div>\n			";
  return buffer;
  }

function program10(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\n				";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.currentTask),stack1 == null || stack1 === false ? stack1 : stack1.assignedToMe), {hash:{},inverse:self.noop,fn:self.program(11, program11, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n			";
  return buffer;
  }
function program11(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n					<div class=\"document-workflow-task\" data-documentid=\"";
  if (stack1 = helpers._id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0._id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n						";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.subtemplate || depth0.subtemplate),stack1 ? stack1.call(depth0, "home.workflow", depth0.currentTask, options) : helperMissing.call(depth0, "subtemplate", "home.workflow", depth0.currentTask, options)))
    + "\n					</div>\n				";
  return buffer;
  }

function program13(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n						";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.subtemplate || depth0.subtemplate),stack1 ? stack1.call(depth0, "comments", depth0.comments, options) : helperMissing.call(depth0, "subtemplate", "comments", depth0.comments, options)))
    + "\n					";
  return buffer;
  }

  buffer += "<div class=\"modal-header\">\n	<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\"><span class=\"glyphicon glyphicon-remove\"></span></button>\n	<h4 class=\"modal-title\">Detaljni prikaz dokumenta: <span>";
  if (stack1 = helpers.title) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.title; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</span></h4>\n</div>\n<div class=\"modal-body\">\n	<div class=\"document-info\"  data-id=\"";
  if (stack1 = helpers._id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0._id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n		<div class=\"document-data\">\n			";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentType), {hash:{},inverse:self.program(3, program3, data),fn:self.program(1, program1, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n			";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.isPaid), {hash:{},inverse:self.program(10, program10, data),fn:self.program(8, program8, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n		</div>\n		<div class=\"document-preview\">\n			<div id=\"docPreviewContainer\">\n				<img src=\"http://localhost:3000/files/document/";
  if (stack2 = helpers._id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0._id; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "/fullsize\" class=\"	document-image\">\n			</div>\n			<div id=\"commentsContainer\">\n				<h4>Komentari</h4>\n				<ul>\n					";
  stack2 = helpers['if'].call(depth0, depth0.comments, {hash:{},inverse:self.noop,fn:self.program(13, program13, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n				</ul>\n				<textarea class=\"comment-content form-control\" placeholder=\"Komentiraj..\"></textarea>\n				<button class=\"btn btn-success btn-sm send-comment displaynone\" data-loading-text=\"Šaljem..\">Pošalji</button>\n				<button class=\"btn btn-default btn-sm cancel-comment displaynone\" >Odustani</button>\n			</div>\n		</div>\n	</div>\n</div>\n<div class=\"modal-footer\">\n	<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Zatvori</button>\n	<button type=\"button\" class=\"btn btn-primary btn-save\" data-loading-text=\"Spremam..\">Spremi promjene</button>\n</div>";
  return buffer;
  });

this["App"]["compiledTemplates"]["details.paymentHistory"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n	<tr>\n		<td class=\"date\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, depth0.paymentDate, "date", options) : helperMissing.call(depth0, "showValue", depth0.paymentDate, "date", options)))
    + "</td>\n		<td class=\"amount text-right\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, depth0.amount, "currency", options) : helperMissing.call(depth0, "showValue", depth0.amount, "currency", options)))
    + "</td>\n	</tr>\n";
  return buffer;
  }

  stack1 = helpers.each.call(depth0, depth0.payments, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  });

this["App"]["compiledTemplates"]["document"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n	<tr class=\"document-item active\">\n		<td><a href=\"#/details/";
  if (stack1 = helpers._id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0._id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">";
  if (stack1 = helpers.documentId) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.documentId; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</a></td>\n		<td>";
  if (stack1 = helpers.documentType) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.documentType; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</td>\n		<td>";
  if (stack1 = helpers.title) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.title; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</td>\n		<td>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", options) : helperMissing.call(depth0, "showValue", ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", options)))
    + "</td>\n		<td class=\"text-right\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.totalAmount), "currency", options) : helperMissing.call(depth0, "showValue", ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.totalAmount), "currency", options)))
    + "</td>\n		<td class=\"text-right ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.isPaid), {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.paidAmount), "currency", options) : helperMissing.call(depth0, "showValue", ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.paidAmount), "currency", options)))
    + "</td>\n	</tr>\n";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return "paid";
  }

  stack1 = helpers.each.call(depth0, depth0.documents, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  });

this["App"]["compiledTemplates"]["documents"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"documents-search-tool left\">\n	<input placeholder=\"Pretraži...\" type=\"text\" class=\"form-control\">\n	<span class=\"glyphicon glyphicon-search\"></span>\n</div>\n<table class=\"document-list table table-striped table-hover sortable\">\n	<thead>\n		<tr class=\"active\">\n			<th>ID</th>\n			<th>Tip</th>\n			<th>Naziv</th>\n			<th>Datum</th>\n			<th>Ukupni iznos</th>\n			<th>Plaćeni iznos</th>\n		</tr>\n	</thead>\n	<tbody></tbody>\n</table>";
  });

this["App"]["compiledTemplates"]["home.docTypeSelector"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<div class=\"btn-group document-types right\">\n	<button type=\"button\" class=\"btn btn-info btn-doc-type\" data-docType=\"URA\">URA</button>\n	<button type=\"button\" class=\"btn btn-info btn-doc-type\" data-docType=\"IRA\">IRA</button>\n</div>";
  });

this["App"]["compiledTemplates"]["home"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\n	<div class=\"card panel panel-primary\" data-id=\"";
  if (stack1 = helpers._id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0._id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n		<div class=\"card-heading panel-heading\">";
  if (stack1 = helpers.title) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.title; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</div>\n		<div class=\"doc-info\">\n			<div class=\"document-type left\" title=\"Tip dokumenta\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentType)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\n			<div class=\"document-id right\" title=\"Identifikator dokumenta\">"
    + escapeExpression(((stack1 = ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentId)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</div>\n		</div>\n\n		<div class=\"card-content panel-body\">\n			<div class=\"document-preview\">\n				<img class=\"preview\" src=\"http://localhost:3000/files/document/";
  if (stack2 = helpers._id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0._id; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "/preview\" />\n			</div>\n		</div>\n		";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.isPaid), {hash:{},inverse:self.program(4, program4, data),fn:self.program(2, program2, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n\n		<div class=\"card-footer panel-footer comments-container\">\n			<ul class=\"comments-list\">\n				";
  stack2 = helpers['if'].call(depth0, depth0.comments, {hash:{},inverse:self.noop,fn:self.program(7, program7, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n			</ul>\n			<textarea class=\"comment-content form-control\" placeholder=\"Komentiraj..\"></textarea>\n			<button class=\"btn btn-success btn-sm send-comment displaynone\" data-loading-text=\"Šaljem..\">Pošalji</button>\n			<button class=\"btn btn-default btn-sm cancel-comment displaynone\" >Odustani</button>\n		</div>\n\n	</div>\n";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return "\n			<div class=\"document-paid\">Plaćeno</div>\n		";
  }

function program4(depth0,data) {
  
  var buffer = "", stack1, stack2;
  buffer += "\n			";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.currentTask),stack1 == null || stack1 === false ? stack1 : stack1.assignedToMe), {hash:{},inverse:self.noop,fn:self.program(5, program5, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n		";
  return buffer;
  }
function program5(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n				<div class=\"document-workflow-task\" data-documentid=\"";
  if (stack1 = helpers._id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0._id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n					";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.subtemplate || depth0.subtemplate),stack1 ? stack1.call(depth0, "home.workflow", depth0.currentTask, options) : helperMissing.call(depth0, "subtemplate", "home.workflow", depth0.currentTask, options)))
    + "\n				</div>\n			";
  return buffer;
  }

function program7(depth0,data) {
  
  var buffer = "", stack1, options;
  buffer += "\n					";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.subtemplate || depth0.subtemplate),stack1 ? stack1.call(depth0, "comments", depth0.comments, options) : helperMissing.call(depth0, "subtemplate", "comments", depth0.comments, options)))
    + "\n				";
  return buffer;
  }

  stack1 = helpers.each.call(depth0, depth0.documents, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  });

this["App"]["compiledTemplates"]["home.workflow"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, options, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing;


  buffer += "<div class=\"workflow-task\">\n	<div class=\"task-title\" data-name=\"";
  if (stack1 = helpers.name) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.name; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\">\n		<span class=\"glyphicon glyphicon-tasks icon left\" title=\"Vaš zadatak\"></span>\n		<div class=\"title\">";
  if (stack1 = helpers.label) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0.label; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "</div>\n	</div>\n	<div class=\"task-actions\">\n		<button type=\"button\" class=\"btn btn-primary complete-task right\">\n			";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.getTaskName || depth0.getTaskName),stack1 ? stack1.call(depth0, depth0, options) : helperMissing.call(depth0, "getTaskName", depth0, options)))
    + "\n		</button>\n	</div>\n</div>";
  return buffer;
  });

this["App"]["compiledTemplates"]["payment"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var stack1, functionType="function", escapeExpression=this.escapeExpression, helperMissing=helpers.helperMissing, self=this;

function program1(depth0,data) {
  
  var buffer = "", stack1, stack2, options;
  buffer += "\n	<tr class=\"document-item active\" data-id=\"";
  if (stack1 = helpers._id) { stack1 = stack1.call(depth0, {hash:{},data:data}); }
  else { stack1 = depth0._id; stack1 = typeof stack1 === functionType ? stack1.apply(depth0) : stack1; }
  buffer += escapeExpression(stack1)
    + "\" data-amount=\""
    + escapeExpression(((stack1 = ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.totalAmount)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "\" data-maxamount=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.getAmountToPay || depth0.getAmountToPay),stack1 ? stack1.call(depth0, depth0, options) : helperMissing.call(depth0, "getAmountToPay", depth0, options)))
    + "\">\n		<td><a href=\"#/details/";
  if (stack2 = helpers._id) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0._id; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" class=\"open-details\">";
  if (stack2 = helpers.documentId) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.documentId; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "</a></td>\n		<td>"
    + escapeExpression(((stack1 = ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.partner)),typeof stack1 === functionType ? stack1.apply(depth0) : stack1))
    + "</td>\n		<td>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", options) : helperMissing.call(depth0, "showValue", ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.documentDate), "date", options)))
    + "</td>\n		<td>";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.paymentDate), "date", options) : helperMissing.call(depth0, "showValue", ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.paymentDate), "date", options)))
    + "</td>\n		<td class=\"text-right\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.totalAmount), "currency", options) : helperMissing.call(depth0, "showValue", ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.totalAmount), "currency", options)))
    + "</td>\n		<td class=\"text-right ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.isPaid), {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.paidAmount), "currency", options) : helperMissing.call(depth0, "showValue", ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.paidAmount), "currency", options)))
    + "</td>\n		<td class=\"text-right ";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.isPaid), {hash:{},inverse:self.noop,fn:self.program(2, program2, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\">";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.getAmountToPay || depth0.getAmountToPay),stack1 ? stack1.call(depth0, depth0, options) : helperMissing.call(depth0, "getAmountToPay", depth0, options)))
    + "</td>\n		<td class=\"docs-to-pay-actions\">\n			";
  stack2 = helpers['if'].call(depth0, ((stack1 = depth0.metadata),stack1 == null || stack1 === false ? stack1 : stack1.isPaid), {hash:{},inverse:self.program(6, program6, data),fn:self.program(4, program4, data),data:data});
  if(stack2 || stack2 === 0) { buffer += stack2; }
  buffer += "\n		</td>\n	</tr>\n";
  return buffer;
  }
function program2(depth0,data) {
  
  
  return "paid";
  }

function program4(depth0,data) {
  
  
  return "\n			";
  }

function program6(depth0,data) {
  
  
  return "\n				<button type=\"button\" class=\"btn btn-xs btn-info btn-add-payment right\">Dodaj plaćanje</button>\n			";
  }

  stack1 = helpers.each.call(depth0, depth0.payments, {hash:{},inverse:self.noop,fn:self.program(1, program1, data),data:data});
  if(stack1 || stack1 === 0) { return stack1; }
  else { return ''; }
  });

this["App"]["compiledTemplates"]["payment.new"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  var buffer = "", stack1, stack2, options, helperMissing=helpers.helperMissing, escapeExpression=this.escapeExpression, functionType="function";


  buffer += "<form class=\"create-payment\">\n	<div class=\"input-field\">\n		<label title=\"Iznos plaćanja\" for=\"amount\">Iznos</label>\n		<input name=\"amount\" type=\"text\" value=\"";
  options = {hash:{},data:data};
  buffer += escapeExpression(((stack1 = helpers.showValue || depth0.showValue),stack1 ? stack1.call(depth0, depth0.amount, "currency", options) : helperMissing.call(depth0, "showValue", depth0.amount, "currency", options)))
    + "\" placeholder=\"Unesite iznos...\" class=\"form-control money-control input-small\" />\n	</div>\n	<div class=\"input-field\">\n		<label title=\"Datum plaćanja\" for=\"paymentDate\">Datum</label>\n		<div class=\"input-append date-input\">\n			<input class=\"date-input-element form-control\" data-format=\"dd.MM.yyyy.\" placeholder=\"Odaberite...\" value=\"\" type=\"text\" />\n			<span class=\"add-on glyphicon glyphicon-calendar\"></span>\n		</div>\n		<input class=\"hidden\" name=\"paymentDate\" type=\"hidden\" value=\"\"/>\n	</div>\n	<button class=\"btn btn-primary btn-save-payment\" data-documentid=\"";
  if (stack2 = helpers.documentId) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.documentId; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" data-maxamount=\"";
  if (stack2 = helpers.amount) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.amount; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" data-amounttopay=\"";
  if (stack2 = helpers.amountLeftToPay) { stack2 = stack2.call(depth0, {hash:{},data:data}); }
  else { stack2 = depth0.amountLeftToPay; stack2 = typeof stack2 === functionType ? stack2.apply(depth0) : stack2; }
  buffer += escapeExpression(stack2)
    + "\" data-loading-text=\"Snimam...\">Spremi plaćanje</button>\n	<button class=\"btn btn-default btn-cancel-payment\">Poništi</button>\n</form>";
  return buffer;
  });

this["App"]["compiledTemplates"]["payments"] = Handlebars.template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};
  


  return "<ul class=\"nav nav-tabs\" role=\"tablist\">\n	<li class=\"active\"><a href=\"#incoming\" role=\"tab\" data-toggle=\"tab\">Ulazni računi</a></li>\n	<li><a href=\"#outgoing\" role=\"tab\" data-toggle=\"tab\">Izlazni računi</a></li>\n</ul>\n\n<div class=\"tab-content\">\n	<div class=\"tab-pane active\" id=\"incoming\">\n\n		<div class=\"documents-search-tool left\">\n			<input placeholder=\"Pretraži...\" type=\"text\" class=\"form-control\">\n			<span class=\"glyphicon glyphicon-search\"></span>\n		</div>\n		<table class=\"document-list table table-striped table-hover sortable\">\n			<thead>\n				<tr class=\"active\">\n					<th>ID dokumenta</th>\n					<th>Partner</th>\n					<th>Datum</th>\n					<th>Dospijeće</th>\n					<th>Ukupan iznos</th>\n					<th>Plaćeni iznos</th>\n					<th>Za platiti</th>\n					<th>Akcije</th>\n				</tr>\n			</thead>\n			<tbody></tbody>\n		</table>\n	</div>\n	<div class=\"tab-pane\" id=\"outgoing\">\n		<div class=\"documents-search-tool left\">\n			<input placeholder=\"Pretraži...\" type=\"text\" class=\"form-control\">\n			<span class=\"glyphicon glyphicon-search\"></span>\n		</div>\n		<table class=\"document-list table table-striped table-hover sortable\">\n			<thead>\n				<tr class=\"active\">\n					<th>ID dokumenta</th>\n					<th>Partner</th>\n					<th>Datum</th>\n					<th>Dospijeće</th>\n					<th>Ukupan iznos</th>\n					<th>Plaćeni iznos</th>\n					<th>Za platiti</th>\n					<th>Akcije</th>\n				</tr>\n			</thead>\n			<tbody></tbody>\n		</table>\n	</div>\n</div>";
  });