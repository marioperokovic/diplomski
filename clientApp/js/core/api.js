App.Api = App.Api || (function (){
	var module = {
		apiPath: "App.conf.apiPath",

		extend: function ( extendedModule ) {
			var m = Object.create( this );

			$.extend( m, extendedModule );

			return m;
		},

		callApi: function( type, data, callback, fail ) {

			var config = {
				
				success: function ( response ) {

				},
				error: function ( response ) {

				},
				complete: function ( response ) {

				}
			};

			if ( App.apis.defaultRequestData ) {
				$.extend( data, App.apis.defaultRequestData );
				// _log( 'imam dodatnih podataka' );
			}
			else {
				// _log( 'nemam dodatnih podataka' );
			}

			$.ajax({
				url: App.conf.apiPath + this.api,
				type: type,
				dataType: 'JSON',
				data: ( data ) ? data : {}
			})
			.done( function ( response ) {
				module.onSuccess( callback, response );
			})
			.fail( function ( response ) {
				module.onError( fail, response, data );
			});
		},

		get: function ( callback, data, fail ) {
			this.callApi( 'GET', data, callback, fail );
		},

		post: function ( callback, data, fail ) {
			this.callApi( 'POST', data, callback, fail );
		},

		put: function ( callback, data, fail ) {
			this.callApi( 'PUT', data, callback, fail );
		},

		delete: function ( callback, data, fail ) {
			this.callApi( 'DELETE', data, callback, fail );
		},

		onSuccess: function ( callback, response ) {
			if ( callback ) {
				callback( response );
			}
		},

		onError: function ( callback, response, requestData ) {
			_log( "[ Error calling API ]" );
			// _log( "[\tRequest: ", requestData );
			_log( "[\tResponse: ", JSON.stringify( response ) );

			if ( callback ) {
				callback( JSON.parse( response.responseText ) );
			}
		}
	};

	return module;
})();