App.Module = App.Module || (function() {
	var module = {
		name: 'Module',
		meta: {},

		extend: function ( extendedModule ) {
			var m = Object.create( this );

			$.extend( m, extendedModule );

			m.start();
			return m;
		},

		start: function () {
			_log( 'inicijaliziram modul ' + this.name );
			var el = this.rootElement;
			if ( this.rootElement === 'document' ) {
				el = document;
			}
			this.$el = $( el );
			this.meta = {};
		},

		render: function ( template, data, method ) {
			_log( 'renderiram template ' + template + ' u ' + this.name );

			this.templates[ template ].fn = App.compiledTemplates[ template ];

			if ( !this.templates[ template ].$el || this.templates[ template ].cacheElement === false ) {
				this.templates[ template ].$el = $( this.templates[ template ].renderIn );
			}

			var html = this.templates[ template ].fn( data );

			if ( method === 'html' ) {
				return html;
			}

			switch ( method ) {
				case 'prepend':
					this.templates[ template ].$el
						.prepend( html );
					break;
				case 'clean':
					this.templates[ template ].$el
						.empty()
						.prepend( html );
					break;
				default: // append
					this.templates[ template ].$el
						.append( html );
			}
		}
	};

	return module;
})();