App.modules.details = App.modules.details || (function () {
	var module = App.Module.extend({
		name: "details",
		rootElement: "#details",

		"templates": {
			"details": { renderIn: "#details .modal-content" },
			"comments": { renderIn: "#" }
		},

		show: function ( docId ) {
			//console.log('Hello from details module!!');
			App.modules.main.navbarClick();

			module.$el.modal('show');

			App.apis.document.get( function ( response ) {
				_log( 'success document.get: ', response );
				module.render( "details", response.output.document, 'clean' );
				module.doc = response.output.document;
				module.cacheElements();
				module.bindEvents();
				module.generateForm();
				App.modules.widgets.init();
				NProgress.done( true );
			}, {
				documentId: docId
			}, function ( error ) {
				_log( 'error document.get ', error );
			});
		},

		cacheElements: function () {
			module.$saveBtn = module.$el.find( '.btn-save' );
		},

		bindEvents: function () {
			module.$el.off('hide.bs.modal').on( 'hide.bs.modal', module.setFormerRoute );
			module.$saveBtn.off('click').on( 'click', module.saveDocumentForm );

			module.$el.find( ".comment-content" ).off('click').on( 'click', module.writeNewComment );
			module.$el.find( ".comment-content" ).off('keypress').on( 'keypress', module.writeNewComment );
			module.$el.find( ".cancel-comment" ).off().on( 'click', module.cancelComment );
			module.$el.find( ".send-comment" ).off().on( 'click', module.sendComment );
		},

		setFormerRoute: function ( e ) {
			if( $(e.target).hasClass('input-append') ) {
				return;
			}
			_log( 'šaljem te na staru rutu' );
			App.routes.setRoute( module.meta.formerRoute || "home" );
		},

		saveDocumentForm: function ( callback ) {
			_log( 'spremam promjene' );
			var properties = module.$el.find('form.details-form').serializeArray(),
				data = module.getFormData( properties );

			_log( 'evo data', data );
			App.apis.document.put( function ( response ) {
				toastr.success( 'Uspješno ste snimili promjene!' );
				module.$el.modal('hide');
				if ( callback && typeof( callback ) == "function" ) {
					callback();
				}
			}, {
				documentId: module.doc._id,
				properties: {
					metadata: data
				}
			}, function ( error ) {
				_log( 'error updating document!', error );
			});
		},

		getFormData: function ( data ) {
			var dataToSend = {};

			for ( var i = 0; i < data.length; i++ ) {
				if ( data[i].value && data[i].value.length > 0 ) {
					if ( data[i].name === "totalAmount" || data[i].name === "pdvAmount" ) {
						data[i].value = money2num( data[i].value );
					}
					dataToSend[ data[i].name ] = data[i].value;
				}
			}

			return dataToSend;
		},

		writeNewComment: function ( e ) {
			if ( e.type === 'click' ) {
				var $textarea = $(this),
					$comments = $textarea.parents( '#commentsContainer' );
				$comments.find( 'button' ).show();
				$textarea.addClass( 'active-area' );
			}
			else {
				if ( e.keyCode === 13 ) {
					e.preventDefault;
					module.sendComment( null, $(this) );
					return false;
				}
			}
		},

		cancelComment: function () {
			var $comments = $(this).parents( '#commentsContainer' ),
				$textarea = $comments.find( 'textarea' );
			$textarea.removeClass( 'active-area' );
			$textarea.val('');
			$comments.find( 'button' ).hide();
		},

		sendComment: function ( e, textarea ) {
			if ( textarea ) {
				var $comments = $( textarea ).parents( '#commentsContainer' );
				$( textarea ).blur();
			}
			else {
				var $comments = $(this).parents( '#commentsContainer' );
			}
			
			var	$textarea = $comments.find( 'textarea' ),
				documentId = $comments.parents( '.document-info' ).attr( 'data-id' ),
				comment = $.trim( $textarea.val() ),
				$sendBtn = $comments.find( '.send-comment' );
			$sendBtn.button( 'loading' );
			
			if( !comment || comment === '' ) {
				bootbox.alert( 'Niste unijeli Vaš komentar!' );
				$sendBtn.button( 'reset' );
				return;
			}
			
			App.apis.comments.post( function ( response ) {
				_log( 'success comments.post ', response );
				$sendBtn.button( 'reset' );
				$textarea.val('');
				$textarea.removeClass( 'active-area' );
				$comments.find( 'button' ).hide();
				var html = module.render( 'comments', [ response.output.comment ], 'html' );
				$comments.find( 'ul' ).append( html );
			}, {
				documentId: documentId,
				comment: comment,
				author: App.user._id
			}, function ( error ) {
				_log( 'error comments.post ', error );
			});
		},

		generateForm: function () {
			module.$el.find('.input-append.date-input')
				.datetimepicker({ pickTime: true })
				.on('changeDate', function( e ) {
					_log( 'date changed!' );
					var $widget = $(this),
						$input = $widget.find('input.date-input-element'),
						selectedDate = $input.val(),
						$hidden = $widget.parents('.input-field').find('input.hidden');
					if( selectedDate === '' ) {
						$hidden.val('');
					}
					else {
						$hidden.val( moment( selectedDate, 'DD.MM.YYYY. HH:mm' ).unix() );
					}
					$input.trigger('blur');
					$('.bootstrap-datetimepicker-widget').fadeOut();
				});
		}
	});

	return module;
})();