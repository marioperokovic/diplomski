App.modules.payments = App.modules.payments || (function () {
	var module = App.Module.extend({
		name: "payments",
		rootElement: "#payments",

		"templates": {
			"payments": { renderIn: "#payments" },
			"payment": { renderIn: "#payments .document-list tbody" },
			"payment.new": { renderIn: "#" }
		},

		show: function () {
			console.log('Hello from payments module!!');
			App.modules.main.navbarClick();
			if ( module.$el.is(':empty') ) {
				module.render( "payments", {} );
			}
			module.$el.show();

			module.getDocuments( 'incoming', function ( payments ) {
				var html = module.render( "payment", { payments: payments }, 'html' );
				module.$el.find( "#incoming .document-list tbody" ).html( html );

				module.getDocuments( 'outgoing', function ( docs ) {
					var html = module.render( "payment", { payments: docs }, 'html' );
					module.$el.find( "#outgoing .document-list tbody" ).html( html );
					$.bootstrapSortable(true);
					module.cacheElements();
					module.bindEvents();
					module.attachPopover();
					NProgress.done( true );
				});
			});
		},

		cacheElements: function () {
			module.$linkToDocument = module.$el.find( '.open-details' );
			module.$searchInput = module.$el.find( '.documents-search-tool input' );
		},

		bindEvents: function () {
			module.$searchInput.off('keyup').on( 'keyup', module.searchDocuments );
			module.$linkToDocument.off('click').on( 'click', module.openDetails );
		},

		getDocuments: function ( direction, callback ) {
			App.apis.payments.get( function ( response ) {
				_log( 'success payments.get in ' + direction + ' direction.', response );

				if ( callback ) callback( response.output.payments );
			}, {
				direction: direction
			}, function ( error ) {
				_log( 'error payments.get ', error );
			});
		},

		openDetails: function () {
			App.modules.details.meta.formerRoute = "payments";
		},

		searchDocuments: function () {
			var inputVal = $(this).val();
			var table = $(this).parents( ".tab-pane" ).find("table");

			table.find('tbody tr').each(function(index, row) {
				var allCells = $(row).find('td');

				if ( allCells.length > 0 ) {
					var found = false;
					allCells.each(function( index, td ) {
						var regExp = new RegExp(inputVal, 'i');
						if ( regExp.test($(td).text()) ) {
							found = true;
							return false;
						}
					});

					found ? $(row).show() : $(row).hide();
				}
			});
		},

		attachPopover: function ( $button ) {
			var attach = function ( $btn ) {
				var $docItem = $btn.parents('.document-item'),
					amount = money2num( $docItem.attr('data-maxamount') ),
					docId = $docItem.attr('data-id');
				$btn.popover({
					html: true,
					title: function () {
						return 'Unesite podatke o plaćanju';
					},
					content: function () {
						return module.render( 'payment.new', {
							amount: amount,
							documentId: docId
						}, 'html');
					},
					trigger: 'click',
					placement: 'left'
				});
				$btn.on( 'shown.bs.popover', function () {

					$btn.parent().find('.popover input[name="amount"]').focus();
					$btn.parent().find('.popover-content .input-append.date-input')
						.datetimepicker({ pickTime: true })
						.on('changeDate', function( e ) {
							_log( 'date changed!' );
							var $widget = $(this),
								$input = $widget.find('input.date-input-element'),
								selectedDate = $input.val(),
								$hidden = $widget.parents('.input-field').find('input.hidden');
							if ( selectedDate === '' ) {
								$hidden.val('');
							} else {
								$hidden.val( moment( selectedDate, 'DD.MM.YYYY. HH:mm' ).unix() );
							}
							$input.trigger('blur');
							$('.bootstrap-datetimepicker-widget').fadeOut();
						});
				});

				$btn.parents('.docs-to-pay-actions')
					.on('click', '.popover-content .btn-cancel-payment', function ( e ) {
						e.preventDefault();
						$btn.popover( 'hide' );
					})
					.on('click', '.popover-content .btn-save-payment', function ( e ) {
						e.preventDefault();
						$(this).attr( 'disabled', true );
						var $form = $(this).parents( 'form' ),
							maxAmount = money2num( $(this).attr( 'data-maxamount' ) ),
							enteredAmount = money2num( $form.find( 'input[name="amount"]' ).val() ),
							date = $form.find( 'input[name="paymentDate"]' ).val(),
							docId = $(this).attr('data-documentid');

						if ( !enteredAmount || !date ) {
							bootbox.alert( 'Nisu uneseni svi podaci!' );
							$(this).attr( 'disabled', false );
							return;
						} else if ( parseFloat( enteredAmount ) > parseFloat( maxAmount ) ) {
							bootbox.alert( 'Ne možete unijeti veći iznos od ukupnog iznosa računa!' );
							$(this).attr( 'disabled', false );
							return;
						}

						App.apis.payments.post( function ( response ) {
							console.log( 'success payments post', response );
							module.updateDocumentOnList( response.output.document );
							$btn.popover( 'hide' );
							module.attachPopover( $btn );
						}, {
							documentId: docId,
							amount: enteredAmount,
							paymentDate: date
						}, function ( error ) {
							console.log( 'error payments post', error );
							toastr.warning( 'Došlo je do pogreške, pokušajte ponovno!' );
							$(this).attr( 'disabled', false );
						});
				});
				
			};

			if ( $button ) {
				attach( $button );
			}
			else {
				module.$el.find('.btn-add-payment').each( function ( k, v ) {
					attach( $( v ) );
				});
			}
		},

		updateDocumentOnList: function ( doc ) {
			var $tr = module.$el.find( ".document-list tr[data-id='" + doc._id + "']" ),
				html = module.render( 'payment', { payments: [ doc ] }, 'html' );
			$tr.replaceWith( html );
		}
	});

	return module;
})();