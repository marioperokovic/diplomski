App.modules.login = App.modules.login || (function () {
	var module = App.Module.extend({
		name: "login",
		rootElement: "#login",

		show: function () {
			if ( App.user ) { 
				_log( 'already signed in' );
				App.routes.setRoute( "home" );
				return;
			}
			_log('Hello from login module!!');
			module.$el.show();
			module.cacheElements();
			module.bindEvents();
			App.modules.main.disableNavigation();
			NProgress.done();
		},

		cacheElements: function () {
			module.$button = module.$el.find( 'button#loginButton' );
			module.$email = module.$el.find( 'input[name="email"]' );
			module.$password = module.$el.find( 'input[name="password"]' );
		},

		bindEvents: function () {
			module.$button.off('click').on( 'click', module.handleLogin );
		},

		handleLogin: function ( e ) {
			e.preventDefault();
			var email = module.$email.val(),
				password = module.$password.val();

			if ( !email || !password ) {
				toastr.warning( "Niste unijeli sve podatke!" );
			}
			else {
				App.apis.user.post( function ( response ) {
					_log( 'Uspješan login!', response );
					toastr.success( "Uspješno ste se prijavili!" );
					App.user = response.output.user;
					App.modules.main.setDefaultRequestData();
					App.modules.main.saveLocal();
					App.modules.main.renderUser();
					App.modules.main.enableNavigation();
					App.routes.setRoute( module.meta.formerRoute || "home" );
				}, {
					method: 'login',
					email: email,
					password: password
				}, function ( error ) {
					_log( 'neuspješan login!', error );
					toastr.warning( "Neispravni podaci!" );
				});
			}
		}
	});

	return module;
})();