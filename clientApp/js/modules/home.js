App.modules.home = App.modules.home || (function () {
	var module = App.Module.extend({
		name: "home",
		rootElement: "#home",

		"templates": {
			"home": { renderIn: "#home" },
			"comments": { renderIn: ".card .comments-list" }
		},

		show: function () {
			// console.log('Hello from home module!!');
			App.modules.main.navbarClick();

			module.getDocuments( function () {
				if ( module.$el.is(':empty') ) {
					module.render( "home", { documents: App.documents } );
				}

				module.$el.show();
				if ( !module.masonry ) {
					module.masonry = module.$el.masonry({
						gutter: 20,
						itemSelector: '.card'
					}).data('masonry');
				}
				
				module.cacheElements();
				module.bindEvents();
				App.modules.widgets.init();
				NProgress.done( true );
			});

		},

		cacheElements: function () {
		},

		bindEvents: function () {
			module.$el.find( ".card .panel-heading" ).off().on('click', module.openDetails );
			module.$el.find( ".card .card-content .document-preview img" ).off().on('click', module.openDetails );
			module.$el.find( ".card .card-footer .comment-content" ).off('click').on( 'click', module.writeNewComment );
			module.$el.find( ".card .card-footer .comment-content" ).off('keypress').on( 'keypress', module.writeNewComment );
			module.$el.find( ".card .card-footer .cancel-comment" ).off().on( 'click', module.cancelComment );
			module.$el.find( ".card .card-footer .send-comment" ).off().on( 'click', module.sendComment );
		},

		openDetails: function ( e ) {
			var docId;
			if ( $(this).hasClass( 'card' ) ) {
				docId = $(this).attr( "data-id" )
			}
			else {
				docId = $(this).parents( '.card' ).attr( "data-id" );
			}
			App.modules.details.meta.formerRoute = "home";
			App.routes.setRoute( "details/" + docId );
		},

		getDocuments: function ( callback ) {
			App.apis.documents.get( function ( response ) {
				_log( 'success documents.get ', response );
				App.documents = response.output.documents;

				if ( callback ) callback();
			}, {}, function ( error ) {
				_log( 'error documents.get ', error );
			});
		},

		writeNewComment: function ( e ) {
			if ( e.type === 'click' ) {
				var $textarea = $(this),
					$comments = $textarea.parents( '.comments-container' );
				$comments.find( 'button' ).show();
				$textarea.addClass( 'active-area' );
				module.refreshMasonry();
			}
			else {
				if ( e.keyCode === 13 ) {
					e.preventDefault;
					module.sendComment( null, $(this) );
					return false;
				}
			}
		},

		cancelComment: function () {
			var $comments = $(this).parents( '.comments-container' ),
				$textarea = $comments.find( 'textarea' );
			$textarea.removeClass( 'active-area' );
			$textarea.val('');
			$comments.find( 'button' ).hide();
			module.refreshMasonry();
		},

		sendComment: function ( e, textarea ) {
			if ( textarea ) {
				var $comments = $( textarea ).parents( '.comments-container' );
			}
			else {
				var $comments = $(this).parents( '.comments-container' );
			}
			
			var	$textarea = $comments.find( 'textarea' ),
				documentId = $comments.parents( '.card' ).attr( 'data-id' ),
				comment = $.trim( $textarea.val() ),
				$sendBtn = $comments.find( '.send-comment' );
			$sendBtn.button( 'loading' );
			
			if( !comment || comment === '' ) {
				bootbox.alert( 'Niste unijeli Vaš komentar!' );
				$sendBtn.button( 'reset' );
				return;
			}
			
			App.apis.comments.post( function ( response ) {
				_log( 'success comments.post ', response );
				$sendBtn.button( 'reset' );
				$textarea.val('');
				$textarea.removeClass( 'active-area' );
				var html = module.render( 'comments', [ response.output.comment ], 'html' );
				$comments.find( 'ul.comments-list' ).append( html );
				module.refreshMasonry();
			}, {
				documentId: documentId,
				comment: comment,
				author: App.user._id
			}, function ( error ) {
				_log( 'error comments.post ', error );
			});
		},

		refreshMasonry: function () {
			module.masonry.layout();
		}
	});

	return module;
})();