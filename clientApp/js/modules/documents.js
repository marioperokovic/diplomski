App.modules.documents = App.modules.documents || (function () {
	var module = App.Module.extend({
		name: "documents",
		rootElement: "#documents",

		"templates": {
			"documents": { renderIn: "#documents" },
			"document": { renderIn: "#documents .document-list tbody" }
		},

		show: function () {
			console.log('Hello from documents module!!');
			App.modules.main.navbarClick();
			
			module.getDocuments( function () {
				if ( module.$el.is(':empty') ) {
					module.render( "documents", {} );
				}
				module.$el.show();

				module.render( "document", { documents: App.documents }, 'clean' );
				$.bootstrapSortable(true);
				module.cacheElements();
				module.bindEvents();
				NProgress.done( true );
			});
		},

		cacheElements: function () {
			module.$linkToDocument = module.$el.find( '.document-item' );
			module.$searchInput = module.$el.find( '.documents-search-tool input' );
		},

		bindEvents: function () {
			module.$linkToDocument.off('click').on('click', module.openDetails );
			module.$searchInput.off('keyup').on( 'keyup', module.searchDocuments );
		},

		openDetails: function ( e ) {
			App.modules.details.meta.formerRoute = "documents";
		},

		getDocuments: function ( callback ) {
			App.apis.documents.get( function ( response ) {
				_log( 'success documents.get ', response );
				App.documents = response.output.documents;

				if ( callback ) callback();
			}, {}, function ( error ) {
				_log( 'error documents.get ', error );
			});
		},

		searchDocuments: function () {
			var inputVal = $(this).val();
			var table = $(this).parents( "#documents" ).find("table");

			table.find('tbody tr').each(function(index, row) {
				var allCells = $(row).find('td');

				if ( allCells.length > 0 ) {
					var found = false;
					allCells.each(function( index, td ) {
						var regExp = new RegExp(inputVal, 'i');
						if ( regExp.test($(td).text()) ) {
							found = true;
							return false;
						}
					});

					found ? $(row).show() : $(row).hide();
				}
			});
		}
	});

	return module;
})();