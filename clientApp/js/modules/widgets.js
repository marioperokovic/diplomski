App.modules.widgets = App.modules.widgets || (function () {
	var module = App.Module.extend({
		name: "widgets",
		rootElement: "document",

		"templates": {
			"home.docTypeSelector": { renderIn: "#" },
			"home": { renderIn: "#" }
		},

		init: function () {
			module.bindEvents();
		},

		bindEvents: function () {
			module.$el.find( '.document-type-selector button' ).off().on('click', module.setDocumentType );
			module.$el.find( '.document-workflow-task button.complete-task' ).off().on('click', module.completeTask );
		},

		setDocumentType: function () {
			$(this).parents('.document-types').find('button').removeClass('active');
			$(this).addClass('active');
			module.$el.find( '.details-form input[name="documentType"]' ).val( $(this).attr('data-doctype') );
		},

		completeTask: function () {
			var that = $(this);
			var complete = function () {
				var docId = that.parents('.document-workflow-task').attr('data-documentid');
				App.apis.document.get( function ( response ) {
					var doc = response.output.document;

					App.apis.workflows.put( function ( r ) {
						_log( 'success put workflows', r );
						toastr.success( 'Zadatak uspješno završen!' );
						if ( $("#details").is(':visible') ) {
							//App.modules.details.show( r.output.document._id );
							App.routes.setRoute( App.modules.details.meta.formerRoute || "home" );
						}
						var $card = $("#home").find( '.card[data-id="' + r.output.document._id + '"]' ),
							html = module.render( 'home', { documents: [ r.output.document ] }, 'html' );
						$card.replaceWith( html );

						// $cardChanged = $("#home").find( '.card[data-id="' + r.output.document._id + '"]' );
						// $cardChanged.addClass('data-updated');
						// setTimeout( function() {
						// 	$cardChanged.removeClass('data-updated');
						// }, 500 );
						App.modules.home.masonry.reloadItems();
						App.modules.home.refreshMasonry();
						App.modules.home.bindEvents();
					}, {
						documentId: doc._id,
						task: doc.currentTask.name,
						taskCompleted: true
					}, function ( error ) {
						_log( 'error put workflow:', error );
						if ( error.type === "missingParameter" ) {
							bootbox.alert( error.message );
						}
					});
				}, {
					documentId: docId
				});
			};

			if ( App.routes.getRoute().indexOf("details") !== -1 ) {
				//$("#details .btn-save").click();
				App.modules.details.saveDocumentForm( complete );
			}
			else {
				_log( 'na strimu sam' );
				complete();
			}
		}
	});

	return module;
})();