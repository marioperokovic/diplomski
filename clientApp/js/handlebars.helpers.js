Handlebars.registerHelper('formatDate', function( timestamp, dateFormat ) {
	var format = dateFormat.hash.format || "DD. MMMM YYYY.";
	return moment.unix( timestamp ).format( format );
});

Handlebars.registerHelper('subtemplate', function ( template, data ) {
	if( App.compiledTemplates[ template ] ) {
		return new Handlebars.SafeString( App.compiledTemplates[ template ]( data ) );
	}
	else {
		return "Nedostaje template '" + template +"'.";
	}
});

Handlebars.registerHelper('showValue', function ( value, format, option ) {
	var returnVal = value;
	if ( !value ) {
		return "- -";
	}
	switch ( format ) {
		case 'as-is':
			returnVal = value;
			break;
		case 'date':
			if ( option === 'native' ) {
				returnVal = value;
			}
			else {
				returnVal = moment.unix( value ).format("DD.MM.YYYY.");
			}
			break;
		case 'currency':
			returnVal = num2money( parseFloat(value) );
		default:
			// returnVal = '- -';
	}
	return returnVal;
});

Handlebars.registerHelper('getTaskName', function ( task ) {
	if ( task.name === "approveOutgoingInvoice" || task.name === "approveIncomingInvoice" ) {
		return "Odobri!";
	}
	else {
		return "Jesam";
	}
});

Handlebars.registerHelper('getAmountToPay', function ( doc ) {
	var totalAmount = doc.metadata.totalAmount,
		paidAmount = doc.metadata.paidAmount;
	if ( !paidAmount ) {
		return num2money( totalAmount );
	}
	else {
		return num2money( totalAmount - paidAmount );
	}
});