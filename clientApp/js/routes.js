App.routes = App.routes || {};

$.extend( App.routes, {
	'/login': App.modules.login.show,
	'/home': App.modules.home.show,
	'/documents': App.modules.documents.show,
	'/payments': App.modules.payments.show,
	'/details/:id': App.modules.details.show
});

App.routes = new Router( App.routes );