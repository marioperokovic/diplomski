$.extend ( App, {
	constants: {
		ENTER_KEY: 13,
		ESC_KEY: 27,
		SCROLL_LOAD_NUMBER: 6
	},
	modules: {},
	run: function () {
		NProgress.start();
		App.modules.main.checkUser();
	}
});

App.templates = $.Deferred();
setTimeout(function () {
	App.templates.resolve();
}, 100);
$.when( App.templates ).done( function () {
	App.run();
});

moment.lang('hr');

App.modules.main = App.modules.main || (function () {
	var module = App.Module.extend({
		name: "main",
		rootElement: "document",

		"templates": {
			'home': { renderIn: "#" }
		},

		init: function () {
			_log( 'inicijaliziram aplikaciju' );
			App.routes.init();

			if ( !App.user && App.routes.getRoute().indexOf( "login" ) === -1 ) {
				App.modules.login.meta.formerRoute = App.routes.getRoute().toString().replace(',','/');
				toastr.warning( "Morate se prijaviti!" );
				$(".app-tab").hide();
				$("#header li").removeClass('active');
				App.routes.setRoute( "login" );
			}
			else if ( App.user ) {
				module.renderUser();
				module.getCoreData();
				toastr.success("Uspješna prijava");
				if ( App.routes.getRoute()[0].length === 0 ) {
					App.routes.setRoute( "home" );
				} else {
					App.routes.setRoute( App.routes.getRoute().toString().replace(',','/') );
				}
			}
			module.cacheElements();
			module.bindEvents();
			NProgress.done();
		},

		show: function () {
		},

		cacheElements: function () {
			module.$logout = module.$el.find( ".user-info .logout" );
			module.$newDocButton = module.$el.find( "#newDocumentBtn" );
			module.$newDocForm = module.$el.find( "#documentUpload" );
			module.$cancelNewDocBtn = module.$newDocForm.find( "button.cancel" );
			module.$registerButton = module.$el.find( "#registrirajSe" );
		},

		bindEvents: function () {
			module.$logout.off('click').on( 'click', module.logout );
			module.$newDocButton.off('click').on( 'click', module.showNewDocContainer );
			module.$newDocForm.off('submit').on( 'submit', module.saveNewDoc );
			module.$cancelNewDocBtn.off('click').on( 'click', module.hideNewDocContainer );
			module.$el.find( '#newDocumentContainer .close' ).off('click').on( 'click', module.hideNewDocContainer );
			module.$registerButton.off('click').on( 'click', module.register );
		},
		
		navbarClick: function ( e ) {
			console.log("Nav bar click!!");
			$('#container .app-tab').hide();
			$('#header li').removeClass("active");
			$('#header').find('a[href="#/' + App.routes.getRoute().join('/') + '"]').parents('li').addClass("active");
		},

		renderUser: function ( clear ) {
			var $userInfo = $(".user-info");
			if ( clear ) {
				$userInfo.find( ".signed-in-user" ).html('');
				$userInfo.addClass( "displaynone" );
			}
			else {
				var user = App.user.firstName.charAt( 0 ) + ". " + App.user.lastName;
				$userInfo.find( ".signed-in-user" ).html( user ).attr( "title", App.user.email );
				$userInfo.removeClass( "displaynone" );
			}
		},

		saveLocal: function () {
			$.cookie.json = true;
			$.cookie( 'user', App.user, { expires: 7 } );
		},

		getLocal: function () {
			$.cookie.json = true;
			var user = $.cookie( 'user' );
			if ( user ) {
				return user;
			} else {
				return null;
			}
		},

		deleteLocalData: function () {
			App.user = null;
			App.apis.defaultRequestData = {};
			$.removeCookie( 'user' );
			App.modules.login.meta.formerRoute = App.routes.getRoute().toString().replace(',','/');

			$(".app-tab").hide();
			module.renderUser( true );
			$("#header li").removeClass('active');
			App.routes.setRoute( "login" );
		},

		logout: function () {
			var r = confirm( "Želite li se odjaviti?" );
			if ( r == true) {
				module.deleteLocalData();	
			}
		},

		checkUser: function () {
			App.user = this.getLocal();
			if ( App.user ) {
				module.setDefaultRequestData();
			}
			module.init();
		},

		disableNavigation: function () {
			$("#header").addClass( 'disabled-header' );
			$("#newDocumentBtn").hide();
		},

		enableNavigation: function () {
			$("#header").removeClass( 'disabled-header' );
			$("#newDocumentBtn").show();
		},

		showNewDocContainer: function () {
			if ( $(this).hasClass('active') ) {
				$( "#newDocumentContainer" ).slideUp();
				$(this).removeClass('active');
			}
			else {
				$(this).addClass('active');
				$( "#newDocumentContainer" ).slideDown();
			}
		},

		hideNewDocContainer: function () {
			module.$newDocButton.removeClass('active');
			$( "#newDocumentContainer" ).slideUp('slow');
			document.getElementById( "documentUpload" ).reset();
		},

		saveNewDoc: function () {
			$(this).find('input[name="userId"]').val( App.user._id );
			$(this).find('input[name="token"]').val( App.user.token );
			$(this).ajaxSubmit({
				error: function ( error ) {
					_log( 'Pogreška! ', error );
				},

				success: function ( response ) {
					_log( 'uspjeh!', response );
					toastr.success( 'Dokument je uspješno pohranjen!' );
					module.hideNewDocContainer();

					$(this).find('input[name="userId"]').val('');
					$(this).find('input[name="token"]').val('');
					if ( $("#home").is(':visible') ) {
						_log('moram ga prependat')
						var html = module.render( 'home', { documents: [ response.output.document ] }, 'html' );
						$("#home").prepend( html );
						App.modules.home.masonry.reloadItems();
						App.modules.home.refreshMasonry();

						$cardChanged = $("#home").find( '.card[data-id="' + response.output.document._id + '"]' );
						$cardChanged.addClass('data-updated');
						setTimeout( function() {
							$cardChanged.removeClass('data-updated');
						}, 4000 );
						App.modules.home.bindEvents();
					}
				}
			});

			return false;
		},

		setDefaultRequestData: function () {
			_log( 'setting default request data!' );
			App.apis.defaultRequestData = {
				'userId': App.user._id,
				'token': App.user.token,
				'email': App.user.email
			};
		},

		getCoreData: function () {
			App.apis.getUserGroups.get( function ( response ) {
				App.usergroups = response.output.usergroups;
			}, {}, function ( error ) {
				_log( 'error getting usergroups' );
			});
		},

		register: function () {
			var $modal = $(this).parents('#register'),
				$form = $modal.find('#registerForm'),
				email = $form.find('input[name="email"]').val(),
				password = $form.find('input[name="password"]').val(),
				firstName = $form.find('input[name="firstName"]').val(),
				lastName = $form.find('input[name="lastName"]').val();

			if ( !email || !password || !firstName || !lastName ) {
				bootbox.alert( 'Nisu uneseni svi podaci!' );
				return;
			}
			else if ( password.length < 6 ) {
				bootbox.alert( 'Lozinka mora imati najmanje 6 znakova!' );
				return;
			}

			App.apis.user.post( function ( response ) {
				_log( 'success registration' );
				toastr.success( 'Uspješno ste se registrirali, sada se možete prijaviti!' );
				$("#loginForm").find('input[name="email"]').val( response.output.user.email );
				$modal.modal('hide');
			}, {
				method: 'createUser',
				email: email,
				password: password,
				firstName: firstName,
				lastName: lastName
			}, function ( error ) {
				_log( 'error registration', error );
				if ( error.type === "emailExists" ) {
					toastr.warning( "Korisnik s ovom email adresom već postoji!" );
					$modal.modal('hide');
				}
				else {
					toastr.warning( "Došlo je do pogreške, pokušajte ponovno!" );
				}
			});

			// clear form after modal is hidden
			$modal.on('hidden.bs.modal', function ( e ) {
				$(this).find('input').each( function(k, v) {
					$( v ).val('');
				});
			});
		}
	});

	return module;
})();