<?php
	date_default_timezone_set("Europe/Zagreb"); 
	$timestamp = date_format( date_create(), 'U' );
	function getVersion() {
		global $timestamp;
		return $timestamp;
	}
?>

<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>Document Management System</title>
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width"  />
		<meta name="google" value="notranslate" />
		<link rel="stylesheet" type="text/css" href="css/libs/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="css/libs/bootstrap-theme.min.css" />
		<link rel="stylesheet" type="text/css" href="css/libs/toastr.min.css" />
		<link rel="stylesheet" type="text/css" href="css/libs/jquery.gridster.css" />
		<link rel="stylesheet" type="text/css" href="css/libs/nprogress.css" />
		<link rel="stylesheet" type="text/css" href="css/libs/bootstrap-datetimepicker.min.css" />

		<link rel="stylesheet/less" type="text/css" href="css/appstyle.less?v=<?php echo getVersion();?>" />
		<link rel="stylesheet/less" type="text/css" href="css/login.less?v=<?php echo getVersion();?>" />
		<link rel="stylesheet/less" type="text/css" href="css/home.less?v=<?php echo getVersion();?>" />
		<link rel="stylesheet/less" type="text/css" href="css/documents.less?v=<?php echo getVersion();?>" />
		<link rel="stylesheet/less" type="text/css" href="css/payments.less?v=<?php echo getVersion();?>" />
		<link rel="stylesheet/less" type="text/css" href="css/details.less?v=<?php echo getVersion();?>" />
		<link rel="stylesheet/less" type="text/css" href="css/widgets.less?v=<?php echo getVersion();?>" />
	</head>
	<body>
		<div id="container">
			<nav id="navigation" class="navbar navbar-default" role="navigation">
				<div class="container-fluid">
					<div class="navbar-header">
						<!-- toogle za mobile prikaz -->
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<!-- <a class="navbar-brand" href="#"><span class="glyphicon glyphicon-home"></span></a> -->
					</div>

					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul id="header" class="nav navbar-nav">
							<li><a href="#/home" title="Početna"><span class="glyphicon glyphicon-home"></span>&nbsp;</a></li>
							<li><a href="#/payments" title="Plaćanja"><span class="glyphicon glyphicon-euro"></span><span class="name">Plaćanja</span></a></li>
							<li><a href="#/documents" title="Dokumenti"><span class="glyphicon glyphicon-file"></span><span class="name">Dokumenti</span></a></li>
						</ul>
						<!-- <form class="navbar-form navbar-right" role="search">
							<div class="form-group">
							<input type="text" class="form-control" placeholder="Pretraživanje">
							</div>
							<button type="submit" class="btn btn-default">Traži</button>
						</form> -->
						<div class="navbar-right user-info displaynone">
							<div class="user-label">Prijavljeni ste kao:</div>
							<div class="signed-in-user" title=""></div>
							<div class="logout"><span class="glyphicon glyphicon-log-out" title="Odjava"></span></div>
						</div>
						
						<button id="newDocumentBtn" class="navbar-right">
							<div>Novi dokument</div>
							<span class="glyphicon glyphicon-cloud-upload"></span>
						</button>
					</div>
				</div>
			</nav>
			
			<div id="login" class="app-tab">
				<h2>Prijava</h2>
				<form id="loginForm">
					<div class="input-field">
						<label for="email">E-mail:</label>
						<input id="email" type="text" name="email" class="form-control"/>
					</div>
					<div class="input-field">
						<label for="password">Lozinka:</label>
						<input id="password" type="password" name="password" class="form-control"/>
					</div>
					<div>
						<button type="submit" id="loginButton" class="btn btn-primary">Login</button>
					</div>
				</form>
				<span>Nemaš račun?</span>
				<button class="btn btn-success btn-sm" data-toggle="modal" data-target="#register">Registriraj se</button>

				<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="Registracija" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Zatvori</span></button>
								<h4 class="modal-title" id="myModalLabel">Registracija novog korisnika</h4>
							</div>
							<div class="modal-body">
								<form id="registerForm">
									<div class="input-field">
										<label for="email">E-mail:</label>
										<input id="email" type="text" name="email" class="form-control"/>
									</div>
									<div class="input-field">
										<label for="password">Lozinka:</label>
										<input id="password" type="password" name="password" class="form-control"/>
									</div>
									<div class="input-field">
										<label for="firstName">Ime:</label>
										<input id="firstName" type="text" name="firstName" class="form-control"/>
									</div>
									<div class="input-field">
										<label for="lastName">Prezime:</label>
										<input id="lastName" type="text" name="lastName" class="form-control"/>
									</div>
								</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Odustani</button>
								<button id="registrirajSe" type="button" class="btn btn-primary">Registriraj se</button>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="home" class="app-tab"></div>
			<div id="payments" class="app-tab"></div>
			<div id="documents" class="app-tab"></div>
			<div id="details" class="modal fade" tabindex="-1" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content"></div>
				</div>
			</div>

			<div id="newDocumentContainer" class="displaynone">
				<button class="close">
					<span class="glyphicon glyphicon-remove"></span>
				</button>
				<form id="documentUpload" role="form" enctype="multipart/form-data" action="http://localhost:3000/document" method="post">
					<div class="form-group">
						<label for="newDocument">Datoteka:</label>
						<input type="file" id="newDocument" name="newDocument" />
					</div>
					<div class="form-group">
						<label for="name">Naziv novog dokumenta:</label>
						<input type="text" id="name" name="name" />
					</div>
					<div class="form-group displaynone">
						<input type="hidden" name="userId">
						<input type="hidden" name="token">
					</div>

					<div class="new-document-buttons">
						<button type="submit" class="right btn btn-primary" id="saveNewDocument">Spremi</button>
						<button type="button" class="right btn btn-default cancel">Odustajem</button>
					</div>
				</form>
			</div>
		</div>

		<script type="text/javascript">
			var App = {
				modules: {},
				routes: {},
				apis: {},
				conf: {
					apiPath: 'http://localhost:3000/'
				}
			};
			less = {
				env: "development",
				logLevel: 1
			};
		</script>
		<!-- libs -->
		<script type="text/javascript" src="js/libs/jquery-2.1.1.js"></script>
		<script type="text/javascript" src="js/libs/director.min.js"></script>
		<script type="text/javascript" src="js/libs/less-1.5.0.min.js"></script>
		<script type="text/javascript" src="js/libs/handlebars.runtime-v1.3.0.js"></script>
		<script type="text/javascript" src="js/libs/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/libs/bootbox.min.js"></script>
		<script type="text/javascript" src="js/libs/jquery.cookie.js"></script>
		<script type="text/javascript" src="js/libs/toastr.min.js"></script>
		<script type="text/javascript" src="js/libs/masonry.pkgd.js"></script>
		<script type="text/javascript" src="js/libs/jquery.form.js"></script>
		<script type="text/javascript" src="js/libs/moment.min.js"></script>
		<script type="text/javascript" src="js/libs/nprogress.js"></script>
		<script type="text/javascript" src="js/libs/bootstrap-datetimepicker.min.js"></script>
		<script type="text/javascript" src="js/libs/bootstrap-sortable.js"></script>

		<!-- app files -->
		<script type="text/javascript" src="js/utils.js?v=<?php echo getVersion();?>"></script>
		<script type="text/javascript" src="js/core/module.js?v=<?php echo getVersion();?>"></script>
		<script type="text/javascript" src="js/core/api.js?v=<?php echo getVersion();?>"></script>
		<script type="text/javascript" src="js/dms.handlebars.js?v=<?php echo getVersion();?>"></script>
		<script type="text/javascript" src="js/handlebars.helpers.js?v=<?php echo getVersion();?>"></script>
		<script type="text/javascript" src="js/apis.js?v=<?php echo getVersion();?>"></script>

		<script type="text/javascript" src="js/main.js?v=<?php echo getVersion();?>"></script>
		<script type="text/javascript" src="js/modules/login.js?v=<?php echo getVersion();?>"></script>
		<script type="text/javascript" src="js/modules/home.js?v=<?php echo getVersion();?>"></script>
		<script type="text/javascript" src="js/modules/documents.js?v=<?php echo getVersion();?>"></script>
		<script type="text/javascript" src="js/modules/payments.js?v=<?php echo getVersion();?>"></script>
		<script type="text/javascript" src="js/modules/details.js?v=<?php echo getVersion();?>"></script>
		<script type="text/javascript" src="js/modules/widgets.js?v=<?php echo getVersion();?>"></script>
		<script type="text/javascript" src="js/routes.js?v=<?php echo getVersion();?>"></script>
	</body>
</html>