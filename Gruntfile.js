var banner = '/*! Mario Peroković - Diplomski rad - <%= pkg.name %> v<%= pkg.version %> <%= grunt.template.today("yyyy-mm-dd") %> */';
var pkgInfo = require('./package.json');

console.log("\n\n------------------------------------------");
console.log("--- building DMS, version " + pkgInfo.version + " ---");
console.log("------------------------------------------\n\n");

module.exports = function( grunt ) {
	// Project configuration.
	grunt.initConfig({
		
		pkg: grunt.file.readJSON('package.json'),
		
		less: {
			development: {
				options: {
					paths: ["css"],
					compress: false,
					report: true
				},
				files: {
					"clientApp/build/css/style.css" : [
						"clientApp/css/libs/*.css",
						"clientApp/css/*.less"
					],
				}
			},
			production: {
				options: {
					paths: ["css"],
					compress: true,
					report: true,
					strictMath: true
				},
				files: {
					"clientApp/build/css/style.min.css": [
						"clientApp/build/css/style.css"
					]
				}
			}
		},

		handlebars: {
			compile: {
				options: {
					namespace: "App.compiledTemplates",
					processName: function( filePath ) {
						var pieces = filePath.split("/");
						return pieces[pieces.length - 1].replace('.hb', '');
					}
				},
				files: {
					"clientApp/js/dms.handlebars.js": "clientApp/templates/*.hb"
				}
			}
		},

		uglify: {
			beautified: {
				options: {
					banner: banner,
					compress: false,
					mangle: false,
					beautify: true,
					report: 'min'
				},
				files: {
					"clientApp/build/js/dms.js": [
						"clientApp/js/libs/jquery-2.1.1.js",
						"clientApp/js/libs/director.min.js",
						"clientApp/js/libs/less-1.5.0.min.js",
						"clientApp/js/libs/handlebars.runtime-v1.3.0.js",
						"clientApp/js/libs/bootstrap.min.js",
						"clientApp/js/libs/bootbox.min.js",
						"clientApp/js/libs/jquery.cookie.js",
						"clientApp/js/libs/toastr.min.js",
						"clientApp/js/libs/masonry.pkgd.js",
						"clientApp/js/libs/jquery.form.js",
						"clientApp/js/libs/moment.min.js",
						"clientApp/js/libs/nprogress.js",
						"clientApp/js/libs/bootstrap-datetimepicker.min.js",
						"clientApp/js/libs/bootstrap-sortable.js",
						
						"clientApp/js/utils.js",
						"clientApp/js/core/module.js",
						"clientApp/js/core/api.js",
						"clientApp/js/dms.handlebars.js",
						"clientApp/js/handlebars.helpers.js",
						"clientApp/js/apis.js",
						
						"clientApp/js/main.js",
						"clientApp/js/modules/login.js",
						"clientApp/js/modules/home.js",
						"clientApp/js/modules/documents.js",
						"clientApp/js/modules/payments.js",
						"clientApp/js/modules/details.js",
						"clientApp/js/modules/widgets.js",
						"clientApp/js/routes.js"
					]
				}
			},
			minified: {
				options: {
					banner: banner,
					compress: false,
					mangle: true,
					beautify: false,
					report: 'min'
				},
				files: {
					"clientApp/build/js/dms.min.js": [ "clientApp/build/js/dms.js" ]
				}
			},
		},

		watch: {
			files: [
			'clientApp/templates/*.hb'
			],
			tasks: ['handlebars']
		}
	});

	// Load the plugins
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-handlebars');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Default task(s).
	grunt.registerTask('default', ['handlebars', 'uglify',  'less']);
};