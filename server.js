var express = require( 'express' ),
	user = require( './routes/login' ),
	document = require( './routes/document' ),
	comments = require( './routes/comments' ),
	utils = require( './routes/utils' ),
	workflows = require( './routes/workflows' );

var app = express();

function allowCrossDomain ( req, res, next ) {
	// Website you wish to allow to connect
	res.setHeader('Access-Control-Allow-Origin', 'http://localhost');
	// Request methods you wish to allow
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
	// Request headers you wish to allow
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
	// Set to true if you need the website to include cookies in the requests sent
	// to the API (e.g. in case you use sessions)
	res.setHeader('Access-Control-Allow-Credentials', true);

	next();
}

app.configure(function(){
	app.use( express.favicon() );
	app.use( express.logger('dev') );
	app.use( express.bodyParser() );
	app.use( allowCrossDomain );
});

// user
// app.post( '/user', user.checkLogin );
app.post( '/user', user.checkMethod );
// documents
app.post( '/document', user.isAuthenticated, document.handleUpload );
app.put( '/document', user.isAuthenticated, document.updateDocument );
app.get( '/document', user.isAuthenticated, document.getDocument );
app.get( '/documents', user.isAuthenticated, document.getAllDocuments );
app.get( '/files/document/:id/:type', document.getDocumentPreview );
// comments
app.post( '/comments', user.isAuthenticated, comments.saveComment );
//utils
app.get( '/usergroups', user.isAuthenticated, utils.getUserGroups );
//workflows
app.put( '/workflows', user.isAuthenticated, workflows.completeTask );
//payments
app.get( '/payments', user.isAuthenticated, document.getPayments );
app.post( '/payments', user.isAuthenticated, document.addPayment );

app.listen( 3000 );
console.log( 'Listening on port 3000...' );