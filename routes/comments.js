var mongojs = require( 'mongojs' ),
	db = mongojs( 'maperokov-diplomski', ['documents', 'users'] );

exports.saveComment = function ( req, res ) {
	console.log( req.body );
	var documentId = req.body.documentId,
		comment = req.body.comment,
		author = req.body.author;

	if ( !documentId || !comment || !author ) {
		res.status( 500 );
		res.send({
			'status': 'error',
			'message': 'Nedostaje parametar!'
		});
	}
	else {
		db.documents.findOne({
			_id: mongojs.ObjectId( documentId )
		}, function ( error, doc ) {
			if ( error ) {
				console.error( error );
				res.status( 500 );
				res.send({
					'status': 'error',
					'message': 'Pogreška!'
				});
			}
			else {
				if ( !doc ) {
					res.status( 500 );
					res.send({
						'status': 'error',
						'message': 'Dokument nije pronađen'
					});
				}
				else {
					var comments = doc.comments || [];

					db.users.findOne({
						_id: mongojs.ObjectId( author )
					}, function ( error, user ) {
						var aut;
						if ( error ) aut = "Anonimni";
						else {
							aut = {
								firstName: user.firstName,
								lastName: user.lastName,
								email: user.email
							};
							var result = {
								_id: mongojs.ObjectId(),
								created: Math.round( +new Date() / 1000 ),
								comment: comment,
								author: aut
							}
							comments.push( result );

							db.documents.update({
								_id: mongojs.ObjectId( documentId )
							}, { 
								$set : { comments: comments }
							}, function ( error ) {
								if ( !error ) {
									res.send({
										'status': 'success',
										'output': {
											comment: result 
										}
									});
								}
							});
						}
					});
				}
			}
		});
	}
};