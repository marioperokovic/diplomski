var mongojs = require( 'mongojs' ),
	db = mongojs( 'maperokov-diplomski', ['documents', 'users', 'usergroups'] );

exports.getUserGroups = function ( req, res ) {
	db.usergroups.find( {}, function ( err, usergroups ) {
		if ( err ) {
			console.log( 'error getting usergroups' );
			console.error( error );
			res.status( 500 );
			res.send({
				'status': 'error',
				'message': 'Nešto je pošlo po zlu'
			});
		}
		else {
			console.log( 'success getting usergroups' );
			res.send({
				'status': 'success',
				'output': {
					usergroups: usergroups
				}
			});
		}
	});
};