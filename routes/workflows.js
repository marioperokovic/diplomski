var mongojs = require( 'mongojs' ),
	db = mongojs( 'maperokov-diplomski', ['documents', 'workflows'] ),
	users = require( './login' );

exports.completeTask = function ( req, res ) {
	// console.log( req.body );
	if ( !req.body.documentId || !req.body.task ) {
		res.status( 500 );
		res.send({
			'status': 'error',
			'type': 'missingParameter',
			'message': 'Nedostaje parametar!'
		});
	}
	else {
		db.documents.findOne({ _id: mongojs.ObjectId( req.body.documentId ) }, function ( err, document ) {
			if ( err ) {
				console.error( err );
			}
			else {
				var oldTask,
					newTask,
					newWorkflow;
				for ( var task in document.workflow.tasks ) {
					if ( document.workflow.tasks[ task ].name === req.body.task ) {
						oldTask = document.workflow.tasks[ task ];

						// check if there is required data on document
						if ( oldTask.requiredData ) {
							for ( var i = 0; i < oldTask.requiredData.length; i++ ) {
								if ( !document.metadata[ oldTask.requiredData[i] ] ) {
									res.status( 500 );
									res.send({
										'status': 'error',
										'type': 'missingParameter',
										'message': 'Nedostaje parametar: ' + oldTask.requiredData[i]
									});
									return null;
								}
							}
						}

						if ( oldTask.transitionTo === 'end' ) { // promijeni workflow
							if ( oldTask.name === 'setDocType' ) { // it was first workflow
								if ( document.metadata.documentType === "URA" ) {
									newWorkflow = "approveIncomingInvoice";
								}
								else {
									newWorkflow = "approveOutgoingInvoice";
								}
							}
							else { // doc approved, need to enter payments
								newWorkflow = "enterPayments"; 
							}
						}
						else {
							newTask = document.workflow.tasks[ document.workflow.tasks[ task ].transitionTo ];
						}
						break;
					}
				}
				if ( newWorkflow ) { // zakači novi workflow na dokument
					db.workflows.findOne({ name: newWorkflow }, function ( error, workflow ) {
						if ( !error ) {
							document.workflow = workflow;
							document.currentTask = workflow.tasks.start;

							db.documents.findAndModify({
								query: {
									_id: mongojs.ObjectId( document._id )
								},
								update: {
									$set: {
										workflow: document.workflow,
										currentTask: document.currentTask,
										documentId: document.metadata.documentId,
										documentType: document.metadata.documentType,
										isPaid: false
									}
								}
							}, function ( error, doc, lastErrorObject ) {
								if ( error ) {
									console.log('pogreškaa updejt nakon transition!');
									console.log(error);
								}
								else {
									users.getEmail( req.body.userId, function ( email ) {
										users.isAssigned( email, document.currentTask.assignedGroup, function ( assigned ) {
											if ( assigned ) {
												document.currentTask.assignedToMe = true;
											}

											res.send({
												'status': 'success',
												'output': {
													document: document
												}
											});
										});
									});
								}
							});
						}
					});
				}
				else { // samo promijeni task na dokumentu
					document.currentTask = newTask;

					db.documents.findAndModify({
						query: {
							_id: mongojs.ObjectId( document._id )
						},
						update: {
							$set: {
								currentTask: document.currentTask
							}
						}
					}, function ( error, doc, lastErrorObject ) {
						if ( error ) {
							console.log('pogreškaa updejt nakon transition!');
							console.log(error);
						}
						else {
							users.getEmail( req.body.userId, function ( email ) {
								users.isAssigned( email, document.currentTask.assignedGroup, function ( assigned ) {
									if ( assigned ) {
										document.currentTask.assignedToMe = true;
									}

									res.send({
										'status': 'success',
										'output': {
											document: document
										}
									});
								});
							});
						}
					});

				}
			}
		});
	}
};