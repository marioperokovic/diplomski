//var fs = require( 'fs' );
var fs = require( 'fs.extra' ),
	path = require( 'path' ),
	mongojs = require( 'mongojs' ),
	db = mongojs( 'maperokov-diplomski', ['documents', 'workflows', 'payments', 'usergroups'] ),
	im = require( 'imagemagick' ),
	gm = require( 'gm' ).subClass({ imageMagick: true }),
	users = require( './login' );

exports.handleUpload = function ( req, res ) {
	var oldPath = req.files.newDocument.path;
	var documentName = req.body.name;

	if ( !documentName ) {
		res.status( 500 );
		res.send({
			'status': 'error',
			'message': 'Input param is missing: name'
		});
	}
	else {
		fs.readFile( oldPath, function ( error, data ) {
			var imageName = req.files.newDocument.name;

			if ( !imageName ) { // error
				console.error( error );
				res.status( 500 );
				res.send({
					'status': 'error',
					'message': 'Nešto je pošlo po zlu'
				});
			}
			else {
				var newPath = 'documents/fullsize/' + imageName;
				var previewPath = 'documents/previews/' + imageName;

				fs.writeFile( newPath, data, function ( error ) {
					fs.unlink( oldPath, function ( error ) {
						if ( error ) {
							console.error( 'nije obrisana slika iz tmp!!', error );
						}
					});

					gm( newPath )
						.resize( 300 )
						.noProfile()
						.write( previewPath, function ( error ) {
							if ( error ) {
								console.log('error creating preview, saving without it..');
								console.log( error );
								saveDocumentToDatabase( newPath, newPath, documentName, req.body.userId, res );
							}
							else {
								console.log('success creating preview!');
								saveDocumentToDatabase( newPath, previewPath, documentName, req.body.userId, res );
							}
						});
				});
			}
		});
	}
};

var saveDocumentToDatabase = function ( fullPath, previewPath, documentName, userId, res ) {	
	db.workflows.findOne({ name: 'classify' }, function ( error, doc ) {
		if( !error ) {
			db.documents.save({
				fullPath: path.resolve( fullPath ),
				previewPath: path.resolve( previewPath ),
				created: Math.round( +new Date() / 1000 ),
				contentType: 'document',
				title: documentName,
				metadata: {
					title: documentName
				},
				workflow: doc,
				currentTask: doc.tasks.start
			}, function ( error, doc ) {
				if ( error ) {
					console.error( error );
					res.status( 500 );
					res.send({
						'status': 'error',
						'message': 'Nešto je pošlo po zlu'
					});
				}
				else {
					console.log( 'uspješno kreiran dokument' );
					users.getEmail( userId, function ( email ) {
						users.isAssigned( email, doc.currentTask.assignedGroup, function ( assigned ) {
							if ( assigned && doc.currentTask.name !== "enterData" ) {
								doc.currentTask.assignedToMe = true;
							}
							res.send({
								'status': 'success',
								'output': {
									document: doc
								}
							});
						});
					});
				}
			});
		}
	});
};

exports.getAllDocuments = function ( req, res ) {
	if ( !req.query.email ) {
		res.status( 500 );
		res.send({
			'status': 'error',
			'message': 'Nedostaje parametar: email!'
		});
	}
	else {
		db.documents.find({ contentType: 'document' }).sort({ created: -1 }, function ( err, docs ) {
			if ( err ) {
				console.error( error );
				res.status( 500 );
				res.send({
					'status': 'error',
					'message': error
				});
			}
			else {
				console.log( req.query.email );
				db.usergroups.find({
					members: { $in: [req.query.email] }
				}, function ( error, groups ) {
					if ( error ) {
						res.status( 500 );
						res.send({
							'status': 'error',
							'message': error
						});
					}
					else {
						for ( var i = 0; i < docs.length; i++ ) {
							for ( var j = 0; j < groups.length; j++ ) {
								if ( docs[i].currentTask.assignedGroup === groups[j].group && docs[i].currentTask.name !== "enterData" ) {
									docs[i].currentTask.assignedToMe = true;
								}
							}
						}
						res.send({
							'status': 'success',
							'output': {
								documents: docs
							}
						});
					}
				});
			}
		});
	}
};

exports.getDocument = function ( req, res ) {
	if ( !req.query.documentId ) {
		res.status( 500 );
		res.send({
			'status': 'error',
			'message': 'Nedostaje parametar: documentId'
		});
	}
	else {
		var documentId = String( req.query.documentId );
		db.documents.findOne({
			_id: mongojs.ObjectId( documentId )
		}, function ( error, doc ) {
			if ( error ) {
				console.error( error );
				res.status( 500 );
				res.send({
					'status': 'error',
					'message': 'Dokument nije pronađen'
				});
			}
			else {
				users.getEmail( req.query.userId, function ( email ) {
					users.isAssigned( email, doc.currentTask.assignedGroup, function ( assigned ) {
						if ( assigned && doc.currentTask.name !== "enterData" ) {
							doc.currentTask.assignedToMe = true;
						}
						
						if ( doc.metadata.isPaid ) {
							db.payments.find({
								documentId: mongojs.ObjectId( doc._id )
							}).sort({ created: 1 }, function ( err, payments ) {
								if ( err ) {
									res.status( 500 );
									res.send({
										'status': 'error',
										'message': err
									});
								}
								else {
									doc.payments = payments;
									res.send({
										'status': 'success',
										'output': {
											document: doc
										}
									});
								}
							});
						}
						else {
							res.send({
								'status': 'success',
								'output': {
									document: doc
								}
							});
						}
					});
				});
			}
		});
	}
};

exports.getDocumentPreview = function ( req, res ) {
	if ( !req.params.id ) {
		res.status( 500 );
		res.send({
			'status': 'error',
			'message': 'Nedostaje parametar: _id'
		});
	}
	else {
		var documentId = req.params.id;
		var type = req.params.type || 'fullsize';
		db.documents.findOne({
			_id: mongojs.ObjectId( documentId )
		}, function ( error, doc ) {
			if ( error ) {
				console.error( error );
				res.status( 500 );
				res.send({
					'status': 'error',
					'message': 'Dokument nije pronađen'
				});
			}
			else {
				if ( !doc ) {
					res.status( 500 );
					res.send({
						'status': 'error',
						'message': 'Dokument nije pronađen'
					});
				}
				else {
					if ( type === 'fullsize' ) {
						res.sendfile( doc.fullPath );
					}
					else {
						res.sendfile( doc.previewPath );
					}
				}
			}
		});
	}
};

exports.updateDocument = function ( req, res ) {
	if ( !req.body.documentId ) {
		res.status( 500 );
		res.send({
			'status': 'error',
			'message': 'Nedostaje parametar: _id'
		});
	}
	else {
		var documentId = req.body.documentId;
		db.documents.findAndModify({
			query: {
				_id: mongojs.ObjectId( documentId )
			},
			update: {
				$set: req.body.properties
			}
		}, function ( error, doc, lastErrorObject ) {
			if ( error ) {
				console.log('Pogreška!!');
				console.log( error );
			}
			else {
				console.log('success updating doc, need to return updated one..');
				db.documents.findOne({
					_id: mongojs.ObjectId( documentId )
				}, function ( error, doc ) {
					if ( error ) {
						console.error( error );
						res.status( 500 );
						res.send({
							'status': 'error',
							'message': 'Dokument nije pronađen'
						});
					}
					else {
						res.send({
							'status': 'success',
							'output': {
								document: doc
							}
						});
					}
				});
			}
		});
	}
};

exports.getPayments = function ( req, res ) {
	if ( !req.query.direction ) {
		res.status( 500 );
		res.send({
			'status': 'error',
			'message': 'Nedostaje parametar: direction'
		});
	}
	else {
		var direction = req.query.direction,
			documentType = "URA";
		if ( direction === "outgoing" ) {
			documentType = "IRA";
		}
		db.documents.find({
			contentType: 'document',
			documentType: documentType,
			"currentTask.name": 'enterData',
			"metadata.isPaid": false
		}).sort({ created: -1 }, function ( err, docs ) {
			if ( err ) {
				console.error( error );
				res.status( 500 );
				res.send({
					'status': 'error',
					'message': 'Nešto je pošlo po zlu'
				});
			}
			else {
				//(console.log( req.query.userId );
				res.send({
					'status': 'success',
					'output': {
						payments: docs
					}
				});
			}
		});
	}
};

exports.addPayment = function ( req, res ) {
	if ( !req.body.documentId || !req.body.amount || !req.body.paymentDate ) {
		res.status( 500 );
		res.send({
			'status': 'error',
			'message': 'Nedostaje parametar!'
		});
	}
	else {
		var docId = req.body.documentId,
			amount = parseFloat( req.body.amount ),
			paymentDate = req.body.paymentDate;
		db.documents.findOne({
			_id: mongojs.ObjectId( docId )
		}, function ( error, doc ) {
			if ( error ) {
				res.status( 500 );
				res.send({
					'status': 'error',
					'message': error
				});
			}
			else {
				var paidAmount = doc.metadata.paidAmount || 0;
				paidAmount += amount;
				if ( parseFloat( paidAmount ) > parseFloat( doc.metadata.totalAmount ) ) {
					res.status( 500 );
					res.send({
						'status': 'error',
						'message': 'Cannot overpay document!'
					});
				}
				else {
					db.payments.save({
						created: Math.round( +new Date() / 1000 ),
						documentId: mongojs.ObjectId( doc._id ),
						amount: amount,
						paymentDate: paymentDate,
						userId: mongojs.ObjectId( req.body.userId )
					}, function ( error, payment ) {
						if ( error ) {
							res.status( 500 );
							res.send({
								'status': 'error',
								'message': error
							});
						}
						else {
							console.log('successfuly created payment, now updating document..');
							var isPaid = false;
							if ( parseFloat( paidAmount ) === parseFloat( doc.metadata.totalAmount ) ) {
								isPaid = true;
							}
							db.documents.findAndModify({
								query: {
									_id: mongojs.ObjectId( doc._id )
								},
								update: {
									$set: {
										"metadata.paidAmount": paidAmount,
										"metadata.isPaid": isPaid
									}
								}
							}, function ( error, updatedDoc, lastErrorObject ) {
								if ( error ) {
									res.status( 500 );
									res.send({
										'status': 'error',
										'message': error
									});
								}
								else {
									console.log('successfuly updated document, finished with payment!');
									db.documents.findOne({ _id: updatedDoc._id }, function ( err, document ) {
										res.send({
											'status': 'success',
											'output': {
												document: document
											}
										});
									});
								}
							});
						}
					});
				}
			}
		});
	}
};