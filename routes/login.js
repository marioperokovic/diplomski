var mongojs = require('mongojs'),
	db = mongojs('maperokov-diplomski', ['users', 'usergroups'])
	crypto = require('crypto'),
	_this = this;

exports.checkMethod = function ( req, res ) {
	var method = req.body.method;
	switch ( method ) {
		case 'login':
			_this.checkLogin( req, res );
			break;
		case 'createUser':
			_this.registerUser( req, res );
			break;
	}
};

exports.checkLogin = function(req, res) {
	var email = req.body.email,
		password = req.body.password,
		shasum = crypto.createHash('sha1').update( String( password ) ).digest('hex'),
		response = {};
	
	db.users.findOne({
		email: email
	}, function( err, user ) {
		if ( !err ) {
			if ( user ) { // found user
				if ( user.password === shasum ) {
					crypto.randomBytes( 48, function ( ex, buf ) {
						var token = buf.toString('hex');
						response = {
							'status': 'success',
							'user': {
								'_id': user._id,
								email: user.email,
								firstName: user.firstName,
								lastName: user.lastName,
								token: token
							}
						};
						db.users.update({
							_id: user._id
						}, {
							$set: { token: token }
						}, function ( error ) {
							if ( !error ) {
								res.send({
									'output': response
								});
							}
						});
					});
				}
				else {
					response = {
						'status': 'error',
						'message': 'User not found!'
					};
					res.status(404);
					res.send({
						'output': response
					});
				}
			}
			else { // user not found
				response = {
					'status': 'error',
					'message': 'User not found!'
				};
				res.status(404);
				res.send({
					'output': response
				});
			}
		}
		else {
			console.error( err );
			response = {
				'status': 'error',
				'message': 'Unknown error!' + err
			};
			res.send({
				'output': response
			});
		}
	});
};

exports.registerUser = function ( req, res ) {
	var email = req.body.email,
		password = crypto.createHash('sha1').update( String( req.body.password ) ).digest('hex'),
		firstName = req.body.firstName,
		lastName = req.body.lastName;
	if ( !email || !password || !firstName || !lastName ) {
		res.status( 500 );
		res.send({
			'status': 'error',
			'message': 'Input param is missing!'
		});
		return;
	}
	db.users.findOne({
		email: email
	}, function ( error, user ) {
		if ( error ) {
			console.log('error get user');
			console.log( error );
		}
		else {
			console.log( 'success get user' );
			if ( !user ) { // there is no user with this email, register him
				db.users.save({
					email: email,
					firstName: firstName,
					password: password,
					lastName: lastName
				}, function ( error, user ) {
					if ( error ) {
						res.status( 500 );
						res.send({
							'status': 'error',
							'message': 'Došlo je do pogreške!'
						});
					}
					else {
						res.send({
							'status': 'success',
							'output': {
								'user': {
									'_id': user._id,
									'email': user.email,
									'firstName': user.firstName,
									'lastName': user.lastName
								}
							}
						});
					}
				});
			}
			else { // user already exists, return such message
				res.status( 500 );
				res.send({
					'status': 'error',
					'type': 'emailExists',
					'message': 'User with this email already exists!'
				});
				return;
			}
		}
	});
};

exports.isAuthenticated = function ( req, res, next ) {
	// console.log( "Provjeravam jel user autoriziran" );
	switch ( req.method ) {
		case 'GET':
			var token = req.query.token,
				userId = req.query.userId;
			break;
		case 'POST':
			var token = req.body.token,
				userId = req.body.userId;
			break;
		case 'PUT':
			var token = req.body.token,
				userId = req.body.userId;
			break;
	}
	if ( !token || !userId ) {
		res.status(500);
		res.send({
			'output': 'Input param is missing!'
		});
	}
	db.users.findOne({
		token: token,
		_id: mongojs.ObjectId( userId )
	}, function ( error, user ) {
		if ( !error ) {
			if ( !user ) { // user is not authorized
				res.status(401);
				res.send({
					'output': 'User is not authorized!'
				});
			}
			else { // user is authorized
				next();
			}
		}
	});
};

exports.getEmail = function ( userId, next ) {
	db.users.findOne({ _id: mongojs.ObjectId( userId ) }, function ( err, user ) {
		if ( err ) {
			res.status(500);
			res.send({
				'output': 'Došlo je do pogreške',
				'error': err
			});
		}
		else {
			next( user.email );
		}
	});
};

exports.isAssigned = function ( email, assignedGroup, next ) {
	db.usergroups.findOne({ group: assignedGroup }, function ( error, group ) {
		if ( !error ) {
			if ( group.members.indexOf( email ) !== -1 ) {
				next( true );
			}
			else {
				next();
			}
		}
	});
};